<?php

Route::get(
    '',
    [
        'as' => 'admin.dashboard',
        function () {
            $content = 'Define your dashboard here.';

            return AdminSection::view($content, 'Dashboard');
        },
    ]
);

//Route::get(
//    '/information',
//    [
//        'as' => 'admin.categories',
//        function () {
//            return App\Admin\Models\Categories::class;
//        },
//    ]
//);
//Route::get(
//    '/categories',
//    [
//        'as' => 'admin.categories',
//        function () {
//            return App\Admin\Models\Categories::class;
//        },
//    ]
//);
//Route::get(
//    '/admin/attributes',
//    [
//        'as' => 'admin.categories',
//        function () {
//            return App\Admin\Models\Attributes::class;
//        },
//    ]
//);
//Route::get(
//    'admin/offers',
//    [
//        'as' => 'admin.categories',
//        function () {
//            return App\Admin\Models\Offers::class;
//        },
//    ]
//);
