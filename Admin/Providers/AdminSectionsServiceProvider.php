<?php

namespace Admin\Providers;

use Admin\Models\Attributes;
use Admin\Models\BonusWithdrawals;
use Admin\Models\Categories;
use Admin\Models\CategoryMoves;
use Admin\Models\Dealings;
use Admin\Models\Exchanges;
use Admin\Models\Finances;
use Admin\Models\Mailsubscriptions;
use Admin\Models\Offers;
use Admin\Models\OffersAnalises;
use Admin\Models\Pages;
use Admin\Models\Permissions;
use Admin\Models\Posts;
use Admin\Models\Problems;
use Admin\Models\ProblemTypes;
use Admin\Models\Roles;
use Admin\Models\Shops;
use Admin\Models\SpamBase;
use Admin\Models\Statuses;
use Admin\Models\Tags;
use Admin\Models\Terms;
use Admin\Models\Users;
use Admin\Policies\DefaultSectionPolicy;
use Admin\Widgets\NavigationNotifications;
use Admin\Widgets\NavigationUserBlock;
use App\Attribute;
use App\BonusWithdrawal;
use App\Category;
use App\CategoryMove;
use App\Dealing;
use App\Exchange;
use App\Finance;
use App\Mailsubscription;
use App\Offer;
use App\OffersAnalise;
use App\Option;
use App\Page;
use App\Permission;
use App\Post;
use App\Problem;
use App\ProblemType;
use App\Role;
use App\Shop;
use App\Spam_base;
use App\Status;
use App\Tag;
use App\Term;
use App\User;
use Illuminate\Routing\Router;
use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Contracts\Navigation\NavigationInterface;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;
//use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use Spatie\Activitylog\Models\Activity;

class AdminSectionsServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $widgets = [
        NavigationNotifications::class,
        NavigationUserBlock::class,
    ];

    /**
     * @var array
     */
    protected $sections = [
        User::class => 'Admin\Models\Users',
        Category::class => 'Admin\Models\Categories',
        CategoryMove::class => 'Admin\Models\CategoryMoves',
        Role::class => 'Admin\Models\Roles',
        Permission::class => 'Admin\Models\Permissions',
        Finance::class => 'Admin\Models\Finances',
        Shop::class => 'Admin\Models\Shops',
        Dealing::class => 'Admin\Models\Dealings',
        Term::class => 'Admin\Models\Terms',
        Tag::class => 'Admin\Models\Tags',
        ProblemType::class => 'Admin\Models\ProblemTypes',
        Mailsubscription::class => 'Admin\Models\Mailsubscriptions',
        Post::class => 'Admin\Models\Posts',
        Page::class => 'Admin\Models\Pages',
        Problem::class => 'Admin\Models\Problems',
        Status::class => 'Admin\Models\Statuses',
        Attribute::class => 'Admin\Models\Attributes',
        Offer::class => 'Admin\Models\Offers',
        Exchange::class => 'Admin\Models\Exchanges',
        Activity::class => 'Admin\Models\ActivityLogs',
        BonusWithdrawal::class => 'Admin\Models\BonusWithdrawals',
        Spam_base::class => 'Admin\Models\SpamBase',
        Option::class => 'Admin\Models\Options',
        OffersAnalise::class => 'Admin\Models\OffersAnalises',
    ];

    protected $policies = [
        // Поддержка
        Problems::class => DefaultSectionPolicy::class,
        Statuses::class => DefaultSectionPolicy::class,
        ProblemTypes::class => DefaultSectionPolicy::class,
        // Рассылка
        Mailsubscriptions::class => DefaultSectionPolicy::class,
        // Пользователи
        Roles::class => DefaultSectionPolicy::class,
        Permissions::class => DefaultSectionPolicy::class,
        Users::class => DefaultSectionPolicy::class,
        // Бухгалтерия
        Dealings::class => DefaultSectionPolicy::class,
        Shops::class => DefaultSectionPolicy::class,
        Finances::class => DefaultSectionPolicy::class,
        Exchanges::class => DefaultSectionPolicy::class,
        BonusWithdrawals::class => DefaultSectionPolicy::class,
        // Товары
        Offers::class => DefaultSectionPolicy::class,
        Attributes::class => DefaultSectionPolicy::class,
        Categories::class => DefaultSectionPolicy::class,
        CategoryMoves::class => DefaultSectionPolicy::class,
        OffersAnalises::class => DefaultSectionPolicy::class,
        // Посты
        Posts::class => DefaultSectionPolicy::class,
        Pages::class => DefaultSectionPolicy::class,
        Terms::class => DefaultSectionPolicy::class,
        Tags::class => DefaultSectionPolicy::class,
        // Внешний вид
        SpamBase::class => DefaultSectionPolicy::class,
        //Настройки
        Option::class => DefaultSectionPolicy::class,

    ];

    /**
     * @param Admin $admin
     *
     * @return void
     */
    public function boot(Admin $admin)
    {
        $this->loadViewsFrom(base_path("Admin/resources/views"), 'admin');
        $this->registerPolicies('Admin\\Policies\\');
        $this->app->call([$this, 'registerRoutes']);
        $this->app->call([$this, 'registerNavigation']);

        parent::boot($admin);

        $this->app->call([$this, 'registerViews']);
//        $this->app->call([$this, 'registerMediaPackages']);
    }

    /**
     * @param NavigationInterface $navigation
     */
    public function registerNavigation(NavigationInterface $navigation)
    {
        require base_path('Admin/navigation.php');
    }

    /**
     * @param WidgetsRegistryInterface $widgetsRegistry
     */
    public function registerViews(WidgetsRegistryInterface $widgetsRegistry)
    {
        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }
    }

    /**
     * @param Router $router
     */
    public function registerRoutes(Router $router)
    {
        $router->group(
            ['prefix' => config('sleeping_owl.url_prefix'), 'middleware' => config('sleeping_owl.middleware')],
            function ($router) {
                require base_path('Admin/routes.php');
            }
        );
    }
    /**
     * @param MetaInterface $meta
     */
//    public function registerMediaPackages(MetaInterface $meta)
//    {
//        $packages = $meta->assets()->packageManager();
//        require base_path('admin/assets.php');
//    }
}
