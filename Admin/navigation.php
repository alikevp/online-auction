<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
//AdminNavigation::setAccessLogic(
//    function (Page $page) {
//        return auth()->user()->isSuperAdmin();
//    }
//);

$navigation->setFromArray(
    [
        [
            'title' => 'Dashboard',
            'icon' => 'fa fa-dashboard',
            'url' => route('admin.dashboard'),
        ],

        [
            'title' => 'Поддержка',
            'icon' => 'fa fa-exclamation-circle',
            'pages' => [
                (new Page(\App\Status::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\Problem::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\ProblemType::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
            ],
        ],
        [
            'title' => 'Рассылка',
            'icon' => 'fa fa-envelope-o',

            'pages' => [
                (new Page(\App\Mailsubscription::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\Spam_base::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
            ],
        ],

        [
            'title' => 'Пользователи',
            'icon' => 'fa fa-group',
            'pages' => [
                (new Page(\App\User::class))
                    ->setIcon('fa fa-users')
                    ->setPriority(0),
                (new Page(\App\Role::class))
                    ->setIcon('fa fa-users')
                    ->setPriority(1),
                (new Page(\App\Permission::class))
                    ->setIcon('fa fa-users')
                    ->setPriority(1),
            ],
        ],

        [
            'title' => 'Бухгалтерия',
            'icon' => 'fa fa-exclamation-circle',
            'pages' => [
                (new Page(\App\Shop::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(10),
                (new Page(\App\Dealing::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\Exchange::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\BonusWithdrawal::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(2),
                (new Page(\App\Finance::class))
                    ->setIcon('fa fa-users')
                    ->setPriority(2),
            ],
        ],

        [
            'title' => 'Товары',
            'icon' => 'fa fa-exclamation-circle',
            'pages' => [
                (new Page(\App\Offer::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\Attribute::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(10),
                (new Page(\App\Category::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\OffersAnalise::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\CategoryMove::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
            ],
        ],

        [
            'title' => 'Посты',
            'icon' => 'fa fa-exclamation-circle',
            'pages' => [
                (new Page(\App\Page::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(10),
                (new Page(\App\Post::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(11),
                (new Page(\App\Term::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(12),
                (new Page(\App\Tag::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(13),
            ],
        ],


        [
            'title' => 'Настройки',
            'icon' => 'fa fa-exclamation-circle',
            'pages' => [
                (new Page(\App\Option::class))
                    ->setIcon('fa fa-list')
                    ->setPriority(10),
            ],
        ],
    ]
);
