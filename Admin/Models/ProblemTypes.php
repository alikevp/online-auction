<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Type;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Terms
 *
 * @property \App\Term $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class ProblemTypes extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Типы вопросов';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $columns = [
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::link('name')->setLabel('Название'),
            AdminColumn::text('label')->setLabel('Ярлык'),
        ];

        $display = AdminDisplay::table()
            ->setApply(function ($query) {
                $query->orderBy('name', 'asc');
            })
            ->paginate(10)
            ->getScopes()
            ->set('problemTypes')
            ->setColumns($columns);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Название')->required(),
            AdminFormElement::text('label', 'Slug'),
            AdminFormElement::text('type.label', 'Описание', Type::class)
                ->required(),
            AdminFormElement::hidden('type.type')
                ->setDefaultValue('support'),
        ]);

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
