<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;


/**
 * Class Finances
 *
 * @property \App\Finance $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Dealings extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Сделки';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay(MetaInterface $meta)
    {
        $meta->addCss('admin-table-fix', asset('css/admin-table-fix.css'));

        $display = AdminDisplay::datatables()
            ->with('user', 'shop', 'bonus_user')
            ->setHtmlAttribute('class', 'admin-table-fix')

            ->setColumns([
                AdminColumn::text('id')->setLabel('Id'),
                AdminColumn::text('order_id')->setLabel('Id заказа'),
                AdminColumn::text('offer_id')->setLabel('Id предложения'),
                AdminColumn::text('shop.name')->setLabel('Магазин'),
                AdminColumn::text('user.name')->setLabel('Имя пользователя'),
                AdminColumn::text('user.surname')->setLabel('Фамилия пользователя'),
                AdminColumn::text('user.patronymic')->setLabel('Отчество пользователя'),
                AdminColumn::text('status')->setLabel('Статус'),
                AdminColumn::text('reward')->setLabel('Выручка со сделки'),
                AdminColumn::datetime('created_at')->setLabel('Дата создания'),
                AdminColumn::datetime('updated_at')->setLabel('Дата обновления'),
                AdminColumn::text('deal_amount')->setLabel('deal_amount'),
                AdminColumn::text('award_size')->setLabel('award_size'),
                AdminColumn::text('award_amount')->setLabel('award_amount'),
                AdminColumn::text('nds')->setLabel('НДС'),
                AdminColumn::text('payment_type')->setLabel('Способ оплаты'),
                AdminColumn::text('bonus_type')->setLabel('Тип бонуса'),
                AdminColumn::text('bonus_user.name')->setLabel('Бонус, Имя пользователя'),
                AdminColumn::text('bonus_user.surname')->setLabel('Бонус, Фамилия пользователя'),
                AdminColumn::text('bonus_user.patronymic')->setLabel('Бонус, Отчество пользователя'),
                AdminColumn::text('bonus_amount')->setLabel('Количество бонуса'),
                AdminColumn::text('item_recieving_status')->setLabel('item_recieving_status'),
                AdminColumn::datetime('item_recieving_date')->setLabel('item_recieving_date'),
                AdminColumn::text('shop_result')->setLabel('shop_result'),
                AdminColumn::datetime('shop_result_date')->setLabel('shop_result_date'),
                AdminColumn::datetime('curator_result_date')->setLabel('curator_result_date'),
                AdminColumn::datetime('bill_send_date')->setLabel('bill_send_date'),
                AdminColumn::datetime('bill_read_date')->setLabel('bill_read_date'),
            ]);

        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('id'),
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Магазин'),
            AdminColumnFilter::text()->setPlaceholder('Имя пользователя'),
            AdminColumnFilter::text()->setPlaceholder('Фамилия пользователя'),
            AdminColumnFilter::text()->setPlaceholder('Отчество пользователя'),
            null,
            // Поиск по диапазону дат
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('From Date')->setFormat('d.m.Y')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('To Date')->setFormat('d.m.Y')
            ),
        ]);

        $display->paginate(15);

        return $display;
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
//    public function onEdit($id)
//    {
//        // todo: remove if unused
//    }
//
//    /**
//     * @return FormInterface
//     */
//    public function onCreate()
//    {
//        return $this->onEdit(null);
//    }

//    /**
//     * @return void
//     */
//    public function onDelete($id)
//    {
//        // todo: remove if unused
//    }
//
//    /**
//     * @return void
//     */
//    public function onRestore($id)
//    {
//        // todo: remove if unused
//    }
}
