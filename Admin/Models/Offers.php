<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Attribute_name;
use App\Attribute_value;
use App\Offer;
use App\Category;
use App\Producer;
use App\Shop;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class Posts
 *
 * @property \App\Post $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Offers extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Товары';

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $offer = Offer::where('id', $model->id)->first();
            DB::table('offers')->where('id', $model->id)
                ->update(
                    [
                        'category_id' => $_POST['parent_id']
                    ]
                );
            if(isset($_POST['attributes'])){
                foreach ($_POST['attributes'] as $attribute){
                    $slug = str_slug($attribute['name'], '-');
                    $attribute_name_check = Attribute_name::where('name', trim($attribute['name']))->where('category_id', $_POST['parent_id'])->first();
                    if(!isset($attribute_name_check)){
                        $attributeNameId = DB::table('attribute_names')->insertGetId(
                            [
                                'slug' => $slug,
                                'name' => $attribute['name'],
                                'category_id' => $_POST['parent_id'],
                                'filter' => 0,
                            ]
                        );
                    }
                    else{
                        $attributeNameId = $attribute_name_check->id;
                    }
                    DB::table('attribute_values')->insert(
                        [
                            'attribute_name_id' => $attributeNameId,
                            'offer_id' => $offer->id,
                            'value' => $attribute['value'],
                        ]
                    );
                    //временно
                    DB::table('attributes')->insert(
                        [
                            'id' => uniqid("atr_"),
                            'slug' => $slug,
                            'name' => $attribute['name'],
                            'category_id' => $_POST['parent_id'],
                            'filter' => 0,
                            'offer_id' => $offer->id,
                            'value' => $attribute['value'],
                        ]
                    );
                    //--
                }
            }
            //фотки
            if(isset($_FILES['fotos'])) {
                $count = 0;
                foreach ($_FILES['fotos']['name'] as $filename) {
                    $tmp = $_FILES['fotos']['tmp_name'][$count];
                    $foto_name = basename($filename);
                    $newfile = public_path() . '/images/catalog/shops/offers/' . $offer->shop_id . '/' . $offer->id . '/' . $foto_name;
                    if (!Storage::disk('local')->exists('/shops/offers/' . $offer->shop_id . '')) {
                        Storage::disk('local')->makeDirectory('/shops/offers/' . $offer->shop_id . '', 0775, true);
                    }
                    if (!Storage::disk('local')->exists('/shops/offers/' . $offer->shop_id . '/' . $offer->id . '')) {
                        Storage::disk('local')->makeDirectory('/shops/offers/' . $offer->shop_id . '/' . $offer->id . '', 0775, true);
                    }
                    move_uploaded_file($tmp, $newfile);
                    $count++;
                }
            }
        });
    }
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::table()
            ->setColumns(
                [
                    AdminColumn::text('id')->setLabel('#'),
                    AdminColumn::text('name')->setLabel('Название'),
                    AdminColumn::text('category.name')->setLabel('Категория'),
                ]
            );

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id,MetaInterface $meta)
    {
        $meta->addJs('sceditor', asset('ckeditor/ckeditor.js'))
            ->addJs('OffersAttrFormAdd', asset('js/jm_v_1.1/admin/OffersAttrFormAdd.js'))
            ->addCss('tree-select', asset('css/admin/tree-select.css'));

        $categories_tree = Category::get()->toHierarchy();
        $display = AdminForm::panel()->setHtmlAttribute('enctype', 'multipart/form-data')
            ->addBody(
                array(
                    AdminFormElement::hidden('id')
                        ->setDefaultValue(uniqid("off_")),
                    AdminFormElement::text('name', 'Название')->required(),
                    AdminFormElement::text('slug', 'Slug'),
                    AdminFormElement::select('producer', null)
                        ->setModelForOptions(Producer::class)
                        ->setLabel('Производитель')
                        ->setDisplay('name')
                        ->setUsageKey('slug')
                        ->required(),
                    AdminFormElement::select('shop_id', null)
                        ->setModelForOptions(Shop::class)
                        ->setLabel('Магазин')
                        ->setDisplay('name')
                        ->required(),
                    AdminFormElement::html('<label class="control-label">Категория<span class="form-element-required">*</span></label>'),
                    AdminFormElement::custom()
                        ->setDisplay(
                            view(
                                'admin.categories.category_tree_select',
                                [
                                    'categories_tree' => $categories_tree,
                                ]
                            )
                        ),
                    AdminFormElement::html('<label class="control-label">Атрибуты</label>'),
                    AdminFormElement::custom()
                        ->setDisplay(
                            view(
                                'admin.offers.attrFormPart'
                            )
                        ),
                    AdminFormElement::wysiwyg('description', 'Описание', 'ckeditor')
                        ->setFilteredValueToField('description'),
                    AdminFormElement::number('price', 'Цена'),
                    AdminFormElement::html('<label class="control-label">Фото</label>'),
                    AdminFormElement::custom()
                        ->setDisplay(
                            view(
                                'admin.offers.fotoFormPart'
                            )
                        ),
                )
            );

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate(MetaInterface $meta)
    {
        return $this->onEdit(null,$meta);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
