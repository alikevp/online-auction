<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Shops
 *
 * @property \App\Shop $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Shops extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Магазины';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::datatables()
            ->setColumns([
                AdminColumn::link('name')->setLabel('Название'),
                AdminColumn::text('inn')->setLabel('ИНН'),
                AdminColumn::text('created_at')->setLabel('Дата регистрации'),
                AdminColumn::text('updated_at')->setLabel('Дата обновления'),
            ]);

        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Имя'),
            AdminColumnFilter::text()->setPlaceholder('ИНН'),
        ]);

        $display->paginate(15);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody([
            AdminFormElement::text('id', '#')->required()->setReadonly(1),
            AdminFormElement::text('name', 'Название')->required()->setReadonly(1),
            AdminFormElement::text('le_name', 'Название')->required()->setReadonly(1),
            AdminFormElement::text('full_le_name', 'Название')->required()->setReadonly(1),
            AdminFormElement::text('inn', 'ИНН')->setReadonly(1),
            AdminFormElement::text('kpp', 'КПП')->setReadonly(1),
            AdminFormElement::text('ogrn', 'ОГРН')->setReadonly(1),
            AdminFormElement::text('director_fio', 'ФИО директора')->setReadonly(1),
            AdminFormElement::text('phone', 'номер')->setReadonly(1),
            AdminFormElement::checkbox('activity')->setLabel('Активация')
            ]);
        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        //
    }

    /**d
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
