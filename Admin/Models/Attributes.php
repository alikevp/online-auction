<?php

namespace Admin\Models;

use App\Attribute;
use App\Category;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Attributes
 *
 * @property \App\Repositories\Attributes $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Attributes extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Атрибуты';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay(Request $request,MetaInterface $meta)
    {
        if (isset($request->category)) {
            $meta->addJs('arttributes_filter', asset('js/arttributes_filter.js'));
//            $columns = [
//                \AdminColumn::text('id', '#'),
//                \AdminColumn::text('name', 'Name'),
//                \AdminColumn::text('value', 'Value'),
//                \AdminColumn::text('category.name', 'Category'),
//                \AdminColumnEditable::checkbox('filter', 'Show?'),
//            ];
//
//            $filters = [
//                null,
//                null,
//                null,
//                \AdminColumnFilter::select()
//                    ->setModelForOptions(Category::class)
//                    ->setDisplay('name')
//                    ->setPlaceholder('Select category'),
//                null
//            ];
//
//            $display = \AdminDisplay::datatablesAsync()
//                ->setApply(function ($query) use ($request){
//                    $query->where('category_id',$request->category)->orderby('name', 'asc');
//                })
//                ->getScopes()
//                ->set('uniqueSlug');
//            $display->setColumnFilters($filters);
//            $display->setColumns($columns);
//            $display->paginate(10);
//
//            return $display;
            $attributes = Attribute::with('category')->where('category_id', $request->category)->uniqueSlug();

//            dd($attributes);
            return view('admin.attributes.show_table', compact('attributes'));
        } else {
            $categories_tree = Category::get()->toHierarchy();

            return view('admin.category_to_attributes', compact('categories_tree'));
        }
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        // todo: remove if unused
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
