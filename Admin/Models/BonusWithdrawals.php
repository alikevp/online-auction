<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Status;
use Auth;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class BonusWithdrawals
 *
 * @property \App\BonusWithdrawal $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class BonusWithdrawals extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Заявки на вывод бонусов';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $columns = [
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('person')->setLabel('Получатель'),
            AdminColumn::text('bank')->setLabel('Банк'),
            AdminColumn::text('card_number')->setLabel('Номер карты'),
            AdminColumn::text('account_number')->setLabel('Номер счёта'),
            AdminColumn::text('amount')->setLabel('Количество'),
            AdminColumn::text('status.label')->setLabel('Статус'),
        ];

        $display = AdminDisplay::table()
            ->setApply(function ($query) {
                $query->orderBy('created_at', 'desk');
            })
            ->paginate(10)
            ->setColumns($columns);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody(array(
            AdminFormElement::hidden('operator_id')
                ->setDefaultValue(Auth::user()->id),
            AdminFormElement::text('person', 'Получатель')->setReadonly(1),
            AdminFormElement::text('bank', 'Банк')->setReadonly(1),
            AdminFormElement::text('card_number', 'Номер карты')->setReadonly(1),
            AdminFormElement::text('account_number', 'Номер счёта')->setReadonly(1),
            AdminFormElement::text('amount', 'Количество')->setReadonly(1),
            AdminFormElement::text('operator.name', 'Последнее изменение')->setReadonly(1),
            AdminFormElement::select('status_id', 'Статус', Status::class)->setDisplay('label'),
        ));

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
//    public function onDelete($id)
//    {
//        // todo: remove if unused
//    }

    /**
     * Переопределение метода для запрета удаления записи
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return bool
     */
    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
