<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

namespace Admin\Models;

use App\Category;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Users
 * @package App\Admin\Models
 * @property Category
 */
class Categories extends Section
{
    protected $checkAccess = true;

    protected $title = 'Категории';

    public function onDisplay(Request $request,MetaInterface $meta)
    {
        $meta->addCss('tree-select', asset('css/admin/tree-select.css'));

        if (isset($request->page)) {
            if ($request->page == 'add_category') {
                $categories_tree = Category::get()->toHierarchy();
                return view('admin.categories.add_category')
                    ->with(compact('categories_tree'));
            } else {

            }
        } else {
//            $display = AdminDisplay::tree()
//                ->setValue('name');
//
//            return $display;

            return view('admin.categories.category_index')
                ->with(compact('categories_tree'));
        }
    }

}