<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Http\Request;
use Meta;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class Dispatch extends Section implements Initializable
{
    protected $checkAccess = true;
    protected $title = 'Рассылка по E-mail';
    protected $alias;
    public function initialize()
    {
//        $this->addToNavigation($priority = 600);
    }

    public function onDisplay()
    {
        return 'null';
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addCss('tree-select', asset('css/admin/tree-select.css'));

        $callback = function($id, Request $request)
        {
            Spam_base::where('id', $id->id)
                ->update(['category_id' => $request->category_id]);
        };
        $categories_tree = Category::all()->toHierarchy()->toArray();
        $cur_spam = Spam_base::where('id', $id)->first();
        $columns = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Название')->required(),
            AdminFormElement::text('direction', 'Направление'),
            AdminFormElement::text('category_upload', 'Загруженная категория'),
//            AdminFormElement::select('category_id', 'Категория', Category::class)->setDisplay('name'),
            AdminFormElement::custom()
                ->setDisplay(
                    view('admin.spam.categories',
                        [
                            'categories_tree' => $categories_tree,
                            'cur_spam' => $cur_spam
                        ]))
                ->setCallback($callback),
            AdminFormElement::text('city', 'Город'),
            AdminFormElement::text('address', 'Адрес'),
            AdminFormElement::text('tel', 'Телефон'),
            AdminFormElement::text('fax', 'Факс'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::text('www', 'Сайт'),
            AdminFormElement::checkbox('activity', 'Активность'),
        ]);
        return $columns;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
