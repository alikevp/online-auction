<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Producer;
use App\Shop;
use App\Page;
use App\Category;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Spatie\Activitylog\Models\Activity;

/**
 * Class ActivityLogs
 *
 * @property \Spatie\Activitylog\Models\Activity $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class ActivityLogs extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Логи работы с базой';

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->addToNavigation($priority = 500);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay(Request $request)
    {
        if(isset($request->group_by)){
            if($request->group_by == 'tables'){
                $activities = Activity::all()->groupBy('subject_type');

                return view('admin.activity_logs.group_by_tables')
                    ->with(compact('activities'));

            } elseif($request->group_by == 'users'){
                $activities = Activity::all()->groupBy('causer_id');

                return view('admin.activity_logs.group_by_users')
                    ->with(compact('activities'));
            }
		}elseif(isset($request->table)){
			$activities = Activity::where('subject_type', $request->table)->latest()->limit(100)->get()->toJson();
			$activities = json_decode($activities,1);
			if($request->table == "App\Page"){
				$pages = Page::get()->toJson();
				$pages = json_decode($pages,1);
				$pagesQ = count($pages);
				for($thsPg=0;$thsPg<$pagesQ;++$thsPg){
					$pageName[$pages[$thsPg]['id']] = $pages[$thsPg]['title'];
				}
				return view('admin.activity_logs.lists')
                    ->with(compact('activities'))
					->with(compact('pageName'));
			}elseif($request->table == "App\Category"){
				$category = Category::get()->toJson();
				$category = json_decode($category,1);
				$categoryQ = count($category);
				for($thsCt=0;$thsCt<$categoryQ;++$thsCt){
					$catName[$category[$thsCt]['id']] = $category[$thsCt]['name'];
				}
                return view('admin.activity_logs.lists')
                    ->with(compact('activities'))
					->with(compact('pageName'))
					->with(compact('catName'));
			}
		}elseif(isset($request->user)){
			$activities = Activity::where('causer_id', $request->user)->latest()->limit(100)->get()->toJson();
			$activities = json_decode($activities,1);
			$category = Category::get()->toJson();
			$category = json_decode($category,1);
			$categoryQ = count($category);
			for($thsCt=0;$thsCt<$categoryQ;++$thsCt){
				$catName[$category[$thsCt]['id']] = $category[$thsCt]['name'];
			}
			$pages = Page::get()->toJson();
			$pages = json_decode($pages,1);
			$pagesQ = count($pages);
			for($thsPg=0;$thsPg<$pagesQ;++$thsPg){
				$pageName[$pages[$thsPg]['id']] = $pages[$thsPg]['title'];
			}
			return view('admin.activity_logs.byuser')
				->with(compact('activities'))
				->with(compact('pageName'))
				->with(compact('catName'));
		} else{
            return view('admin.activity_logs.index');
        }
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        // remove if unused
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
