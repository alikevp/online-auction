<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Finances
 *
 * @property \App\Finance $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Finances extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Реферралы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::table()
            ->with('user')
            ->setColumns([
                AdminColumn::link('user.name')->setLabel('Имя'),
                AdminColumn::text('debit')->setLabel('Приход'),
                AdminColumn::text('credit')->setLabel('Расход'),
                AdminColumn::text('total')->setLabel('Баланс'),
            ]);

        $display->paginate(15);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        // todo: remove if unused
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
