<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

namespace Admin\Models;

use App\Category;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Users
 * @package App\Admin\Models
 * @property Category
 */
class CategoryMoves extends Section
{
    protected $checkAccess = true;

    protected $title = 'Перенос товаров';

    public function onDisplay(Request $request,MetaInterface $meta)
    {
        $meta->addCss('tree-select', asset('css/admin/tree-select.css'));
        $meta->addJs('tree-select', asset('js/jm_v_1.1/admin/CategoruMove.js'));

        $categories_tree = Category::get()->toHierarchy();
        return view('admin.categories.categoryMove')
            ->with(compact('categories_tree'));
    }
}