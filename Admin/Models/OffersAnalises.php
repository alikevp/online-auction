<?php

namespace Admin\Models;

use App\Category;
use App\OffersAnalise;
use App\OffersUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;


/**
 * Class OffersUploadManager
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class OffersAnalises extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Загрузка товаров';

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->addToNavigation($priority = 500);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay(Request $request,MetaInterface $meta)
    {
        $meta->addCss('tree-select', asset('css/admin/tree-select.css'));

        if(isset($request->page)){
            if($request->page == 'nfcat'){
                $offerAnalises = OffersAnalise::where('our_cat', '')
                    ->orWhereNull('our_cat')
                    ->where('upload_id', $request->upload_id)
                    ->when($request->has('order'), function ($query) use ($request) {
                        return $query->orderBy($request->order, 'asc');
                    })
                    ->get();
                $offerAnalises = $offerAnalises->unique('cat_tree');

                return view('admin.offersUploadManager.notFoundCategories')
                    ->with(compact('offerAnalises'));
            }
            elseif($request->page == 'uploads'){
                $uploads = OffersUpload::orderBy('id')->get();

                return view('admin.offersUploadManager.uploads')
                    ->with(compact('uploads'));
            }
            elseif($request->page == 'edit'){
                $offerAnalise = OffersAnalise::where('id', $request->offer_analise)->first();
                $categories_tree = Category::get()->toHierarchy();

                return view('admin.offersUploadManager.category_to_offers')
                    ->with(compact('categories_tree', 'offerAnalise'));
            }
            elseif($request->page == 'edit_by_field'){
                $offerAnaliseUploadId = $request->upload_id;
                $categories_tree = Category::get()->toHierarchy();

                return view('admin.offersUploadManager.edit_by_field')
                    ->with(compact('categories_tree', 'offerAnaliseUploadId'));
            }
            else{

            }
        } else{
            $upload_folders = Storage::disk('parsing')->directories('/shop_uploads');

            return view('admin.offersUploadManager.index', compact('upload_folders'));
        }
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        // remove if unused
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
