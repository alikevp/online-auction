<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Support
 *
 * @property \App\support $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Support extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = "Поддержка";

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables()
            ->setColumns([
                AdminColumn::link('name')->setLabel('Имя'),
                AdminColumn::text('middle')->setLabel('Фамилия'),
                AdminColumn::text('type')->setLabel('Тип вопроса'),
                AdminColumn::text('theme')->setLabel('Тема вопроса'),
                AdminColumn::text('created_at')->setLabel('Дата регистрации'),
                AdminColumn::text('updated_at')->setLabel('Дата обновления'),
            ]);

        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Имя'),
            AdminColumnFilter::text()->setPlaceholder('ИНН'),
        ]);

        $display->paginate(15);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Название')->setReadonly(1),
            AdminFormElement::text('middle')->setLabel('Фамилия')->setReadonly(1),
            AdminFormElement::text('type')->setLabel('Тип вопроса')->setReadonly(1),
            AdminFormElement::text('theme')->setLabel('Тема вопроса')->setReadonly(1),
            AdminFormElement::text('created_at')->setLabel('Дата регистрации')->setReadonly(1),
            AdminFormElement::text('updated_at')->setLabel('Дата обновления')->setReadonly(1),
        ]);

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
