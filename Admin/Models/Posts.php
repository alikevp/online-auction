<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Post;
use App\Term;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Posts
 *
 * @property \App\Post $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Posts extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Статьи';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::table()
            ->getScopes()
            ->set('posts')
            ->setColumns(
                [
                    AdminColumn::text('id')->setLabel('#'),
                    AdminColumn::text('author.full_name')->setLabel('Author'),
                    AdminColumn::link('title')->setLabel('Title'),
                    AdminColumn::text('slug')->setLabel('Slug'),
                    AdminColumn::lists('terms.name', 'Категории'),
                    AdminColumnEditable::checkbox('published')->setLabel('Published'),
                    AdminColumnEditable::checkbox('comment_on')->setLabel('Comments On'),
                    AdminColumn::datetime('created_at', 'Created')->setFormat('d.m.Y'),
                    AdminColumn::datetime('updated_at', 'Updated')->setFormat('d.m.Y'),

                ]
            );

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id,MetaInterface $meta)
    {

        $meta->addJs('ckeditor2', asset('ckeditor/ckeditor.js'));

        $column1 = AdminFormElement::columns(
            [
                [
                    AdminFormElement::text('title', 'Заголовок')->required(),

                    AdminFormElement::hidden('user_id')
                        ->setDefaultValue(auth()->user()->id),
                    AdminFormElement::text('slug', 'Слаг'),
                    AdminFormElement::multiselect('terms', null)
                        ->setModelForOptions(Term::class)
                        ->setLabel('Категории')
                        ->setLoadOptionsQueryPreparer(
                            function ($item, $query) {
                                return $query->categories();
                            }
                        )
                        ->setDisplay('name'),
                    AdminFormElement::multiselect('terms', null)
                        ->setModelForOptions(Term::class)
                        ->setLabel('Метки')
                        ->setLoadOptionsQueryPreparer(
                            function ($item, $query) {
                                return $query->tags();
                            }
                        )
                        ->setDisplay('name'),
                    AdminFormElement::wysiwyg('content', 'Содержимое', 'ckeditor')
                        ->required()
                        ->setFilteredValueToField('content'),
                    AdminFormElement::checkbox('published', 'Опубликован'),
                    AdminFormElement::checkbox('comment_on', 'Комментарии'),
                    AdminFormElement::hidden('post_type')
                        ->setDefaultValue('post'),
                ],
            ]
        )
            ->setHtmlAttribute('class', 'col-md-9');
        if (isset($id)) {
            $term = Term::categoryByPost($id)->first();
            $post = Post::where('id', $id)->first();
            $column2 = AdminFormElement::columns(
                [
                    [
                        AdminFormElement::html(
                            '<a href="/'.$term->label.'/'.$post->slug.'" target="_blank">Посмотреть пост</a>'
                        ),
                    ],
                ]
            )
                ->setHtmlAttribute('class', 'col-md-3');
            $display = AdminForm::panel()->addBody([$column1, $column2]);
        } else {
            $display = AdminForm::panel()->addBody([$column1]);
        }

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
