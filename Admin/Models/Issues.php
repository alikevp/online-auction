<?php

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Problem;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Issues
 *
 * @property \App\Issue $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Issues extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Вопросы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $columns = [
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::link('theme')->setLabel('Тема'),
            AdminColumn::text('problem.label')->setLabel('Проблема'),
            AdminColumn::text('name')->setLabel('Имя'),
            AdminColumn::text('middle')->setLabel('Фамилия'),
            AdminColumn::text('email')->setLabel('Email'),
            AdminColumn::datetime('created_at')->setLabel('Дата создания'),
            AdminColumn::datetime('updated_at')->setLabel('Дата обновления'),
        ];

        $display = AdminDisplay::table()
            ->setApply(function ($query) {
                $query->orderBy('name', 'asc');
            })
            ->paginate(10)
            ->setColumns($columns);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody(array(
            AdminFormElement::text('name', 'Имя')->required(),
            AdminFormElement::text('middle', 'Фамилия')->required(),
            AdminFormElement::text('email', 'Email')->required(),
            AdminFormElement::text('tel', 'Телефон')->required(),
            AdminFormElement::select('problem_id', 'Проблема', Problem::class)
                ->setDisplay('label'),
//            AdminFormElement::select('problems', 'Проблема', Problem::class)
//                ->setDisplay('label'),
            AdminFormElement::text('theme', 'Тема')->required(),
            AdminFormElement::wysiwyg('message', 'Сообщение', 'ckeditor')
                ->required()
                ->setFilteredValueToField('message')
                ->setParameters([
                    "toolbar" => [
                        [
                            "name" => "basicstyles",
                            "items" => [ "Bold", "Italic", "-", "RemoveFormat" ]
                        ]
                    ]
                ]),
        ));

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
