<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Role;
use App\Shop;
use App\User;
use SleepingOwl\Admin\Section;

/**
 * Class Users
 * @package App\Admin\Models
 * @property User
 */
class Users extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    protected $title = 'Пользователи';

    public function onDisplay()
    {
        $columns = [
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::link('full_name')->setLabel('ФИО'),
            AdminColumn::text('email')->setLabel('Email'),
            AdminColumn::text('e_mail')->setLabel('Email Рассылки'),
            AdminColumn::lists('roles.name', 'Роли'),
            AdminColumn::text('shop.name')->setLabel('Shop'),
        ];


        $table = AdminDisplay::datatables()
            ->with('referrals', 'roles', 'shop')
            ->setColumns($columns)
            ->setApply(
                function ($query) {
                    $query->orderBy('id', 'asc');
                }
            );

        if (auth()->user()->hasRole('shop_admin')) {
            $table->getScopes()
                ->set('shopAdmins');
            $table->paginate(15);

            return $table;
        } elseif (auth()->user()->hasRole('super_booker')) {
            $table->getScopes()
                ->set('booker');
            $table->paginate(15);

            return $table;
        } elseif (auth()->user()->hasRole('super_manager')) {
            $table->getScopes()
                ->set('superManager');
            $table->paginate(15);

            return $table;
        } elseif (auth()->user()->hasRole('manager')) {
            $table->getScopes()
                ->set('manager');
            $table->paginate(15);

            return $table;
        } else {
            $table->paginate(15);

            return $table;
        }
    }

    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody(
            [
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::text('e_mail', 'Email рассылки'),

                AdminFormElement::password('password', 'Password')->required(),
                AdminFormElement::multiselect('shop', 'Shop', Shop::class)->setDisplay('name'),
                AdminFormElement::multiselect('referrals', 'Referrals', User::class)->setDisplay('name'),
                AdminFormElement::multiselect('roles', 'Roles', Role::class)->setDisplay('name'),

            ]
        );

        return $display;
    }

    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
