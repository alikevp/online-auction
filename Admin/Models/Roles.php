<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 06.06.17
 * Time: 10:57
 */

namespace Admin\Models;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Permission;
use App\User;
use SleepingOwl\Admin\Section;

/**
 * Class Roles
 * @package App\Admin\Models
 * @mixin /Eloquent
 */
class Roles extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    protected $title = 'Роли';

    public function onDisplay()
    {
        $display = AdminDisplay::table()
            ->setColumns(
                [
                    AdminColumn::link('id')->setLabel('#'),
                    AdminColumn::link('name')->setLabel('Имя'),
                    AdminColumn::text('slug')->setLabel('Описание'),
                    AdminColumn::lists('permissions.slug', 'Привелегии'),
                    AdminColumn::lists('users.email', 'Пользователи'),
                ]
            );

        $display->paginate(15);

        return $display;
    }

    public function onEdit($id)
    {
        $display = AdminForm::panel()->addBody(
            [
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::multiselect('permissions', 'Permissions', Permission::class)
                    ->setDisplay('slug'),
                AdminFormElement::multiselect('users', 'Users', User::class)
                    ->setDisplay('email'),
            ]
        );

        return $display;
    }

    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
