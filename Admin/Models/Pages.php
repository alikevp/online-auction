<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Page;
use App\Post;
use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Class Pages
 *
 * @property \App\Post $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Pages extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Страницы';

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $post = Post::where('id', $model->id)->first();
            if(isset($_POST['parent_id'])) {

                $parent = Post::where('id', $_POST['parent_id'])->first();
                $post->makeChildOf($parent);
            }
            $template = 'page';
            if(isset($_POST['template'])) {
                $template = $_POST['template'];
            }
            function url_tree_build($tree, $url)
            {
                $url .= '/' . $tree->first()->slug . '';

                if (count($tree->first()->children) > 0) {
                    $url = '' . url_tree_build($tree->first()->children, $url) . '';
                }
                return $url;
            }
            if($post->isRoot()) {
                $url = '/page/'.$post->slug.'';
            }
            else{

                $post_parent_tree = $post->ancestorsAndSelf()->get()->toHierarchy();
                $url = '/page';
                $url = url_tree_build($post_parent_tree, $url);
            }
            Post::where('id', $model->id)
                ->update([
                    'template' => $template,
                    'url' => $url
                ]);

        });
        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $post = Post::where('id', $model->id)->first();
            if(isset($_POST['parent_id'])) {
                $parent = Post::where('id', $_POST['parent_id'])->first();
                $post->makeChildOf($parent);
            }
            function url_tree_build($tree, $url)
            {
                $url .= '/' . $tree->first()->slug . '';

                if (count($tree->first()->children) > 0) {
                    $url = '' . url_tree_build($tree->first()->children, $url) . '';
                }
                return $url;
            }
            if($post->isRoot()) {
                $url = '/page/'.$post->slug.'';
            }
            else{
                $post_parent_tree = $post->ancestorsAndSelf()->get()->toHierarchy();
                $url = '/page';
                $url = url_tree_build($post_parent_tree, $url);
            }

            if(isset($_POST['template'])) {
                Post::where('id', $model->id)
                    ->update([
                        'template' => $_POST['template'],
                        'url' => $url
                    ]);
            }
            else{
                Post::where('id', $model->id)
                    ->update([
                        'url' => $url
                    ]);
            }
        });
    }
    /**
     * @return DisplayInterface
     */

    public function onDisplay(Request $request)
    {

        $display = AdminForm::panel()->addHeader(
            AdminFormElement::html('<a href="?view=table"><button type="button" class="btn btn-default">Таблица</button></a>'),
            AdminFormElement::html('<a href="?view=tree"><button type="button" class="btn btn-default">Дерево</button></a>')
        );
        $table_display = AdminDisplay::table()
            ->setModelClass(Page::class)
            ->getScopes()
            ->set('pages')
            ->setColumns([
                AdminColumn::text('id')->setLabel('#'),
                AdminColumn::text('author.full_name')->setLabel('Author'),
                AdminColumn::link('title')->setLabel('Title'),
                AdminColumn::text('slug')->setLabel('Slug'),
                AdminColumnEditable::checkbox('published')->setLabel('Published'),
                AdminColumnEditable::checkbox('comment_on')->setLabel('Comments On'),
                AdminColumn::datetime('created_at', 'Created')->setFormat('d.m.Y'),
                AdminColumn::datetime('updated_at', 'Updated')->setFormat('d.m.Y'),

            ]);

        $table_display->paginate(15);

        if(isset($request->view)){
            if($request->view == 'table'){
                $display->addBody($table_display);
            }
            elseif($request->view == 'tree'){
                $display->addBody(
                    AdminDisplay::tree()->setValue('title')->setModelClass(Page::class)
                );
            }
            else{
                $display->addBody($table_display);
            }
        }
        else{
            $display->addBody($table_display);
        }
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id, MetaInterface $meta)
    {

        $meta->addJs('ckeditor2', asset('ckeditor/ckeditor.js'));

        $column1 = AdminFormElement::columns([
            [
                AdminFormElement::text('title', 'Заголовок')->required(),

                AdminFormElement::hidden('user_id')
                    ->setDefaultValue(auth()->user()->id),
                AdminFormElement::text('slug', 'Слаг'),
                AdminFormElement::multiselect('tags', null)
                    ->setModelForOptions(Term::class)
                    ->setLabel('Метки')
                    ->setLoadOptionsQueryPreparer(function ($item, $query) {
                        return $query->tags();
                    })
                    ->setDisplay('name'),
                AdminFormElement::wysiwyg('content', 'Содержимое', 'ckeditor')
                    ->setFilteredValueToField('content'),
                AdminFormElement::checkbox('published', 'Опубликован'),
                AdminFormElement::checkbox('comment_on', 'Комментарии'),
                AdminFormElement::hidden('post_type')
                    ->setDefaultValue('page')
            ],
        ])
            ->setHtmlAttribute('class', 'col-md-9');
        //Если запись уже создана и есть ид
        if (isset($id)) {
            $post = Post::where('id', $id)->first();
            $pages_tree = Post::where('post_type', 'page')->orderBy('title')->get()->toHierarchy()->toArray();

            foreach ($pages_tree as $key => $row) {
                $pages_tree_title[$key]  = $row['title'];
            }
            array_multisort($pages_tree_title, SORT_NATURAL, SORT_ASC,$pages_tree);
            $templates = Storage::disk('views')->files('/cms/post/page');
            $column2 = AdminFormElement::columns([
                [
                    AdminFormElement::html('<a href="' . $post->url . '" target="_blank">Посмотреть пост</a>'),
                    AdminFormElement::custom()
                        ->setDisplay(
                            view('admin.posts.parent_posts',
                                [
                                    'pages_tree' => $pages_tree,
                                    'cur_post' => $post->toArray(),
                                    'templates' => $templates
                                ]))
                ],
            ])
                ->setHtmlAttribute('class', 'col-md-3');
            $display = AdminForm::panel()->addBody([$column1,$column2]);
        }
        //Если записи еще нет(нет ид)
        else{
            $pages_tree = Post::where('post_type', 'page')->orderBy('title')->get()->toHierarchy()->toArray();
            $cur_post['id']=null;
            $cur_post['parent_id']=null;
            $cur_post['template']=null;

            $templates = Storage::disk('views')->files('/cms/post/page');

            $column2 = AdminFormElement::columns([
                [
                    AdminFormElement::custom()
                        ->setDisplay(
                            view('admin.posts.parent_posts',
                                [
                                    'pages_tree' => $pages_tree,
                                    'cur_post' => $cur_post,
                                    'templates' => $templates
                                ]))
                ],
            ])
                ->setHtmlAttribute('class', 'col-md-3');


            $display = AdminForm::panel()->addBody([$column1,$column2]);
        }

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
