<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

namespace Admin\Models;

use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Category;
use App\Menu;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Section;


/**
 * Class Menu
 * @package App\Admin\Models
 * @property Menu
 */
class Menus extends Section
{
    protected $checkAccess = true;

    protected $title = 'Меню';

    public function onDisplay()
    {
        $display = AdminDisplay::tree()
            ->setValue('name');

        return $display;
    }

    public function onEdit($id)
    {
        $callback = function ($id, Request $request) {
            $id->categories()->sync($request->categories);

        };
        $menu_data = Menu::with('categories')->find($id);
        $categories_tree = Category::all()->toHierarchy()->toArray();
        if (null !== $menu_data->categories) {
            $categories_selected = $menu_data->categories->pluck('id')->toArray();
        } else {
            $categories_selected = null;
        }
        $display = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Name')->required(),
            AdminFormElement::custom()
                ->setDisplay(
                    view('admin.menu.category_to_menu',
                        [
                            'categories_tree' => $categories_tree,
                            'categories_selected' => $categories_selected
                        ]))
                ->setCallback($callback)
        ]);

        return $display;
    }

    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
