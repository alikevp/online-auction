<?php

namespace Admin\Models;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Category;
use App\Spam_base;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Class SpamBase
 *
 * @property \App\Spam_base $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class SpamBase extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'База E-mail';

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
//        $this->addToNavigation($priority = 600);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay(MetaInterface $meta,Request $request)
    {
        $meta->addCss('admin-table-fix', asset('css/admin-table-fix.css'));
        $meta->addCss('tree-select', asset('css/admin/tree-select.css'));
        if(isset($request->page)) {
            if ($request->page == 'uploadResult') {
                return view('admin.spam.uploadResult');
            }
        }
        else{
            $categories_tree = Category::get()->toHierarchy();
            $upload_files= Storage::disk('parsing')->files('/spamBaseUpload');
            $display = AdminDisplay::tabbed();
            $display->setTabs(function ($id) use($categories_tree,$upload_files) {
                $tabs = [];
                $firstTab = AdminDisplay::datatablesAsync()
                    ->setHtmlAttribute('class', 'admin-table-fix')
                    ->setColumns([
                        AdminColumn::link('name')->setLabel('Название'),
                        AdminColumn::text('direction')->setLabel('Направление'),
                        AdminColumn::text('category_upload')->setLabel('Загруженная категория'),
                        AdminColumn::text('category.name')->setLabel('Категория'),
                        AdminColumn::text('city')->setLabel('Город'),
                        AdminColumn::text('address')->setLabel('Адрес'),
                        AdminColumn::text('tel')->setLabel('Телефон'),
                        AdminColumn::text('fax')->setLabel('Факс'),
                        AdminColumn::text('email')->setLabel('Email'),
                        AdminColumn::text('www')->setLabel('Сайт'),
                    ]);
                $firstTab->paginate(15);
                $firstTab->setColumnFilters([
                    null,
                    AdminColumnFilter::text()->setPlaceholder('Направление'),
                    AdminColumnFilter::text()->setPlaceholder('Загруженная категория'),
                    null,
                    AdminColumnFilter::text()->setPlaceholder('Город'),
                    null,
                    null,
                    null,
                    AdminColumnFilter::text()->setPlaceholder('Email'),
                ]);
                $tabs[] = AdminDisplay::tab($firstTab)->setLabel('Данные');
                $secondTab = view('admin.spam.uploadForm')->with(compact('categories_tree','upload_files'));
                $tabs[] = AdminDisplay::tab($secondTab)->setLabel('Загрузка');
                return $tabs;
            });
            return $display;
        }
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id,MetaInterface $meta)
    {
        $meta->addCss('tree-select', asset('css/admin/tree-select.css'));

        $callback = function($id, Request $request)
        {
            Spam_base::where('id', $id->id)
                ->update(['category_id' => $request->category_id]);
        };
        $categories_tree = Category::all()->toHierarchy()->toArray();
        $cur_spam = Spam_base::where('id', $id)->first();
        $columns = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Название')->required(),
            AdminFormElement::text('direction', 'Направление'),
            AdminFormElement::text('category_upload', 'Загруженная категория'),
//            AdminFormElement::select('category_id', 'Категория', Category::class)->setDisplay('name'),
            AdminFormElement::custom()
                ->setDisplay(
                    view('admin.spam.categories',
                        [
                            'categories_tree' => $categories_tree,
                            'cur_spam' => $cur_spam
                        ]))
                ->setCallback($callback),
            AdminFormElement::text('city', 'Город'),
            AdminFormElement::text('address', 'Адрес'),
            AdminFormElement::text('tel', 'Телефон'),
            AdminFormElement::text('fax', 'Факс'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::text('www', 'Сайт'),
            AdminFormElement::checkbox('activity', 'Активность'),
        ]);
        return $columns;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
