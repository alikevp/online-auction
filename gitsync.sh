#!/bin/sh
echo "Change work directory"
cd /var/www/www-root/data/www/jinnmart.ru/
su - www-root
echo "Reset working files"
git reset --hard
git status -su | cut -d' ' -f2- | tr '\n' '\0' | xargs -0 rm
echo "Update working files"
git pull
echo "Composer dump-autoload"
composer dump-autoload
echo "Publish new vendor files"
php artisan vendor:publish --tag=assets --force
echo "Start composer updating process"
composer update
echo "Composer dump-autoload"
composer dump-autoload
echo "Change permission on working files"
chown www-root:www-root -R /var/www/www-root/data/www/jinnmart.ru/