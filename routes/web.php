<?php
Route::get('/', 'ShoppingController@index');

Route::get('confirmsuccess', 'Auth\RegisterController@confirmsuccess');

Route::get('category/{id}', 'ShoppingController@byCategories')
    ->name('category');


Route::get('brand/{producer}', 'ShoppingController@byProducer');

Route::get('categories', 'CategoriesController@index');
Route::get('categories-offers-recount', 'CategoriesController@offers_recount');
Route::post('category_move', 'CategoriesController@category_move');
Route::post('category_move_check', 'CategoriesController@category_move_check');
Route::get('categories-sidebar-form', 'CategoriesController@categories_sidebar_form');

//Filter
Route::get('filter', 'FilterController@filter');
Route::any('filter/filter_parameters_refresh', 'FilterController@filter_parameters_refresh');
Route::any('filter/filter-products-ajax', 'FilterController@filterByAjax');
Route::post('api/filter', 'FilterController@index');
Route::post('api/suggest', 'FilterController@suggest');

//CART
Route::resource('cart', 'CartController');
Route::post('in_cart_add', 'CartController@in_cart_add');
Route::post('in_cart_del', 'CartController@in_cart_del');
Route::post('in_cart_recount', 'CartController@in_cart_recount');
Route::post('in_cart_count', 'CartController@in_cart_count');
Route::post('fast_order', 'CartController@fast_order');

//DEFERRED
Route::get('deferred', 'DeferredController@index');
Route::post('to_cart', 'DeferredController@to_cart');
Route::any('deferred_add', 'DeferredController@deferred_add');
Route::any('deferred_del', 'DeferredController@deferred_del');
Route::post('deferred_recount', 'DeferredController@deferred_recount');
Route::post('deferred_count', 'DeferredController@deferred_count');
Route::post('deferred_to_cart', 'DeferredController@deferred_to_cart');

// Category manager API
Route::post('api/category/find', 'CategoryAPI@search');
Route::post('api/category/root', 'CategoryAPI@changeRoot');
Route::post('api/category/change', 'CategoryAPI@changeParent');
Route::get('category/select/{id}', 'CategoriesController@show');
Route::post('api/category/associate', 'CategoryAPI@associate');
Route::post('api/category/add_category', 'CategoriesController@add_category');


Route::any('order/test-delivery', function () {
    return view('order.test-delivery');
});

Route::get('mail/unsubscribe/{hash}', 'ProfileController@mail_unsubscription');
Route::post('mail/unsubscribe/{hash}', 'ProfileController@mail_unsubscription_post');

Route::group(['middleware' => ['auth', 'verified', 'notbanned']], function () {

    Route::any('/banned-info','ProfileController@bannedProfile');
    //ORDER
    Route::any('my-orders', 'OrderController@myorders');
    Route::any('order/confirm', 'OrderController@confirm');
    Route::any('order/fast_confirm', 'OrderController@fast_confirm');
    Route::any('order/delivery', 'OrderController@delivery');
    Route::any('order/fast_delivery', 'OrderController@fast_delivery');
    Route::any('order/finish', 'OrderController@finish');
    Route::any('order/fast_finish', 'OrderController@fast_finish');
    Route::any('order/mail_send', 'OrderController@mail_send');
    Route::any('orderdetails', 'OrderController@orderdetails');
    Route::any('offer_accept', 'OrderController@offer_accept');
    Route::any('order_cancel', 'OrderController@order_cancel');
    Route::any('order_extend', 'OrderController@order_extend');

    //AUCTION
    Route::any('auction/orderlist', 'AuctionController@orderlist');
    Route::any('auction/active', 'AuctionController@active');
    Route::post('/auction/order/savebet', 'AuctionController@savebet');
    Route::any('/auction/order/editbet', 'AuctionController@editbet');
    Route::post('/auction/order/saveeditedbet', 'AuctionController@saveeditedbet');
    Route::get('/my-auctions', 'AuctionController@myauctions');
    Route::any('auction/order', 'AuctionController@orderdetails')->name('orderDetails');
    Route::get('auction/hided_products', 'AuctionController@hided_products');
    Route::post('auction/hide', 'AuctionController@hideProducts');
    Route::post('auction/unhide', 'AuctionController@unhideProducts');
//    Route::get('auction/{order_id}', 'AuctionController@manyorderdetails');
    Route::get('/auction/order/gift_delete/{id}', 'AuctionController@giftDelete');
    Route::get('/auction/order/delivery_delete/{id}', 'AuctionController@deliveryDelete');
    Route::post('/auction/order/gift_create', 'AuctionController@giftCreate');
    Route::post('/auction/order/delivery_create', 'AuctionController@deliveryCreate');
    Route::get('/auction/accepted', 'AuctionController@accepted');
    Route::get('/get_order_count', 'AuctionController@getOrderCounter');
    Route::get('/get_win_count', 'AuctionController@getWinCounter');

    // Auction Filters
    Route::post('auction/refused', function () {
        return redirect('auction/orderlist');
    })->name('order.refused');

    // Profile
    Route::get('profile', 'ProfileController@index');
    Route::post('personal_data_edit', 'ProfileController@PersonalDataEdit');
    Route::post('set_avatar', 'ProfileController@setAvatarUser');
    Route::post('telefone_edit', 'ProfileController@telefoneEdit');
    Route::post('password_edit', 'ProfileController@passwordEdit');
    Route::post('email_deal', 'ProfileController@EmailDealEdit');
    Route::post('address_edit', 'ProfileController@addressEdit');
    Route::post('address_update/{id}', 'ProfileController@addressUpdate');
    Route::post('address_delete/{id}', 'ProfileController@addressDelete');
    Route::get('mail_subscription', 'ProfileController@mail_subscription');
    Route::post('mail_subscription/sync', 'ProfileController@mail_subscription_sync');



    // Referrals
    Route::get('referrals', 'ProfileController@referrals');
    Route::post('profile/withdraw_bonuses', 'FinanceController@withdraw_bonuses');

    // ShopProfile
    Route::get('shop_profile', 'ShopProfileController@shop_profile');
    Route::get('shop_subscriptions', 'ShopProfileController@shop_subscriptions');
    Route::post('shop_subscriptions/update', 'ShopProfileController@shop_subscriptions_update');
    Route::post('/shop_bank_data_add', 'ShopProfileController@shop_bank_data_add');
    Route::post('/shop_phone_update', 'ShopProfileController@shop_phone_update');
    Route::post('/shop_data_update', 'ShopProfileController@shop_data_update');
    Route::post('/shop_address_add', 'ShopProfileController@shop_address_add');
    Route::post('/shop_address_edit', 'ShopProfileController@shop_address_edit');
    Route::post('/shop_bank_data_edit', 'ShopProfileController@shop_bank_data_edit');
    Route::post('/shop_payment_type_modal', 'ShopProfileController@shop_payment_type_modal');
    Route::post('/payment_type_add', 'ShopProfileController@payment_type_add');
    Route::any('shopdetails', 'ShopProfileController@shop_details');
    Route::post('/shop_comment', 'CommentController@shop_comment');

    Route::get('reviews/{dealing_id}', 'ReviewsController@index');
    Route::post('reviews/{dealing_id}', 'ReviewsController@send');
});


Route::post('in_cart_count', 'CartController@in_cart_count');

Route::get('search', 'ShoppingController@search');

// Auth routes
Auth::routes();

//Route::get('register/shop', function () {
//    return view('auth.register_shop');
//});
Route::get('register/shop', 'ShopProfileController@register_shop');

Route::get('register/{referral}', 'Auth\RegisterController@registerReferral')->name('referral');
Route::post('register/register_shop', 'Auth\RegisterController@register_shop');
Route::get('register/verify/{token}', 'Auth\RegisterController@verify');
Route::get('register/success', 'Auth\RegisterController@success');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('confirm_request', 'Auth\RegisterController@confirmRequest');
Route::get('confirm_email', 'ProfileController@confirmEmail');
Route::post('send_Verification', 'ProfileController@sendVerification');


// Cart
Route::resource('cart', 'CartController');

// Merch
Route::get('/show/{id}/{menuItem?}', 'ShoppingController@show');

//Comment
Route::post('show/comment&={id}', 'CommentController@index');
Route::post('show/{offer_id}/comment&={id}', 'CommentController@index');


//1С-EXCHANGE
Route::get('exchange/buyers_xml_out', 'ExchangeController@buyers_xml_out');
Route::get('exchange/deals_xml_out', 'ExchangeController@deals_xml_out');
Route::get('exchange/shops_xml_out', 'ExchangeController@shops_xml_out');
Route::get('exchange/bills_xml_in', 'ExchangeController@bills_xml_in');

//SUPPORT
Route::post('/api/support/save_request', 'SupportController@save_request');

//FINANCES
Route::get('profile/my_bonus', 'FinanceController@my_bonus');

Route::get('/comparison', function () {
    return view('comparison.comparison');
});


//POSTS
Route::get('review/{slug}', 'PostController@review');
Route::get('review/', 'PostController@reviews');
Route::get('page/{path}', 'PostController@page')->where('path', '[a-zA-Z0-9/_-]+');
Route::get('page/', 'PostController@pages');
Route::get('article/{slug}', 'PostController@article');
Route::get('article/', 'PostController@articles');
Route::get('instruction/', 'PostController@instructions');
Route::get('instruction/{slug}', 'PostController@instruction');

//Admin
Route::post('attribute-filter-change', 'AttributeController@attribute_filter_change');
Route::post('/spamBaseCsvUpload', 'SpamController@spamBaseCsvUpload');
//Offers_upload offer_temp_xml_upload
Route::post('/offer_temp_upload', 'OfferUploadController@offer_temp_upload');
Route::post('/offer_temp_xml_upload', 'OfferUploadController@offer_temp_xml_upload');
Route::post('/offer_temp_analisis/{upload_id}', 'OfferUploadController@offer_temp_analisis');
Route::any('/offer_upload/{id}', 'OfferUploadController@offer_upload');
Route::post('/set_category/{offerAnaliseId}', 'OfferUploadController@set_category');
Route::post('api/category/add_category_from_offers/{offerAnaliseId}', 'OfferUploadController@add_category');
Route::post('set_category_by_field/{offerAnaliseUploadId}', 'OfferUploadController@set_category_by_field');
Route::any('/offer-analise-export', 'OfferUploadController@offerAnaliseExport');
Route::any('/offer-analise-import', 'OfferUploadController@offerAnaliseImport');
Route::get('/offer_dubl_del', 'OfferUploadController@offer_dubl_del');

Route::get('/comparison', function () {
    return view('comparison.comparison');
});
//test
Route::get('send-test-mail', 'TestController@sendMail');
Route::get('test', 'TestController@index');
Route::get('csv_in', 'TestController@csv_in');