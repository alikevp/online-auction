<?php

use App\Attribute;
use App\Category;
use App\Events\UserRegister;
use App\Menu;
use App\Offer;
use App\User;
use Illuminate\Http\Request;

Route::get('breadcrumbs/{category}', function ($category) {
    if (is_numeric($category)) {
        $cat = Menu::find($category)
            ->getAncestorsAndSelf();
    } else {
        $cat = Category::find($category)
            ->getAncestors();
    }
    return $cat;
});

Route::get('user/get', function (){
    return auth()->user();
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('verify', function (Request $request) {
    $user = User::findOrFail($request->input('user'));
    event(new UserRegister($user));
});

Route::post('filter', function (Request $request) {
    if (is_numeric($request->input('menu'))) {
        $menu = Menu::with('categories')->findOrFail($request->input('menu'));
        $products = Offer::whereIn('category_id', $menu->categories->pluck('id'));
    } else {
        $menu = Category::findOrFail($request->input('menu'));
        $products = Offer::whereIn('category_id', $menu->pluck('id'));
    }


    if ($request->has('producer')) {
        $products = $products->whereIn('producer', $request->input('producer'));
    }

    if ($request->has('attribute') & $request->input('attribute' != '')) {
        $attributes = Attribute::whereIn('id', ($request->input('attribute')))->get();
        $products = $products->whereIn('id', $attributes->pluck('offer_id'));
    }

    if ($request->has('min') || $request->has('max')) {
        $min = $request->input('min');
        $max = $request->input('max');
        $products = $products->whereBetween('price', [$min, $max]);
    }

    $products = $products->paginate(20);

    return view('partials.products', ['products' => $products]);
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('menu', function () {
    return Menu::with('categories', 'parent')->orderBy('lft', 'asc')->get();
});

Route::get('menu/{id}', function ($id) {
    $categories = Category::all()->unique();
    $menu = Menu::with('categories')->find($id);
    return compact('categories', 'menu');
});

Route::get('/categories', function () {
    return Category::with('parent')->get()->unique();
});

Route::post('/menu/{menu}/move', function (Request $request, $menu) {

    $direction = $request->input('direction');
    $menu = Menu::findOrFail($menu);

    if ($direction == 'lft') {
        $menu->moveLeft();
    } elseif ($direction == 'rgt') {
        $menu->moveRight();
    }
    return Menu::with('categories')->orderBy('lft', 'asc')->get();
});

Route::post('menu/{menu}/root', function (Request $request, $menu) {
    $menu = Menu::findOrFail($menu);
    $menu->makeRoot();
    $menu->parent_id = null;
    $menu->save();

    return Menu::with('categories')->orderBy('lft', 'asc')->get();
});

Route::post('menu/{menu}/remove', function (Request $request, $menu) {
    $menu = Menu::findOrFail($menu);
    $menu->delete();

    return Menu::with('categories')->orderBy('lft', 'asc')->get();
});

Route::post('menu/{menu}/parent', function (Request $request, $menu) {
    $menu = Menu::findOrFail($menu);
    $parent = Menu::findOrFail($request->input('parentId'));
    $menu->makeChildOf($parent);

    return Menu::with('categories')->orderBy('lft', 'asc')->get();
});


// API для синхронизации с Git
Route::post('gitsync', function () {
    $output = [];
    exec('sudo /var/www/www-root/data/www/jinnmart.ru/gitsync.sh 2>&1', $output);

    foreach ($output as $line) {
        echo $line . "\r\n";
    }

    return response('Done', 200);
});