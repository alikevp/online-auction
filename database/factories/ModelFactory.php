<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'verification_token' => str_random(50),
    ];
});

$factory->define(App\Offer::class, function (Faker\Generator $faker) {
    return [
        'shop_id' => uniqid('shop_'),
        'producer' => App\Producer::all()->random(1)->first()->slug,
        'category_id' => App\Category::allLeaves()->get()->random(1)->first()->id,
        'name' => $faker->word,
        'id' => uniqid('id_'),
        'slug' => str_slug($faker->word),
    ];
});
