<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('problem_id')->nullable();
            $table->string('name');
            $table->string('middle');
            $table->string('email');
            $table->string('tel');
            $table->string('theme');
            $table->text('message');
            $table->timestamps();
        });

        Schema::create('issue_status', function (Blueprint $table) {
            $table->integer('issue_id');
            $table->integer('status_id');

            $table->foreign('issue_id')
                ->references('id')
                ->on('issues')
                ->onDelete('cascade');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_types');
        Schema::dropIfExists('issue_status');
        Schema::dropIfExists('issues');
    }
}
