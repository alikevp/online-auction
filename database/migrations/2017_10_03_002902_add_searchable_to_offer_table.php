<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSearchableToOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE offers ADD searchable tsvector NULL');
        DB::statement(
            'CREATE INDEX offers_searchable_index ON offers USING GIN (searchable)'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'offers',
            function (Blueprint $table) {
                $table->dropColumn('searchable');
            }
        );
    }
}
