<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailsubscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lable');
            $table->timestamps();
        });

        Schema::create('mailsubscription_user', function (Blueprint $table){
            $table->integer('mailsubscription_id');
            $table->integer('user_id');
            $table->text('hash');

            $table->primary(['mailsubscription_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailsubscriptions');
        Schema::dropIfExists('mailsubscription_user');
    }
}
