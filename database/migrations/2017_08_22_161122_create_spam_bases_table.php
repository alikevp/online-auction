<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpamBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spam_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('direction', 1024)->nullable();
            $table->string('category_upload')->nullable();
            $table->string('category_id')->nullable();
            $table->string('city')->nullable();
            $table->string('address', 1024)->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('www')->nullable();
            $table->boolean('activity')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spam_bases');
    }
}
