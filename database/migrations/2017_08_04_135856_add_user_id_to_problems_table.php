<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToProblemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('problems', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
            $table->string('user_name');
            $table->string('email');
            $table->string('tel')->nullable();
            $table->integer('term_id')->nullable();
            $table->renameColumn('name', 'theme');
            $table->renameColumn('label', 'message');
            $table->integer('operator_id')->nullable();
            $table->timestamp('accepted_at');
            $table->timestamp('closed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
