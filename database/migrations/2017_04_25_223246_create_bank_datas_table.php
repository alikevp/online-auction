<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->nullable();
            $table->string('shop_id')->nullable();
            $table->integer('type');
            $table->string('bank_name');
            $table->string('bank_full_name');
            $table->text('bank_address')->nullable();
            $table->integer('bank_bik');
            $table->string('kor_account');
            $table->string('recipient');
            $table->string('account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_datas');
    }
}
