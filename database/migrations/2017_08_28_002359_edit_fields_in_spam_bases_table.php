<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFieldsInSpamBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spam_bases', function (Blueprint $table) {
            $table->text('name')->change();
            $table->text('direction')->change();
            $table->text('category_upload')->change();
            $table->text('city')->change();
            $table->text('address')->change();
            $table->text('tel')->change();
            $table->text('fax')->change();
            $table->text('email')->change();
            $table->text('www')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spam_bases', function (Blueprint $table) {
            //
        });
    }
}
