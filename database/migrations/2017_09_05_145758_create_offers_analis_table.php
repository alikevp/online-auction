<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersAnalisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers_analises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('value');
            $table->integer('upload_id');
            $table->string('cat0');
            $table->string('cat1');
            $table->string('cat2');
            $table->string('cat3');
            $table->text('cat_tree');
            $table->string('our_cat');
            $table->string('price');
            $table->string('producer');
            $table->string('producer_logo');
            $table->text('description');
            $table->text('attributes');
            $table->text('images');
            $table->string('url');
            $table->string('bardcode');
            $table->string('size');
            $table->string('color');
            $table->string('shop');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers_analise');
    }
}
