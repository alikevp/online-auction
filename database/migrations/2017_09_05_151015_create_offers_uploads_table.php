<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('foto_folder');
            $table->integer('total_offers');
            $table->integer('total_cats');
            $table->integer('cats_found');
            $table->integer('cats_unfound');
            $table->integer('models_found');
            $table->integer('models_unfound');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers_uploads');
    }
}
