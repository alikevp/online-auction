<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('shop_id');
            $table->string('product_id');
            $table->string('comment_shop_id');
            $table->string('comment_product_id');
            $table->string('comment_about_us_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_comments');
    }
}
