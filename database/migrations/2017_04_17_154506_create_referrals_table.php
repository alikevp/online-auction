<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referral');
            $table->string('deal');
            $table->boolean('bought');
            $table->timestamps();
        });

        Schema::create('referral_user', function (Blueprint $table) {

            $table->integer('referral_id');
            $table->integer('user_id');
            $table->primary(['referral_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referrals');
        Schema::dropIfExists('referral_user');
    }
}
