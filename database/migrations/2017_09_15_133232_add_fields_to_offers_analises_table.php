<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOffersAnalisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'offers_analises',
            function (Blueprint $table) {
                $table->integer('sync_order')->nullable();
                $table->boolean('sync_done')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'offers_analises',
            function (Blueprint $table) {
                $table->dropColumn('sync_order');
                $table->dropColumn('sync_done');
            }
        );
    }
}
