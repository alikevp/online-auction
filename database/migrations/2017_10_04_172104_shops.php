<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('shops', 'inn')) {
			Schema::table('shops', function (Blueprint $table) {
				$table->string('inn', 32)->unique()->change();
			});
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'shops',
            function (Blueprint $table) {
                $table->dropColumn('inn');
            }
        );
    }
}
