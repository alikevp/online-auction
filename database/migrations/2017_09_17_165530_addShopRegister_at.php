<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopRegisterAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('shops', 'registrated_at')) {
            Schema::table(
                'shops',
                function (Blueprint $table) {

                    $table->string('registrated_at')->nullable();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'shops',
            function (Blueprint $table) {
                $table->dropColumn('registrated_at');
            }
        );
    }
}
