<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOffersAnaliseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('offers_analises', function ($table) {
            $table->text('name')->change()->nullable();
            $table->text('value')->change()->nullable();
            $table->text('cat0')->change()->nullable();
            $table->text('cat1')->change()->nullable();
            $table->text('cat2')->change()->nullable();
            $table->text('cat3')->change()->nullable();
            $table->text('cat_tree')->change()->nullable();
            $table->text('our_cat')->change()->nullable();
            $table->text('price')->change()->nullable();
            $table->text('producer')->change()->nullable();
            $table->text('producer_logo')->change()->nullable();
            $table->text('description')->change()->nullable();
            $table->text('attributes')->change()->nullable();
            $table->text('images')->change()->nullable();
            $table->text('url')->change()->nullable();
            $table->text('bardcode')->change()->nullable();
            $table->text('size')->change()->nullable();
            $table->text('color')->change()->nullable();
            $table->text('shop')->change()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
