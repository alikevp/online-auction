<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuantityOrderOfferGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_offer_groups', function (Blueprint $table) {
            $table->integer('quantity')->nullable();
            $table->string('offer_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_offer_groups', function (Blueprint $table) {
            $table->dropColumn('quantity');
            $table->dropColumn('offer_id');
        });
    }
}
