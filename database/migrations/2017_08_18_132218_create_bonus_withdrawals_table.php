<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('person');
            $table->integer('person_id');
            $table->string('bank');
            $table->integer('card_number');
            $table->integer('account_number');
            $table->double('amount');
            $table->integer('status_id');
            $table->integer('operator_id')->nullable();
            $table->timestamps();
            $table->timestamp('closed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_withdrawals');
    }
}
