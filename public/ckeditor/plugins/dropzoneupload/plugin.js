function tabbedloadjscssfile(e, t) {
    if (t === "js") {
        var n = document.createElement("script");
        n.setAttribute("type", "text/javascript");
        n.setAttribute("id", "ckjs");
        n.setAttribute("src", e);
        document.body.appendChild(n);
    } else if (t === "css") {
        var n = document.createElement("link");
        n.setAttribute("rel", "stylesheet");
        n.setAttribute("type", "text/css");
        n.setAttribute("href", e)
    }
    if (typeof n !== "undefined") document.getElementsByTagName("head")[0].appendChild(n)
}

CKEDITOR.plugins.add("dropzoneupload", {
    init: function (e) {
        CKEDITOR.on("dialogDefinition", function (e) {
            var t = e.data.name;
            var n = e.data.definition;
            var r = e.data;
            if(t == 'image2') {
                i = '';
                i += '<form style="min-width: 600px;min-height: 400px;" action="/fileupload/upload" class="dropzone" id="my-awesome-dropzone">';

                i += '</form>';
                // i += '<script type="text/javascript" id="ckjs" src="http://jm.amediatest.ru/ckeditor/plugins/tabbedimagebrowser/dropzone.min.js"></script>';
                n.addContents({
                    id: "DropZoneUpload",
                    label: "Пакетная загрузка",
                    accessKey: "M",
                    elements: [{
                        type: "hbox",
                        id: "mydsbrowserhboxele",
                        children: [{
                            type: "html",
                            html: '<div id="dropzoneuploadcontainer">' + i + '</div>'
                        }]
                    }, {id: "dsimagetext", type: "text", accessKey: "M", style: "display:none"}]
                });
                tabbedloadjscssfile(CKEDITOR.basePath + CKEDITOR.plugins.basePath + "dropzoneupload/dropzone.min.css", "css");
                tabbedloadjscssfile(CKEDITOR.basePath + CKEDITOR.plugins.basePath + "dropzoneupload/dropzone.min.js", "js");

                n.onShow = function () {
                    $(function () {
                        var myDropzone = new Dropzone("#my-awesome-dropzone");
                    })
                }
            }

        })
    }
})