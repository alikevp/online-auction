CKEDITOR.plugins.setLang('blueImpGallery', 'gl', {//Galician
	plugin : 'BlueImp Gallery',
	addNewItem : 'Add New Item',
	caption : 'Subtítulo',
	deleteSelected : 'Delete Selected',
	deleteSelectedConfirm : 'Está Seguro De Que Desexa Eliminar Seleccionado?',
	atLeast1Item : 'Por Favor, Engade A Imaxe, Polo Menos, 1',
	ieMessage : 'Recurso De Clasificación Non É Compatible Con Ie9 E Abaixo'
});