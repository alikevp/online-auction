CKEDITOR.plugins.setLang('blueImpGallery', 'si', {//Sinhala
	plugin : 'BlueImp ගැලරිය',
	addNewItem : 'නව විෂය එකතු කරන්න',
	caption : 'ශීර්ෂ පාඨය',
	deleteSelected : 'තෝරාගත් Delete',
	deleteSelectedConfirm : 'ඔබ තෝරාගත් මැකීමට අවශ්ය බව ඔබට විශ්වාසද?',
	atLeast1Item : 'අඩුම වශයෙන් 1 රූපය කරුණාකර එක් කරන්න',
	ieMessage : 'ලක්ෂණය තෝරා බේරා Ie9 සහ පහත දක්වා සමඟ අනුකූල නොවේ'
});