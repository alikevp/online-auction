CKEDITOR.plugins.setLang('blueImpGallery', 'en-gb', {
	plugin					: 'BlueImp Gallery',
	addNewItem				: 'Add New Item',
	caption					: 'Caption',
	deleteSelected			: 'Delete Selected',
	deleteSelectedConfirm	: 'Are you sure you want to delete selected?',
	atLeast1Item			: 'Please add at least 1 image',
	ieMessage				: 'Sorting feature is not compatible with IE9 and below'
});