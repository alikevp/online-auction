CKEDITOR.plugins.setLang('blueImpGallery', 'ro', {//Romanian
	plugin : 'Galeria BlueImp',
	addNewItem : 'Adăugați Un Articol Nou',
	caption : 'Legendă',
	deleteSelected : 'Sterge Selectia',
	deleteSelectedConfirm : 'Sunteți Sigur Că Doriți Să Ștergeți Selectat?',
	atLeast1Item : 'Vă Rugăm Să Adăugați Cel Puțin 1 Imagine',
	ieMessage : 'Sortarea Caracteristică Nu Este Compatibil Cu Ie9 Și De Mai Jos'
});