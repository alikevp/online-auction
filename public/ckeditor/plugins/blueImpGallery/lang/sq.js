CKEDITOR.plugins.setLang('blueImpGallery', 'sq', {//Albanian
	plugin : 'BlueImp Gallery',
	addNewItem : 'Add New Item',
	caption : 'Titull',
	deleteSelected : 'Fshij Zgjedhura',
	deleteSelectedConfirm : 'A Jeni I Sigurt Se Doni Të Fshini Të Zgjedhur?',
	atLeast1Item : 'Ju Lutem Shtoni Të Paktën 1 Imazh',
	ieMessage : 'Sorting Funksion Nuk Është Në Përputhje Me Ie9 Dhe Më Poshtë'
});