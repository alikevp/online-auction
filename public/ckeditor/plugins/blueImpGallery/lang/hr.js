CKEDITOR.plugins.setLang('blueImpGallery', 'hr', {//Croatian
	plugin : 'BlueImp Galerija',
	addNewItem : 'Dodaj Novu Stavku',
	caption : 'Naslov',
	deleteSelected : 'Obriši Odabrano',
	deleteSelectedConfirm : 'Jeste Li Sigurni Da Želite Obrisati Odabrani?',
	atLeast1Item : 'Dodajte Barem 1 Sliku',
	ieMessage : 'Sortiranje Značajka Nije Kompatibilan S Ie9 I Ispod'
});