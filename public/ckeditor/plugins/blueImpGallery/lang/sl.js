CKEDITOR.plugins.setLang('blueImpGallery', 'sl', {//Slovenian
	plugin : 'BlueImp Galerija',
	addNewItem : 'Dodaj Nov Element',
	caption : 'Napis',
	deleteSelected : 'Izbriši Izbrano',
	deleteSelectedConfirm : 'Ali Ste Prepričani, Da Želite Izbrisati Izbrani?',
	atLeast1Item : 'Prosimo, Dodajte Vsaj 1 Slika',
	ieMessage : 'Sortiranje Funkcija Ni Združljiva Z Ie9 In Spodaj'
});