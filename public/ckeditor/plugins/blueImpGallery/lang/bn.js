CKEDITOR.plugins.setLang('blueImpGallery', 'bn', {//bengali
	plugin : 'BlueImp গ্যালারি',
	addNewItem : 'নতুন আইটেম যোগ করুন',
	caption : 'ক্যাপশন',
	deleteSelected : 'মুছে নির্বাচিত',
	deleteSelectedConfirm : 'আপনার নির্বাচিত মুছতে চান আপনি কি নিশ্চিত?',
	atLeast1Item : 'অন্তত 1 ইমেজ যোগ করুন',
	ieMessage : 'বাছাই বৈশিষ্ট্য Ie9 এবং নিচে সঙ্গে সামঞ্জস্যপূর্ণ নয়'
});