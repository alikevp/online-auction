CKEDITOR.plugins.setLang('blueImpGallery', 'lv', {//Latvian
	plugin : 'BlueImp Gallery',
	addNewItem : 'Pievienot Jaunu Postenis',
	caption : 'Virsraksts',
	deleteSelected : 'Dzēst Atlasīti',
	deleteSelectedConfirm : 'Vai Tiešām Vēlaties Dzēst Izvēlēto?',
	atLeast1Item : 'Lūdzu, Pievienojiet Vismaz 1 Attēls',
	ieMessage : 'Šķirošanas Funkcija Nav Saderīgs Ar Ie9 Un Zemāk'
});