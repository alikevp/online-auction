CKEDITOR.plugins.setLang('blueImpGallery', 'is', {//Icelandic
	plugin : 'BlueImp Gallery',
	addNewItem : 'Bæta Við Nýjum Hlutir',
	caption : 'Yfirskrift',
	deleteSelected : 'Eyða Valdar',
	deleteSelectedConfirm : 'Ertu Viss Um Að Þú Viljir Eyða Völdum?',
	atLeast1Item : 'Vinsamlegast Bæta Við Að Minnsta Kosti 1 Mynd',
	ieMessage : 'Flokkun Lögun Er Ekki Samhæft Við Ie9 Og Neðan'
});