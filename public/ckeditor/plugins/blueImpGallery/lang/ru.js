CKEDITOR.plugins.setLang('blueImpGallery', 'ru', {//Russian
	plugin : 'BlueImp Галерея',
	addNewItem : 'Добавить Новый Элемент',
	caption : 'Подпись',
	deleteSelected : 'Удалить Выбранное',
	deleteSelectedConfirm : 'Вы Уверены, Что Хотите Удалить Выбранный?',
	atLeast1Item : 'Пожалуйста, Добавьте По Крайней Мере 1 Изображение',
	ieMessage : 'Сортировка Функция Не Совместима С Ie9 И Ниже'
});