CKEDITOR.plugins.setLang('blueImpGallery', 'ja', {//Japanese
	plugin : 'BlueImpギャラリー',
	addNewItem : '新しい項目の追加',
	caption : 'キャプション',
	deleteSelected : '選択を削除します',
	deleteSelectedConfirm : 'あなたが選択した削除してもよろしいですか？',
	atLeast1Item : '少なくとも1画像を追加してください',
	ieMessage : '機能をソートする以下ie9との互換性はありません'
});