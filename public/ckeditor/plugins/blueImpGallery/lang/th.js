CKEDITOR.plugins.setLang('blueImpGallery', 'th', {//Thai
	plugin : 'BlueImp แกลลอรี่',
	addNewItem : 'เพิ่มรายการใหม่',
	caption : 'คำบรรยายภาพ',
	deleteSelected : 'ลบที่เลือกไว้',
	deleteSelectedConfirm : 'คุณแน่ใจหรือไม่ว่าคุณต้องการลบเลือก?',
	atLeast1Item : 'โปรดเพิ่มอย่างน้อย 1 รูป',
	ieMessage : 'คัดแยกคุณลักษณะไม่เข้ากันกับ Ie9 และด้านล่าง'
});