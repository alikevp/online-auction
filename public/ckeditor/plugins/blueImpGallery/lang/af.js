CKEDITOR.plugins.setLang('blueImpGallery', 'af', {//afrikaans
	plugin : 'BlueImp Gallery',
	addNewItem : 'Voeg Nuwe Item',
	caption : 'Onderskrif',
	deleteSelected : 'Verwyder Geselekteerde',
	deleteSelectedConfirm : 'Is Jy Seker Jy Wil Verwyder Gekies?',
	atLeast1Item : 'Voeg Asseblief Beeld Ten Minste 1',
	ieMessage : 'Sorteer Funksie Is Nie Versoenbaar Is Met Ie9 En Onder'
});