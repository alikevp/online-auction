CKEDITOR.plugins.setLang('blueImpGallery', 'it', {//Italian
	plugin : 'Galleria BlueImp',
	addNewItem : 'Aggiungi Nuovo Elemento',
	caption : 'Didascalia',
	deleteSelected : 'Elimina Selezionato',
	deleteSelectedConfirm : 'Sei Sicuro Che Vuoi Da Eliminare Selezionato?',
	atLeast1Item : 'Si Prega Di Aggiungere Un\'Immagine Di Almeno 1',
	ieMessage : 'Ordinamento Funzione Non È Compatibile Con Ie9 E Al Di Sotto'
});