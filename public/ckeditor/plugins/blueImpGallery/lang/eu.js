CKEDITOR.plugins.setLang('blueImpGallery', 'eu', {//Basque
	plugin : 'BlueImp Gallery',
	addNewItem : 'Gehitu Elementu Berria',
	caption : 'Caption',
	deleteSelected : 'Ezabatu',
	deleteSelectedConfirm : 'Ziur Hautatutako Ezabatu Nahi Duzula?',
	atLeast1Item : 'Gehitu Gutxienez 1 Irudia',
	ieMessage : 'Lazcoz Eginbide Ez Da Ie9 Eta Azpitik Bateragarriak'
});