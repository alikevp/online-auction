CKEDITOR.plugins.setLang('blueImpGallery', 'de', {//german
	plugin : 'BlueImp-Galerie',
	addNewItem : 'Füge Neuen Gegenstand Hinzu',
	caption : 'Bildunterschrift',
	deleteSelected : 'Ausgewählte Löschen',
	deleteSelectedConfirm : 'Sind Sie Sicher, Dass Sie Ausgewählt Zu Löschen?',
	atLeast1Item : 'Bitte Fügen Sie Mindestens 1 Bild',
	ieMessage : 'Feature Sortierung Ist Nicht Kompatibel Mit Ie9 Und Unten'
});