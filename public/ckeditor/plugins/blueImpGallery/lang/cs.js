CKEDITOR.plugins.setLang('blueImpGallery', 'cs', {//czech
	plugin : 'BlueImp Gallery',
	addNewItem : 'Přidat Novou Položku',
	caption : 'Titulek',
	deleteSelected : 'Smazat Vybrané',
	deleteSelectedConfirm : 'Jste Si Jisti, Že Chcete Smazat Vybraná?',
	atLeast1Item : 'Prosím, Přidejte Alespoň 1 Snímek',
	ieMessage : 'Třídění Funkce Není Kompatibilní S Ie9 A Nižší'
});