CKEDITOR.plugins.setLang('blueImpGallery', 'zh-cn', {//Chinese Simplified
	plugin : 'BlueImp图库',
	addNewItem : '添加新项',
	caption : '标题',
	deleteSelected : '删除所选',
	deleteSelectedConfirm : '你确定要删除选定的？',
	atLeast1Item : '请新增至少1图像',
	ieMessage : '排序功能与ie9及以下兼容'
});