CKEDITOR.plugins.setLang('blueImpGallery', 'sr', {//Serbian
	plugin : 'БлуеИмп Галерија',
	addNewItem : 'Додај Нову Ставку',
	caption : 'Натпис',
	deleteSelected : 'Делете Селецтед',
	deleteSelectedConfirm : 'Да Ли Сте Сигурни Да Желите Обрисати Изабран?',
	atLeast1Item : 'Молимо Додајте Бар 1 Слику',
	ieMessage : 'Сортирање Функцију Није Компатибилан Са Ие9 И Испод'
});