CKEDITOR.plugins.setLang('blueImpGallery', 'id', {//Indonesian
	plugin : 'BlueImp Gallery',
	addNewItem : 'Add New Item',
	caption : 'Caption',
	deleteSelected : 'Hapus Yang Dipilih',
	deleteSelectedConfirm : 'Apakah Anda Yakin Ingin Menghapus Dipilih?',
	atLeast1Item : 'Tambahkan Setidaknya 1 Gambar',
	ieMessage : 'Fitur Menyortir Tidak Kompatibel Dengan Ie9 Dan Bawah'
});