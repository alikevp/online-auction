CKEDITOR.plugins.setLang('blueImpGallery', 'pl', {//Polish
	plugin : 'BlueImp Galeria',
	addNewItem : 'Dodaj Nowy Element',
	caption : 'Podpis',
	deleteSelected : 'Usuń Wybrane',
	deleteSelectedConfirm : 'Czy Na Pewno Chcesz Usunąć Wybrany?',
	atLeast1Item : 'Należy Dodać Co Najmniej 1 Zdjęcie',
	ieMessage : 'Opcja Sortowania Nie Jest Kompatybilny Z Ie9 I Poniżej'
});