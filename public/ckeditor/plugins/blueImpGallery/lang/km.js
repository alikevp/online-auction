CKEDITOR.plugins.setLang('blueImpGallery', 'km', {//Khmer
	plugin : 'BlueImp វិចិត្រសាល',
	addNewItem : 'បន្ថែមធាតុថ្មី',
	caption : 'ចំណងជើង',
	deleteSelected : 'លុបដែលបានជ្រើស',
	deleteSelectedConfirm : 'តើអ្នកប្រាកដជាចង់លុបជ្រើសឬ?',
	atLeast1Item : 'សូមបន្ថែមយ៉ាងហោចណាស់ 1 រូបភាព',
	ieMessage : 'តម្រៀបលក្ខណៈពិសេសគឺម​​ិនឆបគ្នាជាមួយ Ie9 និងខាងក្រោម'
});