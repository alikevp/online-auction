CKEDITOR.plugins.setLang('blueImpGallery', 'ar', {	//arabic
	plugin : 'BlueImp معرض',
	addNewItem : 'أضف أداة جديدة',
	caption : 'شرح',
	deleteSelected : 'احذف المختار',
	deleteSelectedConfirm : 'هل أنت متأكد أنك تريد حذف المحدد؟',
	atLeast1Item : 'الرجاء إضافة على الأقل 1 صورة',
	ieMessage : 'ميزة الفرز غير متوافق مع Ie9 وأدناه'
});