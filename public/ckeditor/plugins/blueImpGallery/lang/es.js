CKEDITOR.plugins.setLang('blueImpGallery', 'es', {//spanish
	plugin : 'BlueImp Galería',
	addNewItem : 'Agregar Ítem Nuevo',
	caption : 'Subtítulo',
	deleteSelected : 'Eliminar Seleccionado',
	deleteSelectedConfirm : '¿Está Seguro De Que Quiere Borrar Seleccionado?',
	atLeast1Item : 'Por Favor, Añadir Una Imagen Al Menos 1',
	ieMessage : 'Característica De Ordenación No Es Compatible Con Ie9 Y Por Debajo'
});