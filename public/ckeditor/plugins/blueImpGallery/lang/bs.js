CKEDITOR.plugins.setLang('blueImpGallery', 'bs', {//bosnian
	plugin : 'BlueImp Galerija',
	addNewItem : 'Dodaj Nova Stavka',
	caption : 'Naslov',
	deleteSelected : 'Delete Selected',
	deleteSelectedConfirm : 'Jeste Li Sigurni Da Želite Obrisati Odabrani?',
	atLeast1Item : 'Molimo Dodajte Najmanje 1 Slika',
	ieMessage : 'Mogućnost Sortiranja Nije Kompatibilan S Ie9 I Ispod'
});