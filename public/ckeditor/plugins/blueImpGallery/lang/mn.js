CKEDITOR.plugins.setLang('blueImpGallery', 'mn', {//Mongolian
	plugin : 'BlueImp Зургийн цомог',
	addNewItem : 'Шинэ Зүйл Нэмэх',
	caption : 'Тайлбар',
	deleteSelected : 'Устгах Сонгосон',
	deleteSelectedConfirm : 'Таны Сонгосон Устгахыг Хүсэж Та Итгэлтэй Байна Уу?',
	atLeast1Item : 'Наад Зах Нь 1-Р Зураг Нэмнэ Үү',
	ieMessage : 'Онцлогийг Ялгах Ie9 Болон Доор Нь Нийцтэй Биш Юм'
});