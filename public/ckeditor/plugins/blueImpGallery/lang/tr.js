CKEDITOR.plugins.setLang('blueImpGallery', 'tr', {//Turkish
	plugin : 'BlueImp Galeri',
	addNewItem : 'Yeni Öğe Ekle',
	caption : 'Altyazı',
	deleteSelected : 'Silme Seçildi',
	deleteSelectedConfirm : 'Seçilen Silmek Istediğiniz Emin Misiniz?',
	atLeast1Item : 'En Az 1 Görüntü Ekleyin',
	ieMessage : 'Özelliği Sıralama Aşağıda Ie9 Ile Uyumlu Değildir'
});