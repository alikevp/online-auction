CKEDITOR.plugins.setLang('blueImpGallery', 'ko', {//Korean
	plugin : 'BlueImp 갤러리',
	addNewItem : '새 항목 추가',
	caption : '자막',
	deleteSelected : '삭제 선정',
	deleteSelectedConfirm : '당신은 선택 삭제 하시겠습니까?',
	atLeast1Item : '적어도 한 이미지를 추가하세요',
	ieMessage : '기능을 정렬 아래 Ie9와 호환되지 않습니다'
});