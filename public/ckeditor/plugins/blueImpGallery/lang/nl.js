CKEDITOR.plugins.setLang('blueImpGallery', 'nl', {//Dutch
	plugin : 'BlueImp Gallery',
	addNewItem : 'Nieuw Item Toevoegen',
	caption : 'Onderschrift',
	deleteSelected : 'Verwijder Geselecteerde',
	deleteSelectedConfirm : 'Bent U Zeker Dat U Wilt Verwijderen Geselecteerd?',
	atLeast1Item : 'Gelieve Voeg Het Beeld Ten Minste 1',
	ieMessage : 'Sortering Functie Is Niet Compatibel Met Ie9 En Lager'
});