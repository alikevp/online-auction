CKEDITOR.plugins.setLang('blueImpGallery', 'cy', {//welsh
	plugin : 'Oriel BlueImp',
	addNewItem : 'Ychwanegu New Eitem',
	caption : 'Capsiwn',
	deleteSelected : 'Dileu Selected',
	deleteSelectedConfirm : 'Ydych Chi\'N Siŵr Eich Bod Eisiau Dileu Dewis?',
	atLeast1Item : 'Ychwanegwch O Leiaf 1 Ddelwedd',
	ieMessage : 'Nid Yw Didoli Nodwedd Yn Gydnaws Â Ie9 Ac Yn Is'
});