CKEDITOR.plugins.setLang('blueImpGallery', 'gu', {//Gujarati
	plugin : 'BlueImp ગેલેરી',
	addNewItem : 'નવી આઇટમ ઉમેરો',
	caption : 'કૅપ્શન',
	deleteSelected : 'કાઢી નાખો પસંદ',
	deleteSelectedConfirm : 'તમે પસંદ કરેલ કાઢી નાખવા માંગો છો?',
	atLeast1Item : 'ઓછામાં ઓછા 1 છબી ઉમેરો',
	ieMessage : 'સૉર્ટ લક્ષણ Ie9 અને નીચે સાથે સુસંગત નથી'
});