CKEDITOR.plugins.setLang('blueImpGallery', 'hi', {//Hindi
	plugin : 'BlueImp गैलरी',
	addNewItem : 'नया आइटम जोड़ें',
	caption : 'शीर्षक',
	deleteSelected : 'चयनित मिटाएं',
	deleteSelectedConfirm : 'आपके द्वारा चयनित हटाना चाहते हैं आप सुनिश्चित हैं?',
	atLeast1Item : 'कम से कम 1 छवि को जोड़ने कृपया',
	ieMessage : 'छंटनी सुविधा Ie9 और नीचे के साथ संगत नहीं है'
});