CKEDITOR.plugins.setLang('blueImpGallery', 'sk', {//Slovak
	plugin : 'BlueImp Gallery',
	addNewItem : 'Pridať Novú Položku',
	caption : 'Titulok',
	deleteSelected : 'Zmazať Vybrané',
	deleteSelectedConfirm : 'Ste Si Istí, Že Chcete Zmazať Vybrané?',
	atLeast1Item : 'Prosím, Pridajte Aspoň 1 Snímok',
	ieMessage : 'Triedenie Funkcia Nie Je Kompatibilná S Ie9 A Nižšou'
});