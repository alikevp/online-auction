CKEDITOR.plugins.setLang('blueImpGallery', 'fr-ca', {//french
	plugin : 'BlueImp Gallery',
	addNewItem : 'Ajoute Un Nouvel Objet',
	caption : 'Légende',
	deleteSelected : 'Supprimer Sélectionnée',
	deleteSelectedConfirm : 'Etes-Vous Sûr Que Vous Voulez Supprimer Sélectionné?',
	atLeast1Item : 'S\'Il Vous Plaît Ajouter Au Moins 1 \'Image',
	ieMessage : 'Fonction De Tri Est Pas Compatible Avec Ie9 Et Au-Dessous'
});