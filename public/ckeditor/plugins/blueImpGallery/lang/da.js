CKEDITOR.plugins.setLang('blueImpGallery', 'da', {//danish
	plugin : 'BlueImp Gallery',
	addNewItem : 'Tilføj Nyt Element',
	caption : 'Billedtekst',
	deleteSelected : 'Slet Markerede',
	deleteSelectedConfirm : 'Er Du Sikker På At Du Vil Slette Valgt?',
	atLeast1Item : 'Tilføj Venligst Mindst 1 Billede',
	ieMessage : 'Sortering Funktion Er Ikke Kompatibel Med Ie9 Og Nedenfor'
});