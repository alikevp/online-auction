CKEDITOR.plugins.setLang('blueImpGallery', 'fa', {//Persian
	plugin : 'BlueImp گالری',
	addNewItem : 'اضافه کردن آیتم جدید',
	caption : 'عنوان',
	deleteSelected : 'انتخاب شده را پاک کن',
	deleteSelectedConfirm : 'آیا شما مطمئن هستید که میخواهید انتخاب را حذف کنید؟',
	atLeast1Item : 'لطفا حداقل 1 تصویر اضافه کنید',
	ieMessage : 'از ویژگی های مرتب سازی سازگار با Ie9 و زیر نمی'
});