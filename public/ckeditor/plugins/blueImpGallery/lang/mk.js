CKEDITOR.plugins.setLang('blueImpGallery', 'mk', {//Macedonian
	plugin : 'BlueImp Галерија',
	addNewItem : 'Додадете Нов Елемент',
	caption : 'Наслов',
	deleteSelected : 'Избриши Избрани',
	deleteSelectedConfirm : 'Дали Сте Сигурни Дека Сакате Да Го Избришете Избраниот?',
	atLeast1Item : 'Ве Молиме Додадете Најмалку 1 Слика',
	ieMessage : 'Сортирање Функција Не Е Компатибилен Со Ie9 И Под'
});