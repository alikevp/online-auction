CKEDITOR.plugins.setLang('blueImpGallery', 'ka', {//Georgian
	plugin : 'BlueImp გალერეა',
	addNewItem : 'ახალი საქონელი',
	caption : 'წარწერა',
	deleteSelected : 'მონიშნულის წაშლა',
	deleteSelectedConfirm : 'დარწმუნებული ხართ, რომ გსურთ წაშალოთ შერჩეული?',
	atLeast1Item : 'გთხოვთ, დაამატოთ მინიმუმ 1 სურათი',
	ieMessage : 'დახარისხება ფუნქცია არ არის თავსებადი Ie9 და ქვემოთ'
});