CKEDITOR.plugins.setLang('blueImpGallery', 'hu', {//Hungarian
	plugin : 'BlueImp Galéria',
	addNewItem : 'Új Elem Hozzáadása',
	caption : 'Felirat',
	deleteSelected : 'Kijelöltek Eltávolítása',
	deleteSelectedConfirm : 'Biztos Benne, Hogy Törli A Kiválasztott?',
	atLeast1Item : 'Adjon Meg Legalább 1 Kép',
	ieMessage : 'Az Osztályozás Funkció Nem Kompatibilis Ie9 És Alatt'
});