CKEDITOR.plugins.setLang('blueImpGallery', 'nb', {//Norwegian
	plugin : 'BlueImp Gallery',
	addNewItem : 'Legg Til Ny Artikkel',
	caption : 'Caption',
	deleteSelected : 'Slett Valgte',
	deleteSelectedConfirm : 'Er Du Sikker På At Du Vil Slette Valgt?',
	atLeast1Item : 'Legg Til Minst Ett Bilde',
	ieMessage : 'Sortering Funksjonen Er Ikke Kompatibel Med Ie9 Og Under'
});