CKEDITOR.plugins.setLang('blueImpGallery', 'ms', {//Malay
	plugin : 'Gallery BlueImp',
	addNewItem : 'Tambah Item Baru',
	caption : 'Caption',
	deleteSelected : 'Padam Dipilih',
	deleteSelectedConfirm : 'Adakah Anda Pasti Anda Mahu Memadam Dipilih?',
	atLeast1Item : 'Sila Tambah Sekurang-Kurangnya 1 Imej',
	ieMessage : 'Menyusun Ciri Tidak Serasi Dengan Ie9 Ke Bawah'
});