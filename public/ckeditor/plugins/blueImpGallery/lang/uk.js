CKEDITOR.plugins.setLang('blueImpGallery', 'uk', {//Ukrainian
	plugin : 'BlueImp Галерея',
	addNewItem : 'Додати Новий Елемент',
	caption : 'Підпис',
	deleteSelected : 'Видалити Вибране',
	deleteSelectedConfirm : 'Ви Впевнені, Що Хочете Видалити Вибране?',
	atLeast1Item : 'Будь Ласка, Додайте По Крайней Мере 1 Зображення',
	ieMessage : 'Сортування Функція Не Сумісна З Ie9 І Нижче'
});