CKEDITOR.plugins.setLang('blueImpGallery', 'vi', {//Vietnamese
	plugin : 'BlueImp Gallery',
	addNewItem : 'Add New Item',
	caption : 'Lời Chú Thích',
	deleteSelected : 'Xóa Đã Chọn',
	deleteSelectedConfirm : 'Bạn Có Chắc Chắn Muốn Xóa Chọn?',
	atLeast1Item : 'Vui Lòng Thêm Ít Nhất 1 Hình ảnh',
	ieMessage : 'Tính Năng Phân Loại Là Không Tương Thích Với Ie9 Và Dưới'
});