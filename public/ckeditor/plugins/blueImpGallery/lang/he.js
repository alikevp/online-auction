CKEDITOR.plugins.setLang('blueImpGallery', 'he', {//Hebrew
	plugin : 'גלרית BlueImp',
	addNewItem : 'הוסף פריט חדש',
	caption : 'כּוֹתֶרֶת',
	deleteSelected : 'מחק את הנבחר',
	deleteSelectedConfirm : 'האם אתה בטוח שאתה רוצה למחוק את הנבחרת?',
	atLeast1Item : 'הוסף לפחות תמונה 1',
	ieMessage : 'מיון תכונה אינה עולה בקנה אחד עם Ie9 ומתחת'
});