CKEDITOR.plugins.setLang('blueImpGallery', 'sv', {//Swedish
	plugin : 'BlueImp Gallery',
	addNewItem : 'Lägg Till Nytt Objekt',
	caption : 'Bildtext',
	deleteSelected : 'Radera Valda',
	deleteSelectedConfirm : 'Är Du Säker På Att Du Vill Radera Vald?',
	atLeast1Item : 'Lägg Till Minst En Bild',
	ieMessage : 'Sortering Funktionen Är Inte Kompatibel Med Ie9 Och Under'
});