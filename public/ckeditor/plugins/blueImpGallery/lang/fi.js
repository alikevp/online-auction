CKEDITOR.plugins.setLang('blueImpGallery', 'fi', {//Finnish
	plugin : 'BlueImp Galleria',
	addNewItem : 'Lisää Uusi Kohde',
	caption : 'Kuvateksti',
	deleteSelected : 'Poista Valitut',
	deleteSelectedConfirm : 'Oletko Varma, Että Haluat Poistaa Valitut?',
	atLeast1Item : 'Lisää Vähintään 1 Kuva',
	ieMessage : 'Lajittelu Ominaisuus Ei Ole Yhteensopiva Ie9 Ja Alle'
});