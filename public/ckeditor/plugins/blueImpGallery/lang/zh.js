CKEDITOR.plugins.setLang('blueImpGallery', 'zh', {//Chinese Traditional
	plugin : 'BlueImp圖庫',
	addNewItem : '添加新項',
	caption : '標題',
	deleteSelected : '刪除所選',
	deleteSelectedConfirm : '你確定要刪除選定的？',
	atLeast1Item : '請新增至少1圖像',
	ieMessage : '排序功能與ie9及以下兼容'
});