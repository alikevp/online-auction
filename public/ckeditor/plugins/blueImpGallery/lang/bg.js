CKEDITOR.plugins.setLang('blueImpGallery', 'bg', {//bulgarian
	plugin : 'BlueImp Галерия',
	addNewItem : 'Добавяне На Нов Елемент',
	caption : 'Надпис',
	deleteSelected : 'Изтрий Избраните',
	deleteSelectedConfirm : 'Сигурни Ли Сте Че Искате Да Изтриете Избраните?',
	atLeast1Item : 'Моля, Добавете Поне Една Картинка',
	ieMessage : 'Сортиране Функция Не Е Съвместим С Ie9 И По-Долу'
});