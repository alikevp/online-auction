CKEDITOR.plugins.setLang('blueImpGallery', 'et', {//estonian
	plugin : 'BlueImp Galerii',
	addNewItem : 'Lisa Uus Üksus',
	caption : 'Pealkiri',
	deleteSelected : 'Kustuta Valitud',
	deleteSelectedConfirm : 'Olete Kindel, Et Soovid Kustutada Valitud?',
	atLeast1Item : 'Lisage Vähemalt 1 Pilt',
	ieMessage : 'Sorteerimine Funktsioon Ei Ühildu Ie9 Ja Allpool'
});