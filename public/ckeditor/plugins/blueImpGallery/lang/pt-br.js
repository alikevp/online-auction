CKEDITOR.plugins.setLang('blueImpGallery', 'pt-br', {//Portuguese
	plugin : 'BlueImp Gallery',
	addNewItem : 'Add New Item',
	caption : 'Rubrica',
	deleteSelected : 'Delete Selected',
	deleteSelectedConfirm : 'Tem A Certeza Que Pretende Eliminar Selecionado?',
	atLeast1Item : 'Por Favor, Adicione A Imagem, Pelo Menos, 1',
	ieMessage : 'Recurso De Classificação Não É Compatível Com Ie9 E Abaixo'
});