CKEDITOR.plugins.setLang('blueImpGallery', 'el', {//greek
	plugin : 'BlueImp Gallery',
	addNewItem : 'Προσθήκη Νέου Στοιχείου',
	caption : 'Λεζάντα',
	deleteSelected : 'Διαγραφή Επιλεγμένα',
	deleteSelectedConfirm : 'Είστε Σίγουροι Ότι Θέλετε Να Διαγράψετε Το Επιλεγμένο;',
	atLeast1Item : 'Παρακαλώ Προσθέστε Τουλάχιστον 1 Εικόνα',
	ieMessage : 'Διαλογή Λειτουργία Αυτή Δεν Είναι Συμβατή Με Ie9 Και Κάτω'
});