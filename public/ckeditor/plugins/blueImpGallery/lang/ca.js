CKEDITOR.plugins.setLang('blueImpGallery', 'ca', {//catalan
	plugin : 'BlueImp Galeria',
	addNewItem : 'Afegir Nou Element',
	caption : 'Subtítol',
	deleteSelected : 'Eliminar Seleccionats',
	deleteSelectedConfirm : 'Esteu Segur Que Vol Esborrar Seleccionat?',
	atLeast1Item : 'Si Us Plau, Afegir Una Imatge A Mínim 1',
	ieMessage : 'Característica D\'Ordenació No És Compatible Amb Ie9 I Per Sota'
});