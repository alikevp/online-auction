CKEDITOR.plugins.setLang('blueImpGallery', 'eo', {//Esperanto
	plugin : 'BlueImp Galerio',
	addNewItem : 'Aldoni Novaj Ero',
	caption : 'Apudskribo',
	deleteSelected : 'Forigi Selektitaj',
	deleteSelectedConfirm : 'Ĉu Vi Certas Ke Vi Volas Forigi Elektitan?',
	atLeast1Item : 'Bonvolu Aldoni Almenaŭ 1 Bildo',
	ieMessage : 'Ordigado Karakterizaĵo Ne Estas Kongrua Kun Ie9 Kaj Sube'
});