CKEDITOR.plugins.setLang('blueImpGallery', 'lt', {//Lithuanian
	plugin : 'BlueImp Galerija',
	addNewItem : 'Pridėti Naują Elementą',
	caption : 'Antraštė',
	deleteSelected : 'Ištrinti Pasirinkti',
	deleteSelectedConfirm : 'Ar Tikrai Norite Ištrinti Pasirinktas?',
	atLeast1Item : 'Prašome Pridėti Mažiausiai 1 Nuotrauka',
	ieMessage : 'Rūšiavimas Funkcija Yra Nesuderinama Su Ie9 Ir Žemiau'
});