// CKEDITOR.plugins.add('galleryinsert',{
//   init: function(editor){
//     var cmd = editor.addCommand('galleryinsert', {
//       exec:function(editor){
//           CKEDITOR.dialog( editor, 'smiley' );
//       }
//     });
//     cmd.modes = { wysiwyg : 1, source: 1 };// плагин будет работать и в режиме wysiwyg и в режиме исходного текста
//     editor.ui.addButton('galleryinsert',{
//       label: 'Вставить галлерею',
//       command: 'galleryinsert',
//       toolbar: 'insert'
//     });
//   },
//   icons:'galleryinsert', // иконка
// });
// CKEDITOR.dialog.add( 'galleryinsert', function( editor ) {
//     return {
//         title:          'Вставка галлереи',
//         resizable:      CKEDITOR.DIALOG_RESIZE_BOTH,
//         minWidth:       500,
//         minHeight:      400,
//         contents: [
//             {
//                 id:         'tab1',
//                 label:      'First Tab',
//                 title:      'First Tab Title',
//                 accessKey:  'Q',
//                 elements: [
//                     {
//                         type:           'text',
//                         label:          'Test Text 1',
//                         id:             'testText1',
//                         'default':      'hello world!'
//                     }
//                 ]
//             }
//         ]
//     };
// } );



CKEDITOR.plugins.add( 'galleryinsert', {
    			init: function( editor ) {
				editor.addCommand( 'galleryinsert',new CKEDITOR.dialogCommand( 'galleryinsert' ) );

                    editor.ui.addButton('galleryinsert',{
                        label: 'Вставить галлерею',
                        command: 'galleryinsert',
                        toolbar: 'insert'
                    });


				CKEDITOR.dialog.add( 'galleryinsert', function( api ) {
                    // CKEDITOR.dialog.definition
        					var dialogDefinition = {
            						title: 'Sample dialog',
        						minWidth: 390,
        						minHeight: 130,
        						contents: [
        							{
        								id: 'tab1',
        								label: 'Label',
        								title: 'Title',
        								expand: true,
        								padding: 0,
        								elements: [
        									{
        										type: 'html',
        										html: '<p>This is some sample HTML content.</p>'
        									},
        									{
        										type: 'textarea',
        										id: 'textareaId',
        										rows: 4,
        										cols: 40
            									}
        								]
        							}
        						],
        						buttons: [ CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton ],
        						onOk: function() {
        							// "this" is now a CKEDITOR.dialog object.
        							// Accessing dialog elements:
        							var textareaObj = this.getContentElement( 'tab1', 'textareaId' );
        							alert( "You have entered: " + textareaObj.getValue() );
        						}
        					};

        					return dialogDefinition;
        				} );
			},
            icons:'galleryinsert' // иконка
		} );