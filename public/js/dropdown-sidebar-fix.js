$('.sub-category').hover(function(){
    var elem = $(this).parent().find('.main-category');
    var href = elem.find('i');
    elem.addClass('chosen');
    href.css('visibility','hidden');
}, function(){
    var elem = $(this).parent().find('.main-category');
    var href = elem.find('i');
    elem.removeClass('chosen');
    href.removeAttr('style');
});

$('.category-holder').hover(function(){
    if ($(this).find('.child-category').length + $(this).find('.parent-category').length > 30) {
        $('.sub-category').width($(document).width()/1.5);
    } else if ($(this).find('.child-category').length + $(this).find('.parent-category').length > 15){
        $('.sub-category').width($(document).width()/2);
    } else {
        $('.sub-category').width('100%');
    }

    $('.sub-category').masonry({
        itemSelector: '.parent-category-main'
    });

    $('.sub-category').css('position','absolute');
    },
    function(){
    $('.sub-category').removeAttr('style');
});

$('.catalog-wrapper').on('shown.bs.dropdown',function(){
   $(this).find('.dropdown-menu').height($('.catalog-list').height());
});

/*
var sidebar_height=$('.sub-category').height()
setInterval (function (){
    var h=$('.sub-category').height()
    if(h!=sidebar_height){
        if($('.sub-category').height() < $('.catalog-list').height()){
            $('.sub-category').height($('.catalog-list').height());
        }
        sidebar_height=h;
    }
},20);
*/



