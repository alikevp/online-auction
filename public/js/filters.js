new Vue({
    el: '#filter',

    data: {
        picked: [],
        minPrice: 0,
        maxPrice: 0,
        category: null,
        search: null,
        checked: 'checked',
        producer: []
    },

    mounted: function () {
        $('.slider').on('moved.zf.slider', () => {
            this.minPrice = $('#sliderOutput1').val();
            this.maxPrice = $('#sliderOutput2').val();
        });
    },

    methods: {
        producer: function () {
            this.$http.post(urlFilter, {

                products: products,
                category: category,
                search: search
            }).then((response) => {
                $("#products").html(response.body)
            });
        }
        ,
        price: function () {
            this.$http.post(urlFilter, {
                producer: this.producer,
                min: this.minPrice,
                max: this.maxPrice,
                category: category,
                search: search
            }).then((response) => {
                $("#products").html(response.body)
            });
        }
    }

})
;