    function join(arr) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

function showSuggestion(suggestion) {
    console.log(suggestion);
    var data = suggestion.data;
    if(data.name)
        $('#name_short').val(join([data.opf && data.opf.short || "", data.name.short || data.name.full]," "));
    $('#name_full').val(join([data.opf && data.opf.short || "", data.name.full_with_opf]," "));
    $('#name_inn').val(join([data.inn]));
    $('#name_ogrn').val(join([data.ogrn]));
    $('#name_kpp').val(join([data.kpp]));
    $('#director_fio').val(join([data.management.name]));
    $('#name_address').val(join([data.address.unrestricted_value]));
    $('#name_registered').val(join([data.state.registration_date]));
}

$("#dadata_party").suggestions({
    token: "04b92bad6b543b84100ad2012655143cf83fdc93",
    type: "PARTY",
    count: 5,
    /* Вызывается, когда пользователь выбирает одну из подсказок */
    onSelect: showSuggestion
})