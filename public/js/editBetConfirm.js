$( document ).ready(function() {
    $('#validation-error-wrapper-close').click(function(){
        $('#validation-error-wrapper').removeClass('show');
    });
    /* =================== Изменение ставки =================== */

    $('#openConfirmEditBet').click(function(){
        var form = document.getElementById('editBetForm');
        if(form.checkValidity())
        {
            $('#confirm-edit-bet').modal('show');
        }
        else
        {
            $('#validation-error-wrapper').addClass('show');
            $('#validation-error').html('Заполнены не все обязательные поля!');
        }
    });
    $('#editBetConfirmButton').click(function(){
        var form = document.getElementById('editBetForm');
        if(form.checkValidity())
        {
            form.submit();
        }else
        {
            $('#validation-error').text('Заполнены не все обязательные поля!')
        }
    });

    /*==========================================================*/

    /* ================== Добавление доставки ================== */
    $('#openConfirmAddDelivery').click(function(){
        var form = document.getElementById('addDeliveryForm');
        if(form.checkValidity())
        {
            $('#confirmAddDelivery').modal('show');
        }else
        {
            $('#validation-error-wrapper').addClass('show');
            $('#validation-error').html('Заполнены не все обязательные поля!');
        }
    });
    $('#AddDeliveryConfirmButton').click(function(){
        var form = document.getElementById('addDeliveryForm');
        if(form.checkValidity())
        {
            form.submit();
        }else
        {
            $('#validation-error').text('Заполнены не все обязательные поля!')
        }
    });
    /*==========================================================*/

    /* ================== Добавление подарка ================== */
    $('#openConfirmAddGift').click(function(){
        var form = document.getElementById('addGiftForm');
        if(form.checkValidity())
        {
            $('#confirmAddGift').modal('show');
        }else
        {
            $('#validation-error-wrapper').addClass('show');
            $('#validation-error').html('Заполнены не все обязательные поля!');
        }
    });
    $('#AddGiftConfirmButton').click(function(){
        var form = document.getElementById('addGiftForm');
        if(form.checkValidity())
        {
            form.submit();
        }else
        {
            $('#validation-error').text('Заполнены не все обязательные поля!')
        }
    });
    /*==========================================================*/
});