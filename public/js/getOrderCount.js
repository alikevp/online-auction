function setTextOrderCount(data) {
    $('#order-counter').text('(' + data + ')');
}

function setTextWinCount(data) {
    $('#win-counter').text('(' + data + ')');
}

function getOrderCount() {
    var returnValue;

    $.ajax({
        url: "/get_order_count",
        success: function (data) {
            if(data < 100)
            {
                setTextOrderCount(data);
            } else
            {
                setTextOrderCount("99+");
            }
        }
    });
};

function getWinCount() {
    var returnValue;

    $.ajax({
        url: "/get_win_count",
        success: function (data) {
            if(data < 100)
            {
                setTextWinCount(data);
            } else
            {
                setTextWinCount("99+");
            }
        }
    });
};

$( document ).ready(function() {
    getOrderCount();
    getWinCount();
});