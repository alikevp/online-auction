new Vue({
    el: '#categories_manager',

    data: {
        query: '',
        search_id: '',
        search_text: '',
        result: false,
        found: false,
        model: null,
        categories: null,
        categories_tree: null,
        suggestion: null,
        editedCategory: null
    },

    methods: {
        find: function () {
            this.$http.post(searchAPI, {
                search: this.query
            }).then((response) => {
                this.result = !this.result;
                this.categories = response.body;
            });
        },
        setRoot: function (id) {
            this.$http.post(setRootAPI, {
                category: id
            }).then((response) => {

            });
        },
        doneEdit: function (todo) {
            if (!this.editedCategory) {
                return
            }
        },
        selectCategory: function (id) {
            this.$http.post(selectCategoryAPI, {
                category: id
            }).then((response) => {
                $(".dashed-block").html(response.body);
            });
        }
    },

    directives: {
        'edit-focus': function (el, value) {
            if (value) {
                el.focus()
            }
        }
    },

    // mounted: function () {
    //     $('#category-tree').foundation();
    // }
});