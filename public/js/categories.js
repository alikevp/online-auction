Vue.component('element-select-category', Vue.extend({

    props: {
        //....
    },
    data () {
        return {
            categories: []
        }
    },
    mounted () {
        this.$http.get('/api/categories').then(response =>{
            console.log(response.body);
            this.categories = response.body

        });
    },
    methods: {

    },
    computed: {
        //....
    }
    //....
}));