new Vue({
    el: '#search',

    data: {
        query: '',
        categories: null,
        suggestion: null
    },

    methods: {
        find: function () {
            this.$http.post(searchUrl, {
                search: this.query
            }).then((response) => {
                this.suggestion = response.body;
            });
        }
    }
});