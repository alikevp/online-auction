/**
 * Created by sumerk on 21.03.2017.
 */
(function($){
    $.fn.fdatepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
        daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня"
    };
}(jQuery));

var dt = new Date();
dt.setYear(dt.getFullYear() - 18);

$(function(){
    $('#dpMonths').fdatepicker({
        format: 'dd-mm-yyyy',
        disableDblClickSelection: true,
        language: 'ru',
        endDate: dt
    });
});