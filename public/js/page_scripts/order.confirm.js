
$(document).ready(function(){
    var old = $(".items-table tr:nth-child(n+2)")
    var trigger = $(".full-table-trigger")
    old.hide();
    trigger.click(function(){
        event.preventDefault();
        $(".hidden_row").fadeIn();
        $(this).hide();
    });
});