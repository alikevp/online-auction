
$(window).resize(function(){
    var height = $('.gallery-top').height();
    $('.gallery-thumbs').height(height);
})
$(window).resize();
var galleryTop = new Swiper('.gallery-top', {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    zoom:true,
    hashnav: true,
    hashnavWatchState: true
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 5,
    touchRatio: 0.2,
    slideToClickedSlide: true,
    direction: 'vertical',
    loop:true,
    mousewheelControl:true,
});
$(window).resize(function(){
    var height = $('.equivalent-height').height();
    $('.equivalent-block').height(height);
})
$(window).resize();
$('.slide-common').owlCarousel({
    loop:true,
    items:7,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    autoplaySpeed:1000,
    margin:10,
    nav: true,
    navText: ["<i class='fa fa-chevron-left slide-nav slide-previous' aria-hidden='true'></i>","<i class='fa fa-chevron-right slide-nav slide-next' aria-hidden='true'></i>"],
    dots: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:false
        },
        640:{
            items:4,
            nav:false
        },
        800:{
            items:5,
            nav:false
        },
        1000:{
            items:6,
            nav:true
        },
        1040:{
            items:7,
            nav:true
        }
    }
})
/* matchHeight example */

$(function() {
    // apply your matchHeight on DOM ready (they will be automatically re-applied on load or resize)

    // get test settings
    var byRow = $('body');

    // apply matchHeight to each item container's items
    $('.owl-stage').each(function() {
        $(this).children('.owl-item').matchHeight({
            byRow: byRow
        });
    });


});