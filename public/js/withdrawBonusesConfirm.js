function confirm() {
    $('#newWithdrawBonusesForm').find('[type="submit"]').trigger('click');
}


$( document ).ready(function() {

    $('#openConfirmButton').click(function(){
        var form = document.getElementById('newWithdrawBonusesForm');
        if(form.checkValidity())
        {
            $('#confirm-withdraw-bonuses').foundation('open');
        }else
        {
            confirm();
        }
    });

    $('#WithdrawBonusesConfirmButton').click(function(){
        confirm();
    });
});