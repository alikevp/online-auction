$(document).ready(function () {
    $("#sticky-sidebar").stick_in_parent({
        inner_scrolling : true,
        recalc_every: 1,
    });
});
var observeObject = function () {
    var _class = {
        init: function (selector, callback) {
            var element = document.querySelector(selector);

            try {
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        callback(mutation.target, mutation.attributeName, mutation.oldValue);
                    });
                });

                observer.observe(element, {
                    attributes: true,
                    subtree: true,
                    attributeOldValue: true
                });
            } catch (z) {
                element.addEventListener('DOMAttrModified', function (e) {
                    callback(e.target, e.attrName, e.prevValue);
                }, false);
            }
        }
    };

    return _class;
}();

/* А тут инициализируем отслеживание в элементе, передавая селектор */
$(function () {
    observeObject.init('.is-dropdown-submenu', function (target, name, oldValue) {
        /* ссылка на Node, имя атрибута, предыдущее значение */
        // alert(target);
    });
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

$(function(){
    var support_modal_show = getUrlVars()["support_modal_show"];
    if (support_modal_show == 'true') {
        $('#helpModal').foundation('open');
    }
});

//
// function FindPosition(){
//     var SidebarWidth = $('.left-block').width();
//     var WrapHeight = (FotoWidth*2);
//     $('.show').css({
//         'height': FotoWidth,
//     });
//     $('.show').css({
//         'height': WrapHeight,
//     });
// };
// $(document).ready(function () {
//     FindPosition();
// });
// $(window).resize(function () {
//     FindPosition();
// });
