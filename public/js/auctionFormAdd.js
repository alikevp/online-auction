/**
 * Created by sumerk on 10.03.2017.
 */

var gifts_counter = 0;
var delivery_counter = 0;

$( "#delivery-add-button" ).click(function() {
    $( "#delivery-form" ).append('<div class="form-block">'+
        '<label>Тип <span class="red-star">*</span><span class="form-delete-button float-right red-flash-link">Удалить доставку</span><br>'+
        '<select name="delivery_data['+delivery_counter+'][delivery_type]" required>'+
        '<option value="">Тип</option>'+
        '<option value="Самовывоз">Самовывоз</option>'+
        '<option value="Доставка">Доставка</option>'+
        '</select>'+
        '</label>'+
        '<label>Срок доставки (дней) <span class="red-star">*</span>'+
        '<input type="number" min="0" name="delivery_data['+delivery_counter+'][delivery_time]" placeholder="Срок доставки (дней)" required>'+
        '</label>'+
        '<label>Подробности <span class="red-star">*</span>'+
        '<textarea name="delivery_data['+delivery_counter+'][delivery_description]" placeholder="Детально опишите свои условия доставки (Транспортные компании, сроки, стоимость и т.д.)" required></textarea>'+
        '</label>'+
        '</div>');
        delivery_counter = delivery_counter+1;
});

$( "#gift-add-button" ).click(function() {
    $( "#gifts-form" ).append('<div class="form-block">'+
        '<label>Тип <span class="red-star">*</span><span class="form-delete-button float-right red-flash-link">Удалить подарок</span><br>'+
        '<select name="gifts_data['+gifts_counter+'][option_type]" required>'+
        '<option value="">Тип</option>'+
        '<option value="Товар">Товар</option>'+
        '<option value="Скидка">Скидка</option>'+
        '</select>'+
        '</label>'+
        '<label>Название <span class="red-star">*</span>'+
        '<input type="text" name="gifts_data['+gifts_counter+'][option_name]" placeholder="Название" required>'+
        '</label>'+
        '<label>Описание <span class="red-star">*</span>'+
        '<textarea rows="5" cols="45" name="gifts_data['+gifts_counter+'][option_description]" placeholder="Описание"></textarea>'+
        '</label>'+
        '</div>');
    gifts_counter = gifts_counter+1;
});


$('#delivery-form').on('click', '.form-delete-button', function() {
    $(this).parent().parent().remove();
});

$('#gifts-form').on('click', '.form-delete-button', function() {
    $(this).parent().parent().remove();
});

function giftDelete(element, $id) {
    $(element).text("Удаление...");
    $(element).attr("onclick","");
    document.getElementById('ajax-loading-gif').style.display = 'block';
    $.get( "/auction/order/gift_delete/" + $id )
        .done(function( data ) {
            document.getElementById('ajax-loading-gif').style.display = 'none';
            $(element).text("Удалено!");
            $('#gift_row_'+$id).hide("slow");
            $('#gift_row_'+$id).remove();
        });
}

function deliveryDelete(element, $id) {
    $(element).text("Удаление...");
    $(element).attr("onclick","");
    document.getElementById('ajax-loading-gif').style.display = 'block';
    $.get( "/auction/order/delivery_delete/" + $id )
        .done(function( data ) {
            document.getElementById('ajax-loading-gif').style.display = 'none';
            $(element).text("Удалено!");
            $('#delivery_row_'+$id).hide("slow");
            $('#delivery_row_'+$id).remove();
        });
}

// $('.columns').on('click', '.delete-button', function() {
//     $(this).text("Удаление..");
// });