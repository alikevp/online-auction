$(document).ready(function(){
    $(document).on('change', '.filter_atr_checkbox', function (e) {
        e.preventDefault();
        var slug=$(this).data("filter_atr_checkboxSlug");
        var category_id=$(this).data("filter_atr_checkboxCategory");
        var value=$(this).val();
        if(value == 1){var new_value = 0;}
        else{var new_value = 1;}
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/attribute-filter-change',
            data: {slug:slug, category_id:category_id, value:value},
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('#filter_atr_checkbox_'+slug).attr('value', new_value);
                $('#filter_atr_checkbox_'+slug).val(new_value)
            }
        })
    });
});