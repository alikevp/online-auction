<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header('new'); // подключаем header.php ?>
<section class="main-content-wraper">
    <div class="container">
        <div class="row">
            <?php get_sidebar(); // подключаем sidebar.php ?>
            <div class="content-page col-lg-8 col-md-8">
              <div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
                        <p class="history"><?php the_breadcrumb() ?></p>
                        <h1><?php the_title(); // заголовок поста ?></h1>

                        <?php the_content(); // контент ?>
                    </article>
                <?php endwhile; // конец цикла ?>

                  <div class="next-prev-post">
                      <div class="row">
                         <div class="prev btn-post col-lg-6">
                          <?php previous_post_link('%link','<i class="fa fa-chevron-left"></i>Предыдущий                           пост', TRUE); // ссылка на предыдущий пост?>
                         </div>

                         <div class="next btn-post col-lg-6">
                             <?php next_post_link('%link', 'Следующий пост<i class="fa                                              fa-chevron-right"></i>', TRUE); // ссылка на следующий пост ?>
                         </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); // подключаем footer.php ?>
