<?php
/**
 * Шаблон отдельной записи (single-cat-POLEZNYIE-STATI.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header('new'); // подключаем header.php ?>
<section class="main-content-wraper">
    <div class="container">
        <div class="row">
            <?php get_sidebar(); // подключаем sidebar.php ?>
            <div class="col-sm-8">
                <p class="history"><?php the_breadcrumb() ?></p>

                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                    <div class="">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                    </div>
                <?php endwhile; ?>

                <!--      Похожие статьи          -->

                <p class="production_header">Похожие статьи</p>

                <?php
                $args = array('category_name' => 'articles', 'post_type' => 'post', 'numberposts' => '3');
                $postslist = get_posts($args);

                foreach ($postslist as $post):
                    setup_postdata($post);
                    ?>

                    <div class="row articles_wraper">
                        <div class="col-xs-4 articles_img">
                            <?php the_post_thumbnail('production_thumb'); ?>
                        </div>
                        <div class="col-xs-8 articles_content">
                            <div class="articles_date"><?php the_time('d.m.Y', '<p>', '</p>'); ?></div>
                            <div class="articles_title"><a
                                        href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></div>
                            <div class="article_text">
                                <?php echo the_excerpt(); ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <hr>

                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); // подключаем footer.php ?>
