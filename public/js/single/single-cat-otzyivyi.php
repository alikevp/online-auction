<?php
/**
 * Шаблон отдельной записи (single-cat-otzyivyi.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header('new'); // подключаем header.php ?>
<section class="main-content-wraper">
    <div class="container">
        <div class="row">
            <?php get_sidebar(); // подключаем sidebar.php ?>
            <div class="col-sm-8">
                <p class="history"><?php the_breadcrumb() ?></p>

                <h1>Отзывы</h1>
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                    <div class="col-xs-3">
                        <?php the_post_thumbnail('production_thumb'); ?>
                    </div>
                    <div class="col-xs-9">
                        <p class="otzyv_title"><?php the_title() ?></p>
                        <div class="otzyv_date"><?php the_time('d.m.Y', '<p>', '</p>'); ?></div>
                    </div>
                    <div class="col-xs-12 articles_content">
                        <div class="articles_title"><?php the_content(); ?></div>
                    </div>
                    <div class="clear"></div>
                <?php endwhile; ?>

            </div>
        </div>
    </div>
</section>
<?php get_footer(); // подключаем footer.php ?>
