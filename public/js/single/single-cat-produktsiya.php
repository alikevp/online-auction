<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header('new'); ?>

<section class="main-content-wraper">

    <div class="container">
        <div class="row">

            <?php get_sidebar(); // подключаем sidebar.php ?>
            <div class="col-sm-8">
                <p class="history"><?php the_breadcrumb() ?></p>
                <p class="production_header"><?php the_title(); ?></p>
                <?php
                $post_obj = $wp_query->get_queried_object();
                $post_slug = $post_obj->post_name;
                $args = array('tag_slug__and' => array(''.$post_slug.''), 'category_name' => 'product', 'post_type' => 'post', 'numberposts' => '0');
                $postslist = get_posts($args);

                foreach ($postslist as $post): setup_postdata($post);
                    ?>


                    <div class="post-container col-lg-4">
                        <div class="post-data">
                            <a href="<?php the_permalink() ?>" rel="bookmark">
                                <div class="post-img">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <p><?php the_title(); ?></p>
                            </a>
                        </div>
                    </div>

                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>


</section>

<?php get_footer(); ?>
