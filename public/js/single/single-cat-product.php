<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header('new'); ?>

<section class="main-content-wraper">
    <div class="container">
        <div class="row">
            <?php get_sidebar(); // подключаем sidebar.php ?>
            <div class="col-sm-8">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>

                        <h1><?php the_title(); // заголовок поста ?></h1>
                        <div class="flexslider">
                            <ul class="slides">
                                <?php $images = get_field('gallery-slider', false, false);
                                preg_match('~"([^"]*)"~u',$images,$images_ids);
                                $images_ids_array = explode(',',$images_ids[1]);
                                foreach ($images_ids_array as $images_id):
                                    $image_url = wp_get_attachment_url( $images_id );
                                    $converter = array(
                                        '.png' => '-150x150.png',
                                        '.jpg' => '-150x150.jpg',
                                    );
                                    $img_thumb = strtr($image_url, $converter);
                                ?>

                                <li data-thumb="<?php echo $img_thumb; ?>">
                                    <img src="<?php echo $image_url; ?>" />
                                </li>

                                <?php endforeach; // контент ?>
                            </ul>
                        </div>
                        <script type="text/javascript">
                            $(window).load(function(){
                                $('.flexslider').flexslider({
                                    animation: "slide",
                                    controlNav: "thumbnails",
                                    start: function(slider){
                                        $('body').removeClass('loading');
                                    }
                                });
                                var countThumb = $('ol.flex-control-thumbs li').size();
                                var thumbWidth = 100 / countThumb;
                                $('ol.flex-control-thumbs li').css({
                                    'width': thumbWidth+'%'
                                });
                            });
                        </script>
                        <?php the_content(); // контент ?>
                    </article>
                <?php endwhile; // конец цикла ?>
                <?php previous_post_link('%link', 'Предыдущий пост', TRUE); // ссылка на предыдущий пост ?>
                <?php next_post_link('%link', 'Следующий пост', TRUE); // ссылка на следующий пост ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
