function confirm() {
    $('#newBetForm').find('[type="submit"]').trigger('click');
}


$( document ).ready(function() {

    $('#openConfirmButton').click(function(){
        var form = document.getElementById('newBetForm');
        if(form.checkValidity())
        {
            $('#openConfirm').modal('show');
        }else
        {
            $('#validation-error-wrapper').addClass('show');
            $('#validation-error').html('Заполнены не все обязательные поля!');
        }
    });

    $('#addBetConfirmButton').click(function(){
        var form = document.getElementById('newBetForm');
        if(form.checkValidity())
        {
            form.submit();
        }else
        {
            $('#validation-error').text('Заполнены не все обязательные поля!')
        }
    });
    $('#validation-error-wrapper-close').click(function(){
        $('#validation-error-wrapper').removeClass('show');
    });
});