$(document).ready(function(){
    $(document).on('change', '#bank_data_equal', function (e) {
        e.preventDefault();
        if ($('#bank_data_equal').prop('checked')) {
            var html ='';
        }
        else{
            var html =
                '<input type="hidden"  name="bank_data[2][type]" value="2">'+
                '<div class="row">'+
                '<div class="col-sm-12">'+
                '<label class="width-100"><span class="bold-text">БИК (для автоматического поиска)</span><span class="red-star">*</span>'+
                '<input class="form-control" id="bank_search2" type="text" placeholder="БИК" maxlength="9" required>'+
                '</label>'+
                '</div>'+
                '</div>'+
                '<div class="row">'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">Сокращенное наименование банка</span>'+
                '<input class="form-control" id="short_name2" type="text" placeholder="Сокращенное наименование банка" name="bank_data[2][bank_name]">'+
                '</label>'+
                '</div>'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">Полное наименование банка</span><span class="red-star">*</span>'+
                '<input class="form-control" id="full_name2" type="text" placeholder="Наименование банка" name="bank_data[2][bank_full_name]" required>'+
                '</label>'+
                '</div>'+
                '</div>'+
                '<div class="row">'+
                '<div class="col-sm-12">'+
                '<label class="width-100"><span class="bold-text">Адрес банка</span><span class="red-star">*</span>'+
                '<input class="form-control" id="bank_address2" type="text" placeholder="Адрес банка" name="bank_data[2][bank_address]" required>'+
                '</label>'+
                '</div>'+
                '</div>'+
                '<div class="row">'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">БИК</span><span class="red-star">*</span>'+
                '<input class="form-control" id="bank_bik2" type="text" placeholder="БИК" maxlength="9" name="bank_data[2][bank_bik]" required>'+
                '</label>'+
                '</div>'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">Кор. счет</span><span class="red-star">*</span>'+
                '<input class="form-control" id="kor_account2" type="text" placeholder="Кор. счет" maxlength="20" name="bank_data[2][kor_account]" required>'+
                '</label>'+
                '</div>'+
                '</div>'+
                '<div class="row">'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">Счет</span><span class="red-star">*</span>'+
                '<input class="form-control" id="" type="text" maxlength="20" placeholder="Счет" name="bank_data[2][account]" required>'+
                '</label>'+
                '</div>'+
                '<div class="col-sm-6">'+
                '<label><span class="bold-text">Получатель</span><span class="red-star">*</span>'+
                '<input class="form-control" id="firm_recipient" type="text" maxlength="20" placeholder="Получатель" name="bank_data[2][recipient]" required>'+
                '</label>'+
                '</div>'+
                '</div>';
        }
        $('#bank-inputs-block').html(html);

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#bank_search2").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
                var data = suggestion.data;
                if (!data)
                    return;
                $("#short_name2").val(data.name.full);
                $("#full_name2").val(data.name.full);
                $("#bank_bik2").val(data.bic);
                $("#kor_account2").val(data.correspondent_account);
                $("#bank_address2").val(data.address.value);
            }
        });
    });
});

$(document).ready(function(){
    $(document).on('change', '#post_address_equal', function (e) {
        e.preventDefault();
        if ($('#post_address_equal').prop('checked')) {
            var html ='';
        }
        else{
            var html =
            '<div class="row">'+
            '<input type="hidden"  name="address_data[2][type]" value="2">'+
            '<div class="col-sm-12">'+
            '<label>Страна<span class="red-star">*</span>'+
            '<input class="form-control" type="text" placeholder="Ваша страна" name="address_data[2]country]" required>'+
            '</label>'+
            '<label>Населенный пункт/Город<span class="red-star">*</span>'+
            '<input class="form-control" id="dadata_city2" type="text" placeholder="Ваш город" name="address_data[2][city]" required>'+
            '</label>'+
            '<label>Улица<span class="red-star">*</span>'+
            '<input class="form-control" id="dadata_street2" type="text" placeholder="Ваша улица" name="address_data[2][street]" required>'+
            '</label>'+
            '</div>'+
            '<div class="col-sm-4">'+
            '<label>Дом<span class="red-star">*</span>'+
            '<input class="form-control" id="dadata_house2" type="text" placeholder="Номер вашего дома" name="address_data[2][house]" required>'+
            '</label>'+
            '</div>'+
            '<div class="col-sm-4">'+
            '<label>Квартира<span class="red-star"></span>'+
            '<input class="form-control" id="dadata_flat2" type="text" placeholder="Номер вашей квартиры" name="address_data[2][apartment]" >'+
            '</label>'+
            '</div>'+
            '<div class="col-sm-4">'+
            '<label>Почтовый индекс<span class="red-star">*</span>'+
            '<input class="form-control" id="dadata_postal_code2" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="address_data[2][postcode]" required>'+
            '</label>'+
            '</div>'+
            '</div>';
        }
        $('#post-address-block').html(html);

        /**
         * Показывает индекс в отдельном поле
         */
        function showPostalCode(suggestion) {
            $("#dadata_postal_code2").val(suggestion.data.postal_code);
        }

        /**
         * Очищает индекс
         */
        function clearPostalCode(suggestion) {
            $("#dadata_postal_code2").val("");
        }
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93",
            type  = "ADDRESS",
            $region = $("#region"),
            $area   = $("#area"),
            $city   = $("#dadata_city2"),
            $settlement = $("#settlement"),
            $street = $("#dadata_street2"),
            $house  = $("#dadata_house2");

        // регион
        $region.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "region",
        });

        // район
        $area.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "area",
            constraints: $region
        });

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city",
            constraints: $area,
            onSelect: showPostalCode,
            onSelectNothing: clearPostalCode
        });

        // geolocateCity($city);

        // город и населенный пункт
        $settlement.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "settlement",
            constraints: $city,
            onSelect: showPostalCode,
            onSelectNothing: clearPostalCode
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $settlement,
            onSelect: showPostalCode,
            onSelectNothing: clearPostalCode
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street,
            onSelect: showPostalCode,
            onSelectNothing: clearPostalCode
        });
    });
});
