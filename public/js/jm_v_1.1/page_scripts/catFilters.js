
function setAttr(prmName,val){
    var res = '';
    var d = location.href.split("#")[0].split("?");
    var base = d[0];
    // var query = d[1];
    // if(query) {
    //     var params = query.split("&");
    //     for(var i = 0; i < params.length; i++) {
    //         var keyval = params[i].split("=");
    //         if(keyval[0] != prmName) {
    //             res += params[i] + '&';
    //         }
    //     }
    // }
    // res += prmName + '=' + val;
    res = val;
    window.history.pushState(null, null, base + '?' + res);
    return false;
}

$(document).ready(function(){
    $(document).on('change', '.filter_parameter', function (e) {
        e.preventDefault();
        var checkboxesChecked = new Object();
        var order=$('#order_type_input').val();
        checkboxesChecked['order']= order;
        var minPrice = $('#sliderOutput1').val();
        $("#no_price:checked").each(function() {
            var noPrice = $('#no_price').val();
            checkboxesChecked['no_price']= noPrice;
            minPrice = 0;
        });
        checkboxesChecked['min']= minPrice;
        var maxPrice = $('#sliderOutput2').val();
        checkboxesChecked['max']= maxPrice;
        checkboxesChecked['attributes']= new Object();
        $(".filter_parameter_producer:checked").each(function(){
            var val=$(this).val();
            if (checkboxesChecked.hasOwnProperty('producer')){
                checkboxesChecked['producer'].push(val);
            }
            else{
                checkboxesChecked['producer']= [];
                checkboxesChecked['producer'].push(val);
            }

        });
        $(".filter_parameter_attributes:checked").each(function(){
            var slug=$(this).data("filter_parameterSlug");
            var val=$(this).val();
            if (checkboxesChecked['attributes'].hasOwnProperty(slug)){
                checkboxesChecked['attributes'][slug].push(val);
            }
            else{
                checkboxesChecked['attributes'][slug]= [];
                checkboxesChecked['attributes'][slug].push(val);
            }

        });
        var category=$('.filter_parameter_category').val();
        checkboxesChecked['category']= category;
        var str = jQuery.param(checkboxesChecked);
        setAttr('',str);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter_parameters_refresh',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(html){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('#filter_parameters_wraper').html(html);
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter-products-ajax',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(html){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('.products-filter-block').html(html);
            }
        })
    });
});
$(document).ready(function(){
    $("#costSlider").slider().on('slideStop', function(sliderData){
        var checkboxesChecked = new Object();
        var order = $('#order_type_input').val();
        checkboxesChecked['order'] = order;
        var minPrice = sliderData['value'][0];
        $("#no_price:checked").each(function() {
            var noPrice = $('#no_price').val();
            checkboxesChecked['no_price']= noPrice;
            minPrice = 0;
        });
        checkboxesChecked['min'] = minPrice;
        var maxPrice = sliderData['value'][1];
        checkboxesChecked['max'] = maxPrice;
        checkboxesChecked['attributes'] = new Object();
        $(".filter_parameter_producer:checked").each(function () {
            var val = $(this).val();
            if (checkboxesChecked.hasOwnProperty('producer')) {
                checkboxesChecked['producer'].push(val);
            }
            else {
                checkboxesChecked['producer'] = [];
                checkboxesChecked['producer'].push(val);
            }
        });
        $(".filter_parameter_attributes:checked").each(function () {
            var slug = $(this).data("filter_parameterSlug");
            var val = $(this).val();
            if (checkboxesChecked['attributes'].hasOwnProperty(slug)) {
                checkboxesChecked['attributes'][slug].push(val);
            }
            else {
                checkboxesChecked['attributes'][slug] = [];
                checkboxesChecked['attributes'][slug].push(val);
            }

        });
        var category = $('.filter_parameter_category').val();
        checkboxesChecked['category'] = category;
        var str = jQuery.param(checkboxesChecked);
        setAttr('', str);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter_parameters_refresh',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function () {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function (html) {
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('#filter_parameters_wraper').html(html);
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter-products-ajax',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function () {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function (html) {
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('.products-filter-block').html(html);
            }
        })
    });
});
$(document).ready(function(){
    $(document).on('change', '.filter_parameter_price', function (e) {
        e.preventDefault();
        var checkboxesChecked = new Object();
        var order = $('#order_type_input').val();
        checkboxesChecked['order'] = order;
        var minPrice = $('#sliderOutput1').val();
        $("#no_price:checked").each(function() {
            var noPrice = $('#no_price').val();
            checkboxesChecked['no_price']= noPrice;
            minPrice = 0;
        });
        checkboxesChecked['min'] = minPrice;
        var maxPrice = $('#sliderOutput2').val();
        checkboxesChecked['max'] = maxPrice;
        checkboxesChecked['attributes'] = new Object();
        $(".filter_parameter_producer:checked").each(function () {
            var val = $(this).val();
            if (checkboxesChecked.hasOwnProperty('producer')) {
                checkboxesChecked['producer'].push(val);
            }
            else {
                checkboxesChecked['producer'] = [];
                checkboxesChecked['producer'].push(val);
            }
        });
        $(".filter_parameter_attributes:checked").each(function () {
            var slug = $(this).data("filter_parameterSlug");
            var val = $(this).val();
            if (checkboxesChecked['attributes'].hasOwnProperty(slug)) {
                checkboxesChecked['attributes'][slug].push(val);
            }
            else {
                checkboxesChecked['attributes'][slug] = [];
                checkboxesChecked['attributes'][slug].push(val);
            }

        });
        var category = $('.filter_parameter_category').val();
        checkboxesChecked['category'] = category;
        var str = jQuery.param(checkboxesChecked);
        setAttr('', str);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter_parameters_refresh',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function () {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function (html) {
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('#filter_parameters_wraper').html(html);
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter-products-ajax',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function () {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function (html) {
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('.products-filter-block').html(html);
            }
        })
    });
});

$(document).ready(function(){
    $(document).on('change', '#order_type_select', function (e) {
        e.preventDefault();
        var checkboxesChecked = new Object();
        var order=$(this).val();
        $('#order_type_input').attr('value', order);
        $('#order_type_input').val(order);
        checkboxesChecked['order']= order;
        var minPrice = $('#sliderOutput1').val();
        $("#no_price:checked").each(function() {
            var noPrice = $('#no_price').val();
            checkboxesChecked['no_price']= noPrice;
            minPrice = 0;
        });
        checkboxesChecked['min']= minPrice;
        var maxPrice = $('#sliderOutput2').val();
        checkboxesChecked['max']= maxPrice;
        checkboxesChecked['attributes']= new Object();
        $(".filter_parameter_producer:checked").each(function(){
            var val=$(this).val();
            if (checkboxesChecked.hasOwnProperty('producer')){
                checkboxesChecked['producer'].push(val);
            }
            else{
                checkboxesChecked['producer']= [];
                checkboxesChecked['producer'].push(val);
            }

        });
        $(".filter_parameter_attributes:checked").each(function(){
            var slug=$(this).data("filter_parameterSlug");
            var val=$(this).val();
            if (checkboxesChecked['attributes'].hasOwnProperty(slug)){
                checkboxesChecked['attributes'][slug].push(val);
            }
            else{
                checkboxesChecked['attributes'][slug]= [];
                checkboxesChecked['attributes'][slug].push(val);
            }

        });
        var category=$('.filter_parameter_category').val();
        checkboxesChecked['category']= category;
        var str = jQuery.param(checkboxesChecked);
        setAttr('',str);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/filter/filter-products-ajax',
            data: checkboxesChecked,
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(html){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('.products-filter-block').html(html);
            }
        })
    });
});