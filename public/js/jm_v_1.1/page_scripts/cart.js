//cart
function in_cart_count(){
    var method='post';
    var url='/in_cart_count';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: method,
        url: url,cache: false,
        beforeSend: function() {
            //document.getElementById('ajax-loading-gif').style.display = 'block';
        },
        success: function(html){
            //document.getElementById('ajax-loading-gif').style.display = 'none';
            $("#cart-items-count-value").text(html);
            $("#cart-items-count-value-mobile").text(html);
        },
        error: function (ajaxContext) {
            $('#ajax_error').html(ajaxContext.responseText);
        }
    })
};

$(document).ready(function () {
    in_cart_count();
});

$(document).ready(function(){
    $(document).on('click', '.in_cart_add', function (e) {
        e.preventDefault();
        var in_cart_prod_id=$(this).data("inCartProdId");
        var method='post';
        var url=$(this).data("inCartUrl");

        var top = $(this).offset().top-300;
        var left = $(this).offset().left-100
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:in_cart_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';

                $("#product_foto_0")
                    .clone()
                    .css({'position' : 'absolute', 'z-index' : '11100', top: top, left:left})
                    .appendTo("body")
                    .animate({opacity: 0.05,
                        left: $(".cart-items-count").offset()['left'],
                        top: $(".cart-items-count").offset()['top'],
                        width: 20}, 1000, function() {
                        $(this).remove();
                    });
                in_cart_count();
            }
        })
    });
});

$(document).ready(function(){
    $(document).on('click', '.fast_order', function (e) {
        e.preventDefault();
        var in_cart_prod_id=$(this).data("fastOrderProdId");
        var method='post';
        var url=$(this).data("fastOrderUrl");

        var top = $(this).offset().top-300;
        var left = $(this).offset().left-100
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:in_cart_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                console.log(in_cart_prod_id);
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                window.location.replace('/order/fast_delivery');
            }
        })
    });
});

$(document).ready(function(){
    $(document).on('click', '#in_cart_del', function (e) {
        e.preventDefault();
        var in_cart_prod_id=$(this).data("inCartProdId");
        var method='post';
        var url=$(this).data("inCartUrl");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:in_cart_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                in_cart_count();
                $("#cart_prod_row_"+in_cart_prod_id).hide("slow");
                $("#cart_prod_row_"+in_cart_prod_id).remove();
            }
        })
    });
});

$(document).ready(function(){
    $(document).on('change', '#product_count_value', function (e) {
        e.preventDefault();
        var in_cart_prod_id=$(this).data("inCartProdId");
        var inCartProdPrice=$(this).data("inCartProdPrice");
        var totalPrice = $("#total-price-value").text();
        var method='post';
        var url=$(this).data("inCartUrl");
        var prodCurCount=$(this).data("prodCurCount");
        var prodNewCount=$(this).val();
        var productAmount = prodNewCount * inCartProdPrice;
        var OldproductAmount = prodCurCount * inCartProdPrice;
        var ActualTotalPrice = (totalPrice - OldproductAmount) + productAmount;
        $('.product_count_'+in_cart_prod_id).attr('data-prod-cur-count', prodNewCount);
        $('.product_count_'+in_cart_prod_id).data('prodCurCount', prodNewCount);
        $('.product_count_'+in_cart_prod_id).attr('value', prodNewCount);
        $('.product_count_'+in_cart_prod_id).val(prodNewCount);

        $("#product-amount_"+in_cart_prod_id).text(productAmount);
        $("#total-price-value").text(ActualTotalPrice);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:in_cart_prod_id,prodCurCount:prodCurCount,prodNewCount:prodNewCount},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                in_cart_count();
            }
        })
    });
});
//cart_end

//DEFERRED

$(document).ready(function(){
    $(document).on('click', '.deferred_add', function (e) {
        e.preventDefault();
        var deferred_prod_id=$(this).data("deferredProdId");
        var method='post';
        var url=$(this).data("deferredUrl");

        var top = $(this).offset().top-300;
        var left = $(this).offset().left-100
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:deferred_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';

                $("#product_foto_0")
                    .clone()
                    .css({'position' : 'absolute', 'z-index' : '11100', top: top, left:left})
                    .appendTo("body")
                    .animate({opacity: 0.05,
                        left: $(".deferred-items-count").offset()['left'],
                        top: $(".deferred-items-count").offset()['top'],
                        width: 20}, 1000, function() {
                        $(this).remove();
                    });
            }
        })
    });
});

$(document).ready(function(){
    $(document).on('click', '#deferred_del', function (e) {
        e.preventDefault();
        var deferred_prod_id=$(this).data("deferredProdId");
        var method='post';
        var url=$(this).data("deferredUrl");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:deferred_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $("#deferred_prod_row_"+deferred_prod_id).hide("slow");
                $("#deferred_prod_row_"+deferred_prod_id).remove();
            }
        })
    });
});
$(document).ready(function(){
    $(document).on('click', '#deferred_to_cart', function (e) {
        e.preventDefault();
        var deferred_prod_id=$(this).data("deferredProdId");
        var method='post';
        var url=$(this).data("deferredUrl");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {product_id:deferred_prod_id},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $("#deferred_prod_row_"+deferred_prod_id).hide("slow");
                $("#deferred_prod_row_"+deferred_prod_id).remove();
                in_cart_count();
            }
        })
    });
});
//DEFERRED_end