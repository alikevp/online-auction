$(document).ready(function(){
    $(document).on('change', '#delivery', function (e) {
        e.preventDefault();
        if ($('#delivery').prop('checked')) {
            var html =
                '<input class="form-control" id="dadata_street" type="text" name="street"  placeholder="Улица" :value="address.street">'+
                '<input class="form-control" id="dadata_house" type="text" name="house" placeholder="Дом/строение" :value="address.house">'+
                '<input class="form-control" id="dadata_flat" type="text" placeholder="Квартира/Офис" name="apartment" pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$" :value="address.apartment">'
        }
        else{
            var html ='';
        }
        $('#deliveryBlock').html(html);

        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93 ",
            type = "ADDRESS",
            $city = $("#dadata_city"),
            $street = $("#dadata_street"),
            $house = $("#dadata_house");

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement"
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street
        });
    });
});

$(document).ready(function(){
    $(document).on('click', '.addressDataInput', function (e) {

        var country=$(this).data("addressCountry");
        var city=$(this).data("addressCity");
        var street=$(this).data("addressStreet");
        var house=$(this).data("addressHouse");
        var apartment=$(this).data("addressApartment");

        $('#dadata_city').val(city);
        $('#dadata_city').attr('value', city);
        $('#dadata_street').val(street);
        $('#dadata_street').attr('value', street);
        $('#dadata_house').val(house);
        $('#dadata_house').attr('value', house);
        $('#dadata_flat').val(apartment);
        $('#dadata_flat').attr('value', apartment);
    });
});
