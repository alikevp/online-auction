//payment_type_add
$(document).ready(function(){
    $(document).on('change', '.payment_type_select', function (e) {
        e.preventDefault();
        var type=$(this).val();
        var method='post';
        var url='/shop_payment_type_modal';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: method,
            url: url,
            data: {type:type},//здесь мы передаем стандартным пост методом без сериализации. В конечном скрипте данные будут лежать в $_POST['ajax_data']
            cache: false,
            beforeSend: function() {
                document.getElementById('ajax-loading-gif').style.display = 'block';
            },
            success: function(html){
                document.getElementById('ajax-loading-gif').style.display = 'none';
                $('#payment_modal_content').html(html);
            }
        })
    });
});