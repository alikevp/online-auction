/**
 * Created by Roma on 26.06.2017.
 */
var form = $('form');
form.on('invalid.zf.abide', (e) => {
    $('.submit-button').prop('disabled',true);

});
form.on('valid.zf.abide', (e) => {
    if ($('.is-invalid-input', this.form).length == 0)
        $('.submit-button').prop("disabled",false);
});

$(document).on('closed.zf.reveal', '[data-reveal]', function() {
    $('input').removeClass("is-invalid-input");
    $('.submit-button').prop("disabled",false);
});