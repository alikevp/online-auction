$(document).on('click', '#catMoveCheck', function() {
    var method=$('#catMoveForm').attr("method");
    var action='/category_move_check';
    var formData=$('#catMoveForm').serialize();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: method,
        url: action,
        data: formData,
        success: function(result){
            $('#ajax-result-wrapper').removeClass('show');
            $('#ajax-result-wrapper').addClass('fade');
            $('#ajax-result-wrapper').removeClass('alert-success');

            $('#ajax-result-wrapper').addClass('alert-success');
            $('#ajax-result-wrapper').removeClass('fade');
            $('#ajax-result-wrapper').addClass('show');
            $('#ajax-result').html(result);
        }
    });
});
$(document).on('click', '#catMoveExecute', function() {
    var method=$('#catMoveForm').attr("method");
    var action=$('#catMoveForm').attr("action");
    var formData=$('#catMoveForm').serialize();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: method,
        url: action,
        data: formData,
        success: function(result){
            $('#ajax-result-wrapper').removeClass('show');
            $('#ajax-result-wrapper').addClass('fade');
            $('#ajax-result-wrapper').removeClass('alert-success');

            $('#ajax-result-wrapper').addClass('alert-success');
            $('#ajax-result-wrapper').removeClass('fade');
            $('#ajax-result-wrapper').addClass('show');
            $('#ajax-result').html(result);
        }
    });
});
$(document).on('click', '#validation-error-wrapper-close', function() {
    $('#ajax-result-wrapper').removeClass('show');
    $('#ajax-result-wrapper').addClass('fade');
    $('#ajax-result-wrapper').removeClass('alert-success');
});

