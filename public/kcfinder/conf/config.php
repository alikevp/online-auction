<?php

/** This file is part of KCFinder project
 *
 * @desc Base configuration file
 * @package KCFinder
 * @version 3.12
 * @author Pavel Tzonkov <sunhater@sunhater.com>
 * @copyright 2010-2014 KCFinder Project
 * @license http://opensource.org/licenses/GPL-3.0 GPLv3
 * @license http://opensource.org/licenses/LGPL-3.0 LGPLv3
 * @link http://kcfinder.sunhater.com
 */

/* IMPORTANT!!! Do not comment or remove uncommented settings in this file
   even if you are using session configuration.
   See http://kcfinder.sunhater.com/install for setting descriptions */
/**
 * CHECK LARAVEL USER AUTH
 */
require '../../bootstrap/autoload.php';
$app = require '../../bootstrap/app.php';
$app->make('Illuminate\Contracts\Http\Kernel')->handle(Illuminate\Http\Request::capture());
use App\User;
session_start();
if(!isset($_SESSION['file_access'])){$_SESSION['file_access'] = false;}
if(Auth::check()){
    $_SESSION['file_access'] = false;
    $user_data = User::with('roles')->where('id',Auth::user()->id)->first()->toArray();
    $has_access = false;
    foreach ($user_data['roles'] as $role){
        if($role['name'] == 'manage_files'){
            $has_access = true;

            $_SESSION['file_access'] = true;
            break;
        }
    }
}

if($_SESSION['file_access'] == true){
    $_SESSION['KCFINDER'] = array();
    $_SESSION['KCFINDER']['disabled'] = false;
}
else{
    unset($_SESSION['KCFINDER']);
}

$_CONFIG = array(


// GENERAL SETTINGS

    'disabled' => true,
    'uploadURL' => "/uploads",
    'uploadDir' => "",
    'theme' => "default",

    'types' => array(

        // (F)CKEditor types
        'files' => "",
        'flash' => "swf",
        'images' => "*img",

        // TinyMCE types
        'file' => "",
        'media' => "swf flv avi mpg mpeg qt mov wmv asf rm",
        'image' => "*img",
    ),


// IMAGE SETTINGS

    'imageDriversPriority' => "imagick gmagick gd",
    'jpegQuality' => 90,
    'thumbsDir' => ".thumbs",

    'maxImageWidth' => 0,
    'maxImageHeight' => 0,

    'thumbWidth' => 100,
    'thumbHeight' => 100,

    'watermark' => "",


// DISABLE / ENABLE SETTINGS

    'denyZipDownload' => false,
    'denyUpdateCheck' => false,
    'denyExtensionRename' => false,


// PERMISSION SETTINGS

    'dirPerms' => 0755,
    'filePerms' => 0644,

    'access' => array(

        'files' => array(
            'upload' => true,
            'delete' => true,
            'copy' => true,
            'move' => true,
            'rename' => true
        ),

        'dirs' => array(
            'create' => true,
            'delete' => true,
            'rename' => true
        )
    ),

    'deniedExts' => "exe com msi bat cgi pl php phps phtml php3 php4 php5 php6 py pyc pyo pcgi pcgi3 pcgi4 pcgi5 pchi6",


// MISC SETTINGS

    'filenameChangeChars' => array(/*
        ' ' => "_",
        ':' => "."
    */),

    'dirnameChangeChars' => array(/*
        ' ' => "_",
        ':' => "."
    */),

    'mime_magic' => "",

    'cookieDomain' => "",
    'cookiePath' => "",
    'cookiePrefix' => 'KCFINDER_',


// THE FOLLOWING SETTINGS CANNOT BE OVERRIDED WITH SESSION SETTINGS

    '_normalizeFilenames' => false,
    '_check4htaccess' => true,
    //'_tinyMCEPath' => "/tiny_mce",

    '_sessionVar' => "KCFINDER",
    //'_sessionLifetime' => 30,
    '_sessionDir' => "/var/app/ondeck/storage/framework/sessions",
    //'_sessionDomain' => ".mysite.com",
    //'_sessionPath' => "/my/path",

    //'_cssMinCmd' => "java -jar /path/to/yuicompressor.jar --type css {file}",
    //'_jsMinCmd' => "java -jar /path/to/yuicompressor.jar --type js {file}",

);

?>