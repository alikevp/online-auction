<?php
//use Carbon\Carbon;
//$year = Carbon::now()->year;
//$month = Carbon::now()->month;
//$day = Carbon::now()->day;
return [
    /*
    |--------------------------------------------------------------------------
    | Admin Title
    |--------------------------------------------------------------------------
    |
    | Displayed in title and header.
    |
    */

    'title' => 'Jinnmart.ru administrator',

    /*
    |--------------------------------------------------------------------------
    | Admin Logo
    |--------------------------------------------------------------------------
    |
    | Displayed in navigation panel.
    |
    */

    'logo' => 'Jinnmart.ru',
    'logo_mini' => 'J',

    /*
    |--------------------------------------------------------------------------
    | Admin URL prefix
    |--------------------------------------------------------------------------
    */

    'url_prefix' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Middleware to use in admin routes
    |--------------------------------------------------------------------------
    |
    | In order to create authentication views and routes
    | don't forget to execute `php artisan make:auth`.
    | See https://laravel.com/docs/5.2/authentication#authentication-quickstart
    |
    | Note: Please remove 'web' middleware for Laravel 5.1 or older
    |
    */

    'middleware' => ['web', 'admin'],

    /*
    |--------------------------------------------------------------------------
    | Authentication default provider
    |--------------------------------------------------------------------------
    |
    | @see config/auth.php : providers
    |
    */

    'auth_provider' => 'users',

    /*
    |--------------------------------------------------------------------------
    |  Path to admin bootstrap files directory
    |--------------------------------------------------------------------------
    |
    | Default: app_path('Admin')
    |
    */

    'bootstrapDirectory' => app_path('Admin'),

    /*
    |--------------------------------------------------------------------------
    |  Directory for uploaded images (relative to `public` directory)
    |--------------------------------------------------------------------------
    */

    'imagesUploadDirectory' => 'images/uploads',

    /*
    |--------------------------------------------------------------------------
    |  Directory for uploaded files (relative to `public` directory)
    |--------------------------------------------------------------------------
    */

    'filesUploadDirectory' => 'files/uploads',

    /*
    |--------------------------------------------------------------------------
    |  Admin panel template
    |--------------------------------------------------------------------------
    */

    'template' => SleepingOwl\Admin\Templates\TemplateDefault::class,

    /*
    |--------------------------------------------------------------------------
    |  Default date and time formats
    |--------------------------------------------------------------------------
    */

    'datetimeFormat' => 'd.m.Y H:i',
    'dateFormat' => 'd.m.Y',
    'timeFormat' => 'H:i',

    /*
    |--------------------------------------------------------------------------
    | Editors
    |--------------------------------------------------------------------------
    |
    | Select default editor and tweak options if needed.
    |
    */

    'wysiwyg' => [
        'default' => 'ckeditor',

        /*
         * See http://docs.ckeditor.com/#!/api/CKEDITOR.config
         */
        'ckeditor' => [
            'height' => 400,

//            'toolbarGroups'        => [

//                [
//                    "name" => "clipboard",
//                    "items" => [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ]
//                ],
//                [
//                    "name" => "links",
//                    "items" => [ 'Link', 'Unlink', 'Anchor', 'lightbox']
//                ],
//                [
//                    "name" => "insert",
//                    "items" => [ 'Image','Table', 'HorizontalRule', 'SpecialChar' ]
//                ],
//                [
//                    "name" => "editing",
//                    "items" => [ 'Scayt' ]
//                ],
//                [
//                    "name" => "tools",
//                    "items" => [ 'Maximize' ]
//                ],
//                [
//                    "name" => "document",
//                    "items" => [ 'Source', 'Save', '-', 'NewPage', 'Preview', '-', 'Templates' ]
//                ],
//                [
//                    "name" => "basicstyles",
//                    "items" => [ "Bold", "Italic", "Underline", "Strike", "-", "RemoveFormat" ]
//                ],
//                [
//                    "name" => "paragraph",
//                    "items" => [ 'NumberedList', 'BulettedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ]
//                ],
//                [
//                    "name" => "styles",
//                    "items" => [ 'Styles', 'Format' ]
//                ],
//                [
//                    "name" => "about",
//                    "items" => [ 'About' ]
//                ]

//             ],
//
            'extraPlugins'         => 'dropzoneupload,image2,imgbrowse,imageresize,fixed,autosave,textselection,widget,lineutils,blueImpGallery',
            'uploadUrl'            => '/fileupload/upload', //path to custom controller
            'imageResizeMaxWidth'         => '1920',
            'imageResizeMaxHeight'         => '1920',
//            'filebrowserBrowseUrl' => '/ckfinder/ckfinder.html',
//            'filebrowserImageBrowseUrl' => '/ckfinder/ckfinder.html?type=Images',
//            'filebrowserFlashBrowseUrl' => '/ckfinder/ckfinder.html',
//            'filebrowserUploadUrl' => '/ckfinder/ckfinder.html',
//            'filebrowserImageUploadUrl' => '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//            'filebrowserFlashUploadUrl' => '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            'filebrowserBrowseUrl' => '/kcfinder/browse.php?opener=ckeditor&type=files',
            'filebrowserImageBrowseUrl' => '/kcfinder/browse.php?type=images',
            'filebrowserFlashBrowseUrl' => '/kcfinder/browse.php?opener=ckeditor&type=flas',
            'filebrowserUploadUrl' => '/kcfinder/upload.php?opener=ckeditor&type=files',
            'filebrowserImageUploadUrl' => '/kcfinder/upload.php?opener=ckeditor&type=images',
            'filebrowserFlashUploadUrl' => '/kcfinder/upload.php?opener=ckeditor&type=flash',
            'contentsCss' => 'https://blueimp.github.io/Gallery/css/blueimp-gallery.css',
            'allowedContent' => true,
            'blueImpGallery_fileManager' => 'kcfinder',
            'blueImpGallery_fileManager_directory' => '/uploads/images',
            'blueImpGallery_thumbnail_directory' => '/uploads/images/',
            'blueImpGallery_bootstrap' => false,
//            'blueImpGallery_ckfinder_version' => '3',
        ],

        /*
         * See https://www.tinymce.com/docs/
         */
        'tinymce' => [
            'height' => 200,
        ],

        /*
         * See https://github.com/NextStepWebs/simplemde-markdown-editor
         */
        'simplemde' => [
            'hideIcons' => ['side-by-side', 'fullscreen'],
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | DataTables
    |--------------------------------------------------------------------------
    |
    | Select default settings for datatable
    |
    */
    'datatables' => [],

    /*
    |--------------------------------------------------------------------------
    | Breadcrumbs
    |--------------------------------------------------------------------------
    |
    */
    'breadcrumbs' => true,

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started.
    |
    */

    'aliases' => [
        // Components
        'Assets' => KodiCMS\Assets\Facades\Assets::class,
        'PackageManager' => KodiCMS\Assets\Facades\PackageManager::class,
        'Meta' => KodiCMS\Assets\Facades\Meta::class, // todo избавиться
        'Form' => Collective\Html\FormFacade::class,
        'HTML' => Collective\Html\HtmlFacade::class,
        'WysiwygManager' => SleepingOwl\Admin\Facades\WysiwygManager::class,
        'MessagesStack' => SleepingOwl\Admin\Facades\MessageStack::class,

        // Presenters
        'AdminSection' => SleepingOwl\Admin\Facades\Admin::class,
        'AdminTemplate' => SleepingOwl\Admin\Facades\Template::class,
        'AdminNavigation' => SleepingOwl\Admin\Facades\Navigation::class,
        'AdminColumn' => SleepingOwl\Admin\Facades\TableColumn::class,
        'AdminColumnEditable' => SleepingOwl\Admin\Facades\TableColumnEditable::class,
        'AdminColumnFilter' => SleepingOwl\Admin\Facades\TableColumnFilter::class,
        'AdminDisplayFilter' => SleepingOwl\Admin\Facades\DisplayFilter::class,
        'AdminForm' => SleepingOwl\Admin\Facades\Form::class,
        'AdminFormElement' => SleepingOwl\Admin\Facades\FormElement::class,
        'AdminDisplay' => SleepingOwl\Admin\Facades\Display::class,
        'AdminWidgets' => SleepingOwl\Admin\Facades\Widgets::class,
    ],
];
