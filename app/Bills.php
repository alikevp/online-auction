<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bills
 *
 * @property int $id
 * @property string $number
 * @property string $id_shops
 * @property string $contract_award
 * @property string $summ_bill
 * @property string $date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereContractAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereIdShops($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereSummBill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bills whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bills extends Model
{
    //
}
