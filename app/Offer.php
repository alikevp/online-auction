<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Offer
 *
 * @package App
 * @mixin \Eloquent
 * @property string $id
 * @property string $shop_id
 * @property string|null $shop_product_id
 * @property string $category_id
 * @property string $producer
 * @property string|null $product_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $sales_note
 * @property string|null $barcode
 * @property string|null $adult
 * @property string|null $url
 * @property float|null $price
 * @property string|null $oldprice
 * @property string|null $currency_id
 * @property string|null $delivery
 * @property string|null $country_of_origin
 * @property string|null $manufacturer_warranty
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attribute[] $attributes
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read mixed $comments_rating_sum
 * @property-read mixed $count_comments
 * @property-read mixed $files
 * @property-read mixed $limit_name
 * @property-read mixed $rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read \App\Producer $producers
 * @method static \Illuminate\Database\Eloquent\Builder| Offer categorized(Category $category = null)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer getRandom()
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereAdult($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereCountryOfOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereManufacturerWarranty($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereOldprice($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereProducer($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereSalesNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereShopProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder| Offer withPrice()
 */
class Offer extends Model
{
    use Searchable;
    use LogsActivity;
    use Sluggable;

    protected static $recordEvents = ['created', 'updated', 'deleted'];//loged actions
    protected static $logOnlyDirty = true;//log only changed
    protected static $logAttributes = [
        'id',
        'shop_id',
        'shop_product_id',
        'category_id',
        'producer',
        'product_id',
        'name',
        'slug',
        'description',
        'sales_note',
        'barcode',
        'adult',
        'url',
        'price',
        'oldprice',
        'currency_id',
        'delivery',
        'country_of_origin',
        'manufacturer_warranty',
    ];//what to log

    protected $table = 'offers';

    public $timestamps = false;

    public $incrementing = false;

    protected $hidden = [
        'searchable',
    ];

    public function searchableOptions()
    {
        return [
            'column' => 'searchable',
            'maintain_index' => true,
            'rank' => [
                'fields' => [
                    'name' => 'A',
                ],
                'normalization' => 32,
            ],
        ];
    }

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
        ];
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'unique' => true,
            ]
        ];
    }

    /**
     * Has many attributes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany('App\Attribute', 'offer_id');
    }

    public function attribute_value()
    {
        return $this->hasMany(Attribute_value::class);
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'offer_id')->latest('created_at');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * Belongs to category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param $query
     * @param Category|null $category
     * @return mixed
     */
    public function scopeCategorized($query, Category $category = null)
    {
        if (is_null($category)) {
            return $query->with('categories');
        }

        $categoryIds = $category->getDescendantsAndSelf()->pluck('id');

        return $query->with('categories')
            ->join('category_offer', 'category_offer.offer_id', '=', 'offers.id')
            ->whereIn('category_offer.category_id', $categoryIds);
    }

    /**
     * Get all photos of product
     *
     * @return mixed
     */
    public function getFilesAttribute()
    {
        $shop = $this->attributes['shop_id'];
        $id = $this->attributes['id'];
        if (Storage::disk('local')->exists('/shops/offers/'.$shop.'/'.$id.'')) {
            $photos = File::files('images/catalog/shops/offers/'.$shop.'/'.$id);

            if (!$photos) {
                $photos = File::files('images/default');
            }
        } else {
            $photos = File::files('images/default');
        }
        return $photos;
    }

    /**
     * Get producer logo
     *
     * @return mixed
     */
//    public function getLogoAttribute()
//    {
//        $producer = $this->attributes['producer'];
//        $logo     = File::url('producer/' . $producer . '.gif');
//
//        return URL::to($logo);
//    }

    public function producers()
    {
        return $this->hasOne(Producer::class, 'slug', 'producer');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getLimitNameAttribute()
    {
        $limit_name = str_limit($this->name, 20);

        return $limit_name;
    }

    public function getCommentsRatingSumAttribute()
    {
        return $this->comments->sum('rating');
    }

    public function getCountCommentsAttribute()
    {
        return $this->comments->count;
    }

//    public function getRatingAttribute()
//    {
//        $comments = $this->comments;
//        $rating = $comments->sum('rating');
//
//        if (count($comments) != 0) {
//            $rating = (int)$rating / (int)count($comments);
//        }
//
//        return $rating;
//    }

    public function scopeGetRandom($query)
    {
        return $query->inRandomOrder()->withPrice()->take(12);
    }

    /** Возвращает товар, только с ценой
     * @param $query
     * @return mixed
     */
    public function scopeWithPrice($query)
    {
        return $query->where('price', '!=', 0)
            ->orWhere('price', '!=', null);
    }
}
