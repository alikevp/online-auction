<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OfferChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public  $user, $order, $offer;

    /**
     * Create a new event instance.
     *
     * @param $user
     * @param $order
     */
    public function __construct($user,$order, $offer)
    {
        $this->user = $user;
        $this->order = $order;
        $this->offer = $offer;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
