<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentType
 *
 * @property int $id
 * @property string $shop_id
 * @property int $type
 * @property string|null $bank_data_id
 * @property string|null $name
 * @property string|null $account
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereBankDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentType extends Model
{
    //
}
