<?php
/**
 * Copyright (c) 2017. Created by Vilkov Sergey
 */

/**
 * Created by PhpStorm.
 * User: alfred
 * Date: 10.06.17
 * Time: 8:00
 */

namespace App;

trait HasRoles
{
    /**
     * Сохранение роли для выбранного пользователя
     *
     * @param $role
     * @return mixed
     */
    public function assign($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    /**
     * Возвращает true если пользователь имеет роль
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  Permission $permission
     * @return boolean
     */
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }
}
