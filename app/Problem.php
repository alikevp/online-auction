<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Problem
 *
 * @property int $id
 * @property string $theme
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $status_id
 * @property int|null $user_id
 * @property string $user_name
 * @property string $email
 * @property string|null $tel
 * @property int|null $term_id
 * @property int|null $operator_id
 * @property string|null $accepted_at
 * @property string|null $closed_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Issue[] $issues
 * @property-read \App\User|null $operator
 * @property-read \App\Status $status
 * @property-read \App\Term|null $term
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereAcceptedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereOperatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereTermId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereUserName($value)
 * @mixin \Eloquent
 */
class Problem extends Model
{
    use LogsActivity;
    protected static $recordEvents = ['created'];//loged actions
    protected static $logOnlyDirty = true;//log only changed
    protected static $logAttributes = [
        'status_id',
    ];//what to log

    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function term()
    {
        return $this->belongsTo('App\Term', 'term_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function operator()
    {
        return $this->belongsTo('App\User', 'operator_id');
    }
}
