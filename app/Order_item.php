<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_item
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property float|null $price
 * @property int|null $quantity
 * @property string|null $category_id
 * @property-read \App\Order $order
 * @property-read \App\Order_offer_group $order_offer_group
 * @property-read \App\Offer $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $refusedUsers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item refused($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_item whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order_item extends Model
{
    public $incrementing = false;


    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order_offer_group()
    {
        return $this->belongsTo('App\Order_offer_group', 'order_id', 'order_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Offer', 'product_id', 'id');
    }

    public function refusedUsers()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeRefused($query, $id)
    {
        return $query->whereDoesntHave('refusedUsers', function ($query) use ($id) {
            $query->whereId($id);
        })->get();
    }
}
