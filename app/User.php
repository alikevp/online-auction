<?php

namespace App;

use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\CausesActivity;

/**
 * Class User
 *
 * @package App
 * @mixin /Eloquent
 * @property integer referred_by
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property bool $verified
 * @property string|null $verification_token
 * @property string|null $surname
 * @property string|null $type
 * @property string|null $connection_id
 * @property string|null $patronymic
 * @property string|null $telefone
 * @property string|null $date_of_birth
 * @property string|null $sex
 * @property string|null $inn
 * @property bool|null $sync_need
 * @property bool $bought
 * @property string|null $sync_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Address[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Finance[] $finances
 * @property-read mixed $delete_url
 * @property-read mixed $full_name
 * @property-read \App\User|null $invited
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mailsubscription[] $mailable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $referrals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $refusedOrders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read \App\Shop $shop
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $shops
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User admins()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBought($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereConnectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereReferredBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSyncDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSyncNeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelefone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerificationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerified($value)
 */
class User extends Authenticatable
{
    use Notifiable, HasRoles, CausesActivity;

    /**
     * URL для vue
     * @var array
     */
    protected $appends = [
        'deleteUrl',
    ];

    protected static $recordEvents = ['created', 'updated', 'deleted'];//loged actions
    protected static $logOnlyDirty = true;//log only changed
    protected static $logAttributes = [
        'name',
        'surname',
        'patronymic',
        'email',
        'e_mail',
        'password',
        'telefone',
        'telefone',
        'verification_token',
        'type',
        'date_of_birth',
        'sex',
        'sync_need',
        'connection_id',
        'referred_by',
        'url_avatar',
        'bought',
    ];//what to log

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'email',
        'e_mail',
        'password',
        'telefone',
        'telefone',
        'verification_token',
        'type',
        'date_of_birth',
        'sex',
        'sync_need',
        'connection_id',
        'referred_by',
        'url_avatar',
        'bought',
    ];

    public function shops()
    {
        return $this->hasMany('App\User', 'curator_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Change user email confirmed
     */

    public function getDeleteUrlAttribute()
    {
        return url('api/delete', $this->id);
    }

    /**
     * Роли
     *
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class);
    }

    /**
     * Привязываем отказавшегося пользователя к товару
     * @return mixed
     */
    public function refusedOrders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * Привязываем отказавшегося пользователя к товару
     * @return mixed
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * Рефералы
     *
     * @return mixed
     */
    public function referrals()
    {
        return $this->hasMany(self::class, 'referred_by');
    }

    public function invited()
    {
        return $this->belongsTo(self::class, 'referred_by');
    }

    public function mailable()
    {
        return $this->belongsToMany(Mailsubscription::class)->withPivot('hash');
    }

    /**
     * Привязанный магазин
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class, 'connection_id');
    }

    /**
     * История приходов, расходов
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function finances()
    {
        return $this->hasMany(Finance::class);
    }

    /**
     * Уж не админ ли это?
     *
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('super_admin');
    }

    /**
     * Уж не админ ли это?
     *
     * @return bool
     */
    public function isSuperBooker()
    {

        $this->hasRole('super_booker');
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['name'].' '.
            $this->attributes['patronymic'].' '.
            $this->attributes['surname'];
    }

    /**
     * @param $query Builder
     * @return mixed
     */
    public function scopeAdmins($query)
    {
        return $query->whereHas(
            'roles',
            function ($role) {
                $role->where('name', 'admin');
            }
        );
    }

    public function scopeShopAdmins($query)
    {
        return $query->whereHas(
            'shop',
            function ($shop) {
                $shop->where('id', auth()->user()->shop->id);
            }
        )->whereHas(
            'roles',
            function ($role) {
                $role->whereIn(
                    'name',
                    [
                        'shop_admin',
                        'shop_manager',
                    ]
                );
            }
        );
    }

    public function scopeBooker($query)
    {
        return $query->whereHas(
            'roles',
            function ($role) {
                $role->whereIn('name', ['super_booker', 'booker']);
            }
        );
    }

    public function scopeSuperManager($query)
    {
        return $query->whereHas(
            'roles',
            function ($role) {
                $role->whereIn(
                    'name',
                    [
                        'super_manager',
                        'manager',
                        'shop_admin',
                        'shop_manager',
                        'buyer',
                    ]
                );
            }
        );
    }

    public function scopeManager($query)
    {
        return $query->whereHas(
            'roles',
            function ($role) {
                $role->whereIn(
                    'name',
                    [
                        'shop_admin',
                        'shop_manager',
                        'buyer',
                    ]
                );
            }
        );
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
