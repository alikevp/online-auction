<?php

namespace App;

use Baum\Node;
use Spatie\Activitylog\Traits\LogsActivity;

//use SebastianBergmann\CodeCoverage\Report\Xml\Node;

/**
 * App\Category
 *
 * @property-read \App\Category $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $child
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Offer[] $products
 * @mixin \Eloquent
 * @property string $id
 * @property string $parent_id
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $slug
 * @property bool $in_menu
 * @property int $checked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attribute[] $attributes
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Category[] $children
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Menu[] $menus
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Shop[] $shops
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutSelf()
 * @property int $offers_count
 * @property int $leafs_offers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereLeafsOffersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereOffersCount($value)
 */
class Category extends Node
{
    protected $fillable = [
        'parent_id',
        'name',
        'text',
    ];

    use LogsActivity;
    protected static $recordEvents = ['created', 'updated', 'deleted'];//loged actions
    protected static $logAttributes = [
        'id',
        'parent_id',
        'lft',
        'rgt',
        'depth',
        'name',
        'slug',
    ];//what to log


    public $incrementing = false;

    public function products()
    {
        return $this->hasMany('App\Offer');
    }

    /**
     * Подписка магазина на категорию
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

    /**
     * Заказы по категориям
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

//    public function attributesCountRelation()
//    {
//        return $this->hasMany(Attribute::class)->groupBy('slug')->count();
//        // replace module_id with appropriate foreign key if needed
//    }
}
