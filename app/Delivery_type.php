<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Delivery_type
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_delivery[] $orders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Delivery_type whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Delivery_type whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Delivery_type whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Delivery_type whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Delivery_type whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Delivery_type extends Model
{
    public function orders()
    {
        return $this->belongsToMany(Order_delivery::class);
    }
}
