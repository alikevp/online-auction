<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Post
 *
 * @package App
 * @mixin /Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $content
 * @property bool|null $published
 * @property bool|null $comment_on
 * @property string|null $url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $slug
 * @property-read \App\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Term[] $terms
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post byCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post byTags()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereCommentOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereUserId($value)
 */
class Post extends Node
{
    use LogsActivity;

    protected static $recordEvents = ['created', 'updated','deleted'];//loged actions
    protected static $logOnlyDirty = true;//log only changed
    protected static $logAttributes = [
        'title',
        'url'
    ];//what to log

//    protected $table = 'posts';

    public function terms()
    {
        return $this->belongsToMany(Term::class,
            'post_term',
            'post_id',
            'term_id'
        );
    }

    //нужно для прилетания катов и меток с постом
    public function categories()
    {
        return $this->belongsToMany(Term::class,
            'post_term',
            'post_id',
            'term_id'
        );
    }

    public function tags()
    {
        return $this->belongsToMany(Term::class,
            'post_term',
            'post_id',
            'term_id');
    }

    //нужно для прилетания катов и меток с постом

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeByTags($query)
    {
        return $query->whereHas('terms', function ($term) {
            $term->tags();
        });
    }

    public function scopeByCategories($query)
    {
        return $query->whereHas('terms', function ($term) {
            $term->categories();
        });
    }

    /**
     * @return User
     */


    /**
     * Делаем слаг из имени
     * @param $label
     */
    public function setSlugAttribute($label)
    {
        function uniqueSlug($slug, $posts) {
            if($posts->contains('slug', $slug))
            {

                $slug = $posts->where('slug', $slug)->first()->slug;
                $strings = explode('-', $slug);

                if(is_numeric(array_last($strings)))
                {
                    $strings[count($strings) - 1] = (int)(array_last($strings)) + 1;
                    $slug = collect($strings)->implode('-');
                }else{
                    $slug = $slug.'-1';
                }

                $slug = uniqueSlug($slug, $posts);
            }

            return $slug;
        }

        if ($label == '') {
            $slug = str_slug($this->title);
        } else {
            $slug = str_slug($label);
        }

        if(isset($this->attributes['id']))
        {
            $posts= Post::where('id', '!=', $this->attributes['id'])->select('slug')->get();
        }else {
            $posts= Post::select('slug')->get();
        }

        $posts->unique();

        $slug = uniqueSlug($slug, $posts);

        $this->attributes['slug'] = $slug;
    }

    public function scopePages($query)
    {
        return $query->where('post_type', 'page');
    }

    public function scopePosts($query)
    {
        return $query->where('post_type', 'post');
    }
}
