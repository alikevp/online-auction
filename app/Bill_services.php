<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bill_services
 *
 * @property int $id
 * @property string $bill_id
 * @property string $name
 * @property string $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereBillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill_services whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bill_services extends Model
{
    //
}
