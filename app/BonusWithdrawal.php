<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\BonusWithdrawal
 *
 * @property int $id
 * @property string $person
 * @property int $person_id
 * @property string $bank
 * @property string $card_number
 * @property string $account_number
 * @property float $amount
 * @property int $status_id
 * @property int|null $operator_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $closed_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\User|null $operator
 * @property-read \App\Status $status
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereOperatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal wherePerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BonusWithdrawal whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BonusWithdrawal extends Model
{
    use LogsActivity;

    protected static $recordEvents = ['updated'];//loged actions
    protected static $logOnlyDirty = true;//log only changed
    protected static $logAttributes = [
        'status_id',
    ];//what to log

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function operator()
    {
        return $this->belongsTo('App\User', 'operator_id');
    }
}
