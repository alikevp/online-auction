<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Product
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $category_id
 * @property string $producer_id
 * @property string $name
 * @property string $adult
 * @method static \Illuminate\Database\Query\Builder|\App\Product whereAdult($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Product whereProducerId($value)
 */
class Product extends Model
{
    //
}
