<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property-read \App\Offer $offer
 * @mixin \Eloquent
 * @property int $id
 * @property string $user_id
 * @property string $user_name
 * @property string $user_mail
 * @property string $offer_id
 * @property string $advantages
 * @property string $disadvantages
 * @property string $text
 * @property string $ip
 * @property int $rating
 * @property bool $checked
 * @property bool $closed
 * @property bool $spam
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereAdvantages($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereChecked($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereClosed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereDisadvantages($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereOfferId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereRating($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereSpam($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUserMail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUserName($value)
 * @property string|null $shop_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereShopId($value)
 * @property string|null $tip
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereTip($value)
 */
class Comment extends Model
{
    public function offer()
    {
        return $this->belongsTo('App\Offer', 'offer_id');
    }
}
