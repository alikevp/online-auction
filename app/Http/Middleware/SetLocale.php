<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use ClassPreloader\Config;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

//use Symfony\Component\HttpFoundation\Session\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('locale')) {
            $locale = Session::get('locale', config('app.locale'));
        } else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            if ($locale != 'ru' && $locale != 'en') {
                $locale = 'ru';
            }
        }
        app()->setLocale($locale);
        setlocale(LC_ALL, $locale . '_RU.utf8');
        Carbon::setLocale($locale);

        return $next($request);
    }
}
