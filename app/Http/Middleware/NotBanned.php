<?php

namespace App\Http\Middleware;

use Closure;

class NotBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guest()) {
            return redirect('login');
        }
        if (auth()->user()->auctions_overdue <= 5) {
            return $next($request);
        } else {
            return redirect('/banned-info');
        }

        return $next($request);
    }
}
