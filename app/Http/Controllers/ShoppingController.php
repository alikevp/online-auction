<?php

namespace App\Http\Controllers;

use App\Category;
use App\Menu;
use App\Offer;
use App\Repositories\Attributes;
use App\Repositories\OffersRepository;
use App\Repositories\Producers;
use App\Shop;
use Auth;
use Illuminate\Http\Request;
use SEO;
use SEOMeta;
use OpenGraph;

class ShoppingController extends Controller
{
    protected $products;
    protected $attributes;
    protected $producers;

    public function __construct(OffersRepository $products, Attributes $attributes, Producers $producers)
    {
        $this->products = $products;
        $this->attributes = $attributes;
        $this->producers = $producers;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        SEOMeta::setTitleDefault('Джинмарт-Электронная торговая площадка потребителей');
        SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::setCanonical('https://jinnmart.ru/');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::setUrl('https://jinnmart.ru/');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));

        if (Auth::check()) {
            if (Auth::user()->type == 'shop_admin') {
                return redirect('/my-auctions');
            }
        }

//        $i = 0;
//        $categories = collect();
//        while ($i < 2) {
//
//            $category = Category::allLeaves()->get()->random(1);
//            if ($category->first()->products->count() >= 1) {
//
//                $categories->push($category->first());
//                $i++;
//
//            }
//        }

//        $products = Offer::inRandomOrder()->whereNotNull('price')->take(32)->get();
        $products = Offer::GetRandom()->get();

//        $user = Shop::count() + 14716;

//        session()->put('count_shop', $user);

//        $products = Offer::whereIn('category_id', $categories->pluck('id'))->get();

        return view('shopping.index')
            ->with(compact('products', 'categories', 'rating'));
    }


    /**
     * Display products by category
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function byCategories(Request $request,$id)
    {
        if (is_numeric($id)) {
            $category = Menu::with('parent', 'children')->find($id);
            $categories = $category->categories;
        } else {
            $category = Category::with('parent', 'children')->find($id);
            $categories = $category->getDescendants();
        }

        $categories = $categories->toHierarchy();


        $product_count = Offer::where('category_id',$id)->count();
        if($product_count>0) {
            $categories_for_products = Category::where('id', $id)->first()->descendantsAndSelf()->pluck('id')->toArray();
            $products = Offer::whereIn('category_id', $categories_for_products)
                ->when(
                    $request->has('min'),
                    function ($query) use ($request) {
                        return $query->where('price', '>=', $request->input('min'));
                    }
                )
                ->when(
                    $request->has('max'),
                    function ($query) use ($request) {
                        return $query->where('price', '<=', $request->input('max'));
                    }
                )
                ->when(
                    $request->has('producer'),
                    function ($query) use ($request) {
                        return $query->whereIn('producer', $request->input('producer'));
                    }
                )
                ->when(
                    $request->has('order'),
                    function ($query) use ($request) {
                        list($field, $value) = explode('_', $request->input('order'));

                        return $query->orderBy($field, $value);
                    }
                );
            if (!isset($request->no_price)) {
                if ($request->no_price == false) {
                        $products = $products->where('price', '!=', 0);
                }
            }
            if ($request->has('attributes')) {
                foreach ($request->input('attributes') as $name => $values) {
                    $products = $products->whereHas(
                        'attributes',
                        function ($att) use ($name, $values) {
                            $att->where('slug', 'ilike', "%$name%")
                                ->where(
                                    function ($query) use ($values) {
                                        foreach ($values as $value) {
                                            $value = trim($value);
                                            $query->orWhere('value', 'ilike', "%$value%");
                                        }
                                    }
                                );
                        }
                    );
                }
            }
            $attributes = $this->attributes->getAttributes($products->get());
            $producers = $this->producers->getProducers($products->get());



            if ($request->has('min')) {
                $min_price = $request->input('min');
            }
            else{
                $min_price = $products->min('price');
            }
            if ($request->has('max')) {
                $max_price = $request->input('max');
            }
            else{
                $max_price = $products->max('price');
            }
            session()->put('min_price', $min_price);
            session()->put('max_price', $max_price);
            session()->put('products', $products->get());
            session()->put('category', $category);
            session()->put('categories', $categories);
            session()->put('producers', $producers);
            session()->put('attributes', $attributes);

            $products = $products
                ->orderBy('price', 'asc')
                ->paginate(24);
            $get_params = http_build_query($request->request->all());
            $products->withPath('/category/'.$id.'?'.$get_params.'');

            $selected['attributes'] = $request->input('attributes');
            $selected['category'] = $request->category;
            $selected['maxPrice'] = $request->maxPrice;
            $selected['minPrice'] = $request->minPrice;
            $selected['producer'] = $request->producer;
        }
        SEOMeta::setTitleDefault($category->name.'-Джинмарт-Электронная торговая площадка потребителей');
        SEOMeta::setDescription($category->name.' Лучшая цена у нас. Электронная торговая площадка потребителей.');
        SEOMeta::setCanonical('https://jinnmart.ru/category/'.$id);
        SEOMeta::addKeyword(
            [
                $category->name,
                'купить '.$category->name,
                $category->name.' дешево',
                $category->name.' цены',
                $category->name.' скидки',
            ]
        );

        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle($category->name.'-Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription($category->name.' Лучшая цена у нас. Электронная торговая площадка потребителей.');
        OpenGraph::setUrl('https://jinnmart.ru/category/'.$id);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        if (isset($products[0]->files[0])) {
            OpenGraph::addImage(url($products[0]->files[0]));
        } else {
            OpenGraph::addImage(asset('images/logo.png'));
        }

        return view('shopping.category')
            ->with(compact('products', 'max_price', 'min_price', 'category', 'categories', 'attributes', 'producers'));
    }


    public function byProducer($producer)
    {
        $products = $this->products->inProducer($producer);

        $attributes = $this->attributes->getAttributes($products->get());
        $producers = $this->producers->getProducers($products->get());

        $categories = collect();

        foreach ($products->get() as $product) {
            $category = $product->category;
            if (!$categories->contains($product->category->name)) {
                $categories->push($category);
            }
        }

        SEOMeta::setTitleDefault($producer.'-Джинмарт-Электронная торговая площадка потребителей');
        SEOMeta::setDescription(
            'Скидки на '.$producer.'. Лучшая цена у нас. Электронная торговая площадка потребителей.'
        );
        SEOMeta::setCanonical('https://jinnmart.ru/brand/'.$producer);
        SEOMeta::addKeyword(
            [
                $producer,
                'товары '.$producer,
                'купить '.$producer,
                $producer.' отзывы',
                $producer.' цены',
                $producer.' инструкция',
            ]
        );

        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle($producer.'-Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription(
            'Скидки на '.$producer.'. Лучшая цена у нас. Электронная торговая площадка потребителей.'
        );
        OpenGraph::setUrl('https://jinnmart.ru/brand/'.$producer);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage('/images/catalog/producer/'.$producer.'.gif');

        $categories = $categories->unique();
        $products = $products
            ->orderBy('price', 'asc')
            ->paginate(18);


        return view('shopping.producer')
            ->with(compact('products', 'producer', 'categories', 'producer', 'attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string $id
     *
     * @param null $menu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, $menu = null)
    {
        $product = Offer::findOrFail($id);
        $comments = $product->comments;
        $photos = $product->files;
        $category = Category::findOrFail($product->category_id);
		
        SEOMeta::setTitleDefault($product->name.' - Джинмарт-Электронная торговая площадка потребителей');
        SEOMeta::setDescription($product->description);
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle($product->name.' - Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription($product->description);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
		
        $similarProducts = Offer::where(
            [
                ['category_id', '=', $product->category_id],
                ['id', '!=', $product->id],
            ]
        )->limit(18)->get();

        $rating = 0;

        foreach ($comments as $comment) {
            $rating += $comment->rating;
        }

        if (count($comments) != 0) {
            $rating = (int)$rating / (int)count($comments);
        }

        $menuItem = $menu;

        return view('shopping.show')
            ->with(compact('id', 'product', 'category', 'photos', 'similarProducts', 'menuItem', 'comments', 'rating'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        SEOMeta::setTitleDefault('Поиск - Джинмарт-Электронная торговая площадка потребителей');
        SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::setCanonical('https://jinnmart.ru/');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Поиск - Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::setUrl('https://jinnmart.ru/');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
		
        $categories = collect();

        $query = $request->search;

        $products = Offer::search($query);

        $attributes = $this->attributes->getAttributes($products->get());
        $producers = $this->producers->getProducers($products->get());

        foreach ($products->get() as $product) {
            $category = $product->category;
            if (!$categories->contains($product->category->name)) {
                $categories->push($category);
            }
        }
        $categories = $categories->unique();

        session()->put('products', $products->get());
        session()->put('categories', $categories);
        session()->put('producers', $producers);
        session()->put('attributes', $attributes);

        $products = $products
            ->paginate(20);

        return view('shopping.search')
            ->with(compact('products', 'categories', 'attributes', 'producers'));
    }
}
