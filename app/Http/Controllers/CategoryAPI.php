<?php

namespace App\Http\Controllers;

use App\Categories_synonym;
use App\Category;
use Cache;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryAPI extends Controller
{
    public function search(Request $request)
    {
        $query = $request->search;

        $query = $this->getSimilar($query);

        $categories = Category::where('name', 'ilike', "%$query%")->get()->toHierarchy();

        return $categories->toJson();
    }

    public function getSimilar($query)
    {
        $patterns = [
            '/\s*\([^()]*\)\s*/',
            '/[^\d\w ]+/ui',
            '/[\d]+/ui',
        ];
        $string = preg_replace($patterns, " ", $query);
        $string = trim($string);

        // Удаляем лишние пробелы, например "супер      ноутбук" сделает "супер ноутбук"
        $string = preg_replace('/[\s]+/ui', " ", $string);

        return strtolower($string);
    }

    public function changeRoot(Request $request)
    {
        $id = $request->input('category');

        $category = Category::findOrFail($id);
        $category->makeRoot();
        $category->parent_id = null;
        $category->save();

        return $category;
    }

    public function changeParent(Request $request)
    {
        $id = $request->input('category');
        $parent_id = $request->input('parent_id');

        $category = Category::findOrFail($id);
        $parent = Category::findOrFail($parent_id);

        $category->makeChildOf($parent);

        return response()->json(
            [
                'msg' => 'Вы успешно сделали категорию'.$parent->name.'родителем категории'.$category->name,
            ]
        );
    }

    public function associate(Request $request)
    {
        Categories_synonym::create($request->all());

        return redirect()->back();
    }
}
