<?php

namespace App\Http\Controllers;

use App\Bill_services;
use App\Bills;
use App\Spam_base;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\User;
use App\Address;
use App\Document;
use App\Shop;
use App\Dealing;
use Mail;
use App\Events\Exchange\ExchangeExport;


class ExchangeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buyers_xml_out(Request $request)
    {
        $buyers = User::
        where('sync_need',true)
            ->where('type','buyer')
            ->get();
        $count = count($buyers);

        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('root');
        foreach($buyers as $buyer_data) {
            $xml->startElement('buyer');
            $xml->writeAttribute('id', trim($buyer_data->id));
            $xml->writeElement('name', trim($buyer_data->name));
            $xml->writeElement('surname', trim($buyer_data->surname));
            $xml->writeElement('patronymic', trim($buyer_data->patronymic));
            $xml->writeElement('email', trim($buyer_data->email));
            $xml->startElement('telefones');
            $xml->writeElement('telephone', trim($buyer_data->telephone));
            $xml->endElement();
            $xml->writeElement('inn', trim($buyer_data->inn));
            $date = new DateTime($buyer_data->date_of_birth);
            $xml->writeAttribute('date_of_birth', $date->format('dmY'));
            $date_create = new DateTime($buyer_data->created_at);
            $xml->writeElement('created_at', $date_create->format('dmY'));
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();
        $xmlContent = $xml->outputMemory();
        unset($xml);

        $current_time = Carbon::now();
        $current_year =$current_time->year;
        $current_month =$current_time->month;
        $current_day =$current_time->day;

        if (!Storage::disk('1cExchange')->exists('/buyers')) {
            Storage::disk('1cExchange')->makeDirectory('/buyers', 0775, true);
        }
        Storage::disk('1cExchange')->put('/buyers/'.'buyers_'.$current_day.''.$current_month.''.$current_year.'.xml', $xmlContent);
        $filechown = chown('/var/www/1c_user/data/jm_exchange/buyers/'.'buyers_'.$current_day.''.$current_month.''.$current_year.'.xml',"1c_user");
        if($filechown == true){$filechownReport = 'Успешно';}
        else{$filechownReport = 'Ошибка';}

        $buyers = $buyers->pluck('id');
        User::whereIn($buyers)->update([
            'sync_need'=>false
        ]);

        $report = array(
            'date' => $current_time,
            'type' => 'Выгрузка покупателей',
            'count' => $count,
            'filechownReport' => $filechownReport
        );

        event(new ExchangeExport($report));

        $request->session()->flash('alert-success', ''.$report["type"].' успешно выполнена. <br>Выгружено строк: '.$count.'. <br>Изменние хозяина файла: '.$filechownReport.'');
        return redirect('/admin/exchanges');
    }

    public function shops_xml_out(Request $request)
    {
        $shops = Shop::where('sync_need',true)->get();
        $count = count($shops);

        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('root');
        foreach($shops as $shop_data) {
            $xml->startElement('shop');
            $xml->writeAttribute('id', trim($shop_data->id));
            $xml->writeElement('name', trim($shop_data->name));
            $xml->writeElement('le_name', trim($shop_data->le_name));
            $xml->writeElement('full_le_name', trim($shop_data->full_le_name));
            $xml->writeElement('contract_type', trim($shop_data->contract_type));
            $xml->writeElement('contract_award', trim($shop_data->contract_award));
            $xml->writeElement('nds', trim($shop_data->nds));
            $date_active = new DateTime($shop_data->active_till);
            $xml->writeElement('active_till', $date_active->format('dmY'));
            $xml->writeElement('inn', trim($shop_data->inn));
            $xml->writeElement('kpp', trim($shop_data->kpp));
            $xml->writeElement('ogrn', trim($shop_data->ogrn));
            $xml->writeElement('director_fio', trim($shop_data->director_fio));
            $xml->writeElement('legal_address', trim($shop_data->legal_address));
            $xml->startElement('telefones');
            $xml->writeAttribute('importance', '1');
            $xml->writeElement('telephone', trim($shop_data->telephone));
            $xml->endElement();
            $xml->writeElement('bank_name', trim($shop_data->bank_name));
            $xml->writeElement('bank_bik', trim($shop_data->bank_bik));
            $xml->writeElement('kor_account', trim($shop_data->kor_account));
            $xml->writeElement('document_exchange_type', trim($shop_data->document_exchange_type));
            $xml->writeElement('curator_fio', trim($shop_data->curator_fio));
            $xml->writeElement('e_mail', trim($shop_data->e_mail));
            $date_create = new DateTime($shop_data->created_at);
            $xml->writeElement('created_at', $date_create->format('dmY'));
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();
        $xmlContent = $xml->outputMemory();
        unset($xml);

        $current_time = Carbon::now();
        $current_year =$current_time->year;
        $current_month =$current_time->month;
        $current_day =$current_time->day;

        if (!Storage::disk('1cExchange')->exists('/shops')) {
            Storage::disk('1cExchange')->makeDirectory('/shops', 0775, true);
        }
        Storage::disk('1cExchange')->put('/shops/'.'shops_'.$current_day.''.$current_month.''.$current_year.'.xml', $xmlContent);
        $filechown = chown('/var/www/1c_user/data/jm_exchange/shops/'.'shops_'.$current_day.''.$current_month.''.$current_year.'.xml',"1c_user");
        if($filechown == true){$filechownReport = 'Успешно';}
        else{$filechownReport = 'Ошибка';}

        $shops = $shops->pluck('id');
        Shop::whereIn($shops)->update([
            'sync_need'=>false
        ]);

        $report = array(
            'date' => $current_time,
            'type' => 'Выгрузка магазинов',
            'count' => $count,
            'filechownReport' => $filechownReport
        );

        event(new ExchangeExport($report));

        $request->session()->flash('alert-success', ''.$report["type"].' успешно выполнена. <br>Выгружено строк: '.$count.'. <br>Изменние хозяина файла: '.$filechownReport.'');
        return redirect('/admin/exchanges');
    }

    public function deals_xml_out(Request $request)
    {
        $dealings = Dealing::where('sync_need',true)->get();
        $count = count($dealings);

        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('root');
        foreach($dealings as $deal_data) {
            $xml->startElement('deal');
            $xml->writeAttribute('id', trim($deal_data->id));
            $xml->writeElement('buyer_id', trim($deal_data->user_id));
            $xml->writeElement('shop_id', trim($deal_data->shop_id));
            $xml->writeElement('deal_amount', trim($deal_data->deal_amount));
            $xml->writeElement('award_amount', trim($deal_data->award_amount));
            $xml->writeElement('nds', trim($deal_data->nds));
            $xml->writeElement('payment_type', trim($deal_data->payment_type));
            if($deal_data->bonus_type !=0 ) {
                $xml->startElement('bonus');
                $xml->writeElement('bonus_type', trim($deal_data->bonus_type));
                $xml->writeElement('bonus_user_id', trim($deal_data->bonus_user_id));
                $xml->writeElement('bonus_amount', trim($deal_data->bonus_amount));
                $xml->endElement();
            }
            $xml->writeElement('item_recieving_status', trim($deal_data->item_recieving_status));
            $xml->writeElement('item_recieving_date', trim($deal_data->item_recieving_date));
            $xml->writeElement('shop_result', trim($deal_data->shop_result));
            $xml->writeElement('shop_result_date', trim($deal_data->shop_result_date));
            $xml->writeElement('curator_result_date', trim($deal_data->curator_result_date));
            $xml->writeElement('bill_send_date', trim($deal_data->bill_send_date));
            $xml->writeElement('bill_read_date', trim($deal_data->bill_read_date));
            $xml->writeElement('bill_read_date', trim($deal_data->kor_account));
            $date_create = new DateTime($deal_data->created_at);
            $xml->writeElement('created_at', $date_create->format('dmY'));
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();
        $xmlContent = $xml->outputMemory();
        unset($xml);

        $current_time = Carbon::now();
        $current_year =$current_time->year;
        $current_month =$current_time->month;
        $current_day =$current_time->day;

        if (!Storage::disk('1cExchange')->exists('/deals')) {
            Storage::disk('1cExchange')->makeDirectory('/deals', 0775, true);
        }
        Storage::disk('1cExchange')->put('/deals/'.'deals_'.$current_day.''.$current_month.''.$current_year.'.xml', $xmlContent);
        $filechown = chown('/var/www/1c_user/data/jm_exchange/deals/'.'deals_'.$current_day.''.$current_month.''.$current_year.'.xml',"1c_user");
        if($filechown == true){$filechownReport = 'Успешно';}
        else{$filechownReport = 'Ошибка';}

        $dealings = $dealings->pluck('id');
        Dealing::whereIn($dealings)->update([
            'sync_need'=>false
        ]);

        $report = array(
            'date' => $current_time,
            'type' => 'Выгрузка сделок',
            'count' => $count,
            'filechownReport' => $filechownReport
        );

        event(new ExchangeExport($report));

        $request->session()->flash('alert-success', ''.$report["type"].' успешно выполнена. <br>Выгружено строк: '.$count.'. <br>Изменние хозяина файла: '.$filechownReport.'');
        return redirect('/admin/exchanges');
    }

    public function bills_xml_in(Request $request)
    {
        $files = Storage::disk('1cExchange')->files('/bills');
        $filesReport = array();
        foreach ($files as $file)
        {
            $billsCount = 0;
            if(preg_match("|.xml$|",$file)){
                $xml = simplexml_load_file(Storage::disk('1cExchange')->files('/bills/'.$file.''));
                foreach($xml as $item){
                    $billsCount++;
                    $bills = new Bills();
                    $bills->number = $item->number;
                    $bills->id_shops = $item->id_shops;
                    $bills->contract_award = $item->contract_award;
                    $bills->summ_bill = $item->summ_bill;
                    $bills->date = $item->date;
                    $bills->save();

                    foreach($item->uslugi as $items) {
                        $bills_service = new Bill_services();
                        $bills_service->bill_id = $bills->id;
                        $bills_service->name = $items->usluga;
                        $bills_service->amount = $items->summa;
                        $bills_service->save();
                    }
                }
                $file_del = Storage::disk('1cExchange')->delete($file);
                if($file_del){$fileDeleteStatus = 'Успешно';}
                else{$fileDeleteStatus = 'Ошибка';}
                $filesReport[$billsCount]['fileName'] = basename($file);
                $filesReport[$billsCount]['billsCount'] = $billsCount;
                $filesReport[$billsCount]['fileDeleteStatus'] = $fileDeleteStatus;
            }
        }

        $current_time = Carbon::now();
        $report = array(
            'date' => $current_time,
            'type' => 'Загрузка счетов',
            'filesReport' => $filesReport
        );

        event(new ExchangeImport($report));

        $request->session()->flash('alert-success', ''.$report["type"].' успешно выполнена. <br>Обработано файлов: '.count($report["filesReport"]).'');
        return redirect('/admin/exchanges');
    }

}

