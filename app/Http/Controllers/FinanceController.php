<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Finance;
use App\BonusWithdrawal;
use SEO;
use SEOMeta;
use OpenGraph;

class FinanceController extends Controller
{
    public function my_bonus()
    {
        if (Auth::check()) {
			SEOMeta::setTitleDefault('Мои бонусы - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            $bonuses = Finance::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
            return view('finances.my_finances')
                ->with(compact('bonuses'));
        } else {
            return redirect("/login");
        }
    }

    public function withdraw_bonuses(Request $request)
    {
        $bonuses = Finance::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        if ($bonuses->first()->total > 0 && !empty(Auth::user()->inn)) {
            $newBonus = new BonusWithdrawal;
            $newFinance = new Finance;

            $newBonus->person = $request->person;
            $newBonus->person_id = Auth::id();
            $newBonus->bank = $request->bank;
            $newBonus->card_number = $request->card_number;
            $newBonus->account_number = $request->account_number;
            $newBonus->amount = $request->amount;
            $newBonus->status_id = 2;

            $newBonus->save();

            $newFinance->user_id = Auth::id();
            $newFinance->balance = $bonuses->first()->total;
            $newFinance->debit = 0;
            $newFinance->credit = $request->amount;
            $newFinance->total = $bonuses->first()->total - $request->amount;

            $newFinance->save();

            return back()->with('status', 'Заявка на вывод средств отправлена!');
        } else {
            return back();
        }
    }
}
