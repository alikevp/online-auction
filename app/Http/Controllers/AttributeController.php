<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Attribute;

class AttributeController extends Controller
{
    public function attribute_filter_change(Request $request)
    {
        if (Auth::check()) {
            $FilterUpdate = Attribute::
            where([
                ['slug', $request->slug],
                ['category_id', $request->category_id],
            ]);
//                $FilterUpdate->enableLogging();
            $FilterUpdate->update([
                'filter' => $request->value
            ]);
        }
    }
}
