<?php

namespace App\Http\Controllers;

use App\Address;
use App\cart;
use App\Dealing;
use App\Delivery_type;
use App\Events\Order\Confirmed;
use App\Events\Order\StatusChanged;
use App\Mail\OrderConfirmedToShop;
use App\Mail\OrderConfirmedToUser;
use App\Order;
use App\Order_delivery;
use App\Order_offer_group;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Получает товары из сессии, получает адрес доставки из сессии (зачем?)
     * Получает адрес пользователя из БД
     *
     * @param Request $request
     * @return mixed
     */
    public function delivery(Request $request)
    {
		SEOMeta::setTitleDefault('Корзина - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        $post_products = $request->session()->get('products');
        $delivery_data = $request->session()->get('delivery_data');
        $addresses = Auth::user()->addresses;

        return view('order.delivery')
            ->with(compact('post_products', 'delivery_data', 'addresses'));
    }

    public function fast_delivery(Request $request)
    {
		SEOMeta::setTitleDefault('Корзина - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        $post_products = $request->session()->get('product');
        $delivery_data = $request->session()->get('delivery_data');
        $addresses = Auth::user()->addresses;

        return view('order.fast_delivery')
            ->with(compact('post_products', 'delivery_data', 'addresses'));
    }

    public function confirm(Request $request)
    {
		SEOMeta::setTitleDefault('Корзина - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        if($request->delivery) {
            $delArr = json_decode($request->delivery,1);
            $delivery_data = Address::firstOrCreate(array(
                'city' => $delArr['city'],
                'street' => $delArr['street'],
                'house' => $delArr['house'],
                'apartment' => $delArr['apartment']
            ));
        }else {
            $delivery_data = Address::firstOrCreate(array(
                'city' => $request->city,
                'street' => $request->street,
                'house' => $request->house,
                'apartment' => $request->apartment
            ));
        }

        $products = $request->session()->get('products');
        $_previous = $request->session()->get('_previous');


        $delivery_data->user_id = Auth::user()->id;
        $delivery_data->save();

//      dd($delivery_data, Auth::user()->addresses);

		$gotDelType = $request->input('delivery_type');
        $delivery_type[$gotDelType] = 'on';

        $request->session()->put('delivery_data', $delivery_data);
        $request->session()->put('delivery_type', $delivery_type);

        $cart_items = cart::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();
        $cart_array = array();
        foreach ($cart_items as $cart_item) {
            if (!isset(${'count'.$cart_item->product_id})) {
                ${'count'.$cart_item->product_id} = 0;
            }
            ${'count'.$cart_item->product_id}++;
            $cart_array['uniqe'][] = $cart_item->product_id;
            $cart_array['count'][$cart_item->product_id] = ${'count'.$cart_item->product_id};
        }
        return view('order.confirm')
            ->with(compact('products', 'delivery_data', '_previous', 'cart_array'));
    }

    public function fast_confirm(Request $request)
    {
		SEOMeta::setTitleDefault('Корзина - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        $products = $request->session()->get('product');
        $_previous = $request->session()->get('_previous');

        if($request->delivery) {
            $delArr = json_decode($request->delivery,1);
            $delivery_data = Address::firstOrCreate(array(
                'city' => $delArr['city'],
                'street' => $delArr['street'],
                'house' => $delArr['house'],
                'apartment' => $delArr['apartment']
            ));
        }else {
            $delivery_data = Address::firstOrCreate(array(
                'city' => $request->city,
                'street' => $request->street,
                'house' => $request->house,
                'apartment' => $request->apartment
            ));
        }
        $delivery_data->user_id = auth()->user()->id;
        $delivery_data->save();

//        dd($delivery_data, Auth::user()->addresses);

        $delivery_type = $request->input('delivery_type');

        $request->session()->put('delivery_data', $delivery_data);
        $request->session()->put('delivery_type', $delivery_type);

        $cart_items = cart::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();
        $cart_array = array();
        foreach ($cart_items as $cart_item) {
            if (!isset(${'count'.$cart_item->product_id})) {
                ${'count'.$cart_item->product_id} = 0;
            }
            ${'count'.$cart_item->product_id}++;
            $cart_array['uniqe'][] = $cart_item->product_id;
            $cart_array['count'][$cart_item->product_id] = ${'count'.$cart_item->product_id};
        }

        return view('order.fast_confirm')
            ->with(compact('products', 'delivery_data', '_previous', 'cart_array'));
    }

    public function finish(Request $request)
    {
        $post_products = $request->session()->get('products');
        $delivery_data = $request->session()->get('delivery_data');
        $delivery_type = $request->session()->get('delivery_type');

        $active_till = Carbon::now()->addDays(2);

        #dd($active_till);

        $orders = collect();
        foreach ($post_products as $product_item) {
            if ($request->product_price[$product_item->id] == '') {
                $product_price = 0;
            } else {
                $product_price = $request->product_price[$product_item->id];
            }
            $product_count = $request->product_count[$product_item->id];
            $total_product_price = $product_price * $product_count;
            $order = Order::create(
                [
                    'user_id' => Auth::user()->id,
                    'e_mail' => Auth::user()->email,
                    'quantity' => $product_count,
                    'total_price' => $total_product_price,
                    'active_till' => $active_till,
                    'status' => '1',
                    'category_id' => $product_item->category_id,
                    'product_id' => $product_item->id,
                ]
            );

            // Закомментировать для группирования товаров по заказам.
            // Раскомментировать то, что выше.

            $delivery_data->order_id = $order->id;

            $types = Delivery_type::whereIn('slug', array_keys($delivery_type))->get();
            $delivery = Order_delivery::create($delivery_data->toArray());
            $delivery->types()->sync($types);

            $orders->push($order);
        }
        $cart_del = cart::where([['enter_id', '=', $_COOKIE['enter_id']],])
            ->delete();


        event(new Confirmed(Auth::user(), $orders));

//        Mail::to(Auth::user()->email)->send(new OrderConfirmedToUser($orders, $orders->pluck('id')));
//        Mail::to($email)->send(new OrderConfirmedToShop($orders, $orders->pluck('id')));

        $request->session()->flash('alert-success', 'Заказ успешно сформирован.');

        return redirect('my-orders');
    }

    public function fast_finish(Request $request)
    {
        $post_products = $request->session()->get('product');
        $delivery_data = $request->session()->get('delivery_data');
        $delivery_type = $request->session()->get('delivery_type');

        $active_till = Carbon::now()->addDays(2);


        $orders = collect();
        foreach ($post_products as $product_item) {

            if ($request->product_price[$product_item->id] == '') {
                $product_price = 0;
            } else {
                $product_price = $request->product_price[$product_item->id];
            }
            $product_count = $request->product_count[$product_item->id];
            $total_product_price = $product_price * $product_count;
            $order = Order::create(
                [
                    'user_id' => Auth::user()->id,
                    'e_mail' => Auth::user()->email,
                    'quantity' => $product_count,
                    'total_price' => $total_product_price,
                    'active_till' => $active_till,
                    'status' => '1',
                    'category_id' => $product_item->category_id,
                    'product_id' => $product_item->id,
                ]
            );

            // Закомментировать для группирования товаров по заказам.
            // Раскомментировать то, что выше.

            $delivery_data->order_id = $order->id;

            $types = Delivery_type::whereIn('slug', array_keys($delivery_type))->get();
            $delivery = Order_delivery::create($delivery_data->toArray());
            $delivery->types()->sync($types);

            $orders->push($order);
        }

        $cart_del = cart::where([['enter_id', '=', $_COOKIE['enter_id']],])->where('product_id', $post_products[0]->id)
            ->delete();

        event(new Confirmed(Auth::user(), $orders));

        $request->session()->flash('alert-success', 'Заказ успешно сформирован.');

        return redirect('my-orders');
    }

    public function myorders(Request $request)
    {
		SEOMeta::setTitleDefault('Мои аукционы - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        $orders = Order::with('delivery', 'product', 'order_offer_groups')
            ->where('user_id', Auth::user()->id)
            ->when(
                $request->status == null,
                function ($query) use ($request) {
                    return $query->where('status', '=', 1);
                }
            )
            ->when(
                $request->status,
                function ($query) use ($request) {
                    return $query->whereIn('status', $request->status);
                }
            )
            ->when(
                $request->id != '',
                function ($query) use ($request) {
                    return $query->where('id', 'ilike', '%'.$request->id.'%');
                }
            )
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $get_params = http_build_query($request->request->all());
        $orders->withPath('/my-orders?'.$get_params.'');

        return view('order.myorders')
            ->with(compact('orders'));
    }

    public function orderdetails(Request $request)
    {
		SEOMeta::setTitleDefault('Подробности заказа - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
        OpenGraph::setSiteName('Джинмарт');
        OpenGraph::setTitle('Джинмарт-Электронная торговая площадка потребителей');
        OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'ru_RU');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
        OpenGraph::addImage(asset('images/logo.png'));
        $order = Order::with('delivery', 'product')->findOrFail($request->input('order'));
        $offers = Order_offer_group::with('shops', 'gifts', 'deliveries')->where('order_id', $order->id)->orderBy(
            'status',
            'desc'
        )->orderBy('price', 'asc')->get();
        $time_last = Carbon::now()->diff($order->active_till);
        $time_last = ''.$time_last->d.'д '.$time_last->h.'ч '.$time_last->m.'м '.$time_last->s.'с';

        return view('order.orderdetails')
            ->with(compact('order', 'offers', 'time_last'));
    }

    public function order_cancel(Request $request)
    {
        $user = Auth::user();

        $order = Order::with('order_offer_groups', 'product')
            ->where(
                [
                    ['id', '=', $request->input('order')],
                ]
            )->first();

        if ($user->id == $order->user_id) {
            $order->status = 0;
            $order->save();

            event(new StatusChanged($user, $order));
        }

        return back();
    }

    public function order_extend(Request $request)
    {
        $user = Auth::user();

        $order = Order::where(
            [
                ['id', '=', $request->input('order')],
            ]
        )->first();

        if ($user->id == $order->user_id) {
            if ($order->prolongation < 2 && $order->status == 2) {
                $order->prolongation++;

                $order->active_till = (new Carbon(Carbon::now()))->addDays(2);
                $order->status = 1;

                $order->save();

                event(new StatusChanged($user, $order));


                return redirect('/orderdetails?order='.$request->input('order').'')->with(
                    'message',
                    'Заказ успешно продлён. Продлить заказ можно еще '.(3 - $order->prolongation).' раз(а).'
                );
            } else {
                return redirect('/orderdetails?order='.$request->input('order').'')->with(
                    'message',
                    'Превышено кол-во продлений заказа.'
                );
            }
        }
    }

    public function offer_accept(Request $request)
    {
        $order_data = Order::where('id', $request->input('order'))->first();
        $offer_data = Order_offer_group::where('id', $request->input('offer_confirm'))->first();

        $dealing_unique_id = uniqid("deal_");
        $dealing = new Dealing;

        $dealing->id = $dealing_unique_id;
        $dealing->order_id = $request->input('order');
        $dealing->offer_id = $request->input('offer_confirm');
        $dealing->shop_id = $offer_data->shop_id;
        $dealing->user_id = $order_data->user_id;
        $dealing->status = '0';

        $options = array(
            '1' => array(
                'from' => 0,
                'to' => 10000,
                'percent' => 5,
            ),
            '2' => array(
                'from' => 10001,
                'to' => 100000,
                'percent' => 3,
            ),
            '3' => array(
                'from' => 100001,
                'to' => 9999999,
                'percent' => 2,
            ),
        );

        foreach ($options as $option) {
            if ($offer_data->price >= $option['from'] && $offer_data->price <= $option['to']) {
                $dealing->reward = $offer_data->price * ($option['percent'] / 100);
                break;
            }
        }

        $dealing->save();

        $order_data->status = 3;
        $order_data->save();

        if (Auth::user()->referred_by !== null && Auth::user()->bought == false) {
            // Процент пригласившему
            $invited = Auth::user()->invited;

            if ($invited->finances->isNotEmpty()) {
                $balance = $invited->finances()->latest()->first()->total;
            } else {
                $balance = 0;
            }
            $debit = $offer_data->price * 0.01;
            $total = $balance + $debit;

            $invited->finances()->create(
                [
                    'balance' => $balance,
                    'debit' => $debit,
                    'credit' => 0,
                    'total' => $total,
                ]
            );
            $invited->save();

            // Первая покупка
            $user = Auth::user();
            $user->bought = true;
            $user->save();
        }
//
//            if ($user->finances->isNotEmpty()) {
//                $balance = $user->finances()->latest()->first()->total;
//            } else {
//                $balance = 0;
//            }
//
//            $credit = $offer_data->price;
//            $total = $balance - $credit;
//
//            $user->finances()->create([
//                'balance' => $balance,
//                'debit' => 0,
//                'credit' => $credit,
//                'total' => $total,
//            ]);
//            $user->save();

        event(new StatusChanged(Auth::user(), $order_data));

        $offer_data = Order_offer_group::where('id', $request->input('offer_confirm'))->update(['status' => 1]);

        Auth::user()->auctions_overdue = 0;

        return redirect('/orderdetails?order='.$request->input('order').'')->with(
            'message',
            'Предложение успешно принято'
        );
    }
}