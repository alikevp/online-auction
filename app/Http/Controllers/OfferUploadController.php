<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Attributes_test;
use App\Category;
use App\Offer;
use App\Offers_test;
use App\OffersAnalise;
use App\OffersUpload;
use App\Producer;
use App\Producers_test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class OfferUploadController extends Controller
{

    public function offer_temp_upload(Request $request)
    {
        $files = Storage::disk('parsing')->files($request->folder);
        foreach ($files as $file) {
            if (preg_match("|.csv$|", $file) OR preg_match("|.CSV$|", $file)) {
                Config::set('excel.csv.delimiter', ';');
                Config::set('excel.csv.enclosure', '');
                Config::set('excel.csv.lineEnding', '\r\n');

                $upload = new OffersUpload();

                $upload->file = $file;
                $upload->foto_folder = $request->foto_folder;
                $upload->total_offers = 0;
                $upload->total_cats = 0;
                $upload->cats_found = 0;
                $upload->cats_unfound = 0;
                $upload->models_found = 0;
                $upload->models_unfound = 0;
                $upload->save();

                $csvContent = Excel::filter('chunk')->load('public/parsing/'.$file)->chunk(
                    250,
                    function ($csvContent) use ($request, $upload) {
                        foreach ($csvContent as $csv) {
                            $offers = new OffersAnalise();

                            $offers->name = $csv->name;
                            $offers->upload_id = $upload->id;
                            $offers->cat0 = $csv->cat0;
                            $offers->cat1 = $csv->cat1;
                            $offers->cat2 = $csv->cat2;
                            $offers->cat3 = $csv->cat3;
                            $offers->cat_tree = $csv->cat_tree;
                            $offers->price = $csv->price;
                            $offers->producer = $csv->producer;
                            $offers->producer_logo = $csv->producer_logo;
                            $offers->description = $csv->description;
                            $offers->attributes = $csv->attributes;
                            $offers->images = $csv->images;
                            $offers->url = $csv->url;
                            $offers->bardcode = $csv->bardcode;
                            $offers->size = $csv->size;
                            $offers->color = $csv->color;
                            $offers->shop = $csv->shop;

                            $offers->save();
                        }
                    },
                    false
                );
            }
        }
        $request->session()->flash('alert-success', 'Выгрузка добавлена');

        return redirect('/admin/offers_analises?page=uploads');
    }

    public function offer_temp_xml_upload(Request $request)
    {

        \Debugbar::disable();
        $files = Storage::disk('parsing')->files($request->folder);
        $nowTimestamp = Carbon::now()->format('Y.m.d H:i:s');
        $upload_count = 0;
        $offers_count = 0;
        $fileCount = 0;
        foreach ($files as $file) {
            $fileCount++;
            if (preg_match("|.xml$|", $file)) {
                $xml = simplexml_load_string(Storage::disk('parsing')->get($file), null, LIBXML_NOCDATA);
                if ($fileCount == 1){
                    $offerUploadId = DB::table('offers_uploads')->insertGetId(
                        [
                            'file' => $file,
                            'foto_folder' => $request->foto_folder,
                            'total_offers' => 0,
                            'total_cats' => 0,
                            'cats_found' => 0,
                            'cats_unfound' => 0,
                            'models_found' => 0,
                            'models_unfound' => 0,
                            'created_at' => $nowTimestamp,
                            'updated_at' => $nowTimestamp,
                        ]
                    );
                    $upload_count++;
                }
                $offers_analises = $xml->xpath("/root/offer");
                foreach($offers_analises as $offer){

                    if(isset($offer->sizes->size)>0){$offerSizes = serialize(json_decode(json_encode((array)$offer->sizes),1));}else{$offerSizes = null;}
                    if(isset($offer->colors->color)>0){$offerColors = serialize(json_decode(json_encode((array)$offer->colors),1));}else{$offerColors = null;}
                    if(isset($offer->images->image)>0){$offerImages = serialize(json_decode(json_encode((array)$offer->images),1));}else{$offerImages = null;}
                    if(isset($offer->attributes->attr)>0){$offerAttributes = serialize(json_decode(json_encode((array)$offer->attributes),1));}else{$offerAttributes = null;}
                    $cats_count = count($offer->categories);
                    $cats = array_reverse(json_decode(json_encode((array)$offer->categories->cat),1));
                    if(isset($cats[0])){$cat3 = $cats[0];}else{$cat3 = null;}
                    if(isset($cats[1])){$cat2 = $cats[1];}else{$cat2 = null;}
                    if(isset($cats[2])){$cat1 = $cats[2];}else{$cat1 = null;}
                    if(isset($cats[3])){$cat0 = $cats[3];}else{$cat0 = null;}
                    $cat_tree = '';
                    foreach ($offer->categories->cat as $category){
                        $cat_tree .= "$category/";
                    }
                    DB::table('offers_analises')->insert(
                        [
                            'name' => $offer->name,
                            'upload_id' => $offerUploadId,
                            'cat0' => $cat0,
                            'cat1' => $cat1,
                            'cat2' => $cat2,
                            'cat3' => $cat3,
                            'cat_tree' => $cat_tree,
                            'our_cat' => null,
                            'price' => $offer->price,
                            'producer' => $offer->producer,
                            'producer_logo' => $offer->producer_logo,
                            'description' => $offer->description,
                            'attributes' => $offerAttributes,
                            'images' => $offerImages,
                            'url' => $offer->url,
                            'bardcode' => $offer->bardcode,
                            'size' => $offerSizes,
                            'color' => $offerColors,
                            'shop' => $offer->shop,
                            'created_at' => $nowTimestamp,
                            'updated_at' => $nowTimestamp,
                            'sync_order' => null,
                            'sync_done' => false,
                        ]
                    );
                    $offers_count++;
                }
            }
        }
        $result_txt = '';
        $result_txt .= "Загрузок добавлено: $upload_count <br>";
        $result_txt .= "Товаров добавлено: $offers_count <br>";
        $request->session()->flash('alert-success', $result_txt);

        return redirect('/admin/offers_analises?page=uploads');
    }

    public function offer_temp_analisis(Request $request, $upload_id)
    {
        function dropBackWords($word)
        { //тут мы обрабатываем одно слово
            $reg = "/(ый|ой|ая|ое|ые|ому|а|о|у|е|ого|ему|и|ство|ых|ох|ия|ий|ь|я|он|ют|ат)$/i"; //данная регулярная функция будет искать совпадения окончаний
            $word = preg_replace($reg, '', $word); //убиваем окончания

            return $word;
        }

        //Инициируем счетчики
        $total_offers = 0;
        $total_cats = 0;
        $cats_found = 0;
        $cats_unfound = 0;
        $models_found = 0;
        $models_unfound = 0;

        $temp_offers = OffersAnalise::where('upload_id', $upload_id)->whereNull('our_cat')->get();

        $total_offers = count($temp_offers);//увеличиваем счетчик
        $total_cats = count($temp_offers->unique('cat_tree'));//увеличиваем счетчик

        foreach ($temp_offers as $temp_offer) {
            $temp_offer = $temp_offer->toArray();
            $temp_catgory = dropBackWords($temp_offer['cat'.$request->source_cat]);//убираем окончания у категории
            $temp_parent_catgory = dropBackWords($temp_offer['cat'.($request->source_cat-1)]);//убираем окончания у родительскойкатегории
            $existing_categories = Category::where('name', 'ilike', '%'.$temp_catgory.'%')->get();//Пытаемся найти котегорию среди наших
            if (count($existing_categories)>0) {//если есть такая категория
                foreach ($existing_categories as $existing_category){
                    $existing_parent_category = Category::where('name', 'ilike', '%' . $temp_parent_catgory . '%')->where('id', $existing_category->parent_id)->first();//Пытаемся найти родительскую категорию среди наших которая является родительской нашей найденной и содержит название временной.
                    if ($existing_parent_category) {//если все совпало - это наша ката. Делаем ассоциацию
                        if (($request->source_cat - 1) > 0) {
                            $temp_parent_of_parent_catgory = dropBackWords($temp_offer['cat' . ($request->source_cat - 2)]);//убираем окончания у родительскойкатегории
                            $existing_parent_of_parent_category = Category::where('name', 'ilike', '%' . $temp_parent_of_parent_catgory . '%')->where('id', $existing_parent_category->parent_id)->first();//Пытаемся найти родительскую категорию среди наших которая является родительской нашей найденной и содержит название временной.
                            if ($existing_parent_of_parent_category) {
                                if ($temp_offer['our_cat'] == null) {//если ката еще не проассоциирована
                                    OffersAnalise::where('id', $temp_offer['id'])
                                        ->update(['our_cat' => $existing_category->id]);
                                }
                                $cats_found++;//увеличиваем счетчик
                                break;
                            } else {
                                $cats_unfound++;//увеличиваем счетчик
                            }
                        } else {
                            if ($temp_offer['our_cat'] == null) {//если ката еще не проассоциирована
                                OffersAnalise::where('id', $temp_offer['id'])
                                    ->update(['our_cat' => $existing_category->id]);
                            }
                            $cats_found++;//увеличиваем счетчик
                            break;
                        }
                    } else {
                        $cats_unfound++;//увеличиваем счетчик
                    }//
                }
            } else {
                $cats_unfound++;//увеличиваем счетчик
            }
        }
        OffersUpload::where('id', $upload_id)
            ->update(
                [
                    'total_offers' => $total_offers,
                    'total_cats' => $total_cats,
                    'cats_found' => $cats_found,
                    'cats_unfound' => $cats_unfound,
                    'models_found' => $models_found,
                    'models_unfound' => $models_unfound,
                ]
            );
        $request->session()->flash('alert-success', 'Анализ выполнен');

        return redirect('/admin/offers_analises?page=uploads');
    }

    public function offer_upload(Request $request, $id)
    {
        \Debugbar::disable();

        $isTest = true;
//        $nowTimestamp = Carbon::now()->format('Y.m.d H:i:s');
        $producersUploaded = 0;
        $offersUploaded = 0;
        $atrNamesUploaded = 0;
        $atrValuesUploaded = 0;
        $fotosUploaded = 0;
        $offersDubl = 0;
//        if($isTest){
//            DB::table('offers')->where('upload_id', $id)->delete();
//            DB::table('attributes')->where('upload_id', $id)->delete();
//            DB::table('attribute_names')->where('upload_id', $id)->delete();
//            DB::table('attribute_values')->where('upload_id', $id)->delete();
//        }
        $sync_errors = DB::table('offers_analises')->where('upload_id', $id)->whereNotNull('sync_order')->where('sync_done', '!=', true)->get();
        if (count($sync_errors) > 0) {
            foreach ($sync_errors as $error) {
                $offers_errors = DB::table('offers')->where('url', trim($error->url))->get();
                foreach ($offers_errors as $offers_error) {
                    DB::table('offers')->where('id', $offers_error->id)->delete();
                    DB::table('attributes')->where('offer_id', $offers_error->id)->delete();
                    DB::table('attribute_values')->where('offer_id', $offers_error->id)->delete();
                    Storage::disk('local')->deleteDirectory('shops/offers/'.$offers_error->shop_id.'/'.$offers_error->id.'');
                }
            }
        }

        $offersAnalises = DB::table('offers_analises')->where('upload_id', $id)->whereNotNull('our_cat')->where('sync_done', '!=', true)->get();

        $offersUploadData = DB::table('offers_uploads')->where('id', $id)->first();
        $shop_id = uniqid("shop_");

        $sync_order = 0;
        $uploadReady = count($offersAnalises);
        foreach ($offersAnalises as $offerAnalise) {
            DB::table('offers_analises')->where('id', $offerAnalise->id)->update(['sync_order' => $sync_order]);
            $our_cat = str_replace(" }}", "", $offerAnalise->our_cat);

            $offer_exist = DB::table('offers')->where('url', trim($offerAnalise->url))->first();

            if (!$offer_exist) {
                $price = trim($offerAnalise->price);
                if($price == '' OR $price == null){$price = 0;}
                if (preg_match('@[A-zа-яё]@u', $price)) {
                    $price = str_replace("янв", "1", $price);
                    $price = str_replace("фев", "2", $price);
                    $price = str_replace("мар", "3", $price);
                    $price = str_replace("апр", "4", $price);
                    $price = str_replace("май", "5", $price);
                    $price = str_replace("июн", "6", $price);
                    $price = str_replace("июл", "7", $price);
                    $price = str_replace("авг", "8", $price);
                    $price = str_replace("сен", "9", $price);
                    $price = str_replace("окт", "10", $price);
                    $price = str_replace("ноя", "11", $price);
                    $price = str_replace("дек", "12", $price);
                }
                $newOffer = new Offer();

                $newOffer->id = uniqid("off_");
                $newOffer->name = trim($offerAnalise->name);
                $newOffer->category_id = $our_cat;
                $newOffer->slug = str_slug(trim($offerAnalise->name));
                $newOffer->description = trim($offerAnalise->description);
                $newOffer->barcode = trim($offerAnalise->bardcode);
                $newOffer->url = trim($offerAnalise->url);
                $newOffer->shop_id = $shop_id;
                $newOffer->shop_name = $offerAnalise->shop;
                $newOffer->price = $price;
                $newOffer->upload_id = trim($offersUploadData->id);
                $offersUploaded++;
                $producer = DB::table('producers')->where('name', trim($offerAnalise->producer))->first();

                if (!isset($producer)) {

                    $producer = new Producer();

                    $producer->id = uniqid("prod_");
                    $producer->name = trim($offerAnalise->producer);
                    $producer->slug = str_slug(trim($offerAnalise->producer));

                    $producer->save();
                    $producersUploaded++;
                }

                $newOffer->producer = $producer->slug;

                $newOffer->save();

                if (!$isTest) {
                    $offerAnalise->delete();
                }

                //Работаем с атрибутами
                $attributes_arr = unserialize($offerAnalise->attributes);
                if (isset($attributes_arr) AND is_array($attributes_arr) AND count($attributes_arr)>0) {
                    foreach ($attributes_arr['attr'] as $attribute) {
                        if (isset($attribute['name']) && $attribute['name'] !== '') {
                            if (isset($attribute['value']) && $attribute['value'] !== '' && !is_array($attribute['value'])) {
                                $atr_name = strip_tags($attribute['name'], '<br>');
                                $atr_name = trim($atr_name);
                                $atr_value = strip_tags($attribute['value'], '<br>');
                                $atr_value = trim($atr_value);
                                $atr_translit_name = str_slug($atr_name);
                                $atr_unique_id = uniqid("atr_");
                                $attributeDubleCheck = DB::table('attribute_names')->select('id')
                                    ->where('category_id', $newOffer->category_id)
                                    ->where('name', $atr_name)
                                    ->first();
                                if (!$attributeDubleCheck) {
                                    $attributeNameId = DB::table('attribute_names')->insertGetId(
                                        [
                                            'slug' => $atr_translit_name,
                                            'name' => $atr_name,
                                            'category_id' => $newOffer->category_id,
                                            'filter' => 1,
                                            'upload_id' => trim($offersUploadData->id)
                                        ]
                                    );
                                    $atrNamesUploaded++;
                                } else {
                                    $attributeNameId = $attributeDubleCheck->id;
                                }
                                DB::table('attribute_values')->insert(
                                    [
                                        'attribute_name_id' => $attributeNameId,
                                        'product_id' => null,
                                        'offer_id' => $newOffer->id,
                                        'value' => trim($atr_value),
                                        'upload_id' => trim($offersUploadData->id)
                                    ]
                                );
                                $atrValuesUploaded++;
                                //Временно

                                $newAttribute = new Attribute();

                                $newAttribute->id = $atr_unique_id;
                                $newAttribute->category_id = $newOffer->category_id;
                                $newAttribute->name = $atr_name;
                                $newAttribute->slug = $atr_translit_name;
                                $newAttribute->offer_id = $newOffer->id;
                                $newAttribute->value = trim($atr_value);
                                $newAttribute->upload_id = trim($offersUploadData->id);

                                $newAttribute->save();
                                //-----Временно-----
                            }
                        }
                    }
                }
                $sizes_arr = unserialize($offerAnalise->size);
                if (isset($sizes_arr) AND is_array($sizes_arr) AND count($sizes_arr)>0) {
                    foreach ($sizes_arr as $atr_value) {
                        if (is_array($atr_value)) {
                            foreach ($atr_value as $atr_inner_value) {
                                $atr_name = 'Размер';
                                $atr_translit_name = str_slug($atr_name);
                                $atr_inner_value = trim($atr_inner_value);
                                $atr_unique_id = uniqid("atr_");
                                $attributeDubleCheck = DB::table('attribute_names')->select('id')
                                    ->where('category_id', $newOffer->category_id)
                                    ->where('name', $atr_name)
                                    ->first();
                                if (!$attributeDubleCheck) {
                                    $attributeNameId = DB::table('attribute_names')->insertGetId(
                                        [
                                            'slug' => $atr_translit_name,
                                            'name' => $atr_name,
                                            'category_id' => $newOffer->category_id,
                                            'filter' => 1,
                                            'upload_id' => trim($offersUploadData->id)
                                        ]
                                    );
                                    $atrNamesUploaded++;
                                } else {
                                    $attributeNameId = $attributeDubleCheck->id;
                                }
                                DB::table('attribute_values')->insert(
                                    [
                                        'attribute_name_id' => $attributeNameId,
                                        'product_id' => null,
                                        'offer_id' => $newOffer->id,
                                        'value' => trim($atr_inner_value),
                                        'upload_id' => trim($offersUploadData->id)
                                    ]
                                );
                                $atrValuesUploaded++;
                                //Временно
                                $newAttribute = new Attribute();

                                $newAttribute->id = $atr_unique_id;
                                $newAttribute->category_id = $newOffer->category_id;
                                $newAttribute->name = $atr_name;
                                $newAttribute->slug = $atr_translit_name;
                                $newAttribute->offer_id = $newOffer->id;
                                $newAttribute->value = trim($atr_inner_value);
                                $newAttribute->upload_id = trim($offersUploadData->id);

                                $newAttribute->save();
                                //-----Временно------
                            }
                        }
                        else{
                            $atr_name = 'Размер';
                            $atr_translit_name = str_slug($atr_name);
                            $atr_value = trim($atr_value);
                            $atr_unique_id = uniqid("atr_");
                            $attributeDubleCheck = DB::table('attribute_names')->select('id')
                                ->where('category_id', $newOffer->category_id)
                                ->where('name', $atr_name)
                                ->first();
                            if (!$attributeDubleCheck) {
                                $attributeNameId = DB::table('attribute_names')->insertGetId(
                                    [
                                        'slug' => $atr_translit_name,
                                        'name' => $atr_name,
                                        'category_id' => $newOffer->category_id,
                                        'filter' => 1,
                                        'upload_id' => trim($offersUploadData->id)
                                    ]
                                );
                                $atrNamesUploaded++;
                            } else {
                                $attributeNameId = $attributeDubleCheck->id;
                            }
                            DB::table('attribute_values')->insert(
                                [
                                    'attribute_name_id' => $attributeNameId,
                                    'product_id' => null,
                                    'offer_id' => $newOffer->id,
                                    'value' => trim($atr_value),
                                    'upload_id' => trim($offersUploadData->id)
                                ]
                            );
                            $atrValuesUploaded++;
                            //Временно
                            $newAttribute = new Attribute();

                            $newAttribute->id = $atr_unique_id;
                            $newAttribute->category_id = $newOffer->category_id;
                            $newAttribute->name = $atr_name;
                            $newAttribute->slug = $atr_translit_name;
                            $newAttribute->offer_id = $newOffer->id;
                            $newAttribute->value = trim($atr_value);
                            $newAttribute->upload_id = trim($offersUploadData->id);

                            $newAttribute->save();
                            //-----Временно------
                        }
                    }
                }
                //Цвета
                $colors_arr = unserialize($offerAnalise->color);
                if (isset($colors_arr) AND is_array($colors_arr) AND count($colors_arr)>0) {
                    foreach ($colors_arr as $atr_value) {
                        if (is_array($atr_value)) {
                            foreach ($atr_value as $atr_inner_value) {
                                $atr_name = 'Цвет';
                                $atr_translit_name = str_slug($atr_name);
                                $atr_inner_value = trim($atr_inner_value);
                                $atr_unique_id = uniqid("atr_");
                                $attributeDubleCheck = DB::table('attribute_names')->select('id')
                                    ->where('category_id', $newOffer->category_id)
                                    ->where('name', $atr_name)
                                    ->first();
                                if (!$attributeDubleCheck) {
                                    $attributeNameId = DB::table('attribute_names')->insertGetId(
                                        [
                                            'slug' => $atr_translit_name,
                                            'name' => $atr_name,
                                            'category_id' => $newOffer->category_id,
                                            'filter' => 1,
                                            'upload_id' => trim($offersUploadData->id)
                                        ]
                                    );
                                    $atrNamesUploaded++;
                                } else {
                                    $attributeNameId = $attributeDubleCheck->id;
                                }
                                DB::table('attribute_values')->insert(
                                    [
                                        'attribute_name_id' => $attributeNameId,
                                        'product_id' => null,
                                        'offer_id' => $newOffer->id,
                                        'value' => trim($atr_inner_value),
                                        'upload_id' => trim($offersUploadData->id)
                                    ]
                                );
                                $atrValuesUploaded++;
                                //Временно
                                $newAttribute = new Attribute();

                                $newAttribute->id = $atr_unique_id;
                                $newAttribute->category_id = $newOffer->category_id;
                                $newAttribute->name = $atr_name;
                                $newAttribute->slug = $atr_translit_name;
                                $newAttribute->offer_id = $newOffer->id;
                                $newAttribute->value = trim($atr_inner_value);
                                $newAttribute->upload_id = trim($offersUploadData->id);

                                $newAttribute->save();
                                //-----Временно------
                            }
                        }
                        else{
                            $atr_name = 'Цвет';
                            $atr_translit_name = str_slug($atr_name);
                            $atr_value = trim($atr_value);
                            $atr_unique_id = uniqid("atr_");
                            $attributeDubleCheck = DB::table('attribute_names')->select('id')
                                ->where('category_id', $newOffer->category_id)
                                ->where('name', $atr_name)
                                ->first();
                            if (!$attributeDubleCheck) {
                                $attributeNameId = DB::table('attribute_names')->insertGetId(
                                    [
                                        'slug' => $atr_translit_name,
                                        'name' => $atr_name,
                                        'category_id' => $newOffer->category_id,
                                        'filter' => 1,
                                        'upload_id' => trim($offersUploadData->id)
                                    ]
                                );
                                $atrNamesUploaded++;
                            } else {
                                $attributeNameId = $attributeDubleCheck->id;
                            }
                            DB::table('attribute_values')->insert(
                                [
                                    'attribute_name_id' => $attributeNameId,
                                    'product_id' => null,
                                    'offer_id' => $newOffer->id,
                                    'value' => trim($atr_value),
                                    'upload_id' => trim($offersUploadData->id)
                                ]
                            );
                            $atrValuesUploaded++;
                            //Временно
                            $newAttribute = new Attribute();

                            $newAttribute->id = $atr_unique_id;
                            $newAttribute->category_id = $newOffer->category_id;
                            $newAttribute->name = $atr_name;
                            $newAttribute->slug = $atr_translit_name;
                            $newAttribute->offer_id = $newOffer->id;
                            $newAttribute->value = trim($atr_value);
                            $newAttribute->upload_id = trim($offersUploadData->id);

                            $newAttribute->save();
                            //-----Временно------
                        }
                    }
                }
                //работаем с фото
                if ($isTest) {
                    $foto_folder = '' . dirname($offersUploadData->file) . '/' . $offersUploadData->foto_folder . '';
                    if (!Storage::disk('local')->exists('shops/offers/' . $shop_id . '')) {
                        Storage::disk('local')->makeDirectory('shops/offers/' . $shop_id . '', 0775, true);
                    }
                    if (!Storage::disk('local')->exists('shops/offers/' . $shop_id . '/' . $newOffer->id . '')) {
                        Storage::disk('local')->makeDirectory('shops/offers/' . $shop_id . '/' . $newOffer->id . '', 0775, true);
                    }

                    $fotos_arr = unserialize($offerAnalise->images);
                    if (isset($fotos_arr) AND is_array($fotos_arr) AND count($fotos_arr)>0) {
                        foreach ($fotos_arr as $foto) {
                            if (is_array($fotos_arr)) {
                                foreach ($fotos_arr as $inner_foto) {
                                    $foto_name = str_replace("files/", "", $inner_foto);

                                    if (Storage::disk('parsing')->exists('' . $foto_folder . '/' . $foto_name . '')) {
                                        $old_file = public_path() . '/parsing/' . $foto_folder . '/' . $foto_name;
                                        if (is_file($old_file)) {
                                            $newfile = public_path() . '/images/catalog/shops/offers/' . $shop_id . '/' . $newOffer->id . '/' . $foto_name;
                                            copy($old_file, $newfile);
                                            $fotosUploaded++;
                                        }
                                    }
                                }
                            }
                            else {
                                $foto_name = str_replace("files/", "", $foto);

                                if (Storage::disk('parsing')->exists('' . $foto_folder . '/' . $foto_name . '')) {
                                    $old_file = public_path() . '/parsing/' . $foto_folder . '/' . $foto_name;
                                    if (is_file($old_file)) {
                                        $newfile = public_path() . '/images/catalog/shops/offers/' . $shop_id . '/' . $newOffer->id . '/' . $foto_name;
                                        copy($old_file, $newfile);
                                        $fotosUploaded++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                $offersDubl++;
            }
            OffersAnalise::where('id', $offerAnalise->id)->update(['sync_done' => true]);
            $sync_order++;
        }
        $request->session()->flash('alert-success', "Загрузка выполнена.Готово:$uploadReady. Дуюляж:$offersDubl. Производители:$producersUploaded. Товары:$offersUploaded. Назв.атрибутов:$atrNamesUploaded. Значения Атрибутов:$atrValuesUploaded. Фото:$fotosUploaded.");

        return redirect('/admin/offers_analises?page=uploads');
    }

    public function set_category(Request $request, $offerAnaliseId)
    {
        $count = 0;
        $offerAnalise = OffersAnalise::where('id', $offerAnaliseId)->first();
        $offerAnalise->our_cat = $request->category_id;
        $offerAnalise->save();
        $count++;
        $sameOffersAnalises = OffersAnalise::where('cat_tree', $offerAnalise->cat_tree)
            ->where('id', '!=', $offerAnalise->id)
            ->where('our_cat', null)
            ->get();

        if (count($sameOffersAnalises) > 0) {
            foreach ($sameOffersAnalises as $sameOffersAnalise) {
                $sameOffersAnalise->our_cat = $request->category_id;
                $sameOffersAnalise->save();
                $count++;
            }
        }


        $request->session()->flash('alert-success', 'Обновлено '.$count.' товаров');

        return redirect('admin/offers_analises?page=nfcat&upload_id='.$offerAnalise->upload_id);
    }

    public function set_category_by_field(Request $request, $offerAnaliseUploadId)
    {
        $count = 0;
        if (isset($request->field2) AND isset($request->algiritm2) AND $request->algiritm2 !== '') {
            $sameOffersAnalises = OffersAnalise::when(
                $request->algiritm == '=',
                function ($query) use ($request) {
                    return $query->where($request->field, $request->algiritm, $request->value);
                }
            )
                ->when(
                    $request->algiritm == 'like',
                    function ($query) use ($request) {
                        return $query->where($request->field, $request->algiritm, '%' . $request->value . '%');
                    }
                )
                ->when(
                    $request->algiritm2 == '=',
                    function ($query) use ($request) {
                        return $query->where($request->field2, $request->algiritm2, $request->value2);
                    }
                )
                ->when(
                    $request->algiritm2 == 'like',
                    function ($query) use ($request) {
                        return $query->where($request->field2, $request->algiritm2, '%' . $request->value2 . '%');
                    }
                )
                ->get();
        }
        else{
            $sameOffersAnalises = OffersAnalise::when(
                $request->algiritm == '=',
                function ($query) use ($request) {
                    return $query->where($request->field, $request->algiritm, $request->value);
                }
            )
                ->when(
                    $request->algiritm == 'like',
                    function ($query) use ($request) {
                        return $query->where($request->field, $request->algiritm, '%' . $request->value . '%');
                    }
                )
                ->get();
        }
//        dd($sameOffersAnalises);
        if (count($sameOffersAnalises) > 0) {
            foreach ($sameOffersAnalises as $sameOffersAnalise) {
                $sameOffersAnalise->our_cat = $request->category_id;
                $sameOffersAnalise->save();
                $count++;
            }
            $request->session()->flash('alert-success', 'Обновлено '.$count.' товаров');
        } else {
            $request->session()->flash('alert-warning', 'По условиям выборки не найдено ни одного совпадения');
        }

        return redirect('admin/offers_analises?page=nfcat&upload_id='.$offerAnaliseUploadId);
    }

    public function add_category(Request $request, $offerAnaliseId)
    {
        $slug = str_slug($request->name, '-');
        $cat_unique_id = uniqid("cat_");
        $newCategory = new Category();

        $newCategory->id = $cat_unique_id;
        $newCategory->parent_id = $request->parent_id;
        $newCategory->name = $request->name;
        $newCategory->slug = $slug;
        $newCategory->in_menu = true;
        $newCategory->checked = 1;
        $newCategory->offers_count = 0;
        $newCategory->leafs_offers_count = 0;

        $newCategory->save();

        $parent = Category::where('id', $request->parent_id)->first();
        $newCategory->makeChildOf($parent);


        $count = 0;
        $offerAnalise = OffersAnalise::where('id', $offerAnaliseId)->first();
        $offerAnalise->our_cat = $cat_unique_id;
        $offerAnalise->save();
        $count++;
        $sameOffersAnalises = OffersAnalise::where('cat3', $offerAnalise->cat3)
            ->where('id', '!=', $offerAnalise->id)
            ->where('our_cat', null)
            ->get();

        if (count($sameOffersAnalises) > 0) {

            foreach ($sameOffersAnalises as $sameOffersAnalise) {
                $sameOffersAnalise->our_cat = $cat_unique_id;
                $sameOffersAnalise->save();
                $count++;
            }
        }

        $request->session()->flash('alert-success', 'Категория добавлена. Обновлено '.$count.' товаров');

        return redirect($request->redirect_url);
    }

    public function offerAnaliseExport(Request $request)
    {
        \Debugbar::disable();
        $offers_upload= DB::table('offers_uploads')->where('id',$request->upload_id)->first();
        $offers_analises= DB::table('offers_analises')->where('upload_id',$request->upload_id)->get();
        $filename='offer_upload'.$offers_upload->id.'.xml';
        header('Content-type: text/xml');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        $xml = new \XMLWriter();
        $xml->openURI('php://output');
        $xml->setIndent(true);
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('root');
            $xml->startElement('offers_uploads');
                $xml->startElement('upload');
                    $xml->writeAttribute('id', trim($offers_upload->id));
                    $xml->writeElement('file', trim($offers_upload->file));
                    $xml->writeElement('foto_folder', trim($offers_upload->foto_folder));
                    $xml->writeElement('total_offers', trim($offers_upload->total_offers));
                    $xml->writeElement('total_cats', trim($offers_upload->total_cats));
                    $xml->writeElement('cats_found', trim($offers_upload->cats_found));
                    $xml->writeElement('cats_unfound', trim($offers_upload->cats_unfound));
                    $xml->writeElement('models_found', trim($offers_upload->models_found));
                    $xml->writeElement('models_unfound', trim($offers_upload->models_unfound));
                $xml->endElement();
            $xml->endElement();
            $xml->startElement('offers_analises');
                foreach($offers_analises as $offer) {
                    $our_cat = str_replace("}}", "", $offer->our_cat);
                    $our_cat_data= DB::table('categories')->where('id',trim($our_cat))->first();
                    $xml->startElement('offer');
                    $xml->writeAttribute('id', trim($offer->id));
                    $xml->writeElement('name', trim($offer->name));
                    $xml->writeElement('value', trim($offer->value));
                    $xml->writeElement('upload_id', trim($offer->upload_id));
                    $xml->writeElement('cat0', trim($offer->cat0));
                    $xml->writeElement('cat1', trim($offer->cat1));
                    $xml->writeElement('cat2', trim($offer->cat2));
                    $xml->writeElement('cat3', trim($offer->cat3));
                    $xml->writeElement('cat_tree', trim($offer->cat_tree));
                    $xml->writeElement('our_cat', trim($our_cat));
                    if(isset($our_cat)) {
                        if ($our_cat !== '') {
                            $xml->startElement('our_cat_data');
                                $xml->writeAttribute('id', trim($our_cat_data->id));
                                $xml->writeElement('parent_id', trim($our_cat_data->parent_id));
                                $xml->writeElement('lft', trim($our_cat_data->lft));
                                $xml->writeElement('rgt', trim($our_cat_data->rgt));
                                $xml->writeElement('depth', trim($our_cat_data->depth));
                                $xml->writeElement('name', trim($our_cat_data->name));
                                $xml->writeElement('slug', trim($our_cat_data->slug));
                                $xml->writeElement('in_menu', trim($our_cat_data->in_menu));
                                $xml->writeElement('checked', trim($our_cat_data->checked));
                                $xml->writeElement('created_at', trim($our_cat_data->created_at));
                                $xml->writeElement('updated_at', trim($our_cat_data->updated_at));
                                $xml->writeElement('offers_count', trim($our_cat_data->offers_count));
                                $xml->writeElement('leafs_offers_count', trim($our_cat_data->leafs_offers_count));
                            $xml->endElement();
                        }
                    }
                    $xml->writeElement('price', trim($offer->price));
                    $xml->writeElement('producer', trim($offer->producer));
                    $xml->writeElement('producer_logo', trim($offer->producer_logo));
                    $xml->writeElement('description', trim($offer->description));
                    $xml->writeElement('attributes', trim($offer->attributes));
                    $xml->writeElement('images', trim($offer->images));
                    $xml->writeElement('url', trim($offer->url));
                    $xml->writeElement('bardcode', trim($offer->bardcode));
                    $xml->writeElement('size', trim($offer->size));
                    $xml->writeElement('color', trim($offer->color));
                    $xml->writeElement('shop', trim($offer->shop));
                    $xml->writeElement('sync_order', trim($offer->sync_order));
                    $xml->writeElement('sync_done', trim($offer->sync_done));
                    $xml->endElement();
                }
            $xml->endElement();
        $xml->endElement();
        $xml->endDocument();



        $xml->flush();;
//        $temp = tmpfile();
//        fwrite($temp, $xmlContent);
////
//        return response()->file($xmlContent);
//        $request->session()->flash('alert-success', 'Категория добавлена. Обновлено '.$count.' товаров');
//
//        return redirect($request->redirect_url);
    }
    public function offerAnaliseImport()
    {
        \Debugbar::disable();
        $files = Storage::disk('parsing')->files('offerAnaliseImport/');
        $nowTimestamp = Carbon::now()->format('Y.m.d H:i:s');
        $upload_count = 0;
        $offers_count = 0;
        $cats_count = 0;
        foreach ($files as $file) {
            if (preg_match("|.xml$|", $file)) {
                $xml = simplexml_load_string(Storage::disk('parsing')->get($file));
                $offer_upload = $xml->xpath("/root/offers_uploads/upload[1]");
                $offerUploadId = DB::table('offers_uploads')->insertGetId(
                    [
                        'file' => $offer_upload[0]->file,
                        'foto_folder' => $offer_upload[0]->foto_folder,
                        'total_offers' => $offer_upload[0]->total_offers,
                        'total_cats' => $offer_upload[0]->total_cats,
                        'cats_found' => $offer_upload[0]->cats_found,
                        'cats_unfound' => $offer_upload[0]->cats_unfound,
                        'models_found' => $offer_upload[0]->models_found,
                        'models_unfound' => $offer_upload[0]->models_unfound,
                        'created_at' => $nowTimestamp,
                        'updated_at' => $nowTimestamp,
                    ]
                );
                $upload_count++;
                $offers_analises = $xml->xpath("/root/offers_analises/offer");
                foreach($offers_analises as $offer){
                    if (isset($offer->our_cat_data)) {
                        $categoryDubleCheck = DB::table('categories')->select('id')
                            ->where('id', $offer->our_cat_data->attributes()['id'])
                            ->first();
                        if (!$categoryDubleCheck) {
                            DB::table('categories')->insert(
                                [
                                    'id' => $offer->our_cat_data->attributes()['id'],
                                    'parent_id' => $offer->our_cat_data->parent_id,
                                    'lft' => $offer->our_cat_data->lft,
                                    'rgt' => $offer->our_cat_data->rgt,
                                    'depth' => $offer->our_cat_data->depth,
                                    'name' => $offer->our_cat_data->name,
                                    'slug' => $offer->our_cat_data->slug,
                                    'in_menu' => $offer->our_cat_data->in_menu,
                                    'checked' => $offer->our_cat_data->checked,
                                    'created_at' => $nowTimestamp,
                                    'updated_at' => $nowTimestamp,
                                    'offers_count' => $offer->our_cat_data->offers_count,
                                    'leafs_offers_count' => $offer->our_cat_data->leafs_offers_count,
                                ]
                            );
                            $cats_count++;
                        }

                    }
                    if(!isset($offer->sync_order) OR $offer->sync_order == '' OR $offer->sync_order == ' '){$sync_order = null;}
                    else{$sync_order = $offer->sync_order;}
                    if(!isset($offer->sync_done) OR $offer->sync_done == '' OR $offer->sync_done == ' '){$sync_done = null;}
                    else{$sync_done = $offer->sync_done;}
                    if(!isset($offer->our_cat) OR $offer->our_cat == '' OR $offer->our_cat == ' '){$our_cat = null;}
                    else{$our_cat = $offer->our_cat;}
                    DB::table('offers_analises')->insert(
                        [
                            'name' => $offer->name,
                            'value' => $offer->value,
                            'upload_id' => $offerUploadId,
                            'cat0' => $offer->cat0,
                            'cat1' => $offer->cat1,
                            'cat2' => $offer->cat2,
                            'cat3' => $offer->cat3,
                            'cat_tree' => $offer->cat_tree,
                            'our_cat' => $our_cat,
                            'price' => $offer->price,
                            'producer' => $offer->producer,
                            'producer_logo' => $offer->producer_logo,
                            'description' => $offer->description,
                            'attributes' => $offer->attributes,
                            'images' => $offer->images,
                            'url' => $offer->url,
                            'bardcode' => $offer->bardcode,
                            'size' => $offer->size,
                            'color' => $offer->color,
                            'shop' => $offer->shop,
                            'created_at' => $nowTimestamp,
                            'updated_at' => $nowTimestamp,
                            'sync_order' => null,
                            'sync_done' => false,
                        ]
                    );
                    $offers_count++;
                }
            }
        }
        echo "Загрузок добавлено: $upload_count <br>";
        echo "Категорий добавлено: $cats_count <br>";
        echo "Товаров добавлено: $offers_count <br>";
    }
    public function offer_dubl_del(){
        function fotoExistanceCheck($shop_id,$offer_id){
            $existance = false;
            if (Storage::disk('local')->exists('shops/offers/'.$shop_id.'/'.$offer_id.'')) {
                $files = Storage::disk('local')->exists('shops/offers/'.$shop_id.'/'.$offer_id.'');
                if(count($files)>0){
                    $existance = true;
                }
            }
            return $existance;
        }
        $offers_groups = DB::table('offers')->get()->groupBy('name','barcode');

        $dublsCount = 0;
        foreach ($offers_groups as $offersGroupByName) {
            $offersGroupByName = $offersGroupByName->groupBy('barcode');
            foreach ($offersGroupByName as $offers_group){
                if (count($offers_group) > 1) {
                    $dublOffers = array();
                    $firstOffer = $offers_group->first()->id;
                    foreach ($offers_group as $offer) {
                        if (fotoExistanceCheck($offer->shop_id, $offer->id)) {
                            $firstOfferWithFoto = $offer->id;
                        }
                        $dublOffers[$offer->id] = $offer->id;
                    }
                    if(isset($firstOfferWithFoto)){
                        unset($dublOffers[$firstOfferWithFoto]);
                    } else{
                        unset($dublOffers[$firstOffer]);
                    }
                    $dublsCount = $dublsCount + count($dublOffers);
                }
            }
        }
        dd($dublsCount);
    }
}