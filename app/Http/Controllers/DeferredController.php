<?php

namespace App\Http\Controllers;

use App\cart;
use App\Deferred;
use App\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SEO;
use SEOMeta;
use OpenGraph;

class DeferredController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		SEOMeta::setTitleDefault('Отложенные товары - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $deferred_items = Deferred::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();
        if (count($deferred_items) > 0) {
            $cart_array = array();
            foreach ($deferred_items as $deferred_item) {
                if (!isset(${'count'.$deferred_item->product_id})) {
                    ${'count'.$deferred_item->product_id} = 0;
                }
                ${'count'.$deferred_item->product_id}++;
                $deferred_array['uniqe'][] = $deferred_item->product_id;
                $deferred_array['count'][$deferred_item->product_id] = ${'count'.$deferred_item->product_id};
            }
            $products = Offer::
            whereIn('id', $deferred_array['uniqe'])
                ->orderBy('category_id', 'asc')
                ->get();

            return view(
                'deferred.index',
                [
                    'products' => $products,
                    'counts' => $deferred_array['count'],
                ]
            );
        } else {
            return view('deferred.index');
        }
    }

    public function deferred_add(Request $request)
    {
        $unique_id = uniqid("def_");
        $deferred = new Deferred;
        $deferred->id = $unique_id;
        $deferred->enter_id = $_COOKIE['enter_id'];
        $deferred->user_id = $_COOKIE['enter_id'];
        $deferred->product_id = $request->input('product_id');

        $deferred->save();
    }

    public function deferred_del(Request $request)
    {
        $deferred_del = Deferred::where(
            [
                ['enter_id', '=', $_COOKIE['enter_id']],
                ['product_id', '=', $request->input('product_id')],
            ]
        )
            ->delete();
    }

    public function deferred_recount(Request $request)
    {
        $prodCurCount = $request->input('prodCurCount');
        $prodNewCount = $request->input('prodNewCount');
        if ($prodNewCount > $prodCurCount) {
            $limit = ($prodNewCount - $prodCurCount);
            for ($i = 1; $i <= $limit; $i++) {
                $unique_id = uniqid("cart_");
                $deferred = new Deferred;
                $deferred->id = $unique_id;
                $deferred->enter_id = $_COOKIE['enter_id'];
                $deferred->user_id = $_COOKIE['enter_id'];
                $deferred->product_id = $request->input('product_id');

                $deferred->save();
            }
        }
        if ($prodNewCount < $prodCurCount) {
            $limit = ($prodCurCount - $prodNewCount);
            if ($limit > 0) {
                $deferred_del = DB::table('deferreds')
                    ->whereIn(
                        'id',
                        DB::table('deferreds')
                            ->where(
                                [
                                    ['enter_id', '=', $_COOKIE['enter_id']],
                                    ['product_id', '=', $request->input('product_id')],
                                ]
                            )
                            ->take($limit)
                            ->pluck('id')
                    )
                    ->delete();
            }
            if ($limit <= 0) {
                $deferred_del = Deferred::where(
                    [
                        ['enter_id', '=', $_COOKIE['enter_id']],
                        ['product_id', '=', $request->input('product_id')],
                    ]
                )
                    ->delete();
            }
        }
        if ($prodNewCount == $prodCurCount) {
        }
    }

    public function deferred_count(Request $request)
    {
        $deferred_items = Deferred::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->count();
        echo $deferred_items;
    }

    public function deferred_to_cart(Request $request)
    {
        $deferred_item = Deferred::where(
            [
                ['enter_id', '=', $_COOKIE['enter_id']],
                ['product_id', '=', $request->input('product_id')],
            ]
        )->first();

        $cart_item = new Cart;
        $unique_id = uniqid("cart_");
        $cart_item->id = $unique_id;
        $cart_item->product_id = $deferred_item->product_id;
        $cart_item->enter_id = $deferred_item->enter_id;
        $cart_item->user_id = null;

        $cart_item->save();

        $deferred_item->delete();
    }

    public function to_cart()
    {
        $deferred_items = Deferred::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();

        foreach ($deferred_items as $deferred_item) {
            $cart_item = new Cart;
            $unique_id = uniqid("cart_");
            $cart_item->id = $unique_id;
            $cart_item->product_id = $deferred_item->product_id;
            $cart_item->enter_id = $deferred_item->enter_id;
            $cart_item->user_id = "NULL";

            $cart_item->save();

            $deferred_item->delete();
        }

        return redirect('/cart');
    }
}
