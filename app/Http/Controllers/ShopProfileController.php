<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Shop;
use App\PaymentType;
use App\BankData;
use App\Shop_subscription;
use App\Category;
use Validator;
use Hash;
use App\Address;
use SEO;
use SEOMeta;
use OpenGraph;


class ShopProfileController extends Controller
{
    public function register_shop(){
        if(Auth::check()) {
			SEOMeta::setTitleDefault('Требуется действие - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            return view('auth.logout_request');
        }
        else{
			SEOMeta::setTitleDefault('Регистрация - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            return view('auth.register_shop');
        }
    }
    public function shop_profile()
    {
        if(Auth::check())
        {
			SEOMeta::setTitleDefault('Данные магазина - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            $messages = [];
            $user = Auth::user();
            $shop_data = Shop::with('addresses','bank_data','payment_types')->where('id', $user->connection_id)->first();
            if(null !== $shop_data->addresses){
                $addresses  = $shop_data->addresses;
            }else{$addresses  = null;}
            if($shop_data->bank_data !== null){
                $bank_data  = $shop_data->bank_data;
            }else{$bank_data  = null;}
            if($shop_data->payment_types !== null){
                $payment_types  = $shop_data->payment_types;
                $payment_data = array();
                foreach ($payment_types as $payment_type){
                    if($payment_type->type == 4){
                        $bank_data_for_payment = BankData::where('id', $payment_type->bank_data_id)->first();
                        $payment_data[$payment_type->id]['bank_data']['id']=$bank_data_for_payment->id;
                        $payment_data[$payment_type->id]['bank_data']['user_id']=$bank_data_for_payment->user_id;
                        $payment_data[$payment_type->id]['bank_data']['shop_id']=$bank_data_for_payment->shop_id;
                        $payment_data[$payment_type->id]['bank_data']['type']=$bank_data_for_payment->type;
                        $payment_data[$payment_type->id]['bank_data']['bank_name']=$bank_data_for_payment->bank_name;
                        $payment_data[$payment_type->id]['bank_data']['bank_address']=$bank_data_for_payment->bank_address;
                        $payment_data[$payment_type->id]['bank_data']['bank_bik']=$bank_data_for_payment->bank_bik;
                        $payment_data[$payment_type->id]['bank_data']['kor_account']=$bank_data_for_payment->kor_account;
                        $payment_data[$payment_type->id]['bank_data']['inn']=$bank_data_for_payment->inn;
                        $payment_data[$payment_type->id]['bank_data']['kpp']=$bank_data_for_payment->kpp;
                        $payment_data[$payment_type->id]['bank_data']['oktmo']=$bank_data_for_payment->oktmo;
                        $payment_data[$payment_type->id]['bank_data']['kbk']=$bank_data_for_payment->kbk;
                        $payment_data[$payment_type->id]['bank_data']['recipient']=$bank_data_for_payment->recipient;
                        $payment_data[$payment_type->id]['bank_data']['account']=$bank_data_for_payment->account;
                        $payment_data[$payment_type->id]['bank_data']['purpose']=$bank_data_for_payment->purpose;
                    }
                    $payment_data[$payment_type->id]['id']=$payment_type->id;
                    $payment_data[$payment_type->id]['shop_id']=$payment_type->shop_id;
                    $payment_data[$payment_type->id]['type']=$payment_type->type;
                    $payment_data[$payment_type->id]['bank_data_id']=$payment_type->bank_data_id;
                    $payment_data[$payment_type->id]['name']=$payment_type->name;
                    $payment_data[$payment_type->id]['account']=$payment_type->account;
                    $payment_data[$payment_type->id]['description']=$payment_type->description;
                }
            }else{$payment_data  = null;}
            return view('shop.shop_profile')
                ->with(compact('messages', 'shop_data', 'addresses', 'bank_data', 'payment_data'));
        } else
        {
            return redirect("/login");
        }
    }

    public function shop_details(Request $request)
    {
        $user = Auth::user();
        $shop_data = Shop::with('addresses','bank_data','payment_types','comments')->where('id', $request->shop)->first();
		SEOMeta::setTitleDefault($shop_data->name.' - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        if(null !== $shop_data->addresses){
            $addresses  = $shop_data->addresses;
        }else{$addresses  = null;}
        if($shop_data->bank_data !== null){
            $bank_data  = $shop_data->bank_data;
        }else{$bank_data  = null;}
        if($shop_data->payment_types !== null){
            $payment_types  = $shop_data->payment_types;
            $payment_data = array();
            foreach ($payment_types as $payment_type){
                if($payment_type->type == 4){
                    $bank_data_for_payment = BankData::where('id', $payment_type->bank_data_id)->first();
                    $payment_data[$payment_type->id]['bank_data']['id']=$bank_data_for_payment->id;
                    $payment_data[$payment_type->id]['bank_data']['user_id']=$bank_data_for_payment->user_id;
                    $payment_data[$payment_type->id]['bank_data']['shop_id']=$bank_data_for_payment->shop_id;
                    $payment_data[$payment_type->id]['bank_data']['type']=$bank_data_for_payment->type;
                    $payment_data[$payment_type->id]['bank_data']['bank_name']=$bank_data_for_payment->bank_name;
                    $payment_data[$payment_type->id]['bank_data']['bank_address']=$bank_data_for_payment->bank_address;
                    $payment_data[$payment_type->id]['bank_data']['bank_bik']=$bank_data_for_payment->bank_bik;
                    $payment_data[$payment_type->id]['bank_data']['kor_account']=$bank_data_for_payment->kor_account;
                    $payment_data[$payment_type->id]['bank_data']['inn']=$bank_data_for_payment->inn;
                    $payment_data[$payment_type->id]['bank_data']['kpp']=$bank_data_for_payment->kpp;
                    $payment_data[$payment_type->id]['bank_data']['oktmo']=$bank_data_for_payment->oktmo;
                    $payment_data[$payment_type->id]['bank_data']['kbk']=$bank_data_for_payment->kbk;
                    $payment_data[$payment_type->id]['bank_data']['recipient']=$bank_data_for_payment->recipient;
                    $payment_data[$payment_type->id]['bank_data']['account']=$bank_data_for_payment->account;
                    $payment_data[$payment_type->id]['bank_data']['purpose']=$bank_data_for_payment->purpose;
                }
                $payment_data[$payment_type->id]['id']=$payment_type->id;
                $payment_data[$payment_type->id]['shop_id']=$payment_type->shop_id;
                $payment_data[$payment_type->id]['type']=$payment_type->type;
                $payment_data[$payment_type->id]['bank_data_id']=$payment_type->bank_data_id;
                $payment_data[$payment_type->id]['name']=$payment_type->name;
                $payment_data[$payment_type->id]['account']=$payment_type->account;
                $payment_data[$payment_type->id]['description']=$payment_type->description;
            }
        }else{$payment_data  = null;}
        return view('order.shopdetail')
            ->with(compact('messages', 'shop_data', 'addresses', 'bank_data', 'payment_data'));
    }

    public function shop_subscriptions()
    {
        if(Auth::check())
        {
			SEOMeta::setTitleDefault('Профиль - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            $messages = [];
            $user = Auth::user();
            $shop_data = Shop::with('subscriptions')->where('id', $user->connection_id)->first();
            $categories_tree = Category::all()->toHierarchy();
            if(null !== $shop_data->subscriptions){
                $subscription  = $shop_data->subscriptions->pluck('id')->toArray();
            }else{$subscription  = null;}
            return view('shop.shop_subscriptions')
                ->with(compact('messages', 'subscription', 'categories_tree'));
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_subscriptions_update(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();
            $shop_data->subscriptions()->sync($request->category);
            $request->session()->flash('alert-success', 'Успешно. Подписки изменены.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_bank_data_add(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();

            if($request->bank_data_equal == true)
            {
                for($i=1;$i<3;$i++)
                {
                    $bank_data = new BankData;
                    $bank_data->shop_id = $shop_data->id;
                    $bank_data->type = $i;
                    $bank_data->bank_name = $request->bank_data[1]['bank_name'];
                    $bank_data->bank_full_name = $request->bank_data[1]['bank_full_name'];
                    $bank_data->bank_address = $request->bank_data[1]['bank_address'];
                    $bank_data->bank_bik = $request->bank_data[1]['bank_bik'];
                    $bank_data->kor_account = $request->bank_data[1]['kor_account'];
                    $bank_data->account = $request->bank_data[1]['account'];
                    $bank_data->recipient = $request->bank_data[1]['recipient'];
                    $bank_data->save();
                }

            }
            else
            {
                for($i=1;$i<3;$i++)
                {
                    $bank_data = new BankData;
                    $bank_data->shop_id = $shop_data->id;
                    $bank_data->type = $i;
                    $bank_data->bank_name = $request->bank_data[$i]['bank_name'];
                    $bank_data->bank_full_name = $request->bank_data[$i]['bank_full_name'];
                    $bank_data->bank_address = $request->bank_data[$i]['bank_address'];
                    $bank_data->bank_bik = $request->bank_data[$i]['bank_bik'];
                    $bank_data->kor_account = $request->bank_data[$i]['kor_account'];
                    $bank_data->account = $request->bank_data[$i]['account'];
                    $bank_data->recipient = $request->bank_data[$i]['recipient'];
                    $bank_data->save();
                }
            }



            $request->session()->flash('alert-success', 'Платежные данные успешно добавлено.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }


    public function shop_bank_data_edit(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();
            for($i=1;$i<3;$i++)
            {
                $bank_data_update = BankData::
                where([
                    ['type', '=', $i],
                    ['shop_id', '=', $shop_data->id],
                ])
                    ->update([
                        'bank_name' => $request->bank_data[$i]['bank_name'],
                        'bank_full_name' => $request->bank_data[$i]['bank_full_name'],
                        'bank_address' => $request->bank_data[$i]['bank_address'],
                        'bank_bik' => $request->bank_data[$i]['bank_bik'],
                        'kor_account' => $request->bank_data[$i]['kor_account'],
                        'account' => $request->bank_data[$i]['account'],
                        'recipient' => $request->bank_data[$i]['recipient'],
                    ]);
            }


            $request->session()->flash('alert-success', 'Адреса успешно обновлены.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_data_update(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();

            $shop_data = Shop::
                where('id', $shop_data->id)
                ->update([
                    'name' => $request->input('shop_name'),
                    'le_name' => $request->input('le_name'),
                    'full_le_name' => $request->input('full_le_name'),
                    'director_fio' => $request->input('director_fio'),
                    //'inn' => $request->input('shop_inn'),
                    'kpp' => $request->input('kpp'),
                    //'ogrn' => $request->input('ogrn'),
                ]);


            $request->session()->flash('alert-success', 'Данные успешно обновлены.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_address_add(Request $request)
    {
        $user = Auth::user();
        $shop_data = Shop::where('id', $user->connection_id)->first();
        if($request->post_address_equal == true)
        for($i = 1;$i<3;$i++)
        {
            $address = new Address;

            $address->shop_id = $shop_data->id;
            $address->type = $i;
            $address->country = $request->input('country');
            $address->city = $request->address_data[1]['city'];
            $address->street = $request->address_data[1]['street'];
            $address->house = $request->address_data[1]['house'];
            $address->apartment = $request->address_data[1]['apartment'];
            $address->post_index = $request->address_data[1]['postcode'];

            $address->save();
        }
        else{
            for($i = 1;$i<3;$i++)
            {
                $address = new Address;

                $address->shop_id = $shop_data->id;
                $address->type = $i;
                $address->country = $request->input('country');
                $address->city = $request->address_data[$i]['city'];
                $address->street = $request->address_data[$i]['street'];
                $address->house = $request->address_data[$i]['house'];
                $address->apartment = $request->address_data[$i]['apartment'];
                $address->post_index = $request->address_data[$i]['postcode'];

                $address->save();
            }
        }


        $user->sync_need = true;

        $user->save();

//        $success = array(0 => 'Данные обновлены');
//        $messages = array('error' => $success);
//
//        $addresses        = $user->addresses;

        session()->flash('msg', 'Адрес добавлен.');
        return redirect()->back();
    }
    public function shop_address_edit(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();
            for($i=1;$i<3;$i++)
            {
                $address_data_update = Address::
                where([
                    ['type', '=', $i],
                    ['shop_id', '=', $shop_data->id],
                ])
                    ->update([
                        'country' => $request->address_data[$i]['country'],
                        'city' => $request->address_data[$i]['city'],
                        'street' => $request->address_data[$i]['street'],
                        'house' => $request->address_data[$i]['house'],
                        'apartment' => $request->address_data[$i]['apartment'],
                        'post_index' => $request->address_data[$i]['post_index'],
                    ]);
            }


            $request->session()->flash('alert-success', 'Адреса успешно обновлены.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_phone_update(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();

            $shop_phone = Shop::
            where('id', $shop_data->id)
                ->update([
                    'phone' => $request->input('shop_phone')
                ]);


            $request->session()->flash('alert-success', 'Телефон успешно обновлен.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }
    public function shop_payment_type_modal(Request $request)
    {
        if(Auth::check())
        {
            switch ($request->input('type')) {
                case 1:
                    return view('shop.payment_modal.type_1');
                    break;
                case 2:
                    return view('shop.payment_modal.type_2');
                    break;
                case 3:
                    return view('shop.payment_modal.type_3');
                    break;
                case 4:
                    $user = Auth::user();
                    $shop_data = Shop::where('id', $user->connection_id)->first();
                    $bank_data_list = BankData::where('shop_id', $shop_data->id)->get();
                    return view('shop.payment_modal.type_4')
                        ->with(compact('bank_data_list'));
                    break;
                case 5:
                    return view('shop.payment_modal.type_5');
                    break;
            }
        } else
        {
            return redirect("/login");
        }
    }
    public function payment_type_add(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $shop_data = Shop::where('id', $user->connection_id)->first();

            $bank_data = new PaymentType;
            $bank_data->shop_id = $shop_data->id;
            $bank_data->type = $request->input('type');
            $bank_data->bank_data_id = $request->input('bank_data_id');
            $bank_data->name = $request->input('name');
            $bank_data->account = $request->input('account');
            $bank_data->description = $request->input('description');
            $bank_data->save();

            $request->session()->flash('alert-success', 'Тип оплаты успешно добавлен.');
            return redirect('/shop_profile');
        } else
        {
            return redirect("/login");
        }
    }

}
