<?php

namespace App\Http\Controllers;

use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FileuploadController extends Controller
{
    public function form()
    {
        $tags = Tag::get();
        dd($tags);

        return view('fileupload.form');
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $timestamp = Carbon::now()->timestamp;
        $destinationPath = public_path().'/uploads/'.$year.'/'.$month.'/'.$day;
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }
        $filename = $file->getClientOriginalName();
        if (file_exists(''.$destinationPath.'/'.$filename.'')) {
            $basename = preg_replace('/\.\w+$/', '', $filename);
            $extension = $file->getClientOriginalExtension();
            $filename = $basename.'-'.$timestamp.'.'.$extension;
        }
        $upload_success = $file->move($destinationPath, $filename);
        if ($upload_success) {
            return response()->json(
                [
                    'success' => 200,
                ]
            );
        } else {
            return response()->json(
                [
                    'error' => 400,
                ]
            );
        }
    }
}
