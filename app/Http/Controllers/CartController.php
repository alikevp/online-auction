<?php

namespace App\Http\Controllers;

use App\cart;
use App\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SEO;
use SEOMeta;
use OpenGraph;

class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		SEOMeta::setTitleDefault('Корзина - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $cart_items = cart::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();
        if (count($cart_items) > 0) {
            $cart_array = array();
            foreach ($cart_items as $cart_item) {
                if (!isset(${'count'.$cart_item->product_id})) {
                    ${'count'.$cart_item->product_id} = 0;
                }
                ${'count'.$cart_item->product_id}++;
                $cart_array['uniqe'][] = $cart_item->product_id;
                $cart_array['count'][$cart_item->product_id] = ${'count'.$cart_item->product_id};
            }
            $products = Offer::
            whereIn('id', $cart_array['uniqe'])
				->distinct('id')
                ->orderBy('category_id', 'asc')
                ->get();
            $request->session()->put('products', $products);

            return view(
                'cart.index',
                [
                    'products' => $products,
                    'counts' => $cart_array['count'],
                ]
            );
        } else {
            return view('cart.index');
        }
    }

    public function in_cart_add(Request $request)
    {
        $unique_id = uniqid("cart_");
        $cart = new cart;
        $cart->id = $unique_id;
        $cart->enter_id = $_COOKIE['enter_id'];
        $cart->product_id = $request->input('product_id');

        $cart->save();
    }

    public function in_cart_del(Request $request)
    {
        $cart_del = cart::where(
            [
                ['enter_id', '=', $_COOKIE['enter_id']],
                ['product_id', '=', $request->input('product_id')],
            ]
        )
            ->delete();
    }

    public function in_cart_recount(Request $request)
    {
        $prodCurCount = $request->input('prodCurCount');
        $prodNewCount = $request->input('prodNewCount');
        var_dump($prodCurCount);
        echo "<br>";
        var_dump($prodNewCount);
        echo "<br>";
        if ($prodNewCount > $prodCurCount) {
            $limit = ($prodNewCount - $prodCurCount);
            for ($i = 1; $i <= $limit; $i++) {
                $unique_id = uniqid("cart_");
                $cart = new cart;
                $cart->id = $unique_id;
                $cart->enter_id = $_COOKIE['enter_id'];
                $cart->product_id = $request->input('product_id');

                $cart->save();
            }
        }
        if ($prodNewCount < $prodCurCount) {
            $limit = ($prodCurCount - $prodNewCount);
            if ($limit > 0) {
                $cart_del = DB::table('carts')
                    ->whereIn(
                        'id',
                        DB::table('carts')
                            ->where(
                                [
                                    ['enter_id', '=', $_COOKIE['enter_id']],
                                    ['product_id', '=', $request->input('product_id')],
                                ]
                            )
                            ->take($limit)
                            ->pluck('id')
                    )
                    ->delete();
            }
            if ($limit <= 0) {
                $cart_del = cart::where(
                    [
                        ['enter_id', '=', $_COOKIE['enter_id']],
                        ['product_id', '=', $request->input('product_id')],
                    ]
                )
                    ->delete();
            }
        }
        if ($prodNewCount == $prodCurCount) {
        }
    }

    public function in_cart_count(Request $request)
    {
        $cart_items = cart::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->count();
        echo $cart_items;
    }

    public function fast_order(Request $request)
    {
        $unique_id = uniqid("cart_");
        $cart = new cart;
        $cart->id = $unique_id;
        $cart->enter_id = $_COOKIE['enter_id'];
        $cart->product_id = $request->input('product_id');

        $cart->save();

        $cart_items = cart::
        where('enter_id', '=', $_COOKIE['enter_id'])
            ->orderBy('created_at', 'asc')
            ->get();


        #$cart_array = array();
        #foreach ($cart_items as $cart_item){
        #    if(!isset(${'count'.$cart_item->product_id})){
        #        ${'count'.$cart_item->product_id} = 0;
        #    }
        #    ${'count'.$cart_item->product_id}++;
        #    $cart_array['uniqe'][]=$cart_item->product_id;
        #    $cart_array['count'][$cart_item->product_id]=${'count'.$cart_item->product_id};
        #}
        $products = Offer::
        where('id', $request->input('product_id'))
            ->orderBy('category_id', 'asc')
            ->get();


        $request->session()->put('product', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
