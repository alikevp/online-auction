<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index(Request $request, $id)
    {
        $newComment = new Comment;

        if (Auth::check()) {
            $newComment->user_id = Auth::id();
            $newComment->user_name = Auth::user()->name;
            $newComment->user_mail = Auth::user()->email;
        } else {
            $newComment->user_id = "NULL";
            $newComment->user_name = $request->input('name');
            $newComment->user_mail = $request->input('email');
        }

        if ($request->input('rating') <= 5 && $request->input('rating') >= 1) {
            $newComment->id = uniqid("com_");
            $newComment->offer_id = $id;
            $newComment->advantages = $request->input('advantages');
            $newComment->disadvantages = $request->input('disadvantages');
            $newComment->text = $request->input('text');
            $newComment->ip = request()->ip();
            $newComment->rating = $request->input('rating');

            $newComment->save();
        }


        return redirect()->back();
    }

    public function shop_comment(Request $request)
    {
        $newComment = new Comment;

        if (Auth::check()) {
            $newComment->user_id = Auth::id();
            $newComment->user_name = Auth::user()->name;
            $newComment->user_mail = Auth::user()->email;
        } else {
            $newComment->user_id = null;
            $newComment->user_name = $request->input('name');
            $newComment->user_mail = $request->input('email');
        }

        if ($request->input('rating') <= 5 && $request->input('rating') >= 1) {
            $newComment->id = uniqid("com_");;
            $newComment->offer_id = null;
            $newComment->advantages = $request->input('advantages');
            $newComment->disadvantages = $request->input('disadvantages');
            $newComment->text = $request->input('text');
            $newComment->ip = request()->ip();
            $newComment->rating = $request->input('rating');
            $newComment->shop_id = $request->shop_id;

            $newComment->save();
        }


        return redirect()->back();
    }
}
