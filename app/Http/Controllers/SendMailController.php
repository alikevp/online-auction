<?php

namespace App\Http\Controllers;

use App\Mail\TestingMail;
use App\Offer;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Mail;

class SendMailController extends Controller
{
    public function index()
    {
        return view('partials.mail');
    }

    public function send(Request $request)
    {
        $email = $request->input('email');
        $user = User::all()->first();

        /* FOR OrderConfirmedToShop*/
        /*
        $orders = array();
        $orders['ord_1']['order_id'] = 'ord_1';
        $orders['ord_1']['products']['ordit_1']['name'] = 'Продукт некий 1';
        $orders['ord_1']['products']['ordit_2']['name'] = 'Продукт некий 2';
        $orders['ord_1']['products']['ordit_3']['name'] = 'Продукт некий 3';
        $orders['ord_2']['order_id'] = 'ord_2';
        $orders['ord_2']['products']['ordit_1']['name'] = 'Продукт некий 1';
        $orders['ord_2']['products']['ordit_2']['name'] = 'Продукт некий 2';
        $orders['ord_2']['products']['ordit_3']['name'] = 'Продукт некий 3';
        */
        /* FOR OrderConfirmedToShop END*/

        /* FOR orderOfferChangeToUser*/
        /*
        $orders = array();
        $orders['ord_1']['order_id'] = 'ord_1';
        $orders['ord_1']['shop_name'] = 'Магазин 1';
        $orders['ord_1']['products']['ordit_1']['name'] = 'Продукт некий 1';
        $orders['ord_1']['products']['ordit_2']['name'] = 'Продукт некий 2';
        $orders['ord_1']['products']['ordit_3']['name'] = 'Продукт некий 3';
        */
        /* FOR orderOfferChangeToUser END*/

        /* FOR orderStatusChangeToShop*/
        /*
        $orders = array();
        $orders['ord_1']['order_id'] = 'ord_1';
        $orders['ord_1']['status'] = 'Новый статус';
        $orders['ord_1']['products']['ordit_1']['name'] = 'Продукт некий 1';
        $orders['ord_1']['products']['ordit_2']['name'] = 'Продукт некий 2';
        $orders['ord_1']['products']['ordit_3']['name'] = 'Продукт некий 3';
        */
        /* FOR orderStatusChangeToShop END*/

        /* FOR orderNewOfferToUser*/
//        $orders = array();
//        $orders['ord_1']['order_id'] = 'ord_1';
//        $orders['ord_1']['shop_name'] = 'Магазин 1';
//        $orders['ord_1']['products']['ordit_1']['name'] = 'Продукт некий 1';
//        $orders['ord_1']['products']['ordit_2']['name'] = 'Продукт некий 2';
//        $orders['ord_1']['products']['ordit_3']['name'] = 'Продукт некий 3';
        /* FOR orderNewOfferToUser END*/

        $order = Order::all()->random(1)->first();
        $products = Offer::whereIn('id', $order->items->pluck('product_id'))->get();
//        dd($products->first()->limit_name);
        $url = route('orderDetails', ['order' => $order->id]);

        Mail::to($email)->send(new TestingMail($user, $order, $products, $url));

        return redirect()->back();
    }
}
