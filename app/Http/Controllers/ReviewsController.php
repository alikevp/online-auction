<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dealing;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use SEO;
use SEOMeta;
use OpenGraph;

class ReviewsController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dealing_id)
    {
		SEOMeta::setTitleDefault('Ваши отзывы - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $dealing = Dealing::where('id', $dealing_id)->first();

        if($dealing->user_id != Auth::user()->id)
        {
            return redirect('/');
        }

        return view('order.reviews')
            ->with(compact('dealing'));
    }

    public function send(Request $request, $dealing_id)
    {
        $dealing = Dealing::where('id', $dealing_id)->first();
        $user = Auth::user();

        if($dealing->user_id == $user->id)
        {
            $shopComment = new Comment;

            $shopComment->user_id = $user->id;
            $shopComment->user_name = $user->name;
            $shopComment->user_mail = $user->email;

            if($request->input('shop_rating') <= 5 && $request->input('shop_rating') >= 1)
            {
                $shopComment->id = uniqid("com_");;
                $shopComment->offer_id = null;
                $shopComment->advantages = $request->input('shop_advantages');
                $shopComment->disadvantages = $request->input('shop_disadvantages');
                $shopComment->text = $request->input('shop_text');
                $shopComment->ip = request()->ip();
                $shopComment->rating = $request->input('shop_rating');
                $shopComment->shop_id = $dealing->shop()->first()->id;
                $shopComment->tip = "shop";

                $shopComment->save();
            }

            $itemComment = new Comment;

            $itemComment->user_id = $user->id;
            $itemComment->user_name = $user->name;
            $itemComment->user_mail = $user->email;

            if($request->input('item_rating') <= 5 && $request->input('item_rating') >= 1)
            {
                $itemComment->id = uniqid("com_");;
                $itemComment->offer_id = $dealing->order()->first()->product()->first()->id;
                $itemComment->advantages = $request->input('item_advantages');
                $itemComment->disadvantages = $request->input('item_disadvantages');
                $itemComment->text = $request->input('item_text');
                $itemComment->ip = request()->ip();
                $itemComment->rating = $request->input('item_rating');
                $itemComment->tip = "item";

                $itemComment->save();
            }

            $siteComment = new Comment;

            $siteComment->user_id = $user->id;
            $siteComment->user_name = $user->name;
            $siteComment->user_mail = $user->email;

            if($request->input('site_rating') <= 5 && $request->input('site_rating') >= 1)
            {
                $siteComment->id = uniqid("com_");;
                $siteComment->offer_id = null;
                $siteComment->advantages = $request->input('site_advantages');
                $siteComment->disadvantages = $request->input('site_disadvantages');
                $siteComment->text = $request->input('site_text');
                $siteComment->ip = request()->ip();
                $siteComment->rating = $request->input('site_rating');
                $siteComment->tip = "site";

                $siteComment->save();
            }
            $request->session()->flash('alert-success', 'Отзывы сохранены. Спасибо за уделённое время.');
            return redirect()->back();
        } else {
            $request->session()->flash('alert-alert', 'Ошибка. Отзывы не сохранены. 
            Обновите страницу и попробуйте снова. Если ошибка повторится, пожалуйста, обратитесь в службу поддерки.');
            return redirect()->back();
        }
    }
}
