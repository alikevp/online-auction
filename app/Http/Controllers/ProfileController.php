<?php

namespace App\Http\Controllers;

use App\Address;
use App\Events\UserRegister;
use App\Mailsubscription;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use SEO;
use SEOMeta;
use OpenGraph;

class ProfileController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
			SEOMeta::setTitleDefault('Профиль - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            $messages = [];
            $user = Auth::user();

            $addresses = $user->addresses;

            // get photo for avatar of user
            $avatars = Storage::disk('avatar')->files();
            return view('auth.profile')
                ->with(compact('messages', 'addresses', 'avatars'));
        } else {
            return redirect("/login");
        }
    }

    public function personalDataEdit(Request $request)
    {
        $user = User::whereId(Auth::id())->firstOrFail();
        if (null == $request->dob or $request->dob == '') {
            $dob = Carbon::now();
        } else {
            $dob = $request->dob;
        }
        $dob = Carbon::parse($dob);
        $dob = $dob->toDateString();

        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->patronymic = $request->input('patronymic');
        $user->date_of_birth = $dob;
        $user->sex = $request->input('toggle');
        $user->inn = $request->input('inn');

        $user->sync_need = true;

        $user->save();

//        $success = array(0 => 'Данные обновлены');
//        $messages = array('error' => $success);


//        $addresses        = $user->addresses;

        session()->flash('msg', 'Данные обновлены.');

        return redirect()->back();

//        return redirect()->back()->with('success', ['Данные обновлены!']);
    }

    public function setAvatarUser(Request $request)
    {

        $user = User::whereId(Auth::id())->firstOrFail();

        $user->url_avatar = $request->input('ava');

        $user->save();

        return redirect()->back();

    }

    public function addressEdit(Request $request)
    {
        $user = Auth::user();

        $address = new Address;

        $address->country = $request->input('country');
        $address->city = $request->input('city');
        $address->office = $request->input('office');
        $address->street = $request->input('street');
        $address->house = $request->input('house');
        $address->apartment = $request->input('apartment');
        $address->post_index = $request->input('postcode');
        $address->user_id = $user->id;

        $address->save();

        $user->addresses()->attach($address);

        $user->sync_need = true;

        $user->save();

//        $success = array(0 => 'Данные обновлены');
//        $messages = array('error' => $success);
//
//        $addresses        = $user->addresses;

        session()->flash('msg', 'Адрес добавлен.');

        return redirect()->back();
    }

    /**
     * Обновляет данные адреса пользователя
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function addressUpdate(Request $request, $id)
    {
        $user = Auth::user();

        $address = Address::
        where(
            [
                ['id', '=', $id],
                ['user_id', '=', $user->id],
            ]
        )
            ->update(
                [
                    'country' => $request->input('country'),
                    'city' => $request->input('city'),
                    'office' => $request->input('office'),
                    'street' => $request->input('street'),
                    'house' => $request->input('house'),
                    'apartment' => $request->input('apartment'),
                    'post_index' => $request->input('post_index'),
                ]
            );

        session()->flash('msg', 'Адрес обновлён.');

        return redirect()->back();
    }

    /**
     * Удаляет адрес пользователя
     *
     * @param $id
     * @return mixed
     */
    public function addressDelete($id)
    {
        $user = Auth::user();

        $address = Address::
        where(
            [
                ['id', '=', $id],
                ['user_id', '=', $user->id],
            ]
        )->delete();

        $user->addresses()->detach($address);

        session()->flash('msg', 'Адрес удалён.');

        return redirect()->back();
    }

    public function telefoneEdit(Request $request)
    {
        $user = User::whereId(Auth::id())->firstOrFail();

        $user->telefone = $request->input('telefone');

        $user->save();

        $user->sync_need = true;

        $user->save();

//        $success = array(0 => 'Данные обновлены');
//        $messages = array('error' => $success);
//
//        $addresses        = $user->addresses;

        session()->flash('msg', 'Номер телефона обновлен.');

        return redirect()->back();
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'Пожалуйста, укажите текущий пароль.',
            'password.required' => 'Пожалуйста, введите пароль.',
            'password_confirmation.same' => 'Пароль подтверждён неверно.',
        ];

        $validator = Validator::make(
            $data,
            [
                'current-password' => 'required',
                'password' => 'required|same:password',
                'password_confirmation' => 'required|same:password',
            ],
            $messages
        );

        return $validator;
    }

    public function EmailDealEdit(Request $request)
    {
        $users = User::whereId(Auth::id())->firstOrFail();

        $user = User::find($users->id);
        $user->e_mail = $request->email_deal;
        $user->save();
        session()->flash('msg', 'E-mail изменен.');

        return redirect()->back();
    }

    public function passwordEdit(Request $request)
    {
        if (Auth::Check()) {
            $user = User::whereId(Auth::id())->firstOrFail();
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
//            $addresses        = $user->addresses;

            if ($validator->fails()) {
                //return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);

                $messages = $validator->getMessageBag()->toArray();

                //dd($messages);

                session()->flash('msg', 'Пароль не обновлен. Проверьте правильность введённых данных.');

                return redirect()->back();
            } else {
                $current_password = Auth::User()->password;
                if (Hash::check($request_data['current-password'], $current_password)) {
                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);
                    $obj_user->save();


                    $success = array(0 => 'Пароль успешно изменён.');
                    $messages = array('success' => $success);

                    //dd($messages);

                    session()->flash('msg', 'Пароль изменен.');

                    return redirect()->back();
                } else {
                    $error = array(0 => 'Пожалуйста, укажите правильный текущий пароль.');
                    //return response()->json(array('error' => $error), 400);
                    $messages = array('error' => $error);

                    //dd($messages);

                    session()->flash('msg', 'Пожалуйста, укажите правильный текущий пароль.');

                    return redirect()->back();
                }
            }
        } else {
            return redirect()->to('/');
        }
    }

    public function referrals()
    {
		SEOMeta::setTitleDefault('Мои приглашения - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $referrals = Auth::user()->referrals;

        return view('partials.profile.referrals')
            ->with(compact('referrals'));
    }

    public function confirmEmail()
    {

        return view('auth.confirm_failed');
    }


    protected function sendVerification()
    {
        $user = Auth::user();


        event(new UserRegister($user));

        return redirect()->back();
    }

    /**
     * @return mixed
     *
     * Подписка на рассылку
     */
    public function mail_subscription()
    {
        if (Auth::check()) {
            $user = Auth::user();
			SEOMeta::setTitleDefault('Подписки - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            if ($user->type == 'buyer') {
                $user = User::with('mailable')->where('id', $user->id)->first();
                $active_subscription = $user->mailable->pluck('id')->toArray();
                $mail_subscription = Mailsubscription::get();
                foreach ($mail_subscription as $item) {
                    $explode = explode('_', $item->lable);

                }

                $mail_subscription = Mailsubscription::where('lable', 'LIKE', '%'.$explode[0].'%')->get();

                return view('auth.mail_subscription')
                    ->with(compact('mail_subscription', 'active_subscription'));
            } else {
                $user = User::with('mailable')->where('id', $user->id)->first();
                $active_subscription = $user->mailable->pluck('id')->toArray();
                $mail_subscriptions = Mailsubscription::get();
                foreach ($mail_subscriptions as $item) {
                    $explode[] = explode('_', $item->lable);
                }
                foreach ($explode as $item) {
                    $a = $item;
                    if (strpos($a[0], 'buyer') == false) {
                        $mail_subscription = Mailsubscription::where('lable', 'LIKE', '%'.$a[0].'%')->get();

                        return view('auth.mail_subscription')
                            ->with(compact('mail_subscription', 'active_subscription'));
                    }
                }


            }

        } else {
            return redirect("/login");
        }
    }

    public function mail_subscription_sync(Request $request)
    {

        if (Auth::check()) {
            $user = Auth::user();

            $mail_subscriptions = array();
            if (isset($request->subscription)) {
                foreach ($request->subscription as $key => $value) {
                    #$mail_subscriptions[] = $key;
                    $mail_subscriptions[$key] = ['hash' => str_random(60)];
                }
            }
            $user->mailable()->sync($mail_subscriptions);
            #$user->mailable()->sync($mail_subscriptions);

            $request->session()->flash('alert-success', 'Успешно. Подписки обновлены.');

            return redirect('/mail_subscription');
        } else {
            return redirect("/login");
        }
    }


    /**
     * @param $hash
     *
     * Отписка от рассылки
     */

    public function mail_unsubscription($hash)
    {
		SEOMeta::setTitleDefault('Подписки - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $type = DB::table('mailsubscription_user')->where('hash', $hash)->get();
        #dd($type);
        $mailsubscription = Mailsubscription::where('id', $type[0]->mailsubscription_id)->get();

        #dd($mailsubscription);

        return view('auth.mail_unsubscription', compact('mailsubscription', 'hash'));
    }

    public function mail_unsubscription_post($hash)
    {


        $subs = DB::table('mailsubscription_user')->where('hash', $hash)->get();
        $user = User::find($subs[0]->user_id);
        #dd($subs);
        #$subs1 = Mailsubscription::find($subs->id);
        foreach ($subs as $item) {
            $user->mailable()->detach([$item->mailsubscription_id]);
        }

        return redirect('/');
    }
}
