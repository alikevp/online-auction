<?php

namespace App\Http\Controllers\Auth;

use App\Events\Shops\Register;
use App\Events\UserRegister;
use App\Mailsubscription;
use Auth;
use App\Http\Requests\RegisterRequest;
use App\Mail\VerifyEmail;
use App\User;
use App\Address;
use App\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Mail;

class RegisterController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError']]);
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'patronymic' => 'max:255',
            'telefone' => 'max:55',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function adminRegister(RegisterRequest $request)
    {
        $user = $this->create($request->all());

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param $user
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @internal param \App\Http\Requests\RegisterRequest $request
     *
     */
    protected function sendVerification($user)
    {
        event(new UserRegister($user));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \App\Http\Requests\RegisterRequest $request
     *
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function register(RegisterRequest $request)
    {
        $this->createUser($request);

        return redirect()->to('confirm_request');
    }

    public function createUser($user)
    {

        $verification_token = str_random(50);
        if(null == $user->date_of_birth OR $user->date_of_birth == ''){
            $dob = Carbon::now();
        }
        else{
            $dob = $user->date_of_birth;
        }
        $dob = Carbon::parse($dob);
        $dob = $dob->toDateString();

        $new_user = User::create([
            'name' => $user->name,
            'surname' => $user->surname,
            'patronymic' => $user->patronymic,
            'telefone' => $user->telefone,
            'email' => $user->email,
            'e_mail' => $user->email,
            'password' => bcrypt($user->password),
            'type' => 'buyer',
            'referred_by' => $user->referred_by,
            'verification_token' => $verification_token,
            'date_of_birth' => $dob,
            'url_avatar' => '/default/avatars/avatar-1.jpg',
            'sex' => $user->sex,
            'sync_need' => true,
        ]);

        //Задаем подписки
        $subsc = User::find($new_user->id);
        $mail_type = Mailsubscription::all();
        $mail_subscriptions = array();
        foreach ($mail_type as $subscription) {
            $lable_explode = explode("_", $subscription->lable);
                if($lable_explode[0]== 'buer') {
                    $mail_subscriptions[] = ['mailsubscription_id' => $subscription->id, 'hash' => str_random(60)];
                }
            }
        $subsc->mailable()->attach($mail_subscriptions);
        //Задаем подписки -- конец

        $this->sendVerification($new_user);

        $address = new Address;
        $address->city = $user->city;
        $address->user_id = $new_user->id;
        $address->save();



    }

    public function register_shop(RegisterRequest $request)
    {
		$checkInn = Shop::where('inn', $request->shop_inn)->count();
		if($checkInn>0){
			return redirect()->back()->withErrors(['В системе уже зарегистрирован магазин с введенным ИНН.']);
		}else{
			if(null == $request->date_of_birth OR $request->date_of_birth == ''){
				$dob = Carbon::now();
			}
			else{
				$dob = $request->date_of_birth;
			}
			$dob = Carbon::parse($dob);
			$dob = $dob->toDateString();

			$verification_token = str_random(50);
			$current_time = Carbon::now();
			$till_time = $current_time->addYears(1);
			$shop_unique_id = uniqid("shop_");
			$shop = Shop::create([
				'id'              => $shop_unique_id,
				'slug'              => str_slug($request->shop_name),
				'name'              => $request->le_name,
				'activity'          => false,
				'e_mail'            => $request->shop_email,
				'le_name'           => $request->le_name,
				'full_le_name'      => $request->full_le_name,
				'contract_type'     => 1,
				'contract_award'    => 3,
				'nds'               => 0,
				'active_till'       => $till_time,
				'inn'               => $request->shop_inn,
				'kpp'               => $request->kpp,
				'ogrn'              => $request->ogrn,
				'director_fio'      => $request->director_fio,
				'phone'             => $request->shop_phone,
				'curator_fio'       => null,
				'curator_id'        => null,
				'sync_need'         => true,
				'registrated_at'    => $request->shop_registered
			]);

			$user = User::create([
				'name' => $request->name,
				'surname' => $request->surname,
				'patronymic' => $request->patronymic,
				'telefone' => $request->telefone,
				'email' => $request->email,
				'e_mail' => $request->email,
				'password' => bcrypt($request->password),
				'verification_token' => $verification_token,
				'type' => 'shop_admin',
				'connection_id' => $shop->id,
				'date_of_birth' => $dob,
				'url_avatar' => '/default/avatars/Default-avatar.jpg',
				'sex' => $request->sex,
				'sync_need' => true,
			]);

			event(new Register($shop,$user));
			//Задаем подписки
			$subsc = User::find($user->id);
			$mail_type = Mailsubscription::all();
			$mail_subscriptions = array();
			foreach ($mail_type as $subscription) {
				$lable_explode = explode("_", $subscription->lable);
				if($lable_explode[0]== 'shop') {
					$mail_subscriptions[] = ['mailsubscription_id' => $subscription->id, 'hash' => str_random(60)];
				}
			}
			$subsc->mailable()->attach($mail_subscriptions);
			//Задаем подписки -- конец

			$this->sendVerification($user);

			return redirect()->to('confirm_request');
		}
    }

    public function verify($token)
    {
        $user = User::where('verification_token', $token)->first();

        if (!$user) {
            die();
        }

        $user->verified = true;
        $user->verification_token = null;
        $user->save();

        return redirect()->to('confirmsuccess');
    }

    public function confirmsuccess()
    {
        return view('auth.confirm_success');
    }

    public function confirmRequest()
    {
        return view('auth.confirm_request');
    }

    public function registerReferral($referred_by)
    {
        return view('auth.register')
            ->with(compact('referred_by'));
    }
}
