<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use SEO;
use SEOMeta;
use OpenGraph;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function postLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'activated' => 1])) {
            return redirect()->back();
        }

        return redirect()->to('auth/login');
    }


    public function __construct()
    {
		if (isset($_GET['company'])) {
			SEOMeta::setTitleDefault('Авторизация для юридических лиц - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setCanonical(URL::current().'?company');
			OpenGraph::setTitle('Авторизация для юридических лиц');
			OpenGraph::setUrl(URL::current().'?company');
		} else {
			SEOMeta::setTitleDefault('Авторизация - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setCanonical(URL::current());
			OpenGraph::setTitle('Авторизация');
			OpenGraph::setUrl(URL::current());
		}
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
		SEOMeta::addKeyword(['интернет магазин', 'электронный аукцион', 'низкие цены', 'скидка', 'акции']);
		OpenGraph::setSiteName('Джинмарт');
		OpenGraph::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.'); 
		OpenGraph::addProperty('type', 'website');
		OpenGraph::addProperty('locale', 'ru_RU'); 
		OpenGraph::addProperty('locale:alternate', ['en-us']);
		OpenGraph::addImage(URL::to('/images/jinnmart-logo.png'));
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        session_start();
        unset($_SESSION['file_access']);

        return redirect('/');
    }
}
