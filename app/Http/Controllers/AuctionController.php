<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dealing;
use App\Events\Order\OfferChanged;
use App\Events\Order\OfferNew;
use App\Order;
use App\Order_delivery;
use App\Order_offer_delivery;
use App\Order_offer_group;
use App\Order_offer_option;
use App\Shop;
use App\Shop_subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Mail;
use phpDocumentor\Reflection\Types\Null_;
use SEO;
use SEOMeta;
use OpenGraph;

class AuctionController extends Controller
{
    public function savebet(Request $request)
    {
        if (Auth::check()) {
            $user = User::where('id', Auth::user()->id)->with('shop')->first();

            $group_exist = Order_offer_group::where('order_id', $request->input('order_id'))
                ->where('shop_id', $user->connection_id)
                ->first();

            if (!isset($group_exist)) {
                $OfferGroup_unique_id = uniqid("offgr_");
                $OfferGroup = new Order_offer_group;
                $OfferGroup->id = $OfferGroup_unique_id;
                $OfferGroup->order_id = $request->input('order_id');
                $OfferGroup->shop_id = $user->connection_id;
                $OfferGroup->price = $request->input('price');
                $OfferGroup->old_price = 0;
                $OfferGroup->quantity = $request->input('quantity');
                $OfferGroup->offer_id = $request->input('offer_id');
                $OfferGroup->save();
                if (null !== $request->input('delivery_data')) {
                    $delivery_data = $request->input('delivery_data');
                    foreach ($delivery_data as $delivery) {
                        $OfferDelivery_unique_id = uniqid("offdel_");
                        $OfferDelivery = new Order_offer_delivery;
                        $OfferDelivery->id = $OfferDelivery_unique_id;
                        $OfferDelivery->group_id = $OfferGroup_unique_id;
                        $OfferDelivery->type = $delivery['delivery_type'];
                        $OfferDelivery->description = $delivery['delivery_description'];
                        $OfferDelivery->delivery_time = $delivery['delivery_time'];
                        $OfferDelivery->save();
                    }
                }
                if (null !== $request->input('gifts_data')) {
                    $gifts_data = $request->input('gifts_data');
                    foreach ($gifts_data as $gift) {
                        $OfferOption_unique_id = uniqid("offopt_");
                        $OfferOption = new Order_offer_option;
                        $OfferOption->id = $OfferOption_unique_id;
                        $OfferOption->group_id = $OfferGroup_unique_id;
                        $OfferOption->type = $gift['option_type'];
                        $OfferOption->name = $gift['option_name'];
                        $OfferOption->description = $gift['option_description'];
                        $OfferOption->save();
                    }
                }
                $order = Order::with('user')->where('id', $OfferGroup->order_id)->first();
                event(new OfferNew($user, $order, $OfferGroup));
                $request->session()->flash('alert-success', 'Ставка успешно сделана!');

                return redirect('/auction/order?order='.$request->input('order_id'));
            }
        }
    }

    public function deliveryCreate(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $order = Order_offer_group::where(
                [
                    ['id', '=', $request->input('groupId')],
                ]
            )->first();

            $groupShopId = $order->shop_id;

            if ($groupShopId == $user->connection_id) {
                $OfferDelivery_unique_id = uniqid("offdel_");
                $OfferDelivery = new Order_offer_delivery;
                $OfferDelivery->id = $OfferDelivery_unique_id;
                $OfferDelivery->group_id = $request->input('groupId');
                $OfferDelivery->type = $request->input('delivery_type');
                $OfferDelivery->description = $request->input('delivery_description');
                $OfferDelivery->delivery_time = $request->input('delivery_time');
                $OfferDelivery->save();
                $request->session()->flash('alert-success', 'Доставка успешно добавлена.');

                return redirect('/auction/order?order='.$order->order_id)->with('message', 'Ставка успешно изменена');
            }
        }
    }

    public function giftDelete($id)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $gift = Order_offer_option::where(
                [
                    ['id', '=', $id],
                ]
            )->first();

            //dd($gift->group_id);

            $giftCreaterShopId = Order_offer_group::where(
                [
                    ['id', '=', $gift->group_id],
                ]
            )->first()->shop_id;

            if ($giftCreaterShopId == $user->connection_id) {
                $gift->delete();

                return redirect()->back();
            }
        }
    }

    public function giftCreate(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $order = Order_offer_group::where(
                [
                    ['id', '=', $request->input('groupId')],
                ]
            )->first();

            $groupShopId = $order->shop_id;

            if ($groupShopId == $user->connection_id) {
                $OfferOption_unique_id = uniqid("offopt_");
                $OfferOption = new Order_offer_option;
                $OfferOption->id = $OfferOption_unique_id;
                $OfferOption->group_id = $request->input('groupId');
                $OfferOption->type = $request->input('option_type');
                $OfferOption->name = $request->input('option_name');
                $OfferOption->description = $request->input('option_description');
                $OfferOption->save();
                $request->session()->flash('alert-success', 'Подарок успешно добавлен.');

                return redirect('/auction/order?order='.$order->order_id)->with('message', 'Ставка успешно изменена');
            }
        }
    }

    public function deliveryDelete($id)
    {
        if (Auth::check()) {
            $user = Auth::user();

            $delivery = Order_offer_delivery::where(
                [
                    ['id', '=', $id],
                ]
            )->first();

            $deliveryCreaterShopId = Order_offer_group::where(
                [
                    ['id', '=', $delivery->group_id],
                ]
            )->first()->shop_id;

            if ($deliveryCreaterShopId == $user->connection_id) {
                $delivery->delete();

                return redirect()->back();
            }
        }
    }

    public function saveeditedbet(Request $request)
    {
        if (Auth::check()) {
            $user = User::where('id', Auth::user()->id)->with('shop')->first();
            $OfferGroup_unique_id = uniqid("offgr_");

            $OfferGroupUpdate = Order_offer_group::
            where(
                [
                    ['id', '=', $request->input('group_id')],
                    ['shop_id', '=', $user->connection_id],
                ]
            )->first();
            $order = Order::with('user')->where('id', $OfferGroupUpdate->order_id)->first();

            if ($request->input('price') <= $OfferGroupUpdate->price) {//ЕСЛИ новая цена менее старой или равна
                if ($OfferGroupUpdate->status == 0) {// если предложение принято пердложение принято

                    //dd($order);

                    if ($OfferGroupUpdate->price != $request->input('price')) {
                        $OfferGroupUpdate->old_price = $OfferGroupUpdate->price;
                    }

                    $OfferGroupUpdate->price = $request->input('price');

                    $OfferGroupUpdate->save();

                    //                ->update(array(
                    //                    'old_price' => this->price,
                    //                    'price' => $request->input('price'),
                    //                ));
                    //            foreach ($request->input('products') as $product_item){
                    //                $OfferItemUpdate = Order_offer_item::
                    //                where([
                    //                    ['id', '=', $product_item['offeritem_id']],
                    //                    ['group_id', '=', $request->input('group_id')],
                    //                ])
                    //                    ->update(array(
                    //                        'quantity' => $product_item['quantity'],
                    //                    ));
                    //            }

                    //dd($request->input());

                    if ($request->input() != "NULL") {
                        if ($request->input('delivery') != "NULL") {
                            $deliverys = $request->input('delivery');
                            foreach ((array)$deliverys as $delivery_item) {
                                $OfferDeliveryUpdate = Order_offer_delivery::
                                where(
                                    [
                                        ['id', '=', $delivery_item['delivery_id']],
                                        ['group_id', '=', $request->input('group_id')],
                                    ]
                                )
                                    ->update(
                                        [
                                            'type' => $delivery_item['type'],
                                            'description' => $delivery_item['description'],
                                            'delivery_time' => $delivery_item['delivery_time'],
                                        ]
                                    );
                            }
                        }

                        if ($request->input('options') != "NULL") {
                            $options = $request->input('options');
                            foreach ((array)$options as $option_item) {
                                $OfferOpptionUpdate = Order_offer_option::
                                where(
                                    [
                                        ['id', '=', $option_item['option_id']],
                                        ['group_id', '=', $request->input('group_id')],
                                    ]
                                )
                                    ->update(
                                        [
                                            'type' => $option_item['type'],
                                            'name' => $option_item['name'],
                                            'description' => $option_item['description'],
                                        ]
                                    );
                            }
                        }
                    }

//                    Mail::to($user->email)->send(new OrderOfferChangeToShop($user, $order, $OfferGroupUpdate));
//                    Mail::to($order->user->email)->send(new OrderOfferChangeToUser($user, $order, $OfferGroupUpdate));
                    event(new OfferChanged($user, $order, $OfferGroupUpdate));

                    $otherGroups = Order_offer_group::
                    where(
                        [
                            ['shop_id', '!=', $user->connection_id],
                            ['order_id', $order->id],
                        ]
                    )->get();

                    $request->session()->flash(
                        'alert-success',
                        'Успешно. Покупатель оповещен 
                        об изменениях в предложении.'
                    );

                    return redirect('/auction/order?order='.$order->id);
                } else {
                    $request->session()->flash(
                        'alert-warning',
                        'Ошибка. Ваше предложение уже принято, 
                        его нельзя изменить!'
                    );

                    return redirect('/auction/order?order='.$order->id);
                }
            } else {
                $request->session()->flash(
                    'alert-warning',
                    'Ошибка. Можно указывать цену 
                    только ниже предыдущей!'
                );

                return redirect('/auction/order?order='.$order->id);
            }
        } else {
        }
    }

    public function editbet(Request $request)
    {
        if (Auth::check()) {
			SEOMeta::setTitleDefault('Изменение предложения - Джинмарт-Электронная торговая площадка потребителей');
			SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
            $shop = Shop::where('id', Auth::user()->connection_id)->first();
            $my_bet = Order_offer_group::with('deliveries', 'gifts', 'shops')
                ->where(
                    [
                        ['id', $request->offergroup],
                        ['shop_id', $shop->id],
                    ]
                )->first();
            $orders = Order::with('product')->find($my_bet->order_id);

            return view('auction.editbet')
                ->with(compact('orders', 'shop', 'my_bet'));
        } else {
			return redirect('/');
        }
    }

    public function orderlist(Request $request)
    {
		SEOMeta::setTitleDefault('Список заказов из подписки - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $user = Auth::user();
        $shop_data = Shop::with('subscriptions')->where('id', '=', $user->connection_id)->first();
        $subscription_list = $shop_data->subscriptions;
        $hide_order = $user->refusedOrders()->pluck('id');
        $orders = Order::with('delivery', 'product', 'order_offer_groups')
            ->whereIn('category_id', $shop_data->subscriptions->pluck('id'))
            ->whereNotIn('id', $hide_order)
            ->whereNotIn(
                'id',
                Order_offer_group::where(
                    'shop_id',
                    $user->connection_id
                )
                    ->pluck('order_id')
            )
            ->when(
                $request->status == null,
                function ($query) use ($request) {
                    return $query->where('status', '=', 1);
                }
            )
            ->getStatus($request->status)
//            ->when(
//                $request->status,
//                function ($query) use ($request) {
//                    return $query->whereIn('status', $request->status);
//                }
//            )
            ->when(
                $request->city != '',
                function ($query) use ($request) {
                    return $query->whereIn(
                        'id',
                        Order_delivery::where(
                            'city',
                            'ilike',
                            '%'.$request->city.'%'
                        )
                            ->pluck('order_id')
                    );
                }
            )
            ->when(
                $request->id != '',
                function ($query) use ($request) {
                    return $query->where('id', 'ilike', '%'.$request->id.'%');
                }
            )
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('auction.orderlist')
            ->with(compact('orders', 'subscription_list'));
    }

    public function hided_products(Request $request)
    {
		SEOMeta::setTitleDefault('Скрытые заказы - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $user = Auth::user();
        $shop_data = Shop::with('subscriptions')
            ->where('id', '=', $user->connection_id)
            ->first();
        $subscription_list = $shop_data->subscriptions;
        $hide_order = $user->refusedOrders()->pluck('id');
        $orders = Order::with('delivery', 'product')
            ->whereIn('id', $hide_order)
            ->when(
                $request->city != '',
                function ($query) use ($request) {
                    return $query->whereIn(
                        'id',
                        Order_delivery::where(
                            'city',
                            'ilike',
                            '%'.$request->city.'%'
                        )
                            ->pluck('order_id')
                    );
                }
            )
            ->when(
                $request->id != '',
                function ($query) use ($request) {
                    return $query->where('id', 'ilike', '%'.$request->id.'%');
                }
            )
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('auction.hided_products')
            ->with(compact('orders', 'subscription_list'));
    }

    public function hideProduct(Request $request)
    {
        $user_id = Auth::id();
        $order_id = $request->input('order_id');
        $order = new Order();
        $order->refusedUsers()->attach($order_id, $user_id);

        $request->session()->flash('alert-success', 'Успешно. Заказ скрыт!');

        return redirect('/auction/orderlist');
    }

    public function unhideProducts(Request $request)
    {
        $order = Order::find($request->input('order_id'));
        $order->refusedUsers()->detach();

        $request->session()->flash(
            'alert-success',
            'Успешно. Отображение заказа возвращено!'
        );

        return redirect('/auction/hided_products');
    }

    public function orderdetails(Request $request)
    {
		SEOMeta::setTitleDefault('Подробности аукциона - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $orders = Order::with('delivery', 'product')
            ->find($request->input('order'));
        $user = User::with('addresses')
            ->where('id', $orders->user_id)->first();
        $cnctId = Auth::user()->connection_id;
        if (strlen($cnctId) > 0) {
            $shop = Shop::where('id', $cnctId)->first();
            $bets = Order_offer_group::with('deliveries', 'gifts', 'shops')->where(
                [
                    ['order_id', $request->input('order')],
                    ['shop_id', '!=', $shop->id],
                ]
            )
                ->orderBy('status', 'desc')
                ->orderBy('price', 'ASC')
                ->get();
            $my_bets = Order_offer_group::with('deliveries', 'gifts', 'shops')->
            where(
                [
                    ['order_id', $request->input('order')],
                    ['shop_id', $shop->id],
                ]
            )->first();
        } else {
            $shop = 'null';
            $bets = Order_offer_group::with('deliveries', 'gifts', 'shops')
                ->where('order_id', $request->input('order'))
                ->orderBy('status', 'desc')
                ->orderBy('price', 'ASC')
                ->get();
            $my_bets = Order_offer_group::with('deliveries', 'gifts', 'shops')
                ->where('order_id', $request->input('order'))->first();
        }

        $time_last = Carbon::now()->diff($orders->active_till);
        $time_last = ''.$time_last->d.'д '.$time_last->h
            .'ч '.$time_last->m.'м '.$time_last->s.'с';

        return view('auction.orderdetails')
            ->with(
                compact(
                    'orders',
                    'shop',
                    'bets',
                    'my_bets',
                    'user',
                    'time_last'
                )
            );
    }

    public function myauctions(Request $request)
    {
		SEOMeta::setTitleDefault('Подробности аукциона - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $user = Auth::user();
        $subscription_list = Shop_subscription::
        where('shop_id', '=', $user->connection_id)
            ->pluck('category_id');

        $subscription_list = Category::whereIn(
            'id',
            $subscription_list
        )->distinct()->get();

        $orders = Order::with('delivery', 'product', 'order_offer_groups')
            ->whereIn(
                'id',
                Order_offer_group::where(
                    'shop_id',
                    '=',
                    $user->connection_id
                )
                    ->pluck('order_id')
            )
            ->getStatus($request->status)
//            ->when(
//                $request->status,
//                function ($query) use ($request) {
//                    return $query->whereIn('status', $request->status);
//                }
//            )
            ->when(
                $request->city != '',
                function ($query) use ($request) {
                    return $query->whereIn(
                        'id',
                        Order_delivery::where(
                            'city',
                            'ilike',
                            '%'.$request->city.'%'
                        )
                            ->pluck('order_id')
                    );
                }
            )
            ->when(
                $request->id != '',
                function ($query) use ($request) {
                    return $query->where('id', 'ilike', '%'.$request->id.'%');
                }
            )
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('auction.myauctions')
            ->with(compact('orders', 'subscription_list'));
    }

    public function hideProducts(Request $request)
    {
        $order = Order::find($request->input('order_id'));
        $user_id = Auth::id();
        $order->refusedUsers()->attach($user_id);
        $request->session()->put('order_hides', $order->id);

        return \Redirect::back()->with(compact('order_hides'));

    }

    public function accepted(Request $request)
    {
		SEOMeta::setTitleDefault('Подробности аукциона - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $user = Auth::user();
        $subscription_list = Shop_subscription::
        where('shop_id', '=', $user->connection_id)
            ->pluck('category_id');
        $subscription_list = Category::whereIn(
            'id',
            $subscription_list
        )->distinct()->get();
        $orders = Order::with('delivery', 'product', 'order_offer_groups')
            ->whereIn(
                'id',
                Order_offer_group::where(
                    'shop_id',
                    '=',
                    $user->connection_id
                )
                    ->pluck('order_id')
            )
            ->whereIn(
                'id',
                Order_offer_group::where(
                    'shop_id',
                    '=',
                    $user->connection_id
                )
                    ->where('status', '=', 1)
                    ->pluck('order_id')
            )
            ->when(
                $request->city != '',
                function ($query) use ($request) {
                    return $query->whereIn(
                        'id',
                        Order_delivery::where(
                            'city',
                            'ilike',
                            '%'.$request->city.'%'
                        )
                            ->pluck('order_id')
                    );
                }
            )
            ->when(
                $request->id != '',
                function ($query) use ($request) {
                    return $query->where('id', 'ilike', '%'.$request->id.'%');
                }
            )
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('auction.myauctions')
            ->with(compact('orders', 'subscription_list'));
    }

    /**
     * Возвращает кол-во доступных магазину аукционов.
     * Это кол-во пишется в сайдбар в кабинете магазина.
     *
     * @return int
     */
    public function getOrderCounter()
    {
        $user = Auth::user();
        $shop_data = Shop::with('subscriptions')
            ->where('id', '=', $user->connection_id)
            ->first();
        $hide_order = $user->refusedOrders()->pluck('id');


//        $order_list = Order_item::with('order')->whereIn('category_id', $subscription_list)
//            ->whereNotIn('id', $hide_order)
//            ->whereHas('order', function ($q) {
//                $q->where('status', 1);
//            });
        $order_list = Order::where('status', '=', 1)
            ->whereNotIn('id', $hide_order)
            ->whereIn('category_id', $shop_data->subscriptions->pluck('id'))
            ->whereNotIn(
                'id',
                Order_offer_group::where(
                    'shop_id',
                    '=',
                    $user->connection_id
                )
                    ->pluck('order_id')
            )
            ->count();

        return $order_list;
    }

    /**
     * Возвращает кол-во выигранных магазином аукционов.
     * Это кол-во пишется в сайдбар в кабинете магазина.
     *
     * @return int
     */
    public function getWinCounter()
    {
        $user = Auth::user();
        $dealings = Dealing::where('shop_id', $user->connection_id)->count();

        return $dealings;
    }
}