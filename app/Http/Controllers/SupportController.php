<?php

namespace App\Http\Controllers;

use App\Mail\OrderExpiredToShop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Mail;
use App\User;
use App\Problem;
use App\Term;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;

use App\Mail\OrderNewOfferToUser;
use App\Mail\OrderNewOfferToShop;
use App\Mail\OrderOfferChangeToShop;
use App\Mail\OrderOfferChangeToShops;
use App\Mail\OrderOfferChangeToUser;
use App\Mail\OrderConcurentOfferToShops;
use Carbon\Carbon;


class SupportController extends Controller
{
    public function save_request(Request $request)
    {
        $NewRequest = new problem;

        if(Auth::check()) { $NewRequest->user_id = Auth::user()->id; } else ( $NewRequest->user_id = null );
        $NewRequest->user_name = $request->middle.' '.$request->name;
        $NewRequest->email = $request->email;
        $NewRequest->tel = $request->tel;


        $type = Term::where('label', $request->type)->first();
        $NewRequest->term_id = $type->id;

        $NewRequest->theme = $request->theme;
        $NewRequest->message = $request->message;

        $NewRequest->save();

        if(isset($NewRequest->id)){
            return '<div class="alert alert-success" role="alert">Сообщение успешно отправлено. В ближайшее время вам ответят.</div>';
        }
        else{
            return '<div class="alert alert-danger" role="alert">Ошибка сохранения сообщения</div>';
        }
    }
}


