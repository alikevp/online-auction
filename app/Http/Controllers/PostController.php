<?php

namespace App\Http\Controllers;

use App\Repositories\Posts;
use App\Posts\Helpers\PostHelper;

class PostController extends Controller
{
    protected $posts;

    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    /** Вывод страницы review
     *
     * @param $slug
     * @return $this
     */
    public function review($slug)
    {
        $post = $this->posts->getPostWithCategory('review', $slug);

        if (!isset($post)) {
            abort(404);
        }

        return view('cms.post.single.review')
            ->with(compact('post'));
    }

    /** Вывод всех страниц review
     *
     * @return mixed
     */
    public function reviews()
    {
        $posts = $this->posts->getTags('review', 'footer-col-1');

        return view('cms.post.category.reviews')
            ->with(compact('posts'));
    }

    /** Выод страницы page
     *
     * @param $slug
     * @return $this
     */
    public function page($path)
    {


        if (strpos($path, '/') == false) {
            $slug = $path;
        } else {
            $url_elements = explode("/", $path);
            $slug = array_pop($url_elements);
        }

        $args = array(
            'slug' => $slug,
            'post_type' => 'page',
        );
        $post = PostHelper::get_post($args);

        if (!isset($post)) {
            abort(404);
        }

        return view('cms.post.page.' . $post->template)
            ->with(compact('post'));
    }

    /** Вывод всех страниц page
     * @return mixed
     */
    public function pages()
    {
        $posts = $this->posts->getCategories('page');

//        dd($this->posts->getTags($posts))

        return view('cms.post.category.pages')
            ->with(compact('posts'));
    }

    /** Выод страницы article
     *
     * @param $slug
     * @return $this
     */
    public function article($slug)
    {
        $post = $this->posts->getPostWithCategory('article', $slug);

        if (!isset($post)) {
            abort(404);
        }

        return view('cms.post.single.article')
            ->with(compact('post'));
    }

    /** Вывод всех страниц articles
     *
     * @return mixed
     */
    public function articles()
    {
        $posts = $this->posts->getCategories('articles');

        return view('cms.post.category.articles')
            ->with(compact('posts'));
    }

    /** Вывод страницы instruction
     *
     * @param $slug
     * @return $this
     */
    public function instruction($slug)
    {
        $post = $this->posts->getPostWithCategory('instruction', $slug);

        if (!isset($post)) {
            abort(404);
        }

        return view('cms.post.single.instruction')
            ->with(compact('post'));
    }
}
