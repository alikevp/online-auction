<?php

namespace App\Http\Controllers;

use App\Category;
use App\Offer;
use Baum\Node;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use SEO;
use SEOMeta;
use OpenGraph;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
		SEOMeta::setTitleDefault('Работа с категориями - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $categories = Cache::get('categories');
        $category = Category::where('checked', 0)->get()->random();

        $products = $this->getProducts($category);
        $similar = $this->getSimilar($category);

        return View::make(
            'categories.index',
            [
                'categories' => $categories,
                'category' => $category,
                'products' => $products,
                'similar' => $similar->unique(),
            ]
        );
    }

    public function getProducts($category)
    {
        if (count($category->products) >= 3) {
            $products = $category->products->random(3);
        } elseif (count($category->products) == 0) {
            $products = null;
        } else {
            $products = $category->products;
        }

        return $products;
    }

    public function add_category(Request $request)
    {
        $slug = str_slug($request->name, '-');
        $cat_unique_id = uniqid("cat_");
        $newCategory = new Category();

        $newCategory->id = $cat_unique_id;
        $newCategory->parent_id = $request->parent_id;
        $newCategory->name = $request->name;
        $newCategory->slug = $slug;
        $newCategory->in_menu = true;
        $newCategory->checked = 1;
        $newCategory->offers_count = 0;
        $newCategory->leafs_offers_count = 0;

        $newCategory->save();

        $parent = Category::where('id', $request->parent_id)->first();
        $newCategory->makeChildOf($parent);

        $request->session()->flash('alert-success', 'Категория добавлена.');

        return redirect($request->redirect_url);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getSimilar($category)
    {
        // Список слов, которые будут удалены (предлоги, собюзы и прочее)
        // Вот тут идет удаление всякой фигни в скобках, например "колонки (13 см)"
        // на выходе будет простото "колонки"
        // Знаки препинания
        $patterns = [
            '/\s(к|для|ONE|с|в|и|из|без|по|за|со|by|side|hd|д|см|со|ПО)\s/ui',
            '/\s*\([^()]*\)\s*/',
            '/[^\d\w ]+/ui',
            '/[\d]+/ui',
        ];

        // Список окончаний
        $completions = '/(их|и|ов|ая|а|ы|ие|ые|ам|ых|ей|ой|ым|ем|ах|ый)$/ui';

        // Сперва удаляем все в скобках
        // Затем удаляем лишние слова
        // Убираем все цифры
        $string = preg_replace($patterns, " ", $category->name);
        $string = trim($string);

        // Удаляем лишние пробелы, например "супер      ноутбук" сделает "супер ноутбук"
        $string = preg_replace('/[\s]+/ui', " ", $string);

        // Разбиваем всю строку на слова
        $string = explode(" ", $string);

        // Удаляем окончания из слов
        $string = preg_replace($completions, "", $string);

        $similar = collect();
        foreach ($string as $word) {
            $tmpCategory = Category::where('name', 'ilike', "%$word%")->get();
            if ($tmpCategory->count() > 0) {
                foreach ($tmpCategory as $item) {
                    $similar->push($item);
                }
            } else {
                $similar->push($tmpCategory);
            }
        }

        return $similar;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		SEOMeta::setTitleDefault('Работа с категориями - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $categories = Cache::get('categories');
        $category = Category::findOrFail($id);

        $products = $this->getProducts($category);
        $similar = $this->getSimilar($category);

        return View::make(
            'categories.index',
            [
                'categories' => $categories,
                'category' => $category,
                'products' => $products,
                'similar' => $similar->unique(),
            ]
        );
    }

    public function offers_recount()
    {

        $offers_grouped = Offer::select('category_id')->whereNotNull('price')->where('price', '>', 0)->get()->groupBy(
            'category_id'
        );
        Category::where('leafs_offers_count', '!=', 0)->orWhere('offers_count', '!=', 0)->update(
            [
                'leafs_offers_count' => 0,
                'offers_count' => 0,
            ]
        );
        function parents_recount($parent_id, $count)
        {
            $category = Category::where('id', $parent_id)->first();
            if (isset($category->leafs_offers_count)) {
                $new_count = $category->leafs_offers_count + $count;

                Category::where('id', $category->id)
                    ->update(['leafs_offers_count' => $new_count]);

                if (isset($category->parent_id)) {
                    parents_recount($category->parent_id, $new_count);
                }
            }
        }

        $i = 0;
        foreach ($offers_grouped as $category => $offers) {
            $i++;
            Category::where('id', $category)
                ->update(['offers_count' => count($offers)]);
            $category = Category::where('id', $category)->first();
            if(isset($category->parent_id)) {
                parents_recount($category->parent_id, count($offers));
            }
            echo "$i<br>";
        }
//        $categories = Category::where('offers_count', '>', 0)->orWhere('leafs_offers_count', '>', 0)->get(
//        )->toHierarchy();
//        $view = view('cached_views.constructors.CategoriesSidebarConstructor')->with(compact('categories'));
//        $contents = $view->render();
//        if (!Storage::disk('views')->exists('/cached_views/cache')) {
//            Storage::disk('views')->makeDirectory('/cached_views/cache', 0775, true);
//        }
//        Storage::disk('views')->put('/cached_views/cache/CategoriesSidebar.blade.php', $contents);
    }

    public function categories_sidebar_form()
    {
        $categories = Category::where('offers_count', '>', 0)->orWhere('leafs_offers_count', '>', 0)->get(
        )->toHierarchy();
        $view = view('cached_views.constructors.CategoriesSidebarConstructor')->with(compact('categories'));
        $contents = $view->render();
        if (!Storage::disk('views')->exists('/cached_views/cache')) {
            Storage::disk('views')->makeDirectory('/cached_views/cache', 0775, true);
        }
        Storage::disk('views')->put('/cached_views/cache/CategoriesSidebar.blade.php', $contents);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $query = $request->search;

        $categories = Category::where('name', 'ilike', "%$query%")->get();

        return response()->json(
            [
                'categories' => $categories,
            ]
        );
    }

    public function category_move_check(Request $request)
    {
        $offers = DB::table('offers')->where('category_id',$request->cat_source)->where('name','ilike',$request->like_name)->count();
        echo"Найдено $offers товаров.";
    }
    public function category_move(Request $request)
    {
        $offers = DB::table('offers')->select('id')->where('category_id',$request->cat_source)->where('name','ilike',$request->like_name)->get();
        //Обновляем категорию у товаров
        $offers_udpated = count($offers);
        DB::table('offers')->where('category_id',$request->cat_source)->where('name','ilike',$request->like_name)->update(
            [
                'category_id' => $request->cat_destination
            ]);
        //работаем с атрибутами
        foreach ($offers as $offer) {
            //Обновляем категорию у атрибутов(старое)
            DB::table('attributes')->where('offer_id', $offer->id)->update(
                [
                    'category_id' => $request->cat_destination
                ]);
            //Работаем с новыми атрибутами
            //получаем значения по товару
            $attributeValues = DB::table('attribute_values')->select('id','attribute_name_id')->where('offer_id', $offer->id)->get();
            foreach ($attributeValues as $attributeValue) {
                $attributeNameCurrent = DB::table('attribute_names')->where('id', $attributeValue->attribute_name_id)->first();

                $attributeNameNewDubl = DB::table('attribute_names')->where('name', $attributeNameCurrent->name)->where('category_id', $request->cat_destination)->first();

                if (!$attributeNameNewDubl) {
                    $attributeNameId = DB::table('attribute_names')->insertGetId(
                        [
                            'slug' => $attributeNameCurrent->slug,
                            'name' => $attributeNameCurrent->name,
                            'category_id' => $request->cat_destination,
                            'filter' => $attributeNameCurrent->filter,
                            'created_at' => $attributeNameCurrent->created_at,
                            'updated_at' => $attributeNameCurrent->updated_at,
                            'upload_id' => $attributeNameCurrent->upload_id,

                        ]
                    );
                } else {

                    $attributeNameId = $attributeNameNewDubl->id;
                }
                DB::table('attribute_values')->where('id', $attributeValue->id)->update(
                    [
                        'attribute_name_id' => $attributeNameId
                    ]);
            }
        }
        echo"Обновлено $offers_udpated товаров.";
    }
}
