<?php

namespace App\Http\Controllers;

use App\Attribute_name;
use App\Attribute_value;
use App\Mail\TestMail;
use App\Offer;
use App\Spam_base;
use App\Category;
use App\Attribute;
use App\Post;
use App\Mailsubscription_user;
use App\User;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use League\Csv\Reader;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TestController extends Controller
{
    public function index()
    {
        $start = microtime(true);
        $startMemory = 0;
        $startMemory = memory_get_usage();
        $offers1 = DB::table('offers')->get();
        dump($offers1->first());
        echo (((memory_get_usage() - $startMemory) / 1024) / 1024).' Mb'.PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

        $start2 = microtime(true);
        $startMemory2 = 0;
        $startMemory2 = memory_get_usage();
        $offers2 = Offer::get();
        dump($offers2->first());
        echo (((memory_get_usage() - $startMemory2) / 1024) / 1024).' Mb'.PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $start2).' сек.';
    }

    public function sendMail()
    {
//        $mail = new TestMail();
//
//        \Mail::to('vykhodtsevai@gmail.com')->send($mail);
//        $user = Auth::user();
//
//        $subscription = Mailsubscription_user::where('user_id',$user->id)->whereHas('mailsubscription', function ($query) {
//            $query->where('lable',  'new');
//        })->get();


//        $categories = array();
//        $categories[] = 'cat_585d6169a56fa';
//        $categories[] = 'cat_586432701f8b8';
//        $cat_trees = Category::whereIn('id',$categories)->get();
//        $all_cats_tree = array();
//        foreach ($cat_trees as $cat){
//            $cur_cat_ancestors = $cat->ancestorsAndSelf()->pluck('id')->toArray();
//            $cur_cat_descendants = $cat->descendants()->pluck('id')->toArray();
//            $cur_cat_tree = array_merge($cur_cat_ancestors, $cur_cat_descendants);
//            $all_cats_tree = array_merge($cur_cat_tree, $all_cats_tree);
//        }
//        dd($all_cats_tree);
//        $attrs = DB::table('attributes')->get();
//        foreach ($attrs as $attr) {
//            if (isset($attr->category_id) && isset($attr->slug) && isset($attr->value) && isset($attr->name) && isset($attr->offer_id)) {
//                if ($attr->category_id !== '' && $attr->slug !== '' && $attr->value !== '' && $attr->name !== '' && $attr->offer_id !== '') {
//                    $slug = trim($attr->slug);
//                    $slug = ltrim($slug, "-");
//                    $value = trim($attr->value);
//                    $value = ltrim($value, "-");
//                    $name = trim($attr->name);
//                    $name = ltrim($name, "-");
//                    $attributeDubleCheck = DB::table('attribute_names')->select('id')->where(
//                        'category_id',
//                        $attr->category_id
//                    )->where('name', $name)->first();
//
//
//                    if (!$attributeDubleCheck) {
//                        $attributeNameId = DB::table('attribute_names')->insertGetId(
//                            [
//                                'slug' => $slug,
//                                'name' => $name,
//                                'category_id' => $attr->category_id,
//                                'filter' => $attr->filter,
//                            ]
//                        );
//                    } else {
//                        $attributeNameId = $attributeDubleCheck->id;
//                    }
//                    DB::table('attribute_values')->insert(
//                        [
//                            'attribute_name_id' => $attributeNameId,
//                            'product_id' => $attr->product_id,
//                            'offer_id' => $attr->offer_id,
//                            'value' => $attr->value,
//                        ]
//                    );
//                    unset($attr, $slug, $value, $name, $attributeDubleCheck);
//                }
//            }
//        }
//        $users = User::all();
//        foreach ($users as $user){
//            User::where('id', $user->id)->update([
//                'e_mail'=>$user->email
//            ]);
//        }
        $offers = DB::table('offers')->where('upload_id','4')->orWhere('upload_id','3')->get();
        foreach ($offers as $offer) {
            DB::table('offers')
                ->where('id', $offer->id)
                ->update(['producer' => str_slug(trim($offer->producer))]);
        }
    }


    public function csv_in(Request $request)
    {
        //SPAM
//        $files = Storage::disk('parsing')->files('/bd_email/files/active');
//        foreach ($files as $file)
//        {
//            if(preg_match("|.csv$|",$file) OR preg_match("|.CSV$|",$file))
//            {
//                if ($request->has(['spam_upload_submit'])) {
//                    Config::set('excel.csv.delimiter', ';');
//                    Config::set('excel.csv.enclosure', '');
//                    Config::set('excel.csv.lineEnding', '\r\n');
//                    $csvContent = Excel::filter('chunk')->load('public/parsing/'.$file)->chunk(500, function($results) use ($request)
//                    {
//                        $results = $results->toArray();
//                        foreach($results as $csvRow)
//                        {
//                            $newSpamRow = new Spam_base;
//                            if ($request->has(['name'])) {
//                                $newSpamRow->name = $csvRow[$request->name];
//                            }
//                            if ($request->has(['direction'])) {
//                                $newSpamRow->direction = $csvRow[$request->direction];
//                            }
//                            if ($request->has(['category_upload'])) {
//                                $newSpamRow->category_upload = $csvRow[$request->category_upload];
//                            }
//                            if ($request->has(['city'])) {
//                                $newSpamRow->city = $csvRow[$request->city];
//                            }
//                            if ($request->has(['address'])) {
//                                $newSpamRow->address = $csvRow[$request->address];
//                            }
//                            if ($request->has(['tel'])) {
//                                $newSpamRow->tel = $csvRow[$request->tel];
//                            }
//                            if ($request->has(['fax'])) {
//                                $newSpamRow->fax = $csvRow[$request->fax];
//                            }
//                            if ($request->has(['email'])) {
//                                $newSpamRow->email = $csvRow[$request->email];
//                            }
//                            if ($request->has(['www'])) {
//                                $newSpamRow->www = $csvRow[$request->www];
//                            }
//                            $newSpamRow->activity = false;
//
//                            $newSpamRow->save();
//                            unset ($newSpamRow);
//                        }
//                    },false);
//                    return 'Успешно';
//                }
//                else{
//                    $reader = Reader::createFromPath(public_path() . '/parsing/'.$file,'r');
//                    $reader->setHeaderOffset(0);
//                    $reader->setDelimiter(';');
//                    $reader->setEnclosure('"');
//                    $headings = $reader->getHeader();
//                    return view('partials.csv_spam_fields_select')
//                        ->with(compact('headings'));
//                }
//            }
//        }
        //SPAM

        $files = Storage::disk('parsing')->files('/bd_email/files/active');
        foreach ($files as $file) {
            if (preg_match("|.csv$|", $file) OR preg_match("|.CSV$|", $file)) {
                Config::set('excel.csv.delimiter', ',');
                Config::set('excel.csv.enclosure', '"');
//                Config::set('excel.csv.lineEnding', '\r\n');
                $csvContent = Excel::filter('chunk')->load('public/parsing/'.$file)->chunk(
                    250,
                    function ($results) use ($request) {
                        $results = $results->toArray();
                        foreach ($results as $csvRow) {
                            $slug = trim($csvRow['slug']);
                            $slug = ltrim($slug, "-");
                            $value = trim($csvRow['value']);
                            $value = ltrim($value, "-");
                            $name = trim($csvRow['name']);
                            $name = ltrim($name, "-");
                            if ($value !== '') {
                                DB::table('attributes_tests')->insert(
                                    [
                                        'slug' => $slug,
                                        'product_id' => null,
                                        'offer_id' => $csvRow['offer_id'],
                                        'name' => $name,
                                        'category_id' => $csvRow['category_id'],
                                        'value' => $value,
                                        'variation' => $csvRow['filter'],
                                    ]
                                );
                            }
                        }
                    },
                    false
                );

                return 'Успешно';
            }
        }
    }
}
