<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Spam_base;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;


class SpamController extends Controller
{
    public function spamBaseCsvUpload(Request $request)
    {
        if(preg_match("|.csv$|",$request->file) OR preg_match("|.CSV$|",$request->file))
        {
            Config::set('excel.csv.delimiter', ';');
            Config::set('excel.csv.enclosure', '');
//            Config::set('excel.csv.lineEnding', '\r\n');
            $csvContent = Excel::filter('chunk')->load('public/parsing/'.$request->file)->chunk(1000, function($results) use ($request)
            {
                foreach($results as $csvRow)
                {
                    if($csvRow->email !== '') {
                        $categories = implode(",", $request->parent_id);
                        DB::table('spam_bases')->insert(
                            [
                                'name' => trim($csvRow->name),
                                'category_id' => $categories,
                                'tel' => trim($csvRow->tel),
                                'email' => trim($csvRow->email),
                            ]
                        );
                    }
                    unset($csvRow);
                }
                unset($results);
            },false);
            unset($csvContent);
            $request->session()->flash('alert-success', 'Выгрузка добавлена');
        }
        else{
            $request->session()->flash('alert-danger', 'Неверный формат файла');
        }
        return redirect('/admin/spam_bases?page=uploadResult');
    }
}


