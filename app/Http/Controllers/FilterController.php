<?php

namespace App\Http\Controllers;

use App\Category;
use App\Offer;
use App\Repositories\Attributes;
use App\Repositories\OffersRepository;
use App\Repositories\Producers;
use Illuminate\Http\Request;
use SEO;
use SEOMeta;
use OpenGraph;

class FilterController extends Controller
{
    public function __construct(OffersRepository $products, Attributes $attributes, Producers $producers)
    {
        $this->products = $products;
        $this->attributes = $attributes;
        $this->producers = $producers;
    }

    /**
     * @param Request $request
     * @param OffersRepository $offers
     * @return $this
     */
    public function filter(Request $request, OffersRepository $offers)
    {
//        $products = $offers->inCategories(session('category')->id);
//
//        if ($request->has('attributes')) {
//            $products = $offers->hasAttributes($products, $request->input('attributes'));
//        }
//
//        if ($request->has('min')) {
//            $products = $offers->minPrice($products, $request->input('min'));
//        }
//
//        if ($request->has('max')) {
//            $products = $offers->maxPrice($products, $request->input('max'));
//        }
//
//        if ($request->has('producer')) {
//            $products = $offers->withProducer($products, $request->input('producer'));
//        });


        $categories = Category::where('id', $request->category)->first()->descendantsAndSelf()->pluck('id')->toArray();
		SEOMeta::setTitleDefault($categories->name.' - Джинмарт-Электронная торговая площадка потребителей');
		SEOMeta::setDescription('Лучшая цена у нас. Магазин широкого профиля. Электронная торговая площадка потребителей.');
        $products = Offer::whereIn('category_id', $categories)
            ->when(
                $request->has('min'),
                function ($query) use ($request) {
                    return $query->where('price', '>=', $request->input('min'));
                }
            )
            ->when(
                $request->has('max'),
                function ($query) use ($request) {
                    return $query->where('price', '<=', $request->input('max'));
                }
            )
            ->when(
                $request->has('producer'),
                function ($query) use ($request) {
                    return $query->whereIn('producer', $request->input('producer'));
                }
            )
            ->when(
                $request->has('order'),
                function ($query) use ($request) {
                    list($field, $value) = explode('_', $request->input('order'));

                    return $query->orderBy($field, $value);
                }
            );
        if (!isset($request->no_price)) {
            if ($request->no_price == false) {
                $products = $products->where('price', '!=', 0);
            }
        }

        if ($request->has('attributes')) {
            foreach ($request->input('attributes') as $name => $values) {
                $products = $products->whereHas(
                    'attributes',
                    function ($att) use ($name, $values) {
                        $att->where('slug', 'ilike', "%$name%")
                            ->where(
                                function ($query) use ($values) {
                                    foreach ($values as $value) {
                                        $value = trim($value);
                                        $query->orWhere('value', 'ilike', "%$value%");
                                    }
                                }
                            );
                    }
                );
            }
        }
        $max_price = $products->max('price');
        $min_price = $products->min('price');
        $products = $products->paginate(24);
        $get_params = http_build_query($request->request->all());
        $products->withPath('/filter?'.$get_params.'');
        if (session()->has('min_price')) {
            $min_price = session()->get('min_price');
        }
        if (session()->has('max_price')) {
            $max_price = session()->get('max_price');
        }
        if (session()->has('category')) {
            $category = session()->get('category');
        }
        if (session()->has('categories')) {
            $categories = session()->get('categories');
        }
        if (session()->has('producers')) {
            $producers = session()->get('producers');
        }
        if (session()->has('attributes')) {
            $attributes = session()->get('attributes');
        }

//        return redirect()->back()->with(compact('products'));
        return view('shopping.category')
            ->with(compact('products', 'max_price', 'min_price', 'categories', 'category', 'producers', 'attributes'));
    }


    public function filterByAjax(Request $request)
    {
        $categories = Category::where('id', $request->category)->first()->descendantsAndSelf()->pluck('id')->toArray();

        $products = Offer::whereIn('category_id', $categories)
            ->when(
                $request->has('min'),
                function ($query) use ($request) {
                    return $query->where('price', '>=', $request->input('min'));
                }
            )
            ->when(
                $request->has('max'),
                function ($query) use ($request) {
                    return $query->where('price', '<=', $request->input('max'));
                }
            )
            ->when(
                $request->has('producer'),
                function ($query) use ($request) {
                    return $query->whereIn('producer', $request->input('producer'));
                }
            )
            ->when(
                $request->has('order'),
                function ($query) use ($request) {
                    list($field, $value) = explode('_', $request->input('order'));

                    return $query->orderBy($field, $value);
                }
            )
            ->when(
                $request->has('no-price'),
                function ($query) use ($request) {
                    return $query->where('price', '!=', 0);
                }
            );
        if (!isset($request->no_price)) {
            if ($request->no_price == false) {
                $products = $products->where('price', '!=', 0);
            }
        }

        if ($request->has('attributes')) {
            foreach ($request->input('attributes') as $name => $values) {
                $products = $products->whereHas(
                    'attributes',
                    function ($att) use ($name, $values) {
                        $att->where('slug', 'ilike', "%$name%")
                            ->where(
                                function ($query) use ($values) {
                                    foreach ($values as $value) {
                                        $value = trim($value);
                                        $query->orWhere('value', 'ilike', "%$value%");
                                    }
                                }
                            );
                    }
                );
            }
        }
        $max_price = $products->max('price');
        $min_price = $products->min('price');
        $products = $products->paginate(24);
        $get_params = http_build_query($request->request->all());
        $products->withPath('/category/'.$request->category.'?'.$get_params.'');
        return view('shopping.filteredProducts')
            ->with(compact('products'));
    }


    public function filter_parameters_refresh(Request $request, OffersRepository $offers, Attributes $attributes)
    {
        $categories = Category::where('id', $request->category)->first()->descendantsAndSelf()->pluck('id')->toArray();

        $products = Offer::whereIn('category_id', $categories)
            ->when(
                $request->has('minPrice'),
                function ($query) use ($request) {
                    return $query->where('price', '>=', $request->input('minPrice'));
                }
            )
            ->when(
                $request->has('maxPrice'),
                function ($query) use ($request) {
                    return $query->where('price', '<=', $request->input('maxPrice'));
                }
            )
            ->when(
                $request->has('producer'),
                function ($query) use ($request) {
                    return $query->whereIn('producer', $request->input('producer'));
                }
            )
            ->when(
                $request->has('no-price'),
                function ($query) use ($request) {
                    return $query->where('price', '!=', 0);
                }
            );
        if (!isset($request->no_price)) {
            if ($request->no_price == false) {
                $products = $products->where('price', '!=', 0);
            }
        }
        if ($request->has('attributes')) {
            foreach ($request->input('attributes') as $name => $values) {
                $products = $products->whereHas(
                    'attributes',
                    function ($att) use ($name, $values) {
                        $att->where('slug', 'ilike', "%$name%")
                            ->where(
                                function ($query) use ($values) {
                                    foreach ($values as $value) {
                                        $value = trim($value);
                                        $query->orWhere('value', 'ilike', "%$value%");
                                    }
                                }
                            );
                    }
                );
            }
        }
        $products = $products->get();
        $attributes = $attributes->getAttributes($products);

        $selected['attributes'] = $request->input('attributes');
        $selected['category'] = $request->category;
        $selected['maxPrice'] = $request->maxPrice;
        $selected['minPrice'] = $request->minPrice;
        $selected['producer'] = $request->producer;

        return view('shopping.filter_parameters')
            ->with(compact('attributes', 'selected'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->category)) {
            $id = $request->category;
            $category = Category::findOrFail($id);
            $categories = $category->getImmediateDescendants();
            if ($category->isLeaf()) {
                $products = $category->products();
            } else {
                $leaves = $category->leaves()->get()->pluck('id');
                $products = Offer::whereIn('category_id', $leaves);
            }
        } elseif (isset($request->search)) {
            $query = $request->search;
            $products = Offer::where('name', 'like', "%$query%");
        }

        if (isset($request->min) || isset($request->max)) {
            $min = $request->min;
            $max = $request->max;
            $products = $products->whereBetween('price', [$min, $max]);
        }

        if (isset($request->producer) & count($request->producer) > 0) {
            $producer = $request->producer;
            $products = $products
                ->whereIn('producer', $producer);
        }

        $f = Offer::whereHas(
            'attributes',
            function ($query) {
                $query->where(
                    [
                        ['value', 'ilike', '%США%'],
                    ]
                );
            }
        )->with('attributes')->get();

        dd($f->toArray());

        // FIXME: по умолчанию все галки на чекбоксах выключены, надо будет исправить
        $products = $products->paginate(20);

        return view('partials.products', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function suggest(Request $request)
    {
        $query = $request->search;

        $products = Offer::where('name', 'ilike', "%$query%")->get()->random(3)->pluck('name');
        $categories = Category::where('name', 'ilike', "%$query%")->get()->random(3)->pluck('name');

        $suggestions = $products;

        return $suggestions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
