<?php

namespace App\Http\ViewComposers;

use App\Category;
use App\Menu;
use Cache;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use App\Repositories\Categories;
use Baum\Node;

class NavComposer
{
    public function compose(View $view)
    {
        if (Cache::has('categories')) {
            $main_categories = Cache::get('categories');

        } else {
            $main_categories = Cache::remember(
                'categories',
                Carbon::now()->addDay(),
                function () {
                    return Categories::getTree();
                }
            );
        }

        $view->with(compact('main_categories'));
    }
}
