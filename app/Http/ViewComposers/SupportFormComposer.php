<?php

namespace App\Http\ViewComposers;

use App\Term;
use Illuminate\Contracts\View\View;

class SupportFormComposer
{
    public function compose(View $view)
    {
        $problem_types = Term::ProblemTypes()->get();

        return $view->with(compact('problem_types'));
    }
}
