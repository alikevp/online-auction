<?php

namespace App\Listeners\Spam;

use App\Category;
use App\Events\Order\Confirmed;
use App\Mail\SpamNewOrderToShops;
use App\Spam_base;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendSpam
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Confirmed $event
     * @return void
     */
    public function handle(Confirmed $event)
    {
        $cat_trees = Category::whereIn('id', $event->orders->pluck('category_id'))->get();
        $all_cats_tree = array();
        foreach ($cat_trees as $cat) {
            $cur_cat_ancestors = $cat->ancestorsAndSelf()->pluck('id')->toArray();
            $cur_cat_descendants = $cat->descendants()->pluck('id')->toArray();
            $cur_cat_tree = array_merge($cur_cat_ancestors, $cur_cat_descendants);
            $all_cats_tree = array_merge($cur_cat_tree, $all_cats_tree);
        }
        $mails = Spam_base::
            where(
                function ($query) use ($all_cats_tree) {
                    foreach ($all_cats_tree as $cat) {
                        $cat = trim($cat);
                        $query->orWhere('category_id', 'ilike', "%$cat%");
                    }
                }
            )
            ->where('activity', '=', true)
            ->get()
            ->pluck('email');

        foreach ($mails as $to) {
            if(filter_var($to, FILTER_VALIDATE_EMAIL)) {
                $mail = new SpamNewOrderToShops($event->orders);
                Mail::to($to)->send($mail);
            }
        }
    }
}
