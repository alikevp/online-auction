<?php

namespace App\Listeners\Exchange;


use App\Events\Exchange\ExchangeImport;
use App\Mail\Exchange\ImportFrom1c;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class Import
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged $event
     * @return void
     */
    public function handle(ExchangeImport $event)
    {
        $report = $event->report;
        $recipient_mail = 'vai@awsmail.ru';
        $mail = new ImportFrom1c($report);
        Mail::to($recipient_mail)->send($mail);
    }
}
