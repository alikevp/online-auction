<?php

namespace App\Listeners\Exchange;


use App\Events\Exchange\ExchangeExport;
use App\Mail\Exchange\ExportTo1c;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class Export
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged $event
     * @return void
     */
    public function handle(ExchangeExport $event)
    {
        $report = $event->report;
        $recipient_mail = 'vai@awsmail.ru';
        $mail = new ExportTo1c($report);
        Mail::to($recipient_mail)->send($mail);
    }
}
