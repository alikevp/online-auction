<?php

namespace App\Listeners\Shop;

use App\Events\Shops\Register;
use App\Mail\ShopRegisterNotifyToAdmin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class RegisterShopToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Register $event)
    {
        $shop = $event->shop;
        $mail = new ShopRegisterNotifyToAdmin($shop);
        Mail::to("medvedeva@jinnmart.ru")->send($mail);
    }
}
