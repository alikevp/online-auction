<?php

namespace App\Listeners\Shop;

use App\Events\Shops\Register;
use App\Mail\DeliveryRegisterShop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class RegisterShop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Register $event)
    {
        $user = $event->user;
        $shop = $event->shop;
        $mail = new DeliveryRegisterShop($user, $shop);
        Mail::to($user->e_mail)->send($mail);
    }
}
