<?php

namespace App\Listeners;

use App\Events\UserRegister;
use App\Mail\VerifyEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Mail;

class SendVerify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister $event
     *
     * @return void
     */
    public function handle(UserRegister $event)
    {
        $user = $event->user;
        $url = url('register/verify', $user->verification_token);
        $mail = new VerifyEmail($user, $url);
        Mail::to($user->e_mail)->send($mail);
    }
}
