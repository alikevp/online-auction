<?php

namespace App\Listeners\Order;

use App\Category;
use App\Order_offer_group;
use App\Events\Order\OfferNew;
use App\Mail\OrderNewOfferToShop;
use App\Mail\OrderConcurentOfferToShops;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class OfferNewToShop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged  $event
     * @return void
     */
    public function handle(OfferNew $event)
    {
        $order = $event->offer->order;
        $currentShop = $event->offer->shops->first();
        $all_offers = Order_offer_group::with('shops')->where('order_id',$order->id)->get();

        $shops = collect();
        foreach ($all_offers as $offer) {
            foreach ($offer->shops as $shop) {
                $shops->push($shop);
            }
        }

        foreach ($shops->unique('e_mail') as $shop) {
            if ($shop->id == $currentShop->id){
                $mail = new OrderNewOfferToShop($shop, $order);
                Mail::to($shop->e_mail)->send($mail);
            }else {
                $mail = new OrderConcurentOfferToShops($shop, $order);
                Mail::to($shop->e_mail)->send($mail);
            }
        }

    }
}
