<?php

namespace App\Listeners\Order;

use App\Events\Order\Confirmed;
use App\Mail\OrderConfirmedToUser;
use App\Mailsubscription_user;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Mail;

class NotifyUser
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Confirmed $event
     *
     * @return void
     */
    public function handle(Confirmed $event)
    {
        $user = $event->user;
        $orders = $event->orders;


        $subscription = Mailsubscription_user::where('user_id',$user->id)->whereHas('mailsubscription', function ($query) {
            $query->where('lable',  'buyer_auction_tecnical');
       })->first();
        
        if(isset($subscription)) {
            $subscribe = url('mail/unsubscribe/' . $subscription->hash);
            $mail = new OrderConfirmedToUser($user, $orders, $subscribe);

            Mail::to($user->e_mail)->send($mail);
        }
    }
}
