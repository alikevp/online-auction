<?php

namespace App\Listeners\Order;

use App\Events\Order\OfferNew;
use App\Mail\OrderNewOfferToUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class OfferNewToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged  $event
     * @return void
     */
    public function handle(OfferNew $event)
    {
        $order = $event->order;
        $user = $order->user;
        $shop = $event->user->shop;

        $mail = new OrderNewOfferToUser($user, $order, $shop);

        Mail::to($user->e_mail)->send($mail);
    }
}
