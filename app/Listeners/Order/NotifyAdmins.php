<?php

namespace App\Listeners\Order;

use App\Category;
use App\Events\Order\Confirmed;
use App\Mail\OrderConfirmedToShop;
use App\Mailsubscription_user;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class NotifyAdmins
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Confirmed $event
     *
     * @return void
     */
    public function handle(Confirmed $event)
    {
        $orders = $event->orders;
        $categories = Category::with('shops')->whereIn('id', $orders->pluck('category_id')->unique())->get();

        $shops = collect();
        foreach ($categories as $category) {
            foreach ($category->shops as $shop) {
                $shops->push($shop);
            }
        }
        $shops = $shops->unique('e_mail');
        foreach ($shops as $shop) {
            $users = User::where('connection_id', $shop->id)->get();
            foreach ($users as $user) {
                $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas('mailsubscription', function ($query) {
                    $query->where('lable', 'shop_auction_tecnical');
                })->first();
                if (isset($subscription)) {
                    $subscribe = url('mail/unsubscribe/' . $subscription->hash);
                    $mail = new OrderConfirmedToShop($shop, $orders,$subscribe);
                    Mail::to($user->e_mail)->send($mail);
                }
            }
        }
    }
}

