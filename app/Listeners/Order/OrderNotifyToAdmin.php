<?php

namespace App\Listeners\Order;

use App\Events\Order\Confirmed;
use App\Mail\OrderNewNotifyToAdmin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Mail;

class OrderNotifyToAdmin
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Confirmed $event
     *
     * @return void
     */
    public function handle(Confirmed $event)
    {
        $user = $event->user;
        $orders = $event->orders;

        $mail = new OrderNewNotifyToAdmin($user, $orders);
        Mail::to("medvedeva@jinnmart.ru")->send($mail);
    }
}
