<?php

namespace App\Listeners\Order;

use App\Category;
use App\Mailsubscription_user;
use App\Order_offer_group;
use App\Events\Order\OfferChanged;
use App\Mail\OrderOfferChangeToShops;
use App\Mail\OrderOfferChangeToShop;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class ChangeToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged $event
     * @return void
     */
    public function handle(OfferChanged $event)
    {
        $order = $event->order;
        $offer = $event->offer;
        $currentShop = $event->user->shop;
        $all_offers = Order_offer_group::with('shops')->where('order_id', $order->id)->get();

        $shops = collect();
        foreach ($all_offers as $offer) {
            foreach ($offer->shops as $shop) {
                $shops->push($shop);
            }
        }

        foreach ($shops->unique('e_mail') as $shop) {
            if ($shop->id == $currentShop->id) {
                $users = User::where('connection_id', $shop->id)->get();
                foreach ($users as $user) {
                    $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas('mailsubscription', function ($query) {
                        $query->where('lable', 'shop_auction_tecnical');
                    })->first();

                    if (isset($subscription)) {
                        $subscribe = url('mail/unsubscribe/' . $subscription->hash);

                        $mail = new OrderOfferChangeToShop($user,$offer, $shop, $order, $subscribe);
                        Mail::to($shop->e_mail)->send($mail);
                    }
                }
            }
            else {
                $users = User::where('connection_id', $shop->id)->get();
                foreach ($users as $user) {
                    $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas('mailsubscription', function ($query) {
                        $query->where('lable', 'shop_auction_tecnical');
                    })->first();

                    if (isset($subscription)) {
                        $subscribe = url('mail/unsubscribe/' . $subscription->hash);

                        $mail = new OrderOfferChangeToShops($user, $shop,$offer, $order, $subscribe);
                        #$mail = new OrderOfferChangeToShops($user,$shop, $order);
                        Mail::to($shop->e_mail)->send($mail);
                    }
                }
            }
        }
    }
}
