<?php

namespace App\Listeners\Order;

use App\Events\Order\StatusChanged;
use App\Mail\orderStatusChangeToUser;
use App\Mailsubscription_user;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class StatusChangeToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatusChanged  $event
     * @return void
     */
    public function handle(StatusChanged $event)
    {
        $user = $event->user;
        $order = $event->order;
        $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas('mailsubscription', function ($query) {
            $query->where('lable', 'buyer_auction_tecnical');
        })->first();

        if (isset($subscription)) {
            $subscribe = url('mail/unsubscribe/' . $subscription->hash);
            $mail = new orderStatusChangeToUser($user, $order,$subscribe);
            Mail::to($user->e_mail)->send($mail);
        }
    }
}
