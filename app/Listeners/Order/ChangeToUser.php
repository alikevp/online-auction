<?php

namespace App\Listeners\Order;

use App\Events\Order\OfferChanged;
use App\Mail\OrderOfferChangeToUser;
use App\Mailsubscription_user;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class ChangeToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferChanged $event
     * @return void
     */
    public function handle(OfferChanged $event)
    {
        $order = $event->order;
        $user = $order->user;
        $shop = $event->user->shop;

        $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas(
            'mailsubscription',
            function ($query) {
                $query->where('lable', 'buyer_auction_tecnical');
            }
        )->first();

        if (isset($subscription)) {
            $subscribe = url('mail/unsubscribe/'.$subscription->hash);
            $mail = new OrderOfferChangeToUser($user, $order, $shop, $subscribe);

            Mail::to($user->e_mail)->send($mail);
        }
    }
}
