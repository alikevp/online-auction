<?php

namespace App\Listeners\Order;

use App\Category;
use App\Order_offer_group;
use App\Events\Order\StatusChanged;
use App\Mail\OrderStatusChangeToShop;
use App\Mailsubscription_user;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Mail;

class StatusChangeToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatusChanged  $event
     * @return void
     */
    public function handle(StatusChanged $event)
    {
        $order = $event->order;

        $all_offers = Order_offer_group::with('shops')->where('order_id', $order->id)->get();

        $shops = collect();
        foreach ($all_offers as $offer) {
            foreach ($offer->shops as $shop) {
                $shops->push($shop);
            }
        }

        foreach ($shops->unique('e_mail') as $shop) {
            $users = User::where('connection_id', $shop->id)->get();
            foreach ($users as $user) {
                $subscription = Mailsubscription_user::where('user_id', $user->id)->whereHas('mailsubscription', function ($query) {
                    $query->where('lable', 'shop_auction_tecnical');
                })->first();

                if (isset($subscription)) {
                    $subscribe = url('mail/unsubscribe/' . $subscription->hash);

                    $mail = new OrderStatusChangeToShop($shop, $order, $subscribe);
                    Mail::to($shop->e_mail)->send($mail);
                }
            }
        }
    }
}
