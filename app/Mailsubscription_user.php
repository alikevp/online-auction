<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mailsubscription
 *
 * @property int $id
 * @property string $name
 * @property string $lable
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription_user whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription_user whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription_user whereLable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription_user whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription_user whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mailsubscription_user extends Model
{
    protected $table = 'mailsubscription_user';
    public function users()
    {
        return $this->hasOne(User::class);
    }
    public function mailsubscription()
    {
        return $this->hasOne(Mailsubscription::class, 'id', 'mailsubscription_id');
    }
}
