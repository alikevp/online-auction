<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_offer_option
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $group_id
 * @property string $type
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_option whereUpdatedAt($value)
 */
class Order_offer_option extends Model
{
    public $incrementing = false;
    //
}
