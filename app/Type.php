<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Type
 *
 * @property int $id
 * @property int|null $term_id
 * @property string $type
 * @property string $label
 * @property int|null $parent
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Tag $tag
 * @property-read \App\Term|null $term
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereTermId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Type extends Model
{
    public function term()
    {
        return $this->belongsTo(Term::class);
    }
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
