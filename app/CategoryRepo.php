<?php
/**
 * Created by PhpStorm.
 * User: valfr
 * Date: 24.01.2017
 * Time: 16:30
 */

namespace App;

class CategoryRepo
{
    public function getCategories($id = null)
    {
        $categories = Category::where('parent_id', $id)->get();
        $categories = $this->addRelation($categories);

        return $categories;
    }

    protected function selectChild($item)
    {
        $item = $this->addRelation($item);

        return $item;
    }

    protected function addRelation($categories)
    {
        $categories->map(
            function ($item, $key) {
                $child = $this->selectChild($item->child);
                $item = array_add($item, 'child', $child);

                return $item;
            }
        );

        return $categories;
    }
}
