<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Compare
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $user_id
 * @property string $enter_id
 * @property string $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereEnterId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare whereUserId($value)
 */
class Compare extends Model
{
    //
}
