<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order_offer_group;
use App\Dealing;
use App\Comment;

/**
 * App\Shop
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property bool $activity
 * @property string $e_mail
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereActivity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereEMail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop whereUpdatedAt($value)
 * @property string|null $le_name
 * @property string|null $full_le_name
 * @property string|null $contract_type
 * @property float|null $contract_award
 * @property float|null $nds
 * @property string|null $active_till
 * @property string|null $inn
 * @property string|null $kpp
 * @property string|null $ogrn
 * @property string|null $director_fio
 * @property string|null $phone
 * @property string|null $document_exchange_type
 * @property string|null $curator_fio
 * @property string|null $curator_id
 * @property string|null $registrated_at
 * @property bool|null $sync_need
 * @property string|null $sync_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Address[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankData[] $bank_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \App\User|null $curator
 * @property-read mixed $commentcount
 * @property-read mixed $dealrate
 * @property-read mixed $offerrate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentType[] $payment_types
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Category[] $subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereActiveTill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereContractAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereContractType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereCuratorFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereCuratorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereDirectorFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereDocumentExchangeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereFullLeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereLeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereNds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereOgrn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereRegistratedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereSyncDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shop whereSyncNeed($value)
 */
class Shop extends Model
{
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'slug',
        'name',
        'activity',
        'e_mail',
        'le_name',
        'full_le_name',
        'contract_type',
        'contract_award',
        'active_till',
        'inn',
        'kpp',
        'ogrn',
        'director_fio',
        'phone',
        'curator_fio',
        'curator_id',
        'sync_need',
        'registrated_at',
    ];


    public function curator()
    {
        return $this->belongsTo('App\User', 'curator_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Банковские счета
     *
     * @return mixed
     */
    public function bank_data()
    {
        return $this->hasMany('App\BankData', 'shop_id');
    }
    /**
     * Адреса
     *
     * @return mixed
     */
    public function addresses()
    {
        return $this->hasMany('App\Address', 'shop_id');
    }

    /**
     * Подписки
     *
     * @return mixed
     */
    public function subscriptions()
    {
        return $this->belongsToMany(Category::class);
    }
    /**
     * Типы оплаты
     *
     * @return mixed
     */
    public function payment_types()
    {
        return $this->hasMany('App\PaymentType', 'shop_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'shop_id');
    }

    public function getOfferrateAttribute()
    {
        $shop_id = $this->attributes['id'];
        $shop_offers_count = Order_offer_group::where([
            ['shop_id', '=', $shop_id],
        ])->count();
        return $shop_offers_count;
    }

    public function getDealrateAttribute()
    {
        $shop_id = $this->attributes['id'];
        $shop_deals_count = Dealing::where([
            ['shop_id', '=', $shop_id],
        ])->count();
        return $shop_deals_count;
    }

    public function getCommentcountAttribute()
    {
        $shop_id = $this->attributes['id'];
        $shop_comments = Comment::where([
            ['shop_id', '=', $shop_id],
        ])->count();
        return $shop_comments;
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
