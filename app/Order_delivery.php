<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_delivery
 *
 * @property string $id
 * @property string $order_id
 * @property string $city
 * @property string|null $street
 * @property string|null $house
 * @property string|null $apartment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Delivery_type[] $types
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereApartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_delivery whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order_delivery extends Model
{
    public $incrementing = false;

    protected $table = 'order_deliveries';

    protected $fillable = [
        'city',
        'street',
        'house',
        'apartment',
        'order_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(
            function ($table) {
                $table->id = uniqid("orddel_");
            }
        );
    }

    public function types()
    {
        return $this->belongsToMany(Delivery_type::class);
    }

//    public function setIdAttribute()
//    {
//        $this->attributes['id'] = uniqid("orddel_");
//    }
}
