<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_offer_group
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $order_id
 * @property string $shop_id
 * @property float $price
 * @property float $old_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method public product
 * @property string $user_id
 * @property string $e_mail
 * @property \Carbon\Carbon $active_till
 * @property int $status
 * @property string|null $canceled_at
 * @property int|null $quantity
 * @property float|null $total_price
 * @property int $prolongation
 * @property string|null $product_id
 * @property string|null $category_id
 * @property-read \App\Category|null $category
 * @property-read \App\Order_delivery $delivery
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_offer_group[] $order_offer_groups
 * @property-read \App\Offer $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $refusedUsers
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order refused($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereActiveTill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCanceledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereEMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereProlongation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 */
class Order extends Model
{
    public $incrementing = false;

    protected $table = 'orders';

    protected $dates = [
        'created_at',
        'updated_at',
        'active_till',
    ];

    protected $fillable = [
        'user_id',
        'e_mail',
        'active_till',
        'quantity',
        'total_price',
        'status',
        'category_id',
        'product_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(
            function ($table) {
                $table->id = uniqid("ord_");
            }
        );
    }

    public function delivery()
    {
        return $this->belongsTo('App\Order_delivery', 'id', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function order_offer_groups()
    {
        return $this->hasMany('App\Order_offer_group', 'order_id', 'id');
    }

    public function product()
    {
        return $this->hasOne('App\Offer', 'id', 'product_id');
    }

    /**
     * Категория из заказа
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Привязываем отказавшихся пользователей
     *
     * @return mixed
     */
    public function refusedUsers()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeRefused($query, $id)
    {
        return $query->whereDoesntHave(
            'refusedUsers',
            function ($query) use ($id) {
                $query->whereId($id);
            }
        )->get();
    }

    public function scopeGetStatus($query, $status)
    {
        return $query->when(
            $status,
            function ($q) use ($status) {
                return $q->whereIn('status', $status);
            }
        );
    }
}
