<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Deferred
 *
 * @property int $id
 * @property string $user_id
 * @property string $enter_id
 * @property string $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereEnterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deferred whereUserId($value)
 * @mixin \Eloquent
 */
class Deferred extends Model
{
    //
}
