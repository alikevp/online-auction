<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Attribute
 *
 * @property-read \App\Offer $offer
 * @mixin \Eloquent
 * @property int $id
 * @property string $slug
 * @property string $product_id
 * @property string $offer_id
 * @property string $name
 * @property string $category_id
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereOfferId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attribute whereValue($value)
 * @property bool $filter
 * @property string|null $upload_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute uniqueSlug()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereFilter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereUploadId($value)
 */
class Attribute extends Model
{
    protected $fillable = [
        'filter',
    ];

    use LogsActivity;
    protected static $recordEvents = ['created', 'updated','deleted'];
    protected static $logOnlyDirty = true;
    protected static $logAttributes = [
        'filter',
    ];

    public $timestamps = false;

    public $incrementing = false;

    public function offer()
    {
        return $this->belongsTo('App\Offer', 'offer_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeUniqueSlug($query)
    {
        return $query->get()->groupBy('slug');
    }
}
