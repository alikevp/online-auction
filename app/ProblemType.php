<?php

namespace App;

/**
 * App\ProblemType
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Problem[] $problems
 * @property-read \App\Type $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categories()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categoryByPost($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term problemTypes()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term tags()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemType whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProblemType extends Term
{
    public function problems()
    {
        return $this->belongsToMany(Problem::class);
    }
}
