<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Referral
 *
 * @property int $id
 * @property int $referral
 * @property string|null $deal
 * @property bool $bought
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereBought($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereDeal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Referral whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Referral extends Model
{
    protected $fillable = [
        'referral', 'bought'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'referral');
    }
}
