<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Finance
 *
 * @package App
 * @mixin /Eloquent
 * @property int $id
 * @property int $user_id
 * @property float $balance
 * @property float $debit
 * @property float $credit
 * @property float $total
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereDebit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Finance whereUserId($value)
 */
class Finance extends Model
{
    protected $fillable = [
        'user_id', 'balance', 'total', 'debit', 'credit'
    ];
    /**
     * Привязанный пользователь
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
