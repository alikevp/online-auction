<?php
/**
 * Created by PhpStorm.
 * User: Alfred
 * Date: 24.07.2017
 * Time: 13:49
 */

namespace App\Repositories;

use App\Category;

class Categories
{
    /** Возвращает категории из меню
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function inMenus($id)
    {
        return Category::whereHas(
            'menus',
            function ($menus) use ($id) {
                $menus->where('id', $id);
            }
        )->get()->pluck('id');
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection|mixed|string
     */
    public static function inCategory($id)
    {
        $category = Category::find($id);

        if ($category->isLeaf()) {
            return $category->id;
        } else {
            return $category->getLeaves()->pluck('id');
        }
    }

    public static function getTree()
    {
        return Category::all()->toHierarchy();
    }
}
