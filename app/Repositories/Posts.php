<?php
/**
 * Created by PhpStorm.
 * User: Alfred
 * Date: 02.08.2017
 * Time: 17:05
 */

namespace App\Repositories;

use App\Post;
use Illuminate\Support\Collection;

class Posts
{
    /** Возвращает только опубликованные статьи
     *
     * @return Collection
     */
    public function getPublished()
    {
        return Post::latest()->where('published', true)->get();
    }

    /** Возвращает статьи по категориям
     *
     * @param $category string
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getCategories($category)
    {
        return $this->getByCategories($category)->paginate(20);
    }

    /** Получение категорий
     * @param $category string
     * @return \Eloquent
     */
    protected function getByCategories($category)
    {
        return Post::whereHas('terms', function ($term) use ($category) {
            $term->categories()->where('label', $category);
        });
    }

    /** Возвращает статьи по тегу
     *
     * @param $category string
     * @param $tag string
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getTags($category, $tag)
    {
        return $this->getByCategories($category)
            ->whereHas('terms', function ($term) use ($tag) {
                $term->tags()->where('label', $tag);
            })->paginate(20);
    }

    /** Вывод статьи по слагу из категории
     *
     * @param $category
     * @param $slug
     * @return mixed
     */
    public function getPostWithCategory($category, $slug)
    {
        return $this->getCategories($category)->where('slug', $slug)->first();
    }
}
