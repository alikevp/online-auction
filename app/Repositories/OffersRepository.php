<?php

namespace App\Repositories;

use App\Offer;

class OffersRepository
{
    protected $categories;
    protected $offer;

    public function __construct(Categories $categories, Offer $offer)
    {
        $this->categories = $categories;
        $this->offer = $offer;
    }

    /**
     * Возвращает товар из категорий или из меню
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function inCategories($id)
    {
        return $this->offer->whereHas(
            'category',
            function ($category) use ($id) {
                if (count($this->categories->inCategory($id)) > 1) {
                    $category->whereIn('id', $this->categories->inCategory($id));
                } else {
                    $category->where('id', $this->categories->inCategory($id));
                }
            }
        )->where('price', '!=', 0);
    }

    public function inProducer($producer)
    {
        return $this->offer->whereHas(
            'producers',
            function ($query) use ($producer) {
                $query->where('slug', $producer);
            }
        )->where('price', '!=', 0);
    }

    /**
     * Возвращает товар, с заданными атрибутами
     *
     * @param $products
     * @param $attributes
     * @return mixed
     */
    public function hasAttributes($products, $attributes)
    {
        return $products->whereHas(
            'attributes',
            function ($att) use ($attributes) {
                foreach ($attributes as $name => $value) {
                    $att->where('slug', $name)
                        ->whereIn('value', $value);
                }
            }
        );
    }

    /**
     * Возвращает товар, цена которого выше или равна $minPrice
     *
     * @param $products
     * @param $minPrice
     * @return mixed
     */
    public function minPrice($products, $minPrice)
    {
        return $products->where('price', '>=', $minPrice);
    }

    /**
     * Возвращает товар, цена которого ниже или равна $maxPrice
     *
     * @param $products
     * @param $minPrice
     * @return mixed
     */
    public function maxPrice($products, $minPrice)
    {
        return $products->where('price', '<=', $minPrice);
    }

    /**
     * Возвращает товары от выбранных производителей
     * @param $products
     * @param $producers
     * @return mixed
     */
    public function withProducer($products, $producers)
    {
        return $products->whereIn('producer', $producers);
    }
}
