<?php
/**
 * Created by PhpStorm.
 * User: Alfred
 * Date: 02.08.2017
 * Time: 12:56
 */

namespace App\Repositories;

use App\Producer;
use Illuminate\Support\Collection;

class Producers
{
    /**
     * @param $products Collection
     * @return Collection|static
     */
    public function getProducers($products)
    {
        return Producer::whereIn('slug', $products->pluck('producer'))
            ->orderBy('name', 'asc')
            ->get()
            ->unique('name');
    }
}
