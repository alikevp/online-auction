<?php
/**
 * Created by PhpStorm.
 * User: Alfred
 * Date: 02.08.2017
 * Time: 12:52
 */

namespace App\Repositories;

use App\Attribute;
use Illuminate\Support\Collection;

class Attributes
{
    /**Получает аттрибуты товара
     *
     * @param $products Collection
     * @return Collection|static
     */
    public function getAttributes($products)
    {
        return Attribute::whereIn('offer_id', $products->pluck('id'))
            ->select('slug', 'name', 'value')
            ->orderBy('value', 'asc')
            ->get()
            ->unique('value')
            ->groupBy('slug');
    }
}
