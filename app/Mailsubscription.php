<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mailsubscription
 *
 * @property int $id
 * @property string $name
 * @property string $lable
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription whereLable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mailsubscription whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mailsubscription extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('hash');
    }
}
