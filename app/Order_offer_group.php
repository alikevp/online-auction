<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_offer_group
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $order_id
 * @property string $shop_id
 * @property float $price
 * @property float $old_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $status
 * @property int|null $quantity
 * @property string|null $offer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_offer_delivery[] $deliveries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_offer_option[] $gifts
 * @property-read \App\Order $order
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_item[] $order_items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Shop[] $shops
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereOldPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_group whereUpdatedAt($value)
 */
class Order_offer_group extends Model
{
    public $incrementing = false;

    //

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function order_items()
    {
        return $this->hasMany('App\Order_item', 'order_id', 'order_id');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'connection_id', 'shop_id');
    }

    public function shops()
    {
        return $this->hasMany('App\Shop', 'id', 'shop_id');
    }

    public function gifts()
    {
        return $this->hasMany('App\Order_offer_option', 'group_id', 'id');
    }

    public function deliveries()
    {
        return $this->hasMany('App\Order_offer_delivery', 'group_id', 'id');
    }
}
