<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Shop_subscription
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $shop_id
 * @property string $category_id
 * @property string $e_mail
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereEMail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shop_subscription whereUpdatedAt($value)
 */
class Shop_subscription extends Model
{
    //
}
