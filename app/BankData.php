<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BankData
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $shop_id
 * @property int $type
 * @property string $bank_name
 * @property string $bank_full_name
 * @property string|null $bank_address
 * @property int $bank_bik
 * @property string $kor_account
 * @property string $recipient
 * @property string $account
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereBankBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereBankFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereKorAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereRecipient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankData whereUserId($value)
 * @mixin \Eloquent
 */
class BankData extends Model
{
    //
}
