<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;
/**
 * App\Page
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categories()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categoryByPost($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term problemTypes()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term tags()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Post
{
    protected $table = 'posts';

    public function setSlugAttribute($label)
    {
        function uniqueSlug($slug, $posts) {
            if($posts->contains('slug', $slug))
            {

                $slug = $posts->where('slug', $slug)->first()->slug;
                $strings = explode('-', $slug);

                if(is_numeric(array_last($strings)))
                {
                    $strings[count($strings) - 1] = (int)(array_last($strings)) + 1;
                    $slug = collect($strings)->implode('-');
                }else{
                    $slug = $slug.'-1';
                }

                $slug = uniqueSlug($slug, $posts);
            }

            return $slug;
        }

        if ($label == '') {
            $slug = str_slug($this->title);
        } else {
            $slug = str_slug($label);
        }

        if(isset($this->attributes['id']))
        {
            $posts= Post::where('id', '!=', $this->attributes['id'])->select('slug')->get();
        }else {
            $posts= Post::select('slug')->get();
        }
        $posts->unique();

        $slug = uniqueSlug($slug, $posts);

        $this->attributes['slug'] = $slug;
    }
}
