<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_offer_item
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $group_id
 * @property string $order_item_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_item whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_item whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_item whereUpdatedAt($value)
 * @property int|null $quantity
 * @property string $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_item whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_item whereQuantity($value)
 */
class Order_offer_item extends Model
{
    public $incrementing = false;
    //
}
