<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Compare_item
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $compare_id
 * @property string $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Compare_item whereCompareId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare_item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare_item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare_item whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Compare_item whereUpdatedAt($value)
 */
class Compare_item extends Model
{
    //
}
