<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order_offer_delivery
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $group_id
 * @property string $type
 * @property string $company
 * @property float $price
 * @property int $delivery_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereDeliveryTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order_offer_delivery whereUpdatedAt($value)
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order_offer_delivery whereDescription($value)
 */
class Order_offer_delivery extends Model
{
    public $incrementing = false;
    //
}
