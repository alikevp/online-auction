<?php

namespace App\Providers;

use app\Posts\Facades\PostFacade;
use App\Posts\Helpers\PostHelper;
use Illuminate\Support\ServiceProvider;

class PostsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Posts', PostHelper::class);
    }
}
