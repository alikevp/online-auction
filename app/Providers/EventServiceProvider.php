<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Order\Confirmed' => [
            'App\Listeners\Order\NotifyUser',
            'App\Listeners\Order\NotifyAdmins',
            'App\Listeners\Spam\SendSpam',
            'App\Listeners\Order\OrderNotifyToAdmin',
        ],
        'App\Events\Shops\Register' =>[
            'App\Listeners\Shop\RegisterShop',
            'App\Listeners\Shop\RegisterShopToAdmin',
        ],
        'App\Events\Order\StatusChanged' =>[
            'App\Listeners\Order\StatusChangeToUser',
            'App\Listeners\Order\StatusChangeToAdmin',
        ],
        'App\Events\Order\OfferChanged' =>[
            'App\Listeners\Order\ChangeToUser',
            'App\Listeners\Order\ChangeToAdmin',
        ],
        'App\Events\Order\OfferNew' =>[
            'App\Listeners\Order\OfferNewToUser',
            'App\Listeners\Order\OfferNewToShop',
        ],
        'App\Events\UserRegister'         => [
            'App\Listeners\SendVerify',
        ],
        'App\Events\Exchange\ExchangeExport'         => [
            'App\Listeners\Exchange\Export',
            'App\Listeners\Exchange\Import',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
