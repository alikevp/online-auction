<?php

namespace App\Providers;

use App\Category;
use App\Http\ViewComposers\NavComposer;
use App\Http\ViewComposers\SupportFormComposer;
use Cache;
use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!Cache::has('categories')) {
            $categories = Cache::remember(
                'categories',
                24 * 20,
                function () {
                    return Category::where('offers_count', '>', 0)->orWhere('leafs_offers_count', '>', 0)->get(
                    )->toHierarchy();
                }
            );

        } else {
            $categories = Cache::get('categories');
        }
        view()->share(compact('categories'));


        View::composer('partials.supportForm', SupportFormComposer::class);

        view()->composer('partials.catalogAndSearch', NavComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
