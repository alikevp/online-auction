<?php

namespace App\Providers;

use Barryvdh\Debugbar\ServiceProvider as DebugBar;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_ALL, 'ru_RU.utf8');
        if ($this->app->environment() !== 'production') {
            Artisan::call('cache:clear');
        }


        if (file_exists($assetsFile = __DIR__ . '/../../resources/assets/admin/assets.php')) {
            include $assetsFile;
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(DebugBar::class);
            $this->app->alias('Debugbar', DebugBar::class);
        }
    }
}
