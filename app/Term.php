<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Term
 *
 * @package App
 * @mixin /Eloquent
 * @property string name, label
 * @property int $id
 * @property string $label
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read \App\Type $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categories()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term categoryByPost($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term problemTypes()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term tags()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereUpdatedAt($value)
 */
class Term extends Model
{
    protected $fillable = [
        'name',
    ];

    protected $table = 'terms';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'term_id');
    }

    /**
     * Делаем слаг из имени
     * @param $label
     */
    public function setLabelAttribute($label)
    {
        if ($label == '') {
            $this->attributes['label'] = str_slug($this->name);
        } else {
            $this->attributes['label'] = str_slug($label);
        }
    }

    public function scopeCategories($query)
    {
        return $query->whereHas('type', function ($type) {
            $type->where('type', 'category');
        });
    }

    public function scopeCategoryByPost($query, $id)
    {
        return $query->whereHas('posts', function ($type) use ($id) {
            $type->where('id', $id);
        });
    }

    public function scopeTags($query)
    {
        return $query->whereHas('type', function ($type) {
            $type->where('type', 'tag');
        });
    }

    public function scopeProblemTypes($query)
    {
        return $query->whereHas('type', function ($type) {
            $type->where('type', 'support');
        });
    }
}
