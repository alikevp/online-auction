<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Products_synonym
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $product_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Products_synonym whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products_synonym whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products_synonym whereProductId($value)
 */
class Products_synonym extends Model
{
    //
}
