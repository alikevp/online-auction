<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model_minus_word
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $word
 * @property string $language
 * @property string $type
 * @property int $activity
 * @method static \Illuminate\Database\Query\Builder|\App\Model_minus_word whereActivity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model_minus_word whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model_minus_word whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model_minus_word whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model_minus_word whereWord($value)
 */
class Model_minus_word extends Model
{
    //
}
