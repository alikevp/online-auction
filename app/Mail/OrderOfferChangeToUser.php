<?php

namespace App\Mail;

use App\Order;
use App\User;
use App\Offer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderOfferChangeToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $offer;
    public $subscribe;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $order, $offer,$subscribe)
    {
        $this->user = $user;
        $this->order = $order;
        $this->offer = $offer;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ставка изменена');
        return $this->markdown('email.orderOfferChangeToUser');
    }
}
