<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmedToShop extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $orders;
    public $subscribe;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $orders
     * @param $subscribe
     */
    public function __construct($user, $orders,$subscribe)
    {

        $this->user = $user;
        $this->orders = $orders;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Новый заказ');
        return $this->markdown('email.orderConfirmedToShop');
    }
}
