<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderStatusChangeToShop extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $subscribe;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $order
     */
    public function __construct($user, $order,$subscribe)
    {
        $this->user = $user;
        $this->order = $order;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Статус аукциона изменен');

        return $this->markdown('email.orderStatusChangeToShop');
    }
}
