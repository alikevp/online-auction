<?php

namespace App\Mail;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestingMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $order, $products, $url;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     * @param \App\Order $order
     * @param $products
     * @param $url
     */
    public function __construct(User $user, Order $order, $products, $url)
    {
        $this->user = $user;
        $this->order = $order;
        $this->products = $products;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.orderConfirmedToShop');
    }
}
