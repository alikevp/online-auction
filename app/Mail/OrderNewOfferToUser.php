<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderNewOfferToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $shop;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     * @param $order
     * @param $shop
     */
    public function __construct($user, $order, $shop)
    {
        $this->user = $user;
        $this->order = $order;
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Новая ставка');

        return $this->markdown('email.orderNewOfferToUser');
    }
}
