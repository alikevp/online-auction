<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmedToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $orders, $subscribe;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     * @param $orders
     * @param $subscribe
     */
    public function __construct($user, $orders, $subscribe)
    {
        $this->user = $user;
        $this->orders = $orders;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ваш заказ сформирован');
        return $this->markdown('email.orderConfirmedToUser');
    }
}
