<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSpamMail extends Mailable
{
    use Queueable, SerializesModels;

//    public $mails;

    /**
     * Create a new message instance.
     *
     * @param array $mails
     */
    public function __construct()
    {
//        $this->mails = $mails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ваш заказ сформирован');
        return $this->view('email.spamNewOrderToShops');
    }
}
