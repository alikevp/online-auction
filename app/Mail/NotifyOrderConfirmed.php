<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyOrderConfirmed extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    public $user, $products;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $products
     */
    public function __construct($user, $products)
    {
        $this->user = $user;
        $this->products = $products;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Новый заказ');
        return $this->markdown('email.orderConfirmed');
    }
}
