<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeliveryRegisterShop extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $shop;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$shop)
    {
        $this->user = $user;
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Успешная регистрация магазина');
        $this->from('documents@jinnmart.ru');

        return $this->markdown('email.RegisterShop');
    }
}
