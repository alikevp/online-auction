<?php

namespace App\Mail;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class orderStatusChangeToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $subscribe;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     * @param $order
     */
    public function __construct(User $user, $order,$subscribe)
    {
        $this->user = $user;
        $this->order = $order;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Статус аукциона изменен');
        return $this->markdown('email.orderStatusChangeToUser');
    }
}
