<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyDealingReview extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    public $user;
    public $dealing;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $dealing
     * @internal param $products
     */
    public function __construct($user, $dealing)
    {
        $this->user = $user;
        $this->dealing = $dealing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ваши отзывы');

        return $this->markdown('email.dealingsReviews');
    }
}
