<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConcurentOfferToShops extends Mailable
{
    use Queueable, SerializesModels;

    public $shop;
    public $order;

    /**
     * Create a new message instance.
     *
     * @param $shop
     * @param $order
     */
    public function __construct($shop, $order)
    {
        $this->shop = $shop;
        $this->order = $order;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Новая ставка конкурента');

        return $this->markdown('email.orderConcurentOfferToShops');
    }
}
