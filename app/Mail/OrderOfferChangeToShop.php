<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderOfferChangeToShop extends Mailable
{
    use Queueable, SerializesModels;

    public $shop;
    public $order;
    public $subscribe;
    public $user;
    public $offer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $offer, $shop, $order, $subscribe)
    {
        $this->shop = $shop;
        $this->user = $user;
        $this->order = $order;
        $this->subscribe = $subscribe;
        $this->offer = $offer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ставка изменена');

        return $this->markdown('email.orderOfferChangeToShop');
    }
}
