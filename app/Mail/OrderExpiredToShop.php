<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderExpiredToShop extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $orders;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct(User $user,$orders)
    {
        $this->user = $user;
        $this->orders = $orders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Срок аукциона истек');
        return $this->markdown('email.orderExpiredToShop');
    }
}
