<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderNewNotifyToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $orders;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     * @param $orders
     */
    public function __construct($user, $orders)
    {
        $this->user = $user;
        $this->orders = $orders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Сформирован новый заказ');
        return $this->markdown('email.admin_notify.orderConfirmedToAdmin');
    }
}
