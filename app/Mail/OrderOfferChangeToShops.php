<?php

namespace App\Mail;

use App\Order;
use App\User;
use App\Offer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderOfferChangeToShops extends Mailable
{
    use Queueable, SerializesModels;

    public $shop;
    public $order;
    public $subscribe;
    public $user;
    public $offer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $offer, $shop, $order, $subscribe)
    {
        $this->shop = $shop;
        $this->user = $user;
        $this->order = $order;
        $this->subscribe = $subscribe;
        $this->offer = $offer;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Ставка конкурента изменена');
        return $this->markdown('email.orderOfferChangeToShops');
    }
}
