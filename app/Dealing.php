<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Dealing
 *
 * @property string $id
 * @property string $order_id
 * @property string $offer_id
 * @property string $shop_id
 * @property string $user_id
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property float $deal_amount
 * @property float $award_size
 * @property float $award_amount
 * @property float $nds
 * @property int $payment_type
 * @property int $bonus_type
 * @property string|null $bonus_user_id
 * @property float|null $bonus_amount
 * @property int $item_recieving_status
 * @property string|null $item_recieving_date
 * @property int|null $shop_result
 * @property string|null $shop_result_date
 * @property string|null $curator_result_date
 * @property string|null $bill_send_date
 * @property string|null $bill_read_date
 * @property string|null $sync_date
 * @property bool|null $sync_need
 * @property-read \App\User|null $bonus_user
 * @property-read \App\Order_delivery $delivery
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $order
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order_item[] $order_items
 * @property-read \App\Order_item $order_offer_items
 * @property-read \App\Shop $shop
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereAwardAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereAwardSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereBillReadDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereBillSendDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereBonusAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereBonusType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereBonusUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereCuratorResultDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereDealAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereItemRecievingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereItemRecievingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereNds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereMailSend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereShopResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereShopResultDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereSyncDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereSyncNeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereUserId($value)
 * @mixin \Eloquent
 * @property float|null $reward
 * @property bool|null $mail_send
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dealing whereReward($value)
 */
class Dealing extends Model
{

    public $incrementing = false;

    public function order()
    {
        return $this->hasMany('App\Order', 'id', 'order_id');
    }

    public function delivery()
    {
        return $this->belongsTo('App\Order_delivery', 'order_id', 'order_id');
    }

    public function order_items()
    {
        return $this->hasMany('App\Order_item', 'order_id', 'order_id');
    }

    public function order_offer_items()
    {
        return $this->belongsTo('App\Order_item', 'order_id', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function bonus_user()
    {
        return $this->belongsTo('App\User', 'bonus_user_id', 'id');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shop_id', 'id');
    }
}
