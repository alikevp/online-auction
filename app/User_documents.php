<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\User_documents
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $user_id
 * @property string $code
 * @property string $serie
 * @property string $number
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereSerie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User_documents whereUserId($value)
 * @mixin \Eloquent
 */
class User_documents extends Model
{
    //
}
