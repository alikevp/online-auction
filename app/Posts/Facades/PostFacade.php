<?php
/**
 * Created by PhpStorm.
 * User: Alfred
 * Date: 21.08.2017
 * Time: 8:39
 */

namespace app\Posts\Facades;

use App\Posts\Helpers\PostHelper;
use Illuminate\Support\Facades\Facade;

class PostFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PostHelper::class;
    }
}
