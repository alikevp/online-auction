<?php

namespace App\Posts\Helpers;

use App\Post;
use Baum\Node;

class PostHelper
{
    public static function get_posts($args)
    {
        return Post::with(
            array(
                'categories' => function ($query) {
                    $query->categories();
                },
                'tags' => function ($query) {
                    $query->tags();
                },
            )
        )
            ->when(
                isset($args['category']),
                function ($query) use ($args) {
                    return $query->whereHas(
                        'terms',
                        function ($term) use ($args) {
                            $term->categories()->whereIn('label', $args['category']);
                        }
                    );
                }
            )
            ->when(
                isset($args['tag_in']),
                function ($query) use ($args) {
                    return $query->whereHas(
                        'terms',
                        function ($term) use ($args) {
                            $term->tags()->whereIn('label', $args['tag_in']);
                        }
                    );
                }
            )
            ->when(
                isset($args['order_by']),
                function ($query) use ($args) {
                    return $query->orderBy($args['order_by'][0], $args['order_by'][1]);
                }
            )
            ->when(
                isset($args['slug']),
                function ($query) use ($args) {
                    return $query->where('slug', $args['slug']);
                }
            )
            ->when(
                isset($args['post_type']),
                function ($query) use ($args) {
                    return $query->where('post_type', $args['post_type']);
                }
            )
            ->get();
    }

    public static function get_post($args)
    {
        return Post::with(
            array(
                'categories' => function ($query) {
                    $query->categories();
                },
                'tags' => function ($query) {
                    $query->tags();
                },
            )
        )
            ->when(
                isset($args['category']),
                function ($query) use ($args) {
                    return $query->whereHas(
                        'terms',
                        function ($term) use ($args) {
                            $term->categories()->whereIn('label', $args['category']);
                        }
                    );
                }
            )
            ->when(
                isset($args['tag_in']),
                function ($query) use ($args) {
                    return $query->whereHas(
                        'terms',
                        function ($term) use ($args) {
                            $term->tags()->whereIn('label', $args['tag_in']);
                        }
                    );
                }
            )
            ->when(
                isset($args['order_by']),
                function ($query) use ($args) {
                    return $query->orderBy($args['order_by'][0], $args['order_by'][1]);
                }
            )
            ->when(
                isset($args['slug']),
                function ($query) use ($args) {
                    return $query->where('slug', $args['slug']);
                }
            )
            ->when(
                isset($args['post_type']),
                function ($query) use ($args) {
                    return $query->where('post_type', $args['post_type']);
                }
            )
            ->first();

    }

    public static function get_post_children($args)
    {
        return Post::where('slug', $args['slug'])->first()->getDescendants()->toHierarchy();

    }

    public static function get_post_and_children($args)
    {
        return Post::where('slug', $args['slug'])->first()->getDescendantsAndSelf()->toHierarchy();

    }
}
