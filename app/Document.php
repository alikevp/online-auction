<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Document
 *
 * @property int $id
 * @property string $type
 * @property string $code
 * @property string $serie
 * @property string $number
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereSerie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereUserId($value)
 * @mixin \Eloquent
 */
class Document extends Model
{
    //
}
