<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoComment extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function comment_shop()
    {
        return $this->belongsTo('App\Comment', 'comment_shop_id');
    }
    public function comment_product()
    {
        return $this->belongsTo('App\Comment', 'comment_product_id');
    }
    public function comment_us()
    {
        return $this->belongsTo('App\Comment', 'comment_about_us_id');
    }
}
