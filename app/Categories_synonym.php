<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Categories_synonym
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $category_id
 * @property string $category_slug
 * @property string $name
 * @property string $shop_id
 * @method static \Illuminate\Database\Query\Builder|\App\Categories_synonym whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Categories_synonym whereCategorySlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Categories_synonym whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Categories_synonym whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Categories_synonym whereShopId($value)
 */
class Categories_synonym extends Model
{
    public $fillable = [
        'category_id', 'category_slug', 'name', 'shop_id'
    ];
    public $timestamps = false;
}
