<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Issue
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $problem_id
 * @property string $name
 * @property string $middle
 * @property string $email
 * @property string $tel
 * @property string $theme
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Problem|null $problem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereMiddle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereProblemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Issue whereUserId($value)
 * @mixin \Eloquent
 */
class Issue extends Model
{
    public function problem()
    {
        return $this->belongsTo(Problem::class);
    }
}
