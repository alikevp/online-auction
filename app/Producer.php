<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Producer
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Offer[] $products
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\App\Producer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Producer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Producer whereSlug($value)
 */
class Producer extends Model
{
    public $timestamps = false;

    public $incrementing = false;


    public function products()
    {
        return $this->belongsTo(Offer::class, 'producer');
    }
}
