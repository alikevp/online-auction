<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Producers_to_categories
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Producers_to_categories whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Producers_to_categories whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Producers_to_categories whereUpdatedAt($value)
 */
class Producers_to_categories extends Model
{
    //
}
