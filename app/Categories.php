<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Categories
 *
 * @property int $id
 * @property string|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property string $name
 * @property string $slug
 * @property bool $in_menu
 * @property int $checked
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $offers_count
 * @property int $leafs_offers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories uniqueSlug()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereLeafsOffersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereOffersCount($value)
 */
class Categories extends Model
{

    public function scopeUniqueSlug($query)
    {
        return $query->get()->groupBy('slug');
    }
}
