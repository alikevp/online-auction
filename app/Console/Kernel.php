<?php

namespace App\Console;

use App\Console\Commands\CheckDealingsMail;
use App\Console\Commands\OrderStatusChange;
use App\Console\Commands\ReferralSystem;
use App\Console\Commands\ScheduleListCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ReferralSystem::class,
        OrderStatusChange::class,
        ScheduleListCommand::class,
        CheckDealingsMail::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('referral:send')
            ->weekly()->saturdays()->at('13:00');

        $schedule->command('order:check')
            ->everyMinute();

        $schedule->command('dealing:check')
            ->dailyAt('12:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
