<?php

namespace App\Console\Commands;

use App\Dealing;
use App\Mail\NotifyDealingReview;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckDealingsMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dealing:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяет статус отправки email у сделок';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dealings = Dealing::whereMailSend(false)->get();

        foreach ($dealings as $dealing) {
            if ($dealing->user != null) {
                if (!Carbon::now()->between($dealing->created_at, $dealing->created_at->addDays(10))) {
                    $mail = new NotifyDealingReview($dealing->user, $dealing);
                    \Mail::to($dealing->user->email)->send($mail);
                    $dealing->mail_send = true;
                    $dealing->save();
                }
            }
        }
        $this->info('Mail has been send');
    }
}
