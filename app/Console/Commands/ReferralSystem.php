<?php

namespace App\Console\Commands;

use App\Mail\ReferralSystem as RF;
use App\User;
use Illuminate\Console\Command;
use Mail;

class ReferralSystem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправить пользователям оповещение о реферальной системе';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $user = User::whereEmail('valfred88@gmail.com')->first();
//        Mail::to($user->email)->send(new RF($user));

        foreach (User::where('type', '=', "buyer")->get() as $user) {
            if (count($user->referrals) == 0) {
                $subscription = Mailsubscription_user::where('user_id', $user->id)
                    ->whereHas(
                        'mailsubscription',
                        function ($query) {
                            $query->where('lable', 'buyer_promotion');
                        }
                    )->first();

                if (isset($subscription)) {
                    $subscribe = url('mail/unsubscribe/'.$subscription->hash);
                    Mail::to($user->e_mail)->send(new RF($user, $subscribe));
                }
            }

            $this->info('The messages were sent successfully!');
        }
    }
}
