<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use Carbon\Carbon;

class OrderStatusChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Изменить статус просроченных аукционов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        echo "Orders status check activate";
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::all();
        $now = Carbon::now();

        foreach ($orders as $order) {
            // Если срок заказа истёк и его статус активен
            if (($order->active_till <= $now) && ($order->status == 1)) {
                $order->status = 2;
                $order->save();
            }
        }
    }
}
