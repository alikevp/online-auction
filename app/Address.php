<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Address
 *
 * @property int $id
 * @property string|null $country
 * @property string $city
 * @property string|null $street
 * @property string|null $house
 * @property string|null $apartment
 * @property string|null $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $post_index
 * @property string|null $region
 * @property int|null $type
 * @property string|null $shop_id
 * @property-read mixed $full_address
 * @property-read \App\User $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereApartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address wherePostIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address whereUserId($value)
 * @mixin \Eloquent
 */
class Address extends Model
{
    protected $fillable = [
        'country',
        'city',
        'street',
        'house',
        'apartment',
        'post_index',
        'region',
    ];

    /**
     * Привязка адреса к пользователю
     *
     * @return mixed
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullAddressAttribute()
    {
        $full_address = $this->attributes['city'].' '.
            $this->attributes['street'].' '.
            $this->attributes['house'].' '.'кв. '.
            $this->attributes['apartment'];

        return $full_address;
    }
}
