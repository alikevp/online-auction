<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\cart
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $user_id
 * @property string $enter_id
 * @property string $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereEnterId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\cart whereUserId($value)
 */
class cart extends Model
{
    //
}
