<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Spam_base extends Model
{
    use LogsActivity;

    protected $fillable = ['name', 'text'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
