<?php

namespace App\Helpers;

use App\Post;

class Posts{

    public static function get_posts($args){
        return Post::with(array(
            'categories' => function($query)
                {
                    $query->categories();
                },
            'tags' => function($query)
                {
                    $query->tags();
                }
            )
        )
            ->when(isset($args['category']), function($query) use ($args){
                return $query-> whereHas('terms', function ($term) use ($args) {
                    $term->categories()->whereIn('label', $args['category']);
                });
            })
            ->when(isset($args['tag_in']), function($query) use ($args){
                return $query-> whereHas('terms', function ($term) use ($args) {
                    $term->tags()->whereIn('label', $args['tag_in']);
                });
            })
            ->when(isset($args['order_by']), function($query) use ($args){
                return $query->orderBy($args['order_by'][0], $args['order_by'][1]);
            })
            ->get();

    }

}
