<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\support
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $name
 * @property string|null $middle
 * @property string|null $email
 * @property string|null $tel
 * @property string|null $type
 * @property string|null $theme
 * @property string|null $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereMiddle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\support whereUserId($value)
 * @mixin \Eloquent
 */
class support extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'middle',
        'email',
        'tel',
        'type',
        'theme',
        'message',
    ];
}

