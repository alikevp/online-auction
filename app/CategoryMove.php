<?php

namespace App;

use Baum\Node;
use Spatie\Activitylog\Traits\LogsActivity;

//use SebastianBergmann\CodeCoverage\Report\Xml\Node;

/**
 * App\Category
 *
 * @property-read \App\Category $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $child
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Offer[] $products
 * @mixin \Eloquent
 * @property string $id
 * @property string $parent_id
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $slug
 * @property bool $in_menu
 * @property int $checked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attribute[] $attributes
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Category[] $children
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Menu[] $menus
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Shop[] $shops
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Eloquent\Builder|\Baum\Node withoutSelf()
 * @property int $offers_count
 * @property int $leafs_offers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereLeafsOffersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereOffersCount($value)
 */
class CategoryMove extends Category
{

}
