<!-- Start Top Bar -->
<div class="row" id="navbar">
    <div class="medium-12 header columns">
        <div class="large-2 medium-2 small-12 columns logo-div">
            <a href="/">
                <img src="{{ asset('images/logo.png') }}" class="logo-img" alt="logo">
            </a>
        </div>
        <div class="large-10 medium-10 small-12 columns header-right">
            <div class="medium-12 small-12 columns">
                <div class="large-2 medium-12 small-12  columns logo-text">
                    <span class="shop-name">аукци<span>on-line</span></span>
                </div>
                <div class="large-6 medium-6 small-6 columns hide-for-small-only form-left-side">
                    <span class="shop-info">Хочешь получить <span>ЛУЧШУЮ ЦЕНУ? - </span>проведи <span>СОБСТВЕННЫЙ АУКЦИОН!</span></span>
                </div>
                <div class="large-4 medium-6 small-6 columns hide-for-small-only sellers">
                    <div class="medium-12 columns form-right-side">
                        <span class="sellers-count">{{session()->get('count_shop')}}</span><br>
                        <span class="sellers-text">продавцов</span>
                    </div>
                </div>
            </div>
            <div class="large-8 medium-12 small-12 columns" id="search">
                <div class="search-bar-div">
                    <form method="get" action="{{ action('ShoppingController@search') }}">
                        <div class="row search-row collapse">
                            <div class="medium-7 small-10 search-column columns">
                                <input name="search"
                                       id="search"
                                       type="text"
                                       list="suggestions"
                                       class="search-input"
                                       @keyup="find"
                                       v-model="query"
                                       placeholder="Поиск по сайту..."
                                       autocomplete="off">
                                <div class="suggestion-div medium-5 columns">
                                    <datalist id="suggestions">
                                        <option v-for="suggest in suggestion">
                                            {{--<a href="#">--}}
                                            @{{ suggest }}
                                            {{--</a>--}}
                                        </option>
                                        {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                                    </datalist>
                                    {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                                </div>
                                {{--<p v-for="suggest in suggestion">--}}
                                {{--@{{ suggest.name }}--}}
                                {{--</p>--}}
                            </div>
                            <div class="medium-4 example-column columns hide-for-small-only">
                                <span class="example-span">например, смартфон</span>
                            </div>
                            <div class="medium-1 small-2 button-column columns">
                                <button type="submit"
                                        class="button search-button"
                                        @click="find">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="large-4 medium-12 small-12 columns">
                @if(Auth::check())
                    <?php $user = Auth::user(); ?>
                    @if($user->type == 'shop_admin')
                        @include('partials.navRightMenuForAdmin')
                    @else
                        @include('partials.navRightMenuForUser')
                    @endif
                @else
                    @include('partials.navRightMenuForUser')
                @endif
            </div>
        </div>
    </div>
    <hr class="main-hr">
    <div class="medium-12 category-menu-div columns">
        <ul class="dropdown menu category-menu" data-dropdown-menu>
            @include('partials.category')
        </ul>
    </div>
</div>
{{--<script>--}}
{{--let searchUrl = "{{ URL::to('api/suggest') }}";--}}
{{--</script>--}}
{{--<!-- End Top Bar -->--}}
