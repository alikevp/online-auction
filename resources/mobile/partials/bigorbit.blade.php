{{--<div class="medium-12 medium-centered columns category-info">--}}
    {{--<span class="category-name">{{ $category->name }}</span>--}}
    {{--<hr class="category-hr">--}}
    {{--<span class="category-tag">НОВИНКИ</span>--}}
{{--</div>--}}
<div class="owl-carousel">
    {{--@foreach($category->products->chunk(7) as $chunk)--}}
        @foreach($offers as $product)
            <div class="medium-12 columns item-column">
                <div class="to-top">
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        <img src="{{ URL::to(array_first($product->files)) }}" class="item-img" alt=""><br>
                    </a>
                    @if($product->producer == "" || $product->producer == "0")
                        <span class="item-brand">Производитель не указан</span><br>
                    @else
                        <span class="item-brand">{{ strtoupper($product->producer) }}</span><br>
                    @endif
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        @if($product->name == "" || $product->name == "0")
                            <span class="item-name">Название не указано</span><br>
                        @else
                            <span class="item-name">{{ $product->name }}</span><br>
                        @endif
                    </a>
                </div>
                <div class="to-bottom">
                    <div class="star-rating">
                        <div class="star-rating__wrap">
                            <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled @if($product->rating == 5) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="Отлично"></label>
                            <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" disabled @if($product->rating == 4) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="Хорошо" ></label>
                            <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled @if($product->rating == 3) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="Приемлимо"></label>
                            <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled @if($product->rating == 2) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="Плохо"></label>
                            <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled @if($product->rating == 1) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="Очень плохо"></label>
                        </div>
                    </div>
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        @if($product->price == "" || $product->price == 0)
                            <span class="item-cost">Цена не указана</span>
                        @else
                            <span class="item-cost">~{{ $product->price }} руб.</span>
                        @endif
                    </a>
                    <br>
                    <a href="#" class="button add-item-to-basket-button in_cart_add" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                </div>
            </div>
        @endforeach
    {{--@endforeach--}}
</div>

<hr class="items-hr">