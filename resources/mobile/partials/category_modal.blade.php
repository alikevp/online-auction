<div class="reveal large" id="{{ $target }}" data-reveal>
    <h1 class="modal-h1">{{ $modal_title }}</h1>
    <div class="medium-6 columns">
        <div class="search-bar-div">
            <div class="row search-row collapse">
                <div class="medium-8 search-column columns">
                    <input type="text"
                           class="search-input"
                           placeholder="Поиск категорий"
                           autocomplete="off"
                           v-model="search_text"
                           @keyup.enter="find">
                </div>
                <div class="medium-4 button-column columns">
                    <button type="submit"
                            class="button search-button float-right"
                    @click="find">
                    Найти
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="medium-6 columns">
        <div class="search-bar-div">
            <form method="get" action=".">
                <div class="row search-row collapse">
                    <div class="medium-8 search-column columns">
                        <input type="text"
                               class="search-input"
                               placeholder="Поиск категорий"
                               autocomplete="off"
                               v-model="search_id"
                               @keyup.enter="find">
                    </div>
                    <div class="medium-4 button-column columns">
                        <button type="submit"
                                class="button search-button float-right"
                        @click="find">
                        Найти
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br><br>
    {{--<div class="medium-12 columns items-row category-tree">--}}
        {{--<ul class="vertical menu" id="category-tree">--}}
            {{--<category class="category" :model="categories"></category>--}}
            {{--<a class="category-controls-name" href="#">--}}
            {{--<span>@{{ category.name }}</span>--}}
            {{--</a>--}}
            {{--<div class="category-controls">--}}
            {{--<a href="#">Сделать дочерней</a>--}}
            {{--</div>--}}
            {{--<ul class="menu vertical nested" v-show="false">--}}
            {{--<li>--}}
            {{--<a class="category-controls-name" href="#">--}}
            {{--<span></span>--}}
            {{--</a>--}}
            {{--<div class="category-controls">--}}
            {{--<a href="#">Сделать дочерней</a>--}}
            {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="category-controls-name"--}}
            {{--href="#"><span>Категория 1.2</span></a>--}}
            {{--<div class="category-controls">--}}
            {{--<a href="#">Сделать дочерней</a>--}}
            {{--</div>--}}
            {{--<div class="category-content items-row ">--}}
            {{--<div class="row itemsrow">--}}
            {{--<div class="medium-12 columns">--}}
            {{--<div class="orbit" role="region" data-orbit>--}}
            {{--<ul class="orbit-container">--}}
            {{--<button class="orbit-previous"><span--}}
            {{--class="show-for-sr">Previous Slide</span><i--}}
            {{--class="fa fa-chevron-left"--}}
            {{--aria-hidden="true"></i></button>--}}
            {{--<button class="orbit-next"><span--}}
            {{--class="show-for-sr">Next Slide</span><i--}}
            {{--class="fa fa-chevron-right"--}}
            {{--aria-hidden="true"></i></button>--}}
            {{--<li class="is-active orbit-slide">--}}
            {{--<div class="medium-12 columns">--}}
            {{--<div class="medium-2 columns">--}}
            {{--<img src="http://placehold.it/170x170"--}}
            {{--alt=""><br>--}}
            {{--<span class="itembrand">Производитель</span><br>--}}
            {{--<a href="#">--}}
            {{--<span class="itemname">Название</span><br>--}}
            {{--</a>--}}
            {{--<span class="itemcost">Цена</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
    <button class="close-button" data-close aria-label="Close modal" type="button">
        Отмена
    </button>
</div>