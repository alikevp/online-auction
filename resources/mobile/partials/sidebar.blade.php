<div class="title-bar" data-responsive-toggle="left-block" data-hide-for="large" style="background: #8bc63e; margin-bottom: 10px;">
    <button class="menu-icon" type="button" data-toggle="left-block"></button>
    <div class="title-bar-title">Фильтр</div>
</div>
<div class="large-3 medium-12 small-12 columns sidebar" id="left-block">
    <ul class="vertical dropdown menu" data-dropdown-menu>
        @if(session()->has('categories'))
            @include("partials.sidebar_part",["categories"=>session()->get('categories')])
        @else
            @include("partials.sidebar_part",["categories"=>$categories])
        @endif
    </ul>
    <form action="{{ url('filter') }}" method="get">
        <div class="medium-12 columns filters-div">
            <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                <li class="accordion-item is-active" data-accordion-item>
                    <a href="#" class="accordion-title">Цена, р</a>
                    <div class="accordion-content" id="price-content-div" data-tab-content>
                        <table>
                            <tr>
                                <td id="input-1-td">
                                    <input type="number" id='sliderOutput1' v-model="min" name="min">
                                </td>
                                <td id="dash-td">—</td>
                                <td id="input-2-td">
                                    <input type="number" id='sliderOutput2' v-model="max" name="max">
                                </td>
                            </tr>
                        </table>
                        <div class="slider" id="priceSlider" data-slider
                             data-initial-start='{{ $products->min('price') }}'
                             data-initial-end='{{ $products->max('price') }}'
                             data-start='{{ $products->min('price') }}'
                             data-end='{{ $products->max('price') }}'>
                            <span class="slider-handle"
                                  data-slider-handle role="slider"
                                  tabindex="1"
                                  aria-controls='sliderOutput1'></span>
                            <span class="slider-fill" data-slider-fill></span>
                            <span class="slider-handle"
                                  data-slider-handle role="slider"
                                  tabindex="1"
                                  aria-controls='sliderOutput2'></span>
                        </div>
                        <span onclick="$('#price-content-div').parent().parent().foundation('up', $('#price-content-div'));"
                              class="togle float-right">Свернуть</span>
                        <br><br>
                    </div>
                </li>
                <li class="accordion-item is-active" data-accordion-item>
                    <a href="#" class="accordion-title">Производитель</a>
                    <div class="accordion-content" id="brand-content-div" data-tab-content>
                        <div class="brand-div">

                            @foreach($producers as $producer)
                                <div>
                                    <input type="checkbox" id="{{ $producer->slug }}"
                                           value="{{ $producer->slug }}" name="producer[]"
                                           @if(isset($_GET['producer']))
                                            @if(in_array($producer->slug, $_GET['producer'])) checked @endif
                                            @endif
                                    >
                                    <label for="{{ $producer->slug }}">{{ $producer->name }}</label>
                                </div>
                                <br>
                            @endforeach
                            <br>
                        </div>
                        <span onclick="$('#brand-content-div').parent().parent().foundation('up', $('#brand-content-div'));"
                              class="togle float-right">Свернуть</span><br><br>
                    </div>
                </li>
                @foreach($attributes as $attribute)
                    <li class="accordion-item  @if(isset($_GET[$attribute->first()->slug])) is-active @endif" data-accordion-item>
                        <a href="#" class="accordion-title">{{ $attribute->first()->name }}</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="brand-div">
                                @foreach($attribute as $key=>$at)
                                    <div>
                                        <input type="checkbox"
                                               id="{{ $at->slug }}_{{ $key }}"
                                               value="{{ $at->value }}"
                                               name="attributes[{{ $at->slug }}][]"
                                               @if(isset($_GET['attributes'][$at->slug]))
                                               @if(in_array($at->value, $_GET['attributes'][$at->slug])) checked @endif
                                                @endif
                                        >
                                        <label for="{{ $at->slug }}_{{ $key }}">
                                            {{ $at->value }}
                                        </label>
                                    </div>
                                    <br>
                                @endforeach
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            {{--v-on:click="producer"--}}
            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--            <input type="hidden" name="category" value="{{ session()->get('category')->id }}">--}}
            <input type="hidden" name="order" id="order_type_input" value="price_asc">
            <input type="submit" class="button float-center" value="ПОКАЗАТЬ">
            <a href="#" class="full-search-link">Расширенный поиск</a>
        </div>
    </form>
</div>
