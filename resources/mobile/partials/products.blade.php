<div class="large-9 medium-12 small-12 columns ">
    <div class="medium-12 columns text-right">
        <select name="order" id="order_type_select">
            <option value="price_desc" @if(isset($_GET['order'])) @if($_GET['order'] == 'price_desc') selected @endif @endif>Цена &#8595;</i></option>
            <option value="price_asc"  @if(isset($_GET['order'])) @if($_GET['order'] == 'price_asc') selected @endif @else selected @endif>Цена &#8593;</option>
            <option value="name_desc" @if(isset($_GET['order'])) @if($_GET['order'] == 'name_desc') selected @endif @endif>Название &#8595;</i></option>
            <option value="name_asc" @if(isset($_GET['order'])) @if($_GET['order'] == 'name_asc') selected @endif @endif>Название &#8593;</option>
        </select>
    </div>
    @foreach($products->chunk(6) as $chunk)
        <div class="row small-up-2 medium-up-3 large-up-6 products">
            @foreach($chunk as $product)
                <div class="columns item-column">
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        <img src="{{ URL::to(array_first($product->files)) }}" class="item-img" alt=""><br>
                    </a>
                    <span class="item-brand">{{ strtoupper($product->producer) }}</span><br>
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        <span class="item-name">{{ $product->name }}</span><br>
                    </a>
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        @if($product->price == "" || $product->price == 0)
                            <span class="item-cost">Цена не указана</span>
                        @else
                            <span class="item-cost">~{{ $product->price }} руб.</span>
                        @endif
                    </a>
                    <br>
                    <a href="#" class="button add-item-to-basket-button in_cart_add"
                       data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                </div>
            @endforeach
        </div>
    @endforeach
    {{--{{ $products->appends(Request::only('search'))->links('vendor.pagination.foundation') }}--}}
</div>