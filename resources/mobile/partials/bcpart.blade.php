@if($category->isChild())
    @include('partials.bcpart', ['category' => $category->parent])
    <li><a href="{{ url('category', ['id' => $category->parent->id]) }}">{{ $category->parent->name }}</a></li>
@endif