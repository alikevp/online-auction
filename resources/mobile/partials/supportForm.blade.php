<a href="#" data-open="helpModal" id="popup__toggle" onclick="return false;">
    <div class="circlephone" style="transform-origin: center;"></div>
    <div class="circle-fill" style="transform-origin: center;"></div>
    <div class="img-circle" style="transform-origin: center;">
        <div class="img-circleblock" style="transform-origin: center;"><i class="fa fa-question" aria-hidden="true"></i>
        </div>
    </div>
</a>

<div class="reveal custom-medium-reveal"
     id="helpModal" data-reveal>
    <div class="medium-12 columns">
        <h3 id="modalTitle" class="modal-h-3">Форма для связи с поддержкой</h3>
        <div>
            <form id="support_form" method="post" @submit.prevent="saveRequest">
                <div class="row">
                    <div class="large-6 columns">
                        <label>Фамилия
                            <input type="text" placeholder="Иванов" name="middle" pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$"
                                   v-model="middle"/>
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Имя<span class="red-star">*</span>
                            <input type="text" placeholder="Иван" name="name" pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$"
                                   v-model="name" required/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Е-mail для связи<span class="red-star">*</span>
                            <input type="email" placeholder="test@mail.ru" name="email" v-model="email" required/>
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Телефон
                            <input type="text" maxlength="15" placeholder="8(999)999-99-99" name="tel"
                                   pattern="^[\s0-9+()-]+$" v-model="tel"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Причина<span class="red-star">*</span>
                            <select name="type" v-model="type" required>
                                <option value="" disabled selected>Выберите причину</option>
                                @foreach($problem_types as $problem_type)
                                    <option value="{{ $problem_type->id }}">{{ $problem_type->name }}</option>
                                @endforeach
                                {{--<option value="14">Вопрос</option>--}}
                                {{--<option value="16">Ошибка на сайте</option>--}}
                                {{--<option value="15">Отзыв</option>--}}
                                {{--<option value="17">Проблемы с заказом</option>--}}
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Тема<span class="red-star">*</span>
                            <input type="text" placeholder="Тема вашего сообщения" name="theme" v-model="theme"
                                   required/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Сообщение
                            <textarea placeholder="Опишите полный текст проблемы" rows="10" name="message"
                                      v-model="message" ></textarea>
                        </label>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{--<input type="hidden" name="user_id"  v-model="user_id" />--}}
                <div class="medium-10 medium-centered">
                    <button type="submit" class="button common-button button-accept">Отправить</button>
                    <a data-close="helpModal" class="button common-button button-decline">Отменить</a>
                </div>
            </form>
            <div id="ajax_update"></div>
        </div>
    </div>
    <button class="close-button" data-close
            aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
{{--модаль за продавца--}}
<a href="#" data-open="helpModal" id="popup__toggle" onclick="return false;">
    <div class="circlephone" style="transform-origin: center;"></div>
    <div class="circle-fill" style="transform-origin: center;"></div>
    <div class="img-circle" style="transform-origin: center;">
        <div class="img-circleblock" style="transform-origin: center;"><i class="fa fa-question" aria-hidden="true"></i>
        </div>
    </div>
</a>

<div class="reveal custom-medium-reveal"
     id="helpModal" data-reveal>
    <div class="medium-12 columns">
        <h3 id="modalTitle" class="modal-h-3">Форма для связи с поддержкой</h3>
        <div>
            <form>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Фамилия
                            <input type="text" placeholder="Иванов"/>
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Имя
                            <input type="text" placeholder="Иван"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Е-mail для связи
                            <input type="email" placeholder="test@mail.ru"/>
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Телефон
                            <input type="text" maxlength="15" placeholder="8(999)999-99-99"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Причина
                            <select>
                                <option value="question">Вопрос</option>
                                <option value="site_error">Ошибка на сайте</option>
                                <option value="comment">Отзыв</option>
                                <option value="order_problem">Проблемы с заказом</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Тема
                            <input type="text" placeholder="Тема вашего сообщения"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Сообщение
                            <textarea placeholder="Опишите полный текст проблемы" rows="10"></textarea>
                        </label>
                    </div>
                </div>
                <div class="medium-10 medium-centered">
                    <button type="submit" class="button common-button button-accept">Отправить</button>
                    <a data-close="helpModal" class="button common-button button-decline">Отменить</a>
                </div>
            </form>
        </div>
    </div>
    <button class="close-button" data-close
            aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>