<form action="">
    <div class="medium-12">
        <legend>Статус: </legend>
        <input id="checkbox1" name="status[]" value="1" type="checkbox" @if(null == app('request')->input('status')) checked @endif @if(null !== app('request')->input('status'))@if(in_array(1,app('request')->input('status'))) checked @endif @endif><label for="checkbox1">Активные</label><br>
        <input id="checkbox2" name="status[]" value="0" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(0,app('request')->input('status'))) checked @endif @endif><label for="checkbox2">Отмененные</label><br>
        <input id="checkbox3" name="status[]" value="2" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(2,app('request')->input('status'))) checked @endif @endif}}><label for="checkbox3">Просроченные</label><br>
        <input id="checkbox4" name="status[]" value="3" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(3,app('request')->input('status'))) checked @endif @endif}}><label for="checkbox4">Принятые</label><br>
        <hr>
    </div>
    <div class="medium-12">
        <label>Id аукциона:
            <input type="text" name="id" placeholder="Id аукциона" value="{{ app('request')->input('id') == "" ? "" : app('request')->input('id')}}">
        </label>
        <hr>
    </div>
    {{--<div class="medium-12">--}}
        {{--<label>Город:--}}
            {{--<input type="text" name="city" placeholder="Город доставки" value="{{ app('request')->input('city') == "" ? "" : app('request')->input('city')}}">--}}
        {{--</label>--}}
        {{--<hr>--}}
    {{--</div>--}}
    <div class="medium-12">
        <button type="submit" class="button common-button" style="width: 100%;">Фильтровать</button>
    </div>
</form>