<div class="medium-2 columns">
    <ul class="left-menu menu vertical">
        @if(Auth::Check())
            <li>
                <a href="{{ action('ProfileController@index') }}" class="left-menu-active">Профиль</a>
            </li>
            <hr>
            <li>
                <a href="{{ action('CategoriesController@index') }}" class="left-menu-active">Правка категорий</a>
            </li>
            <li>
                <a href="{{ action('AuctionController@orderlist') }}" class="left-menu-active">Заказы</a>
            </li>
            <li>
                <a href="{{ action('AuctionController@myauctions') }}" class="left-menu-active">Аукционы</a>
            </li>
        @else
            <li>
                <a href="/profile">Вход в личный кабинет</a>
            </li>
            <hr>
            <li class="{{ Request::path() == 'login' ? 'active' : '' }}">
                <a href="/login">Вход</a>
            </li>
            <li class="{{ Request::path() == 'register' ? 'active' : '' }}">
                <a href="/register">Регистрация</a>
            </li>
            <li class="{{ Request::path() == 'password/reset' ? 'active' : '' }}">
                <a href="/password/reset">Восстановление пароля</a>
            </li>
            <li class="{{ Request::path() == 'cart' ? 'active' : '' }}">
                <a href="/cart">Моя корзина</a>
            </li>
        @endif
    </ul>
</div>