<div class="row">
    <div class="medium-12 columns">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
                <li>
                    <a href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
                </li>
                @include('partials.bcpart', ['category' => $category])
                <li>
                    <span>{{ $category->name }}</span>
                </li>
            </ul>
        </nav>
    </div>
</div>