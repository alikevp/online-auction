<div class="large-10 medium-12 small-12 columns right-block">
    <h1>Мои данные</h1>
    @if (Session::has('msg'))
    <div class="callout {{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" data-closable="slide-out-right">
    <p>
        {{ Session::get("msg") }}
    </p>
    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
{{--@foreach($messages as $message)--}}
{{--<div class="callout callout-custom" data-closable="slide-out-right">--}}
    {{--<p>--}}
        {{--{!! \Session::get('success') !!}--}}
        {{--</p>--}}
    {{--<button class="close-button" aria-label="Dismiss alert" type="button" data-close>--}}
        {{--<span aria-hidden="true">&times;</span>--}}
        {{--</button>--}}
    {{--</div>--}}
{{--@endforeach--}}
<div class="large-6 medium-12 small-12 columns dashed-divs">
    <div class="medium-12 columns dashed-block">
        <h2>Персональные данные</h2>
        <table class="unstriped personal-data-table" id="user-info">
            <tr>
                <td><span>ФИО:</span></td>
                <td><span>{{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->patronymic }}</span></td>
            </tr>
            <tr>
                <td><span>E-mail: </span></td>
                <td>
                    <span>{{ Auth::user()->email }}</span>
                    @if(!Auth::user()->verified)
                    <div class="arrow-text float-right">
                        <a href="#" v-on:click="sendMail({{ Auth::user()->id }})">
                            Подтвердить
                        </a>
                    </div>
                    @endif
                </td>
            </tr>
            <tr>
                <td><span>Дата рождения:</span></td>
                <td><span>{{ Auth::user()->date_of_birth }}</span></td>
            </tr>                            <tr>
                <td><span>Пол:</span></td>
                <td>
                                    <span>
                                        @if(Auth::user()->sex == "m")
                                            Мужской
                                        @elseif(Auth::user()->sex == "f")
                                            Женский
                                        @else
                                            Не указан
                                        @endif
                                    </span>
                </td>
            </tr>
            <tr>
                <td><span>ИНН:</span></td>
                <td><span>{{ Auth::user()->inn }}</span></td>
            </tr>
        </table>
        <script>
            new Vue({
                el: '#user-info',

                methods: {
                    sendMail: function (id) {
                        console.log(id);
                        this.$http.post('api/verify', {
                            user: id
                        }).then((response) => {
                            console.log(response);
                        });
                    }
                }
            });
        </script>
        <a class="flash-link" data-open="personal-data-edit">Изменить персональные данные</a>
        <div class="reveal custom-medium-reveal" id="personal-data-edit" data-reveal>
            <form action="/personal_data_edit" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">ПЕРСОНАЛЬНЫЕ ДАННЫЕ</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="medium-6 columns modal-left">
                        <label>Имя <span class="red-star">*</span>
                            <input type="text" placeholder="Ваше имя" name="name" value="{{ Auth::user()->name }}" required>
                        </label>
                        <label>Фамилия <span class="red-star">*</span>
                            <input type="text" placeholder="Ваша фамилия" name="surname" value="{{ trim(Auth::user()->surname) }}" required>
                        </label>
                        <label>Отчество <span class="red-star">*</span>
                            <input type="text" placeholder="Ваше отчество" name="patronymic" value="{{ Auth::user()->patronymic }}" required>
                        </label>
                        <div class="medium-12 columns sex float-left">
                            <div class="medium-12 columns sex-columns-label">
                                            <span>
                                               Пол<span class="red-star">*</span>
                                            </span>
                            </div>
                            <div class="medium-6 columns">
                                <label><input type="radio" value="m" name="toggle" {{ Auth::user()->sex == "m" ? "checked" : ""}}><span>Мужской</span></label>
                            </div>
                            <div class="medium-6 columns">
                                <label><input type="radio" value="f" name="toggle" {{ Auth::user()->sex == "f" ? "checked" : ""}}><span>Женский</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="medium-6 columns modal-right">
                                          <span class="gifts-info">
                                              Чтобы получать подарки и сюрпризы, пожалуйста, укажите свои данные. <br><br>
                                          </span>
                        <div class="medium-12 columns">
                            <label>Дата рождения<br>
                                <div class="row collapse date" id="dpMonths" data-date-format="MM-dd-yyyy" data-start-view="year" data-min-view="month">
                                    <div class="medium-1 columns">
                                        <span class="prefix"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <div class="medium-11 columns">
                                        <input size="16" type="text" id="checkdata" name="dob" value="{{ Auth::user()->date_of_birth != NULL ? Auth::user()->date_of_birth : "" }}">
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>ИНН<span class="red-star">*</span>
                                <input type="text" id="checkinn" placeholder="Ваш ИНН" name="inn" maxlength="12" value="{{ trim(Auth::user()->inn) }}"  required>
                            </label>

                        </div>
                    </div>
                    <br>
                </div>
                <div class="medium-12 columns modal-footer">
                    <div class="medium-12 columns">
                        <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                        <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                        <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="medium-12 columns dashed-block">
        <h2>Адреса доставки</h2>
        @foreach($addresses as $key => $address)
        <span class="info">
            {{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}
        </span>
        <a class="flash-link" data-open="address-update{{ $key }}">Изменить адрес</a>
        <a class="flash-link" data-open="address-delete{{ $key }}" style="margin-left: 30px">Удалить адрес</a>
        <hr>
        <div class="reveal custom-medium-reveal" id="address-update{{ $key }}" data-reveal>
            <form action="/address_update/{{ $address->id }}" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">ОБНОВИТЬ АДРЕС</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="medium-12 columns">
                        <label>Страна<span class="red-star">*</span>
                            <input type="text" placeholder="Ваша страна" name="country" value="{{ $address->country }}" required>
                        </label>
                        <label>Населенный пункт/Город<span class="red-star">*</span>
                            <input id="dadata_city" type="text" placeholder="Ваш город" name="city" value="{{ $address->city }}" required>
                        </label>
                        <label>Улица<span class="red-star">*</span>
                            <input id="dadata_street" type="text" placeholder="Ваша улица" name="street" value="{{ $address->street }}" required>
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Дом<span class="red-star">*</span>
                            <input id="dadata_house" type="text" placeholder="Номер вашего дома" name="house" value="{{ $address->house }}" required>
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Квартира
                            <input id="dadata_flat" type="text" placeholder="Номер вашей квартиры" name="apartment" pattern="^[A-Za-zА-Яа-яЁё/\s-]+$" value="{{ $address->apartment }}" >
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Почтовый индекс<span class="red-star">*</span>
                            <input id="dadata_postal_code" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="post_index" value="{{ $address->post_index }}" required>
                        </label>
                    </div>
                    <div class="medium-12 columns modal-footer">
                        <div class="medium-12 columns">
                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="reveal custom-medium-reveal" id="address-delete{{ $key }}" data-reveal>
            <form action="/address_delete/{{ $address->id }}" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">УДАЛИТЬ АДРЕС</h1>
                    <div class="medium-12 columns">
                        <span class="info">
                            {{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}
                        </span>
                    </div>
                    <hr>
                    <div class="medium-12 columns">
                        <span>
                            Вы уверены, что хотите удалить этот адрес?
                        </span>
                    </div>
                    <div class="medium-12 columns modal-footer">
                        <div class="medium-12 columns">
                            <button class="common-button button-accept submit-button" type="submit">Удалить</button>
                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <br>
        @endforeach
        <a class="address-add" data-open="address-edit">ДОБАВИТЬ АДРЕС</a>
        <div class="reveal custom-medium-reveal" id="address-edit" data-reveal>
            <form action="/address_edit" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">АДРЕС</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="medium-12 columns">
                        <label>Страна<span class="red-star">*</span>
                            <input type="text" placeholder="Ваша страна" name="country" required>
                        </label>
                        <label>Населенный пункт/Город<span class="red-star">*</span>
                            <input id="dadata_city" type="text" placeholder="Ваш город" name="city" required>
                        </label>
                        <label>Улица<span class="red-star">*</span>
                            <input id="dadata_street" type="text" placeholder="Ваша улица" name="street" required>
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Дом<span class="red-star">*</span>
                            <input id="dadata_house" type="text" placeholder="Номер вашего дома" name="house" required>
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Квартира<span class="red-star"></span>
                            <input id="dadata_flat" type="text" pattern="^[A-Za-zА-Яа-яЁё/\s-]+$" placeholder="Номер вашей квартиры" name="apartment" >
                        </label>
                    </div>
                    <div class="medium-4 columns">
                        <label>Почтовый индекс<span class="red-star">*</span>
                            <input id="dadata_postal_code" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="postcode" required>
                        </label>
                    </div>
                    <div class="medium-12 columns modal-footer">
                        <div class="medium-12 columns">
                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="large-6 medium-12 small-12 columns dashed-divs">
    <div class="medium-12 columns dashed-block">
        <h2>Номер мобильного телефона</h2>
        <span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
        <span class="number">
                            {{ Auth::user()->telefone }}
                        </span>
        <a class="flash-link" data-open="telefone-edit">Изменить номер телефона</a>
        <div class="reveal custom-medium-reveal" id="telefone-edit" data-reveal>
            <form action="/telefone_edit" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">МОБИЛЬНЫЙ ТЕЛЕФОН</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
                        <input type="text" id="checktel" placeholder="Ваш телефон" name="telefone" value="{{ Auth::user()->telefone }}" required>
                    </label>
                    <div class="medium-12 columns modal-footer">
                        <div class="medium-12 columns">
                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="medium-12 columns dashed-block">
        <h2>Адрес электронной почты</h2>
        <span class="e-mail">{{ Auth::user()->email }}</span>
    </div>
    <div class="medium-12 columns dashed-block">
        <h2>Пароль</h2>

        <span class="info">Здесь вы можете изменить свой пароль для входа в личный кабинет</span>
        <a class="flash-link" data-open="password-edit" href="#">Смена пароля</a>
        <div class="reveal custom-medium-reveal" id="password-edit" data-reveal>
            <form id="form-change-password" role="form" method="POST" action="password_edit" data-abide novalidate data-live-validate="true">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">Пароль</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label for="current-password">Текущий пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Пароль" required>
                    </div>
                    <label for="password">Новый пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" required>
                    </div>
                    <label for="password_confirmation">Повторите новый пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Новый пароль еще раз" required>
                    </div>
                </div>
                <div class="medium-12 columns modal-footer">
                    <div class="medium-12 columns">
                        <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                        <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                        <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>