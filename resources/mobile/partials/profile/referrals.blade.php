@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-referrals-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Мои бонусы
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            @include('partials.profile_sidebar')

            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Мои приглашения</h1>
                <div class="medium-12 columns orders-wraper" id="items-table-block">
                    <div class="row cur_balance">
                        Ссылка для приглашения: <br><span id="guest-referral">{{ route('referral', Auth::user()->id) }}</span>
                        <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="1" title="Скопировать в буфер обмена">
                            <a class="clipboard-btn-referral" data-clipboard-action="copy" data-clipboard-target="#guest-referral"><i class="fa fa-copy"></i></a>
                        </span>
                        <p class="description">Чтобы получать бонусы, сообщите эту ссылку своим занакомым для их регистрации на сайте. <a class="flash-link" href="">Подробнее</a></p>
                    </div>
                </div>
                <h3 class="order-title">История приглашений:</h3>
                <div class="medium-12 columns orders-wraper" id="items-table-block">
                    <div class="row">
                        @if(count($referrals)>0)
                            <table class="unstriped personal-data-table" id="user-info">
                                <thead>
                                <td>Дата регистрации</td>
                                <td>ФИО</td>
                                <td>Первая покупка</td>
                                <td>Списания</td>
                                <td>ИТОГ</td>
                                </thead>
                                @foreach($referrals as $item)
                                    <tr>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->surname }} {{ $item->name }} {{ $item->patronymic }}</td>
                                        <td>
                                            @if($item->bought == true)
                                                Состоялась
                                            @else
                                                Нет
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <div class="row cur_balance">Вы еще не пригласили ни одного друга.</span></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/plugins/clipboard/clipboard.min.js')}}"></script>
    <script>
        var clipboard = new Clipboard('.clipboard-btn-referral');
        clipboard.on('success', function(e) {
            alert('Ссылка успешно скопированна в буфер обмена');
        });
    </script>
@endsection
