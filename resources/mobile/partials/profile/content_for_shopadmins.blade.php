<div class="large-10 medium-12 small-12 columns right-block">
    <h1>Данные администратора магазина</h1>
    @if (Session::has('msg'))
    <div class="callout {{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" data-closable="slide-out-right">
    <p>
        {{ Session::get("msg") }}
    </p>
    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
{{--@foreach($messages as $message)--}}
{{--<div class="callout callout-custom" data-closable="slide-out-right">--}}
    {{--<p>--}}
        {{--{!! \Session::get('success') !!}--}}
        {{--</p>--}}
    {{--<button class="close-button" aria-label="Dismiss alert" type="button" data-close>--}}
        {{--<span aria-hidden="true">&times;</span>--}}
        {{--</button>--}}
    {{--</div>--}}
{{--@endforeach--}}
<div class="large-6 medium-12 small-12 columns dashed-divs">
    <div class="small-12 columns dashed-block">
        <h2>Персональные данные</h2>
        <table class="unstriped personal-data-table" id="user-info">
            <tr>
                <td><span>ФИО:</span></td>
                <td><span>{{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->patronymic }}</span></td>
            </tr>
            <tr>
                <td><span>E-mail: </span></td>
                <td>
                    <span>{{ Auth::user()->email }}</span>
                    @if(!Auth::user()->verified)
                    <div class="arrow-text float-right">
                        <a href="#" v-on:click="sendMail({{ Auth::user()->id }})">
                            Подтвердить
                        </a>
                    </div>
                    @endif
                </td>
            </tr>
            <tr>
                <td><span>Дата рождения:</span></td>
                <td><span>{{ Auth::user()->date_of_birth }}</span></td>
            </tr>                            <tr>
                <td><span>Пол:</span></td>
                <td>
                                    <span>
                                        @if(Auth::user()->sex == "m")
                                            Мужской
                                        @elseif(Auth::user()->sex == "f")
                                            Женский
                                        @else
                                            Не указан
                                        @endif
                                    </span>
                </td>
            </tr>
        </table>
        <script>
            new Vue({
                el: '#user-info',

                methods: {
                    sendMail: function (id) {
                        console.log(id);
                        this.$http.post('api/verify', {
                            user: id
                        }).then((response) => {
                            console.log(response);
                        });
                    }
                }
            });
        </script>
        <a class="flash-link" data-open="personal-data-edit">Изменить персональные данные</a>
        <div class="reveal custom-medium-reveal" id="personal-data-edit" data-reveal>
            <form action="/personal_data_edit" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">ПЕРСОНАЛЬНЫЕ ДАННЫЕ</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="medium-12 columns modal-left">
                        <label>Имя <span class="red-star">*</span>
                            <input type="text" placeholder="Ваше имя" name="name" value="{{ Auth::user()->name }}" required>
                        </label>
                        <label>Фамилия <span class="red-star">*</span>
                            <input type="text" placeholder="Ваша фамилия" name="surname" value="{{ trim(Auth::user()->surname) }}" required>
                        </label>
                        <label>Отчество <span class="red-star">*</span>
                            <input type="text" placeholder="Ваше отчество" name="patronymic" value="{{ Auth::user()->patronymic }}" required>
                        </label>
                    </div>
                </div>
                <div class="medium-12 columns modal-footer">
                    <div class="medium-12 columns">
                        <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                        <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                        <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="large-6 medium-12 small-12 columns dashed-divs">
    <div class="small-12 columns dashed-block">
        <h2>Номер мобильного телефона</h2>
        <span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
        <span class="number">
                            {{ Auth::user()->telefone }}
                        </span>
        <a class="flash-link" data-open="telefone-edit">Изменить номер телефона</a>
        <div class="reveal custom-medium-reveal" id="telefone-edit" data-reveal>
            <form action="/telefone_edit" method="post" data-abide novalidate data-live-validate="true">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">МОБИЛЬНЫЙ ТЕЛЕФОН</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
                        <input type="text" id="checktel" placeholder="Ваш телефон" name="telefone" value="{{ Auth::user()->telefone }}" required>
                    </label>
                    <div class="medium-12 columns modal-footer">
                        <div class="medium-12 columns">
                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="small-12 columns dashed-block">
        <h2>Адрес электронной почты</h2>
        <span class="e-mail">{{ Auth::user()->email }}</span>
    </div>
    <div class="small-12 columns dashed-block">
        <h2>Пароль</h2>
        <span class="info">Здесь вы можете изменить свой пароль для входа в личный кабинет</span>
        <a class="flash-link" data-open="password-edit" href="#">Смена пароля</a>
        <div class="reveal custom-medium-reveal" id="password-edit" data-reveal>
            <form id="form-change-password" role="form" method="POST" action="password_edit" data-abide novalidate data-live-validate="true">
                <div class="medium-12 columns">
                    <h1 class="modal-h-3">Пароль</h1>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label for="current-password">Текущий пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Пароль" required>
                    </div>
                    <label for="password">Новый пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" required>
                    </div>
                    <label for="password_confirmation">Повторите новый пароль <span class="red-star">*</span></label>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Новый пароль еще раз" required>
                    </div>
                </div>
                <div class="medium-12 columns modal-footer">
                    <div class="medium-12 columns">
                        <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                        <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                        <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>