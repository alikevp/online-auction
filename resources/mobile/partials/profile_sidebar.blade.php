<div class="title-bar" data-responsive-toggle="left-block" data-hide-for="large" style="background: #8bc63e; margin-bottom: 10px;">
    <button class="menu-icon" type="button" data-toggle="left-block"></button>
    <div class="title-bar-title">Меню</div>
</div>
<div class="large-2 medium-12 small-12 columns" id="left-block">
    <ul class="left-menu menu vertical">
        <li>
            <h5>Навигация</h5>
            <hr>
        </li>
        @if(Auth::check())
            <?php $user = Auth::user(); ?>
            <li><a class="{{ Request::path() == 'profile' ? 'left-menu-active' : '' }}" href="/profile"><i class="fa fa-user" aria-hidden="true"></i>Профиль</a></li>
                <li><a class="{{ Request::path() == 'mail_subscription' ? 'left-menu-active' : '' }}" href="/mail_subscription"><i class="fa fa-envelope-o" aria-hidden="true"></i>Оповещения</a></li>
            @if($user->type != "shop_admin")
                <li><a class="{{ Request::path() == 'my-orders' ? 'left-menu-active' : '' }}" href="/my-orders"><i class="fa fa-bars" aria-hidden="true"></i>Мои аукционы</a></li>
                <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Моя корзина</a></li>
                <li><a href="{{ url('referrals') }}" class="{{ Request::path() == 'referrals' ? 'left-menu-active' : '' }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Мои приглашения</a></li>
                <li><a href="{{ url('profile/my_bonus') }}" class="{{ Request::path() == 'my_bonus' ? 'left-menu-active' : '' }}"><i class="fa fa-money" aria-hidden="true"> </i> Бонусы</a></li>
            @endif
            @if( $user->type == 'shop_admin')
                <li>
                    <br>
                    <h6>Магазин</h6>
                    <hr>
                </li>
                <li><a class="{{ Request::path() == 'auction/orderlist' ? 'left-menu-active' : '' }}" href="/auction/orderlist"><i class="fa fa-bars" aria-hidden="true"></i>Доступные заказы
                        @include('partials.orderCounter')
                    </a></li>
                <li><a class="{{ Request::path() == 'my-auctions' ? 'left-menu-active' : '' }}" href="/my-auctions"><i class="fa fa-gavel" aria-hidden="true"></i>Мои аукционы</a></li>
                <li><a class="{{ Request::path() == 'auction/accepted' ? 'left-menu-active' : '' }}" href="/auction/accepted"><i class="fa fa-check" aria-hidden="true"></i>Выигранные аукционы</a></li>
            @endif
            <hr>
            <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Выход</a></li>
            @if(Request::path() == 'auction/orderlist')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                        <a class="flash-link" href="orderlist">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.orderlistFilter')
                <br>
            @endif

            @if(Request::path() == 'my-auctions')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('pages')))
                        <a class="flash-link" href="my-auctions">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.myauctionsFilter')
                <br>
            @endif

            @if(Request::path() == 'my-orders')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                        <a class="flash-link" href="my-orders">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.myOrdersFilter')
                <br>
            @endif
        @else
            <li><a class="{{ Request::path() == 'login' ? 'left-menu-active' : '' }}" href="/login"><i class="fa fa-user" aria-hidden="true"></i> Вход кабинет</a></li>
            <li><a class="{{ Request::path() == 'register' ? 'left-menu-active' : '' }}" href="/register"><i class="fa fa-user-plus" aria-hidden="true"></i> Регистрация</a></li>
            <li><a class="{{ Request::path() == 'password/reset' ? 'left-menu-active' : '' }}" href="/password/reset"><i class="fa fa-refresh" aria-hidden="true"></i> Восстановить пароль</a></li>
            <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Моя корзина</a></li>
        @endif
    </ul>
</div>