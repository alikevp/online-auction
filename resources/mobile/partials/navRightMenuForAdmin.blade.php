<div class="medium-6 small-6 header-menu-left columns">
    <a href="/my-auctions"><i class="fa header-menu-icon fa-gavel" aria-hidden="true"></i> Аукционы<span
                class="deferred-items-count"></span>
    </a><br>
    <a href="/auction/orderlist"><i class="fa header-menu-icon fa-bars" aria-hidden="true"></i> Все заказы</a>
</div>
<div class="medium-6 small-6 header-menu-right columns">
    <a href="/logout"><i class="fa header-menu-icon fa-sign-out"
                         aria-hidden="true"></i> Выход
    </a>
    <ul class="dropdown menu" data-dropdown-menu role="menubar">
        <li role="menuitem" class="is-dropdown-submenu-parent opens-right" aria-haspopup="true" aria-label="Профиль" data-is-click="false">
            <a href="#"><i class="fa header-menu-icon fa-user-o" aria-hidden="true"></i> Профиль</a>
            <ul class="menu submenu is-dropdown-submenu first-sub vertical" data-submenu="" role="menu">
                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a class="dropdown-item" href="/profile">Профиль</a></li>
                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a class="dropdown-item" href="/auction/orderlist">Доступные заказы</a></li>
                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a class="dropdown-item" href="/my-auctions">Мои аукционы</a></li>
                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a class="dropdown-item" href="/auction/accepted">Принятые аукционы</a></li>
                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a class="dropdown-item" href="/logout">Выход</a></li>
            </ul>
        </li>
    </ul>
    <br>
</div>