@foreach($categories as $node)
    @if($node->isRoot())
        <li role="menuitem" class="is-dropdown-submenu-parent opens-right" aria-haspopup="true"
            aria-label="{{ $node->name }}" data-is-click="false">
            <a class="internal_links" href="{{ url('category', ['id' =>$node->id])}}">{{ $node->name }}</a>
            @if(count($node->children))
                <ul class="menu submenu is-dropdown-submenu first-sub vertical" data-submenu="" role="menu">
                    @include('partials.category', ['categories' => $node->children])
                </ul>
            @endif
        </li>
    @else
        <li>
            <a class="internal_links" href="{{ url('category', ['id' =>$node->id])}}">{{ $node->name }}</a>
        </li>
    @endif
@endforeach