<form action="">
    <div class="medium-12">
        <label>Город:
            <input type="text" name="city" placeholder="Город доставки" value="{{ app('request')->input('city') == "" ? "" : app('request')->input('city')}}">
        </label>
        <hr>
    </div>
    <div class="medium-12">
        <label>Id аукциона:
            <input type="text" name="id" placeholder="Id аукциона" value="{{ app('request')->input('id') == "" ? "" : app('request')->input('id')}}">
        </label>
        <hr>
    </div>
    {{--<div class="medium-12">--}}
        {{--<ul class="accordion" data-accordion data-allow-all-closed="true">--}}
            {{--<li class="accordion-item" data-accordion-item>--}}
            {{--<a class="accordion-title">Категории, на которые Вы подписаны:</a>--}}
            {{--<div class="accordion-content" data-tab-content>--}}
            {{--@php($i = 5)--}}
            {{--@php($j = 0)--}}
            {{--@foreach($subscription_list as $subscription_item)--}}
                {{--<input id="checkbox{{ $i }}" name="categories[]" value="{{ $subscription_item->id }}" type="checkbox" {{ !app('request')->input() || (count(app('request')->input()) == 1 && app('request')->input('page')) || (isset(app('request')->input('categories')[$j]) && app('request')->input('categories')[$j] == $subscription_item->id)  ? "checked" : "" }} ><label for="checkbox{{ $i }}">{{ $subscription_item->name }}</label>--}}
                {{--<br>--}}
                {{--@php($i++)--}}
                {{--@if(isset(app('request')->input('categories')[$j]) && app('request')->input('categories')[$j] == $subscription_item->id)--}}
                    {{--@php($j++)--}}
                {{--@endif--}}
            {{--@endforeach--}}
            {{--</div>--}}
            {{--</li>--}}
        {{--</ul>--}}
        {{--<hr>--}}
    {{--</div>--}}
    <div class="medium-12">
        <button type="submit" class="button common-button" style="width: 100%;">Фильтровать</button>
    </div>
</form>