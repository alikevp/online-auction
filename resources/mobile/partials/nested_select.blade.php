@foreach($categories_tree as $node)
    @if($node->isRoot())
        <option value="{{ $node->id }}" disabled="disabled" style="color:#001dff;"><span>+</span>{{ $node->name }}</option>
        @include('partials.nested_select', ['categories_tree' => $node->children])
    @elseif($node->isLeaf())
        <option value="{{ $node->id }}" @if(count($subscription)>0) @if(in_array($node->id, $subscription)) selected @endif @endif>{{ $node->name }}</option>
    @elseif ($node->isChild())
        <option value="{{ $node->id }}" disabled="disabled"  style="color:#ff0050;"><span>++</span>{{ $node->name }}</option>
        @include('partials.nested_select', ['categories_tree' => $node->children])
    @endif
@endforeach
