@extends('layouts.master')

@section('style')
    <link href="{{asset('owlcarousel')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Корзина
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            <div class="medium-12 columns" id="buy-steps-block">
                <div class="medium-12">
                    <h3>МОЯ КОРЗИНА</h3>
                </div>
                <div class="medium-10 medium-offset-1 columns" id="buy-steps">
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div buy-step-div-active">
                                <i class="buy-step-icon fi-shopping-bag size-36"></i>
                            </div>
                            <span class="step-name step-name-active">Корзина</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-3 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="buy-step-icon fi-shopping-bag size-36"></i>
                            </div>
                            <span>Вход/Регистрация</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="buy-step-icon fi-shopping-bag size-36"></i>
                            </div>
                            <span>Доставка</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="buy-step-icon fi-shopping-bag size-36"></i>
                            </div>
                            <span>Оплата</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="medium-12 columns" id="items-table-block">
                <table id="items-table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Описание</th>
                        <th>Цена</th>
                        <th>Промокод</th>
                        <th>Сумма</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>
                            1
                        </th>
                        <td>
                            <div class="medium-3 columns">
                                <img class="item-image" src="http://placehold.it/65x110" alt="item image">
                            </div>
                            <div class="medium-9 columns">
                                <span class="item-name">Купальник, RUXARA</span><br>
                                <span class="item-article">Артикул: 3483029</span><br>
                                <span class="item-color">Цвет: Розовый</span><br>
                                <span class="item-size">Размер: 50</span><br>
                                <span class="item-amount">
                                <table class="item-amount-table">
                                    <tr>
                                        <td>Кол-во:</td>
                                        <td><input type="number" class="item-amount-input" value="1"></td>
                                    </tr>
                                </table>
                            </span>
                            </div>
                        </td>
                        <td>1530</td>
                        <td>---</td>
                        <td>1530</td>
                        <td>
                            <a href="#" class="button tiny item-delete-button">X</a><br>
                            <a href="#" class="item-change-button">Изменить размер</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="medium-12 columns" id="order-block">
                <div class="medium-5 columns">
                    <form>
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="row collapse">
                                    <div class="small-6 columns">
                                        <input type="text" id="coupon-input">
                                    </div>
                                    <div class="small-6 columns">
                                        <a href="#" class="button postfix" id="coupon-button">
                                            ПОЛУЧИТЬ СКИДКУ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="medium-7 columns" id="price-block">
                    <span id="order-price">Сумма заказа: 1530</span><br>
                    <span id="total-price">ИТОГО: 1530</span>
                </div>
            </div>
            <div class="medium-12 columns" id="order-control-buttons-block">
                <a href="#" class="button order-control-button">ВОЙТИ И ЗАКАЗАТЬ</a>
                <a href="#" class="button order-control-button">ЗАРЕГИСТРИРОВАТЬСЯ И ЗАКАЗАТЬ</a>
            </div>
        </div>
    </div>
@endsection
