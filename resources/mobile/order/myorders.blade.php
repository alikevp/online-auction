@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-auctions-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    
@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
@endsection
@section('title')
    Мои аукционы
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (count($orders)>0)
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12">
                        <h3>Мои аукционы</h3>
                    </div>
                </div>
                <div class="medium-12 columns">
                    @if (session('message') !== null)
                        <div class="medium-12 columns">
                            <div class="success callout" data-closable="slide-out-right">
                                <h5>Успешно.</h5>
                                <p>Предложение успешно сделано.</p>
                                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif


                    <div class="medium-12 columns orders-wraper" id="items-table-block">
                        <div class="row">
                            @foreach ($orders as $order)
                                <div class="medium-12 columns order-block">
                                    <div class="row">
                                        <div class="large-2 medium-12 small-12 columns order-column order-logo">

                                            <img class="item-image" src="{{ url(array_first($order->product->files)) }}" alt="item image">
                                        </div>
                                        <div class="large-7 medium-12 small-12 columns order-column order-details">
                                            <div class="order-title">
                                                <span>{{ $order->product->name }}</span>
                                            </div>
                                            <div class="order-description">

                                                <div>№ заказа: <span>{{ $order->id }}</span></div>
                                                <div>Производитель: <span>{{ $order->product->producer }}</span></div>
                                                <div>Количество: <span>{{ $order->quantity }}</span></div>
                                                <div>Активен до: <span>{{ $order->active_till }}</span></div>
                                                @if(null !== $order->delivery)
                                                    @foreach($order->delivery->types as $type)
                                                        @if($type->slug == 'delivery')
                                                            <div>Доставка: <span><a class="flash-link" data-open="delyveryModal{{ $order->delivery->id }}">Показать</a></span></div>
                                                            <div class="reveal" id="delyveryModal{{ $order->delivery->id }}" data-reveal>
                                                                <div class="medium-12 columns">
                                                                    <h3 class="modal-h-3">Доставка</h3>
                                                                    <span class="item-article">Город: {{ $order->delivery->city }}</span>
                                                                    <br>
                                                                    <span class="item-article">Улица: {{ $order->delivery->street }}</span>
                                                                    <br>
                                                                    <span class="item-article">Дом: {{ $order->delivery->house }}</span>
                                                                    <br>
                                                                    <span class="item-article">Квартира: {{ $order->delivery->apartment }}</span>
                                                                    <br>
                                                                    <div>
                                                                        <button class="close-button" data-close
                                                                                aria-label="Close modal"
                                                                                type="button">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif($type->slug == 'pickup')
                                                            <div>Самовывоз: <span>{{ $order->delivery->city }}</span></div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div>Доставка: <span>Не указана</span></div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="large-3 medium-12 small-12 columns order-column order-actions">
                                            <div class="actions-offers">
                                                Предложений: <span>{{ count($order->order_offer_groups) }}</span>
                                            </div>
                                            <div class="actions-best-price">
                                                @if(count($order->order_offer_groups)>0)
                                                    @if(count($order->order_offer_groups)>1)
                                                        Лучшая цена: <span>{{ $order->order_offer_groups->min('price') }}</span>
                                                    @else
                                                        Лучшая цена: <span>{{ $order->order_offer_groups->first()->price }}</span>
                                                    @endif
                                                @else
                                                @endif

                                            </div>
                                            <div class="actions-offers hide-for-medium-only hide-for-small-only">
                                                @if(($order->status == 1))
                                                    <div class="acepted acepted-active">Активен</div>
                                                @elseif(($order->status == 0))
                                                    <div class="acepted acepted-cancel">Отменён</div>
                                                @elseif(($order->status == 2))
                                                    <div class="acepted acepted-end">Истёк</div>
                                                @elseif(($order->status == 3))
                                                    <div class="acepted acepted-acepted">Принят</div>
                                                @endif
                                            </div>
                                            <div class="actions-buttons">
                                                <a href="/orderdetails?order={{ $order->id }}"
                                                   class="button common-button">Подробности</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="reveal custom-medium-reveal" id="edit-bet" data-reveal>
                            <div id="modal-content-block"></div>
                        </div>
                    </div>
                </div>
            @else
                <div class="medium-12 columns">
                    <span>Аукционов нет.</span>
                </div>
            @endif
            <hr>
            {{ $orders->links('vendor.pagination.foundation') }}
        </div>
    </div>
@endsection
