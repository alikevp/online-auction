@extends('layouts.master')

@section('style')
    <link href="{{asset('css/basket.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-cart-page.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/order.confirm.js') }}"></script>
@endsection
@section('title')
    Корзина
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (isset($products))
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12">
                        <h3>ПОДТВЕРЖДЕНИЕ ЗАКАЗА</h3>
                    </div>
                    <div class="medium-9 medium-offset-2 columns" id="buy-steps">
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Корзина</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Доставка</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-3 columns">
                            <a href="#">
                                <div class="buy-step-div buy-step-div-active">
                                    <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span class="step-name step-name-active">Подтверждение</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Завершение</span>
                            </a>
                        </div>
                    </div>
                </div>
                <form method="post" action="/order/fast_finish">
                    <div class="medium-12 columns" id="items-table-block">
                        <div class="medium-12 columns" id="total-block">
                            @if(array_has(session('delivery_type'), 'pickup'))
                                <p class="info">Самовывоз по адресу: <span
                                            class="address">{{ $delivery_data->city }}</span></p>
                            @endif
                            @if(array_has(session('delivery_type'), 'delivery'))
                                <p class="info">Доставка по адресу: <span
                                            class="address">{{ $delivery_data->full_address }}</span></p>
                            @endif
                            <p class="info">Пожалуйста, внимательно проверьте детали заказа, и нажмите "Продолжить".</p>

                            <table class="items-table unstriped">
                                <thead>
                                <tr>
                                    <th>Товар</th>
                                    <th>Цена</th>
                                    <th>Количество</th>
                                    <th>Сумма</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total_price = 0; ?>
                                <tr id="cart_prod_row_{{ $products->first()->id }}">
                                    <td>
                                        <div class="medium-3 columns">
                                            <img class="item-image" src="{{ URL::to($products->first()->files[0]) }}" alt="item image">
                                        </div>
                                        <div class="medium-9 columns">
                                            <span class="item-name">{{ $products->first()->name }}</span><br>
                                        </div>
                                    </td>
                                    <td id="product-price_{{ $products->first()->id }}">{{ $products->first()->price }}
                                        <input class="hidefield" hidden="hidden" type="hidden" name="product_price[{{ $products->first()->id }}]" value="{{ $products->first()->price }}">
                                    </td>

                                    <td id="product-quantity_{{ $products->first()->id }}">{{ $cart_array['count'][$products->first()->id] }}
                                        <input class="hidefield" hidden="hidden" type="hidden" name="product_count[{{ $products->first()->id }}]" value="{{ $cart_array['count'][$products->first()->id] }}">
                                    </td>
                                    <td id="product-amount_{{ $products->first()->id }}">{{ ($products->first()->price) * ($cart_array['count'][$products->first()->id]) }}</td>
                                </tr>
                                <?php
                                $cur_price = ($products->first()->price) * ($cart_array['count'][$products->first()->id]);
                                $total_price = $cur_price + $total_price;
                                ?>
                                @foreach ($products as $key=>$product)
                                    @if($key == 0)
                                        <?php continue; ?>
                                    @endif
                                    <tr id="cart_prod_row_{{ $product->id }}" class="hidden_row">
                                        <td>
                                            <div class="medium-3 columns">
                                                <img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image">
                                            </div>
                                            <div class="medium-9 columns">
                                                <span class="item-name">{{ $product->name }}</span><br>
                                            </div>
                                        </td>
                                        <td id="product-price_{{ $product->id }}">{{ $product->price }}
                                            <input class="hidefield" hidden="hidden" type="hidden" name="product_price[{{ $product->id }}]" value="{{ $product->price }}">
                                        </td>

                                        <td id="product-quantity_{{ $product->id }}">{{ $cart_array['count'][$product->id] }}
                                            <input class="hidefield" hidden="hidden" type="hidden" name="product_count[{{ $product->id }}]" value="{{ $cart_array['count'][$product->id] }}">
                                        </td>

                                        <td id="product-amount_{{ $product->id }}">{{ ($product->price) * ($cart_array['count'][$product->id]) }}</td>
                                    </tr>
                                    <?php
                                    $cur_price = ($product->price) * ($cart_array['count'][$product->id]);
                                    $total_price = $cur_price + $total_price;
                                    ?>
                                @endforeach
                                </tbody>
                            </table>
                                <div class="order-wraper medium-12 columns">
                                    <div id="order-block" class="row">
                                        <div class="medium-12 columns">
                                            <a class="full-table-trigger flash-link" href="#">В заказе товаров: {{ count($products) }}, на общую сумму {{ $products->sum('price') }} руб. <span> (Просмотреть все)</span></a>
                                            <div class="row"><div class="medium-6 columns order-title-big"><span>Завершение оформления заказа</span></div></div>
                                            <div class="row"><div class="medium-6 columns order-title-medium"><span>Проверьте товары, доставку и стоимость покупки</span></div></div>
                                            <div class="row">
                                                <div class="medium-6 columns" id="price-block">
                                                    <div class="row">
                                                        <div class="medium-6 columns"><span id="price-description">Стоимость товаров:</span></div>
                                                        <div class="medium-6 columns"><span id="price-value"><span id="total-price-value">{{ $total_price }} @include('partials.priceTooltip')</span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="medium-6 columns">
                                                    <input class="hidefield" hidden="hidden" type="hidden" name="total_count" value="{{ count($products) }}">
                                                    <input class="hidefield" hidden="hidden" type="hidden" name="total_price" value="{{ $products->sum('price') }}">
                                                    <input class="hidefield" hidden="hidden" type="hidden" name="total_count" value="{{ count($products) }}">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <a href="/cart" class="button common-button order-submit">НАЗАД В КОРЗИНУ</a>
                                                    <input type="submit" name="order_submit" class="button common-button order-submit" value="ПРОДОЛЖИТЬ">
                                                </div>
                                            </div>
                                            <div class="row"><div class="medium-6 columns order-agreement"><span>Оформляя заказ, вы соглашаетесь с условиями <a href="" class="flash-link">публичной оферты</a>.</span></div></div>
                                        </div>
                                    </div>
                                </div>


                        </div>
                    </div>

                </form>
            @else
                <div class="medium-12 columns">
                    <span>Заказ не сформирован. Вернитесь в корзину и сформируйте заказ.</span>
                    <a href="/cart" class="button order-control-button">НАЗАД В КОРЗИНУ</a>
                </div>
            @endif
        </div>
    </div>
@endsection
