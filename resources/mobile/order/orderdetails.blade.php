@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
@endsection
@section('title')
    Подробности заказа
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            <div class="medium-12 columns" id="buy-steps-block">
                <div class="medium-12">
                    <a class="flash-link" href="/my-orders">Назад</a>
                    <h3>Подробности заказа</h3>
                </div>
            </div>
            <div class="medium-12 columns">
                @if (session('message') !== null)
                    <div class="medium-12 columns">
                        <div class="success callout" data-closable="slide-out-right">
                            <h5>Информация об изменении заказа:</h5>
                            <p>{{ session('message') }}</p>
                            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <h3 class="order-title">{{ $order->product->name }}</h3>
                <div class="medium-6 columns shadow-block equivalent-height">
                    @if(count($offers)>0)
                        <div class="order-info-block order-best-price">Лучшая цена: <span>{{ ($offers->first()->price != null) ? $offers->first()->price : 'Предложений пока нет' }}
                                &#8381;</span></div>
                        <div class="order-info-block order-dest-price-shop">Магазин: <span><a class="flash-link" href="/shopdetails?shop={{ $offers->first()->shops->first()->id }}">{{ $offers->first()->shops->first()->name }}</a></span>
                        </div>
                    @else
                        <div class="order-info-block order-best-price">Предложений пока нет</div>
                    @endif
                    <div class="order-info-block order-status">Статус аукциона:
                        <span>
                            @if($order->status == 0)
                                Отменен
                            @elseif($order->status == 1)
                                Активен
                            @elseif($order->status == 2)
                                Истек
                            @elseif($order->status == 3)
                                Принят
                            @endif
                        </span>
                    </div>
                    <div class="order-info-block orderdetalais-actions">
                        @if($order->status == 1)
                            <a class="button common-button button-decline" href="/order_cancel?order={{ $order->id }}">Завершить</a>
                        @elseif($order->status == 2)
                            <a class="button common-button button-accept" href="/order_extend?order={{ $order->id }}">Продлить</a>
                        @endif
                    </div>
                </div>
                <div class="medium-6 columns shadow-block equivalent-height">
                    @if(null !== $order->delivery)

                        @foreach($order->delivery->types as $type)
                            @if($type->slug == 'delivery')
                                <div class="order-info-block order-city">Доставка: <span><a class="flash-link" data-open="delyveryModal{{ $order->delivery->id }}">Показать</a></span></div>
                    <div class="reveal" id="delyveryModal{{ $order->delivery->id }}" data-reveal>
                        <div class="medium-12 columns">
                            <h3 class="modal-h-3">Доставка</h3>
                            <span class="item-article">Город: {{ $order->delivery->city }}</span>
                            <br>
                            <span class="item-article">Улица: {{ $order->delivery->street }}</span>
                            <br>
                            <span class="item-article">Дом: {{ $order->delivery->house }}</span>
                            <br>
                            <span class="item-article">Квартира: {{ $order->delivery->apartment }}</span>
                            <br>
                            <div>
                                <button class="close-button" data-close
                                        aria-label="Close modal"
                                        type="button">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    @elseif($type->slug == 'pickup')
                        <div class="order-info-block order-city">Самовывоз: <span>{{ $order->delivery->city }}</span></div>
                    @endif
                    @endforeach
                    @else
                        <div>Доставка: <span>Не указана</span></div>
                    @endif
                    <div class="order-info-block order-start">Начало аукциона:
                        <span>{{ $order->created_at->formatLocalized('%d %B %Y %H:%m') }}</span></div>
                    <div class="order-info-block order-finish">До окончания осталось:
                        <span>{{ $time_last }}</span></div>
                </div>
                <h3 class="order-title">Предложения по заказу:</h3>
                <div class="medium-12 columns orders-wraper" id="items-table-block">
                    <div class="row">
                        {{--@foreach ($orders as $order)--}}
                        @if($offers->isNotEmpty())
                            @foreach ($offers as $bet)
                                <div class="medium-12 columns order-block">
                                    <div class="row">
                                        <div class="medium-2 columns order-column order-logo">
                                            {{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
                                        </div>
                                        <div class="medium-7 columns order-column order-details">
                                            <div class="order-title">

                                                <a href="/shopdetails?shop={{ $bet->shops->first()->id }}"><span>{{ $bet->shops->first()->name }}</span></a>
                                            </div>
                                            <div class="order-description">
                                                <div>Предложение выставлено: <span>{{ $bet->created_at }}</span></div>

                                                @if(count($bet->deliveries)>0)
                                                    <div>Доставка: <span>{{ count($bet->deliveries) }} вариант(а/ов). Самый быстрый - {{ $bet->deliveries->min('delivery_time')  }} дней. <a
                                                                    class="flash-link"
                                                                    data-open="offerDeliveres-1">(Подробнее)</a></span></div>
                                                    <div class="reveal custom-medium-reveal" id="offerDeliveres-1" data-reveal>
                                                        <div class="medium-12 columns">
                                                            <span>Доставки:</span>
                                                            <table class="items-table unstriped">
                                                                <tr>
                                                                    <th>Тип</th>
                                                                    <th>Срок доставки (дней)</th>
                                                                    <th>Подробности</th>
                                                                </tr>
                                                                @foreach ($bet->deliveries as $delivery)
                                                                    <tr>
                                                                        <td>{{ $delivery->type }}</td>
                                                                        <td>{{ $delivery->delivery_time }}</td>
                                                                        <td><a class="flash-link"
                                                                               data-open="offerDeliveriesDetails-{{ $delivery->id }}">(Подробности)</a>
                                                                        </td>
                                                                        <div class="reveal custom-medium-reveal"
                                                                             id="offerDeliveriesDetails-{{ $delivery->id }}" data-reveal>
                                                                            <div class="medium-12 columns">
                                                                                <span>Подробности доставки:</span>
                                                                                <div>
                                                                                    {{ $delivery->description }}
                                                                                </div>
                                                                            </div>
                                                                            <button class="close-button" data-close
                                                                                    data-open="offerDeliveres-1"
                                                                                    aria-label="Close reveal" type="button">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div>Доставки: <span>нет</span>
                                                    </div>
                                                @endif
                                                @if(count($bet->gifts)>0)
                                                    <div>Подарки: <span>{{ count($bet->gifts) }} вариант(а/ов)<a class="flash-link"
                                                                                                                 data-open="offerGifts-1">(Подробнее)</a></span>
                                                    </div>
                                                    <div class="reveal custom-medium-reveal" id="offerGifts-1" data-reveal>
                                                        <div class="medium-12 columns">
                                                            <span>Подарки:</span>
                                                            <table class="items-table unstriped">
                                                                <tr>
                                                                    <th>Тип</th>
                                                                    <th>Название</th>
                                                                    <th>Описание</th>
                                                                </tr>
                                                                @foreach ($bet->gifts as $gift)
                                                                    <tr>
                                                                        <td>{{ $gift->type }}</td>
                                                                        <td>{{ $gift->name }}</td>
                                                                        <td>{{ $gift->description }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div>Подарки: <span>нет</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="medium-3 columns order-column order-actions">
                                            <div class="order-info-block shop-rate">Рейтинг:
                                                <span>
                                                        {{ $bet->shops->first()->offerrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                                                                   aria-haspopup="true" class="has-tip top"
                                                                                                   data-disable-hover="false" tabindex="1"
                                                                                                   title="Проведено аукционов">(?)</span>
                                                        /
                                                    {{ $bet->shops->first()->dealrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                                                              aria-haspopup="true" class="has-tip top"
                                                                                              data-disable-hover="false" tabindex="1"
                                                                                              title="Успешных продаж">(?)</span>
                                                    </span>
                                            </div>
                                            <div class="order-info-block shop-comments">Отзывы: <span> {{ $bet->shops->first()->commentcount }} <a href="/shopdetails?shop={{ $bet->shops->first()->id }}#recall"
                                                                                                            class="flash-link">Читать</a> </span>
                                            </div>
                                            <div class="order-info-block shop-comments">Цена: <span>{{ $bet->price }} &#8381;</span>
                                            </div>
                                            <div class="order-info-block shop-comments">Старая цена: <span>{{ $bet->old_price }} &#8381;</span>
                                            </div>
                                            <div class="actions-buttons">

                                                @if(($order->status == 1 OR $order->status == 2) && $bet->status == 0)

                                                    <a data-open="confirm-accept-{{ $bet->id }}" class="button common-button">Принять
                                                        предложение</a>

                                                    <div class="reveal" id="confirm-accept-{{ $bet->id }}" data-reveal>
                                                        <!-- проверка на существование телефона -->
                                                        @if( empty(Auth::user()->telefone) )
                                                           <h3>Для принятия предложения Вам необходимо указать свой контактный телефон, по которому с Вами сможет связаться представитель магазина</h3>
                                                            <form action="/telefone_edit" method="post" data-abide novalidate>
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <div class="medium-12 columns">
                                                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    <label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
                                                                        <input type="text" id="checktel" placeholder="Ваш телефон" name="telefone"  required>
                                                                    </label>
                                                                    <div class="medium-12 columns modal-footer">
                                                                        <div class="medium-12 columns">
                                                                            <button class="common-button button-accept" type="submit">СОХРАНИТЬ</button>
                                                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        @else
                                                                <h2 id="modalTitle">Вы действительно хотите принять это
                                                                    предложение?</h2>
                                                                <p>После подтверждения магазин будет оповещён о том, что Вы приняли
                                                                    его
                                                                    предложение.</p>
                                                                <hr>
                                                                <a href="\offer_accept?order={{ $order->id }}&offer_confirm={{ $bet->id }}"
                                                                   class="button common-button button-accept">Согласен</a>
                                                                <a data-close="confirm-accept-1"
                                                                   class="button common-button button-decline">ОТМЕНИТЬ</a>
                                                            @endif
                                                    </div>
                                                @elseif($bet->status == 1)
                                                    <div class="acepted">Принято</div>
                                                @elseif(($order->status !== 1 OR $order->status !== 2))
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <span>Предложений от магазинов не поступало.</span>
                        @endif
                    </div>
                    <div class="reveal custom-medium-reveal" id="edit-bet" data-reveal>
                        <div id="modal-content-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection