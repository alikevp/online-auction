@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/itempage.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    {{ $shop_data->name }}
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            <?php $user = Auth::user(); ?>
            @if($user->type == 'shop_admin')
                @include('partials.profile_sidebar_for_admins')
            @else
                @include('partials.profile_sidebar')
            @endif

            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Данные магазина {{ $shop_data->name }}</h1>
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="medium-12 columns">
                            <div class="{{ $msg }} callout" data-closable="slide-out-right">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                @endforeach
                    <div class="row">
                        <div class="medium-12 columns">
                            <div class="" id="items-button-group-top-div">
                                <div class="button-group items-button-group main-tabs"><ul class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs id="deeplinked-tabs">
                                        <li class="tabs-title is-active"><a href="#desc" aria-selected="true">Общие данные</a></li>
                                        <li class="tabs-title"><a href="#adr">Адреса</a></li>
                                        <li class="tabs-title"><a href="#pay">Варианты оплаты</a></li>
                                        <li class="tabs-title"><a href="#recall">Отзывы <span class="value">{{ count($shop_data->comments) }}</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="medium-12 columns shadow-block tabs-content" data-tabs-content="deeplinked-tabs">
                        <div class="medium-12 columns item-about-div tabs-panel is-active" id="desc">
                            <table class="unstriped personal-data-table" id="user-info">
                                <tr>
                                    <td><span>Название:</span></td>
                                    <td><span>{{ $shop_data->name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Юридическое название:</span></td>
                                    <td><span>{{ $shop_data->le_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Полное юридическое название:</span></td>
                                    <td><span>{{ $shop_data->full_le_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ФИО директора:</span></td>
                                    <td><span>{{ $shop_data->director_fio }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ИНН:</span></td>
                                    <td><span>{{ $shop_data->inn }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>КПП:</span></td>
                                    <td><span>{{ $shop_data->kpp }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ОГРН:</span></td>
                                    <td><span>{{ $shop_data->ogrn }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Телефон:</span></td>
                                    <td><span>{{ $shop_data->phone }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>E-mail:</span></td>
                                    <td><span>{{ $shop_data->e_mail }}</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="medium-12 columns item-about-div tabs-panel" id="adr">
                            @if(count($addresses)>0)
                                @foreach($addresses as $address)
                                    <span class="info">

                                        </span>
                                    <table class="unstriped personal-data-table" id="user-info">
                                        <tr>
                                            <td>
                                                <span>
                                                @if($address->type == 1)
                                                        Основной
                                                    @elseif($address->type == 0)
                                                        Второстепенный
                                                    @else
                                                        Не указан
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}</td>
                                        </tr>
                                    </table>
                                    <br>
                                @endforeach
                            @else
                                <div class="medium-12 columns">
                                    <p>Адреса не указаны.</p>
                                </div>
                            @endif
                        </div>
                        <div class="medium-12 columns item-about-div tabs-panel" id="pay">
                            @if(count($payment_data)>0)
                                @foreach($payment_data as $row)
                                    <table class="unstriped personal-data-table" id="user-info">
                                        <tr>
                                            <td colspan="2">
                                                <span>
                                                    @if($row['type'] == 1)
                                                    Наличные при получении
                                                    @elseif($row['type'] == 2)
                                                        Карта при получении
                                                    @elseif($row['type'] == 3)
                                                        Карта(перевод)
                                                    @elseif($row['type'] == 4)
                                                        Банковский перевод
                                                    @elseif($row['type'] == 5)
                                                        Электронная коммерция
                                                    @else
                                                        Не указан
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        @if($row['type'] == 1)
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 2)
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 3)
                                            <tr>
                                                <td><span>Банк:</span></td>
                                                <td><span>{{ $row['name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>№ карты:</span></td>
                                                <td><span>{{ $row['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 4)
                                            <tr>
                                                <td><span>Наименование банка:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Адрес банка:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_address'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>БИК:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_bik'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Кор. счет:</span></td>
                                                <td><span>{{ $row['bank_data']['kor_account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>ИНН:</span></td>
                                                <td><span>{{ $row['bank_data']['inn'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>КПП:</span></td>
                                                <td><span>{{ $row['bank_data']['kpp'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>ОКТМО:</span></td>
                                                <td><span>{{ $row['bank_data']['oktmo'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>КБК:</span></td>
                                                <td><span>{{ $row['bank_data']['kbk'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Счет:</span></td>
                                                <td><span>{{ $row['bank_data']['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Получатель:</span></td>
                                                <td><span>{{ $row['bank_data']['recipient'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Назначение платежа:</span></td>
                                                <td><span>{{ $row['bank_data']['purpose'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 5)
                                            <tr>
                                                <td><span>Сервис:</span></td>
                                                <td><span>{{ $row['name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Счет:</span></td>
                                                <td><span>{{ $row['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @else
                                            Не указан
                                        @endif
                                    </table>
                                    <div style="clear: both"></div>
                                    <br>
                                @endforeach
                                <div style="clear: both"></div>
                            @else
                                <div class="medium-12 columns">
                                    <p>Варианты оплаты не указаны.</p>
                                </div>
                            @endif
                        </div>
                        <div class="medium-12 columns columns item-about-div tabs-panel" id="recall">
                            @if(count($shop_data->comments) <1)
                                <div class="medium-12 columns">
                                    <p>Отзывов нет.</p>
                                </div>
                            @else
                                @foreach($shop_data->comments as $comment)
                                    <div class="medium-12 columns comment">
                                        <span class="commenter-name"><p>{{ $comment->user_name }}</p></span>
                                        <span class="rating"><p class="title">Рейтинг: </p><p class="value">{{ $comment->rating }}</p></span>
                                        <span class="advantages"><p class="title">Достоинства: </p><p class="value">{{ $comment->advantages }}</p></span>
                                        <span class="disadvantages"><p class="title">Недостатки: </p><p class="value">{{ $comment->disadvantages }}</p></span>
                                        <span class="text"><p class="title">Комментарий: </p><br><p class="value">{{ $comment->text }}</p></span>
                                        <span class="date"><p>{{ $comment->created_at }}</p></span>
                                    </div>
                                @endforeach
                            @endif
                            <hr>
                            <ul class="vertical menu" data-accordion-menu>
                                <li>
                                    <a href="#" class="button write-review">НАПИСАТЬ ОТЗЫВ</a>
                                    <ul class="menu vertical nested comment-menu">
                                        <li>
                                            <div class="medium-6 columns comment-form">
                                                <form action="shop_comment?shop={{ $shop_data->id  }}" method="post" data-abide novalidate>
                                                    <div class="row">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        @if(Auth::check())
                                                            <div class="medium-12 columns">
                                                                <label>Имя
                                                                    <input type="text" value="{{ Auth::user()->name  }}" name="name" placeholder="Ваше имя" required disabled>
                                                                </label>
                                                            </div>
                                                            <div class="medium-12 columns">
                                                                <label>Email
                                                                    <input type="email" value="{{ Auth::user()->email  }} " name="email" placeholder="Ваш email" required disabled>
                                                                </label>
                                                            </div>
                                                        @else
                                                            <div class="medium-12 columns">
                                                                <label>Имя
                                                                    <input type="text" name="name" placeholder="Ваше имя" required>
                                                                </label>
                                                            </div>
                                                            <div class="medium-12 columns">
                                                                <label>Email
                                                                    <input type="email" name="email" placeholder="Ваш email" required>
                                                                </label>
                                                            </div>
                                                        @endif
                                                        <div class="medium-12 columns">
                                                            <label>Рейтинг
                                                                <select name="rating" required>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3" selected>3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </label>
                                                        </div>
                                                        <div class="medium-12 columns">
                                                            <label>Достоинства
                                                                <textarea name="advantages" placeholder="Достоинства магазина" required></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="medium-12 columns">
                                                            <label>Недостатки
                                                                <textarea name="disadvantages" placeholder="Недостатки магазина" required></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="medium-12 columns">
                                                            <label>Комментарий
                                                                <textarea name="text" placeholder="Комментарий" required></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="medium-12 columns">
                                                            <input type="hidden" name="shop_id" value="{{ $shop_data->id  }}">
                                                            <button class="button comment-form-button" type="submit">Отправить</button>
                                                        </div>
                                                        <section id="recall" data-magellan-target="recall">
                                                            <a data-open="recall-rules-modal" class="button recall-rules">Правила оформления отзывов</a>
                                                        </section>
                                                        <div class="reveal" id="recall-rules-modal" data-reveal>
                                                            <h1>Правила оформления отзывов</h1>
                                                            <p class="lead">
                                                                Подробно расскажите о своём опыте использования товара. Обратите внимание на качество, удобство модели, её соответствие заявленным характеристикам.
                                                            </p>
                                                            <p>
                                                                Мы не публикуем отзывы, которые содержат оскорбления и ненормативную лексику.
                                                            </p>

                                                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
@endsection
