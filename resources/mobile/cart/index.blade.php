@extends('layouts.master')

@section('style')
    <link href="{{asset('css/basket.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-cart-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/cart.index.js') }}"></script>
@endsection
@section('title')
    Корзина
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (isset($products))
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12 small-12 columns">
                        <h3>МОЯ КОРЗИНА</h3>
                    </div>
                    <div class="large-10 large-offset-1 medium-12 hide-for-small-only columns" id="buy-steps">
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div buy-step-div-active">
                                    <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span class="step-name step-name-active">Корзина</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Доставка</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-3 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Подтверждение</span>
                            </a>
                        </div>
                        <div class="medium-1 columns">
                            <hr class="buy-step-hr">
                        </div>
                        <div class="buy-step medium-2 columns">
                            <a href="#">
                                <div class="buy-step-div">
                                    <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                                </div>
                                <span>Завершение</span>
                            </a>
                        </div>
                    </div>
                </div>
                <form method="post" action="/order/delivery">
                    <div class="medium-12 columns" id="items-table-block">
                        <table class="items-table">
                            <thead>
                            <tr>
                                <th>Товар</th>
                                <th>Цена</th>
                                <th>Количество</th>
                                <th>Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_price = 0; ?>
                                @foreach ($products as $product)
                                    <tr id="cart_prod_row_{{ $product->id }}">
                                        <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][id]" value="{{ $product->id }}">
                                        <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][name]" value="{{ $product->name }}">
                                        <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][foto]" value="{{ URL::to($product->files[0]) }}">
                                        <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][price]" value="{{ $product->price }}">
                                        <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][category_id]" value="{{ $product->category_id }}">

                                        <td>
                                            <div class="large-3 medium-2 small-12 columns">
                                                <a href="{{ url('show', $product->id) }}"><img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image"></a>
                                            </div>
                                            <div class="large-9 medium-10 small-12 columns">
                                                <a href="{{ url('show', $product->id) }}"><span class="item-name">{{ $product->name }}</span><br></a>
                                                <a href="#" class="common-button item-delete-button" id="in_cart_del" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_del">Убрать</a>
                                            </div>
                                        </td>
                                        <td id="product-price_{{ $product->id }}">{{ $product->price }}</td>
                                        <td id="product-quantity_{{ $product->id }}">
                                        <div class="number">
                                            <span class="minus">-</span>
                                                <input type="text" class="item-amount-input product_count_{{ $product->id }}" id="product_count_value" name="products[{{ $product->id }}][count]" value="{{ $counts[$product->id] }}" data-prod-cur-count="{{ $counts[$product->id] }}" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-prod-price="{{ $product->price }}" data-in-cart-url="/in_cart_recount">
                                            <span class="plus">+</span>

                                            <div style="clear:both"></div>
                                        </div>
                                        </td>
                                        <td id="product-amount_{{ $product->id }}">{{ ($product->price) * ($counts[$product->id]) }}</td>
                                    </tr>

                                    <?php
                                            $cur_price = ($product->price) * ($counts[$product->id]);
                                            $total_price = $cur_price + $total_price;
                                    ?>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="order-wraper medium-12 columns">
                            <div id="order-block" class="row">
                                <div class="medium-12 columns">
                                    <div class="row"><div class="medium-6 columns order-title-big"><span>Оформление заказа</span></div></div>
                                    <div class="row"><div class="medium-6 columns order-title-medium"><span>Проверьте стоимость покупки</span></div></div>
                                    <div class="row">
                                            <div class="medium-6 columns" id="price-block">
                                            <div class="row">
                                                <div class="medium-6 columns"><span id="price-description">Стоимость товаров:</span></div>
                                                <div class="medium-6 columns"><span id="price-value"><span id="total-price-value">{{ $total_price }}</span>@include('partials.priceTooltip')</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="medium-6 columns">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            @if(Auth::check())
                                                <input type="submit" name="order_submit" class="button common-button order-submit" value="ПЕРЕЙТИ К ОФОРМЛЕНИЮ ЗАКАЗА">
                                            @else
                                                <input type="submit" name="order_submit" class="button common-button order-submit" value="АВТОРИЗОВАТЬСЯ И ПРОДОЛЖИТЬ">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row"><div class="medium-6 columns order-agreement"><span>Оформляя заказ, вы соглашаетесь с условиями <a href="" class="flash-link">публичной оферты</a>.</span></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @else
                <div class="medium-12 columns" >
                    <span>Корзина пуста</span>
                </div>
            @endif
        </div>
    </div>
@endsection
