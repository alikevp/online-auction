<title>@yield('title')</title>


<link href="{{ elixir('css/app.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/mediaqueries-general.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/mediaqueries-support-buttom.css') }}" rel="stylesheet" type="text/css">
{{--<script src="{{ asset('js/jquery.js') }}"></script>--}}
{{--<script src="{{ asset('js/foundation.js') }}"></script>--}}

<script src="{{ elixir('js/app.js') }}"></script>
{{--<script src="{{ asset('js/vendor.js') }}"></script>--}}
{{--<script src="{{ asset('js/vue-resource.js') }}"></script>--}}
<script src="{{ asset('js/in_cart.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/jquery.sticky-kit.min.js') }}"></script>
<link href="https://cdn.jsdelivr.net/jquery.suggestions/17.2/css/suggestions.css" type="text/css" rel="stylesheet" />
<!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/17.2/js/jquery.suggestions.min.js"></script>

{{--Scripts--}}
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        'base_url'  => url(),
    ]); ?>
</script>