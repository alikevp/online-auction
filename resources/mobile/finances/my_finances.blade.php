@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Мои бонусы
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            @include('partials.profile_sidebar')

            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Мои бонусы</h1>

                <div class="medium-12 columns orders-wraper" id="items-table-block">

                    <div class="row cur_balance">Текущий баланс: <span>
                            @if(count($bonuses)>0)
                                {{ $bonuses->first()->total }}
                            @else
                                0
                            @endif
                        </span></div>
                </div>

                <h3 class="order-title">История операций:</h3>
                @if(count($bonuses)>0)
                    <div class="medium-12 columns orders-wraper" id="items-table-block">
                        <div class="row">
                            <table class="unstriped personal-data-table" id="user-info">
                                <thead>
                                <td>Дата</td>
                                <td>Входящий баланс</td>
                                <td>Начисления</td>
                                <td>Списания</td>
                                <td>ИТОГ</td>
                                </thead>
                                @foreach($bonuses as $item)
                                    <tr>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->balance }}</td>
                                        <td>{{ $item->debit }}</td>
                                        <td>{{ $item->credit }}</td>
                                        <td>{{ $item->total }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                @else
                    <h3>Операций нет.</h3>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
