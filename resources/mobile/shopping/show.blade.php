@extends('layouts.master')

@section('style')
    <link href="{{ asset('css/itempage.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mediaqueries-show-page.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/gallery.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/swiper.min.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.theme.default.min.css') }}">
@endsection
@section('head_scripts')
    <script src="{{ asset('js/swiper.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/shopping.show.js') }}"></script>
@endsection

@section('title')
    {{  $product->name  }}
@endsection

@section('breadcrumbs')

    @include('partials.breadcrumbs')

@endsection

@section('content')
    <div class="row" id="content">
        <div class="medium-12 columns">
            <h3 class="item-name">{{ $product->name }}</h3>
            <div class="star-rating">
                <div class="star-rating__wrap">
                    <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled @if($rating == 5) checked="checked" @endif>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="Отлично"></label>
                    <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" disabled @if($rating == 4) checked="checked" @endif>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="Хорошо" ></label>
                    <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled @if($rating == 3) checked="checked" @endif>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="Приемлимо"></label>
                    <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled @if($rating == 2) checked="checked" @endif>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="Плохо"></label>
                    <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled @if($rating == 1) checked="checked" @endif>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="Очень плохо"></label>
                </div>
            </div>
            <a href="#recall" class="small-reviews-link">Отзывов: {{ count($comments) }}</a>
        </div>
        <div class="large-8 medium-12 small-12 columns">
            <div class="medium-12 columns shadow-block equivalent-height">
                <div class="large-8 medium-8 small-10 columns">
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            @foreach($photos as $key => $photo)
                                @if($key == 0)
                                    <div class="swiper-slide" id="product_foto_0" data-hash="slide-{{ $key }}"><img src="{{ URL::to($photo) }}"></div>
                                @else
                                    <div class="swiper-slide" data-hash="slide-{{ $key }}"><img src="{{ URL::to($photo) }}"></div>
                                @endif
                            @endforeach
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>


                </div>
                <div class="large-4 medium-4 small-1 columns">
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            @foreach($photos as $key => $photo)
                                <div class="swiper-slide"><a href="#slide-{{ $key }}"><img class="float-center" src="{{ URL::to($photo) }}"></a></div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="large-4 medium-12 small-12 columns">
            <div class="medium-12 columns shadow-block equivalent-block">

                <div class="medium-12 columns">
                    @if($product->price == 0)
                        <span class="item-price">Цена не указана @include('partials.priceTooltip')</span>
                    @else
                        <span class="item-price">~{{ $product->price }} Руб. @include('partials.priceTooltip')</span>
                    @endif
                    {{--<label class="item-size">РАЗМЕР (RU)<br>--}}
                    {{--<select class="item-size-select">--}}
                    {{--<option value="Any">Неважно</option>--}}
                    {{--</select>--}}
                    {{--</label>--}}
                    <div class="">
                        <a href="#" class="button add-item-to-basket-button in_cart_add" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                        <a href="#" class="button add-item-to-basket-button fast_order" data-fast-order-prod-id="{{ $product->id }}" data-fast-order-url="/fast_order">НАЧАТЬ АУКЦИОН</a>
                        <a href="#" class="button prorogue-item-button deferred_add" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_add"><i class="fa fa-heart-o" aria-hidden="true"></i>Отложить</a>
                        {{--<a href="#" class="button compare-item-button">--}}
                        {{--<i class="fa fa-list" aria-hidden="true"></i>--}}
                        {{--Сравнить--}}
                        {{--</a>--}}
                    </div>
                    <div class="medium-12 columns brand-div">
                        <img src="/images/catalog/producer/{{ $product->producer }}.gif" alt="brand" id="brand-img"><br>
                        <a href="/brand/{{ $product->producer  }}" class="brand-link">Все товары бренда</a>
                    </div>
                    {{--<h4>Коротко о товаре:</h4>--}}
                    {{--<span id="item-info">--}}
                    {{--{!! $product->description !!}--}}
                    {{--</span>--}}
                </div>
            </div>
        </div>

        <div class="large-8 medium-12 small-12 columns">

                <div class="" id="items-button-group-top-div">
                    <div class="button-group items-button-group main-tabs">
                        {{--<li><a href="{{ url('show', ['id' => $product->id]) }}/desc" class="button @if($menuItem == 'desc' || $menuItem == '') active-button @endif">Описание</a></li>--}}
                        {{--<li><a href="{{ url('show', ['id' => $product->id]) }}/attr" class="button @if($menuItem == 'attr') active-button @endif">Характеристики</a></li>--}}
                        {{--<li><a href="#" class="button">Цены <span class="value">1</span></a></li>--}}
                        {{--<li><a href="#" class="button">Карта <span class="value">2</span></a></li>--}}
                        {{--<li><a href="{{ url('show', ['id' => $product->id]) }}/recall" class="button @if($menuItem == 'recall') active-button @endif">Отзывы <span class="value">{{ count($comments) }}</span></a></li>--}}
                        {{--<li><a href="#" class="button">Обзоры <span class="value">0</span></a></li>--}}
                        {{--<li><a href="#" class="button">Обсуждения <span class="value">0</span></a></li>--}}
                        <ul class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs id="deeplinked-tabs">
                            <li class="tabs-title is-active"><a href="#desc" aria-selected="true">Описание</a></li>
                            <li class="tabs-title"><a href="#attr">Характеристики</a></li>
                            <li class="tabs-title"><a href="#recall">Отзывы <span class="value">{{ count($comments) }}</span></a></li>
                        </ul>
                    </div>
                </div>
        </div>
        <div class="large-8 medium-12 small-12 columns">
            <div class="medium-12 columns shadow-block tabs-content" data-tabs-content="deeplinked-tabs">

                    <div class="medium-12 columns item-about-div tabs-panel is-active" id="desc">
                        <div class="medium-12 columns">
                            <h4>Коротко о товаре:</h4>
                            <span id="item-info">
                                @if($product->description != '')
                                    <p>{!! $product->description !!}</p>
                                @else
                                    <p>Описание отсутствует</p>
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="medium-12 columns">
                        <div class="medium-12 columns item-about-div two-col tabs-panel" id="attr">
                            @foreach($product->attributes as $attribute)
                                <span class="item-attr">
                                    <span class="item-attr-name">{{ $attribute->name }}:</span>
                                    <span class="item-attr-value">{{ $attribute->value  }}</span>
                                </span>
                                <br>
                            @endforeach
                        </div>
                    </div>
                    <div class="medium-12 columns columns item-about-div tabs-panel" id="recall">
                        @if(count($comments) == 0)
                            <div class="medium-12 columns">
                                <p>Отзывов нет.</p>
                            </div>
                        @else
                            @foreach($comments as $comment)
                                <div class="medium-12 columns comment">
                                    <span class="commenter-name"><p>{{ $comment->user_name }}</p></span>
                                    <span class="rating"><p class="title">Рейтинг: </p><p class="value">{{ $comment->rating }}</p></span>
                                    <span class="advantages"><p class="title">Достоинства: </p><p class="value">{{ $comment->advantages }}</p></span>
                                    <span class="disadvantages"><p class="title">Недостатки: </p><p class="value">{{ $comment->disadvantages }}</p></span>
                                    <span class="text"><p class="title">Комментарий: </p><br><p class="value">{{ $comment->text }}</p></span>
                                    <span class="date"><p>{{ $comment->created_at }}</p></span>
                                </div>
                            @endforeach
                        @endif
                        <hr>
                    <div class="button-group items-button-group">
                        <section id="recall" data-magellan-target="recall">
                            <li><a href="{{ url('show', ['id' => $product->id]) }}/recall" class="button active-button green-button">Отзывы ({{ count($comments) }})</a></li>
                            {{--<li><a href="#" class="button">Комментарии</a></li>--}}
                            {{--<li><a href="#" class="button">Вопросы</a></li>--}}
                            <li><a data-open="recall-rules-modal" class="button recall-rules flash-link">Правила оформления отзывов</a></li>
                        </section>
                        <div class="reveal" id="recall-rules-modal" data-reveal>
                            <h1>Правила оформления отзывов</h1>
                            <p class="lead">
                                Подробно расскажите о своём опыте использования товара. Обратите внимание на качество, удобство модели, её соответствие заявленным характеристикам.
                            </p>
                            <p>
                                Мы не публикуем отзывы, которые содержат оскорбления и ненормативную лексику.
                            </p>
                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <ul class="vertical menu" data-accordion-menu>
                        <li>
                            <a href="#" class="button write-review green-button">НАПИСАТЬ ОТЗЫВ</a>
                            <ul class="menu vertical nested comment-menu">
                                <li>
                                    <div class="medium-6 columns comment-form">
                                        <form action="comment&={{ $id  }}" method="post">
                                            <div class="row">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                @if(Auth::check())
                                                    <div class="medium-12 columns">
                                                        <label>Имя
                                                            <input type="text" value="{{ Auth::user()->name  }}" name="name" placeholder="Ваше имя" required disabled>
                                                        </label>
                                                    </div>
                                                    <div class="medium-12 columns">
                                                        <label>Email
                                                            <input type="email" value="{{ Auth::user()->email  }} " name="email" placeholder="Ваш email" required disabled>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="medium-12 columns">
                                                        <label>Имя
                                                            <input type="text" name="name" placeholder="Ваше имя" required>
                                                        </label>
                                                    </div>
                                                    <div class="medium-12 columns">
                                                        <label>Email
                                                            <input type="email" name="email" placeholder="Ваш email" required>
                                                        </label>
                                                    </div>
                                                @endif
                                                <div class="medium-12 columns">
                                                    <label>Рейтинг
                                                        <select name="rating" required>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3" selected>3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </label>
                                                </div>
                                                <div class="medium-12 columns">
                                                    <label>Достоинства
                                                        <input type="text" name="advantages" placeholder="Достоинства товара" required>
                                                    </label>
                                                </div>
                                                <div class="medium-12 columns">
                                                    <label>Недостатки
                                                        <input type="text" name="disadvantages" placeholder="Недостатки товара" required>
                                                    </label>
                                                </div>
                                                <div class="medium-12 columns">
                                                    <label>Комментарий
                                                        <textarea name="text" placeholder="Комментарий" required></textarea>
                                                    </label>
                                                </div>
                                                <div class="medium-12 columns">
                                                    <button class="button green-button" type="submit">Отправить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <hr>
                    {{--<a href="#" class="viewed">Вы смотрели</a><span class="viewed-value"> (1)</span><br>--}}
                    {{--<img src="http://placehold.it/150x200" class="viewed-photo" alt="">--}}
                </div>
            </div>
        </div>
        <div class="items-row">
            <div class="medium-12 columns">
                <h5>Похожие товары</h5>
                <div class="owl-carousel slide-common">
                    @foreach($similarProducts as $similarProduct)
                         <div class="medium-12 columns item-column">
                            <div class="to-top">
                                <a href="{{ url('show', ['id' => $similarProduct->id]) }}">
                                    <img src="{{ URL::to(array_first($similarProduct->files)) }}" class="item-img" alt=""><br>
                                </a>
                                @if($similarProduct->producer == "" || $similarProduct->producer == "0")
                                    <span class="item-brand">Производитель не указан</span><br>
                                @else
                                    <span class="item-brand">{{ strtoupper($similarProduct->producer) }}</span><br>
                                @endif
                                <a href="{{ url('show', ['id' => $similarProduct->id]) }}">
                                    @if($similarProduct->name == "" || $similarProduct->name == "0")
                                        <span class="item-name">Название не указано</span><br>
                                    @else
                                        <span class="item-name">{{ $similarProduct->name }}</span><br>
                                    @endif
                                </a>
                            </div>
                            <div class="to-bottom">
                                <div class="star-rating">
                                    <div class="star-rating__wrap">
                                        <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled @if($similarProduct->rating == 5) checked="checked" @endif>
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="Отлично"></label>
                                        <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" disabled @if($similarProduct->rating == 4) checked="checked" @endif>
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="Хорошо" ></label>
                                        <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled @if($similarProduct->rating == 3) checked="checked" @endif>
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="Приемлимо"></label>
                                        <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled @if($similarProduct->rating == 2) checked="checked" @endif>
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="Плохо"></label>
                                        <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled @if($similarProduct->rating == 1) checked="checked" @endif>
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="Очень плохо"></label>
                                    </div>
                                </div>
                                <a href="{{ url('show', ['id' => $similarProduct->id]) }}">
                                    @if($similarProduct->price == "" || $similarProduct->price == 0)
                                        <span class="item-cost">Цена не указана</span>
                                    @else
                                        <span class="item-cost">~{{ $similarProduct->price }} руб.</span>
                                    @endif
                                </a>
                                <br>
                                <a href="#" class="button add-item-to-basket-button in_cart_add" data-in-cart-prod-id="{{ $similarProduct->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
@endsection