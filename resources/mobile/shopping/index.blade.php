@extends('layouts.master')

@section('style')
    <link href="{{ asset('css/mainpage.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mediaqueries-main-page.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owlcarousel/owl.theme.default.min.css') }}">
@endsection
@section('head_scripts')
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/shopping.index.js') }}"></script>

@endsection
@section('title')
    Jinnmart.ru интернет магазин широкого спектра товаров.
@endsection

@section('content')
    @include('partials.ad')


    @foreach($products->chunk(16) as $offers)
        <div class="row items-row">
            <div class="medium-12 columns">
                @include('partials.bigorbit')
            </div>
        </div>
    @endforeach


@endsection