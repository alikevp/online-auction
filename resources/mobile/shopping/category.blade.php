@extends('layouts.master')

@section('style')
    {{--<link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css">--}}
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mainpage.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mediaqueries-main-page.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/shopping.category.js') }}"></script>
@endsection
@section('title')
    Shopping Cart
@endsection

@section('breadcrumbs')

    <breadcrumbs category="{{ $category->id }}"></breadcrumbs>
    {{--@include('partials.breadcrumbs')--}}

@endsection

@section('content')

    @include('partials.ad')

    <div class="row">

        @include('partials.sidebar')

        <div id="products">
            @include('partials.products')
        </div>

    </div>

@endsection

@section('scripts')
    <script>
        let urlFilter = "{{ URL::to('api/filter') }}";
        {{--let category = "{{ $category->id }}";--}}
        let search = null;
    </script>
    <script src="{{ asset('js/filters.js') }}"></script>
@endsection