@extends('layouts.master')

@section('style')
    {{--<link href="{{ asset('owlcarousel/sidebar.owlcarousel') }}" rel="stylesheet" type="text/owlcarousel">--}}
    <link href="{{ asset('owlcarousel') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mainpage.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    {{--{{ $producer  }}--}}
@endsection

@section('content')

    @include('partials.ad')

    <div class="row">

        <div class="medium-12 columns">
            <h1>Товары от производителя: {{ $producer }}</h1>
        </div>
        <hr>
        <br>

        @include('partials.sidebar')

        <div id="products">
            @include('partials.products')
        </div>

    </div>

@endsection

@section('scripts')
    <script>
        let urlFilter = "{{ URL::to('api/filter') }}";
        let category = null;
        let search = null;
    </script>
    <script src="{{ asset('js/filters.js') }}"></script>
@endsection