@extends('layouts.master')

@section('style')
    <link href="{{ asset('owlcarousel') }}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Main page
@endsection

@section('content')

    @include('partials.ad')

    <div class="row items-row">
        <div class="medium-8 columns">
            <div class="medium-3 medium-centered columns category-info">
                <span class="category-name">ОДЕЖДА</span>
                <hr class="category-hr">
                <span class="category-tag">НОВИНКИ</span>
            </div>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(4) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    @include('partials.orbit', ['product' => $product])
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(4) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    @include('partials.orbit', ['product' => $product])
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
        </div>
        <div class="medium-4 columns">
            <img src="http://placehold.it/380x590" alt="">
        </div>
    </div>

    <div class="row items-row">
        <div class="medium-4 columns">
            <img src="http://placehold.it/380x310" alt="">
        </div>
        <div class="medium-8 columns">
            <div class="medium-3 medium-centered columns category-info">
                <span class="category-name">ОДЕЖДА</span>
                <hr class="category-hr">
                <span class="category-tag">НОВИНКИ</span>
            </div>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(4) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    @include('partials.orbit', ['product' => $product])
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
        </div>
    </div>
    <div class="row items-row">
        <div class="medium-4 columns">
            <img src="http://placehold.it/380x310" alt="">
        </div>
        <div class="medium-8 columns">
            <div class="medium-3 medium-centered columns category-info">
                <span class="category-name">ОДЕЖДА</span>
                <hr class="category-hr">
                <span class="category-tag">НОВИНКИ</span>
            </div>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(4) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    @include('partials.orbit', ['product' => $product])
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
        </div>
    </div>

    <div class="row items-row">
        <div class="medium-12 columns">
            <div class="medium-3 medium-centered columns category-info">
                <span class="category-name">ОДЕЖДА</span>
                <hr class="category-hr">
                <span class="category-tag">НОВИНКИ</span>
            </div>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(6) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    <div class="medium-2 columns">
                                        <img src="{{ URL::to($product->files[0]) }}" class="item-img" alt=""><br>
                                        <span class="item-brand">{{ strtoupper($product->producer) }}</span><br>
                                        <a href="{{ url('show', ['id' => $product->id]) }}">
                                            <span class="item-name">{{ $product->name }}</span><br>
                                        </a>
                                        <span class="item-cost">{{ $product->price }} руб.</span>
                                    </div>
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
        </div>
    </div>

    <div class="row items-row">
        <div class="medium-12 columns">
            <div class="medium-3 medium-centered columns category-info">
                <span class="category-name">РУБРИКА</span>
                <hr class="category-hr">
                <span class="category-tag">НОВИНКИ</span>
            </div>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous">
                        <span class="show-for-sr">Previous Slide</span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </button>
                    <button class="orbit-next">
                        <span class="show-for-sr">Next Slide</span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    @foreach($products->chunk(6) as $chunk)
                        <li class="is-active orbit-slide">
                            <div class="medium-12 columns">
                                @foreach($chunk as $product)
                                    <div class="medium-2 columns">
                                        <img src="{{ URL::to($product->files[0]) }}" class="item-img" alt=""><br>
                                        <span class="item-brand">{{ strtoupper($product->producer) }}</span><br>
                                        <a href="{{ url('show', ['id' => $product->id]) }}">
                                            <span class="item-name">{{ $product->name }}</span><br>
                                        </a>
                                        <span class="item-cost">{{ $product->price }} руб.</span>
                                    </div>
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <hr class="items-hr">
        </div>
    </div>

@endsection