@extends('layouts.master')

@section('style')
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/mainpage.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Shopping Cart
@endsection

@section('breadcrumbs')

@endsection

@section('content')

    @include('partials.ad')

    <div class="row">

        @include('partials.sidebar')

        @include('partials.products')
    </div>

@endsection

@section('scripts')
    {{--<script>--}}
    {{--let urlFilter = "{{ URL::to('api/filter') }}";--}}
    {{--let category = null;--}}
    {{--let search = "{{ Request::input('search') }}";--}}
    {{--</script>--}}
    {{--<script src="{{ asset('js/filters.js') }}"></script>--}}
@endsection