<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Title</title>
</head>
<body style="width:100%; background-color: #f5f5f5;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
<table width="640px" wborder="0" cellspacing="0" cellpadding="0" style="border: 1px solid #d7d7d7; border-radius: 8px; background-color: white; table-layout:fixed; margin-left: auto; margin-right: auto; margin-top: 10px;">
    <tr>
        <td style="text-align: center">
            <p style="font-size: 10px font-weight: bold;">
                <a href="#" style="text-decoration: none; color: #636363;">
                    Письмо отображается некорректно?
                </a>
            </p>
            <hr noshade size="2" color="#cd4daa">
            <a href="http://mag.amediatest.ru/" target="_blank">
                <img style="margin: 10px;" src="http://mag.amediatest.ru/images/logo.png" alt="logo">
            </a>
        </td>
    </tr>
    <tr style="background-color: #d7d7d7; height: 90px;">
        <td>
            <table width="100%" height="75px" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Телефоны</a></td>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Ноутбуки, планшеты, компьютеры</a></td>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Телевизоры и видео</a></td>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Малая бытовая техника</a></td>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Крупная бытовая техника</a></td>
                    <td style="text-align: center; border-right: 1px solid #cd4daa; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Фото и видео</a></td>
                    <td style="text-align: center; padding: 5px; font-weight: bold;"><a href="#" style="font-size: 14px; color: black; text-decoration: none;">Другие категории</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 30px; padding-right: 30px; padding-top: 20px; padding-bottom: 20px;">
            <p style="font-weight: bold;">
                Уважаемый человек!<br><br>
                Вы успешно зарегистрировались на нашем сайте и для подтверждения регистрации необходимо перейти по ссылке:
            </p>
            <a href="{{ $link = route('email-verification.check',
            $user->verification_token). '?email=' . urlencode($user->email) }}" style="display:block; width: 305px; height: 60px; background-color: #ce43da; color: white; text-align: center; line-height: 60px; font-size: 20px; text-decoration: none;">
                Подтвердить регистрацию
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <hr noshade size="2" color="#cd4daa">
        </td>
    </tr>
    <tr>
        <td style="padding: 20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Телевизоры и цифровое ТВ</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Ноутбуки</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Планшеты</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Apple</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Смартфоны и сотовые телефоны</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Электронные книги</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Фото и видео</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Аудио</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Красота и здоровье</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Малая бытовая техника</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Крупная бытовая техника</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Автотехника</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Игры и развлечения</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Компьютерные программы</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td>
                        <a href="#" style="font-size: 14px; color: black; text-decoration: none;">
                            <table width="120px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="https://placehold.it/100x100" alt="category">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <p style="font-weight: bold; height: 50px;  font-size: 12px; color: #333333;">Аксессуары</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p style="font-weight: bold; text-align: center; color: #c4c4c4; font-size: 12px;">
    Вы можете отписаться и не получать информацию об акциях, распродажах и спецпредложениях.<br>Для этого <a href="#" style="text-align: center; color: #c4c4c4; font-size: 12px;">перейдите по ссылке.</a><br>Данное сообщение отправлено автоматически и не требует ответа.<br>Цены и скидки на товары действительны на момент отправки данного письма.<br>Ваш любимы магазин:<br> www.blablabla.ru<br>Волгоград, ул. Уличная, д1, 4000000
</p>
</body>
</html>