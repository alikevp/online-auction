@extends('layouts.master')
@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @include('partials.cabinet.sidebar')
            <div class="medium-10 columns">
                <form action="{{ action('AdminMenuController@update', ['menu' =>$menu->id]) }}" method="post">
                    {{ method_field('put') }}
                    {{ csrf_field() }}
                    <label for="name">Назвение</label>
                    <input type="text" id="name" name="name" value="{{ $menu->name }}">

                    <label for="search">Поиск</label>
                    <input type="text" id="search" name="search">

                    <label for="category">Категория</label>
                    <select name="category[]" id="category" multiple>
                        @foreach($categories->unique() as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="button primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
@endsection