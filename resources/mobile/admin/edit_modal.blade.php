<div class="reveal custom-medium-reveal" id="user-data-edit" data-reveal>
    <form action="/personal_data_edit" method="post" data-abide novalidate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="medium-12 columns">
            <h1 class="modal-h-1">ДАННЫЕ ПОЛЬЗОВАТЕЛЯ</h1>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="medium-12 columns modal-left">
                <label>Имя <span class="red-star">*</span>
                    <input type="text" placeholder="Ваше имя" name="name" value="" required>
                </label>
                <label>Фамилия <span class="red-star">*</span>
                    <input type="text" placeholder="Ваша фамилия" name="surname" value="" required>
                </label>
                <label>Отчество <span class="red-star">*</span>
                    <input type="text" placeholder="Вашу отчество" name="patronymic" value="" required>
                </label>
                <label>Тип<span class="red-star">*</span>
                    <select name="type" required>
                        <option value="admin">Адимн</option>
                        <option value="buyer">Покупатель</option>
                        <option value="shop_admin">Админ магазина</option>
                    </select>
                </label>
                <label>E-mail <span class="red-star">*</span>
                    <input type="email" placeholder="E-mail" name="email" value="" required>
                </label>
                <label>Новый пароль <span class="red-star">*</span>
                    <input type="password" placeholder="Новый пароль" name="password" value="" required>
                </label>
                <label>Подтвердите новый пароль <span class="red-star">*</span>
                    <input type="password" placeholder="Подтвердите новый пароль" name="password_confirm" value="" required>
                </label>
            </div>
            <br>
        </div>
        <div class="medium-12 columns modal-footer">
            <div class="medium-12 columns">
                <button class="save-button submit" type="submit">СОХРАНИТЬ</button>
                <button class="cancel-button" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                <span class="info float-right">*Обязательные для заполнения поля</span>
            </div>
        </div>
    </form>
</div>