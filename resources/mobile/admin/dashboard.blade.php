@extends('layouts.aws_admin')
@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @include('partials.cabinet.sidebar')
            <div class="medium-10 columns">
                <router-link to="/">
                    <a class="button primary">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                        Список
                    </a>
                </router-link>
                <router-link to="/create">
                    <a class="button primary">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Добавить
                    </a>
                </router-link>
                <router-view></router-view>
            </div>
        </div>
    </div>
@endsection