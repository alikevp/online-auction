@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Подробности аукциона
@endsection

@section('content')
    <div class="row" id="content">

        <div class="medium-2 columns">
            <ul class="left-menu">
                <li>
                    <a href="#" class="left-menu-active">Вход в личный кабинет</a>
                </li>
                <hr>
                <li>
                    <a href="#">Вход</a>
                </li>
                <li>
                    <a href="#">Регистрация</a>
                </li>
                <li>
                    <a href="#">Восстановление пароля</a>
                </li>
                <li
                ><a href="#" class="left-menu-active">Моя корзина</a>
                </li>
            </ul>
        </div>

        <div class="medium-10 columns" id="right-block">
            <div class="medium-12 columns" id="items-table-block">
                <table class="items-table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Товар</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Старая цена</th>
                        <th>Доставка</th>
                        <th>Подарки</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @php($i = 1)
                        @foreach($order_products as $order_product)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $order_product->name  }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @php($i++)
                        @endforeach
                    </tbody>
                </table>
                <div class="reveal custom-medium-reveal" id="edit-bet" data-reveal>
                    <div id="modal-content-block"></div>
                </div>
            </div>
        </div>

    </div>
@endsection
