@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-auctions-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
    <script src="{{ asset('js/auctionFormAdd.js') }}"></script>
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
    <script src="{{ asset('js/addBetConfirm.js') }}"></script>
@endsection
@section('title')
    Подробности аукциона
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar_for_admins')
        <div class="large-10 medium-12 small-12 columns" id="right-block">
            {{--@if (isset($order_details))--}}
            <div class="medium-12 columns" id="buy-steps-block">
                <div class="medium-12">
                    <a class="flash-link" href="{{ url()->previous() }}">Назад</a>
                    <h3>Подробности аукциона</h3>
                </div>
            </div>
            <div class="medium-12 columns">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="medium-12 columns">
                            <div class="{{ $msg }} callout" data-closable="slide-out-right">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                @endforeach
                <h3 class="order-title">{{ $orders->product->name }}</h3>
                <div class="medium-6 columns shadow-block equivalent-height">

                        @if(count($my_bets)>0)
                            @if(count($bets)>0)
                                @if($my_bets->price < $bets->first()->price)
                                    <div class="order-info-block order-best-price">Ваша цена лучшая: <span> {{ $my_bets->price }} &#8381;</span></div>
                                @else
                                    <div class="order-info-block order-best-price">Лучшая цена: <span> {{ $bets->first()->price }} &#8381;</span></div>
                                @endif
                            @else
                                <div class="order-info-block order-best-price">Ваша цена лучшая: <span> {{ $my_bets->price }} &#8381;</span></div>
                            @endif
                        @else
                            @if(count($bets)>0)
                                <div class="order-info-block order-best-price">Лучшая цена: <span> {{ $bets->first()->price }} &#8381;</span></div>
                            @else
                                <div class="order-info-block order-best-price">Лучшая цена: <span> Ставок пока нет</span></div>
                            @endif
                        @endif
                    <div class="order-info-block order-status">Статус аукциона:
                        <span>
                            @if($orders->status == 0)
                                Отменен
                            @elseif($orders->status == 1)
                                Активен
                            @elseif($orders->status == 2)
                                Истек
                            @elseif($orders->status == 3)
                                Принят
                            @endif
                        </span>
                    </div>
                </div>
                <div class="medium-6 columns shadow-block equivalent-height">
                    @if(null !== $orders->delivery)
                        @foreach($orders->delivery->types as $type)
                            @if($type->slug == 'delivery')
                                <div class="order-info-block order-city">Доставка: <span><a class="flash-link" data-open="delyveryModal{{ $orders->delivery->id }}">Показать</a></span></div>
                                <div class="reveal" id="delyveryModal{{ $orders->delivery->id }}" data-reveal>
                                    <div class="medium-12 columns">
                                        <h3 class="modal-h-3">Доставка</h3>
                                        <span class="item-article">Город: {{ $orders->delivery->city }}</span>
                                        <br>
                                        <span class="item-article">Улица: {{ $orders->delivery->street }}</span>
                                        <br>
                                        <span class="item-article">Дом: {{ $orders->delivery->house }}</span>
                                        <br>
                                        @if($orders->status == 3)
                                            <span class="item-article">Квартира: {{ $orders->delivery->apartment }}</span>
                                        @else
                                            <span class="item-article">Квартира стрыта
                                            <span style="color:gray; border-bottom: 0px;" data-tooltip
                                                  aria-haspopup="true" class="has-tip top"
                                                  data-disable-hover="false" tabindex="1"
                                                  title="Данные будут открыты после завершения аукциона">(?)</span>
                                                            </span>
                                            </span>
                                        @endif
                                        <br>
                                        <div>
                                            <button class="close-button" data-close
                                                    aria-label="Close modal"
                                                    type="button">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @elseif($type->slug == 'pickup')
                                <div class="order-info-block order-city">Самовывоз: <span>{{ $orders->delivery->city }}</span></div>
                            @endif
                        @endforeach
                    @else
                        <div>Доставка: <span>Не указана</span></div>
                    @endif
                    <div class="order-info-block order-start">Начало аукциона:
                        <span>21 мая 2017г 14:57</span></div>
                    <div class="order-info-block order-finish">До окончания осталось:
                        <span>1д 5ч 7м 12с</span></div>

                </div>
                    @if($orders->status == 3)
                        <h3 class="order-title">Данные покупателя:</h3>
                        <div class="medium-12 columns orders-wraper" id="items-table-block">
                            <div class="row">
                                <table class="unstriped personal-data-table" id="user-info">
                                    <tr>
                                        <td><span>ФИО:</span></td>
                                        <td><span>{{ $user->surname }} {{ $user->name }} {{ $user->patronymic }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>E-mail: </span></td>
                                        <td>
                                            <span>{{ $user->e_mail }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span>Дата рождения:</span></td>
                                        <td><span>{{ $user->date_of_birth }}</span></td>
                                    </tr>                            <tr>
                                        <td><span>Пол:</span></td>
                                        <td>
                                    <span>
                                        @if($user->sex == "m")
                                            Мужской
                                        @elseif($user->sex == "f")
                                            Женский
                                        @else
                                            Не указан
                                        @endif
                                    </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span>Телефон:</span></td>
                                        <td><span>{{ $user->telefone }}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @else
                        <h3 class="order-title">Покупатель скрыт.
                            <span style="color:gray; border-bottom: 0px;" data-tooltip
                                  aria-haspopup="true" class="has-tip top"
                                  data-disable-hover="false" tabindex="1"
                                  title="Данные будут открыты после завершения аукциона">(?)</span>
                            </span>
                        </h3>
                    @endif
                @if(count($my_bets)>0)
                    <h3 class="order-title">Моя ставка:</h3>
                    <div class="medium-12 columns orders-wraper" id="items-table-block">
                        <div class="row">
                            <div class="medium-12 columns order-block">
                                <div class="row">
                                    <div class="medium-2 columns order-column order-logo">
                                        {{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
                                    </div>
                                    <div class="medium-7 columns order-column order-details">
                                        <div class="order-title">
                                            <span>{{ $my_bets->shops->first()->name }}</span>
                                        </div>
                                        <div class="order-description">
                                            <div>Предложение выставлено: <span>{{ $my_bets->created_at }}</span></div>

                                            @if(count($my_bets->deliveries)>0)
                                                <div>Доставка: <span>{{ count($my_bets->deliveries) }} вариант(а/ов). Самый быстрый - {{ $my_bets->deliveries->min('delivery_time')  }} дней. <a
                                                                class="flash-link"
                                                                data-open="offerDeliveres-1">(Подробнее)</a></span></div>
                                                <div class="reveal custom-medium-reveal" id="offerDeliveres-1" data-reveal>
                                                    <div class="medium-12 columns">
                                                        <h3 class="modal-h-3">Доставки:</h3>
                                                        <button class="close-button" data-close aria-label="Close modal" type="button">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <table class="items-table unstriped">
                                                            <tr>
                                                                <th>Тип</th>
                                                                <th>Срок доставки (дней)</th>
                                                                <th>Подробности</th>
                                                            </tr>
                                                            @foreach ($my_bets->deliveries as $delivery)
                                                            <tr>
                                                                <td>{{ $delivery->type }}</td>
                                                                <td>{{ $delivery->delivery_time }}</td>
                                                                <td><a class="flash-link"
                                                                       data-open="offerDeliveriesDetails-{{ $delivery->id }}">(Подробности)</a>
                                                                </td>
                                                                <div class="reveal custom-medium-reveal"
                                                                     id="offerDeliveriesDetails-{{ $delivery->id }}" data-reveal>
                                                                    <div class="medium-12 columns">
                                                                        <span>Подробности доставки:</span>
                                                                        <div>
                                                                            {{ $delivery->description }}
                                                                        </div>
                                                                    </div>
                                                                    <button class="close-button" data-close
                                                                            data-open="offerDeliveres-1"
                                                                            aria-label="Close reveal" type="button">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @else
                                                <div>Доставки: <span>нет</span>
                                                </div>
                                            @endif
                                            @if(count($my_bets->gifts)>0)
                                                <div>Подарки: <span>{{ count($my_bets->gifts) }} вариант(а/ов)<a class="flash-link"
                                                                                      data-open="offerGifts-1">(Подробнее)</a></span>
                                                </div>
                                                <div class="reveal custom-medium-reveal" id="offerGifts-1" data-reveal>
                                                    <div class="medium-12 columns">
                                                        <h3 class="modal-h-3">Подарки:</h3>
                                                        <button class="close-button" data-close aria-label="Close modal" type="button">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <table class="items-table unstriped">
                                                            <tr>
                                                                <th>Тип</th>
                                                                <th>Название</th>
                                                                <th>Описание</th>
                                                            </tr>
                                                            @foreach ($my_bets->gifts as $gift)
                                                                <tr>
                                                                    <td>{{ $gift->type }}</td>
                                                                    <td>{{ $gift->name }}</td>
                                                                    <td>{{ $gift->description }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @else
                                                <div>Подарки: <span>нет</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="medium-3 columns order-column order-actions">
                                        <div class="order-info-block shop-rate">Рейтинг:
                                            <span>
                                                {{ $my_bets->shops->first()->offerrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                          aria-haspopup="true" class="has-tip top"
                                                          data-disable-hover="false" tabindex="1"
                                                          title="Проведено аукционов">(?)</span>
                                                /
                                                {{ $my_bets->shops->first()->dealrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                         aria-haspopup="true" class="has-tip top"
                                                         data-disable-hover="false" tabindex="1"
                                                         title="Успешных продаж">(?)</span>
                                            </span>
                                        </div>
                                        <div class="order-info-block shop-comments">Отзывы: <span>{{ $my_bets->shops->first()->commentcount }} <a href="/shopdetails?shop={{ $my_bets->shops->first()->id }}#recall/"
                                                                                                        class="flash-link">Читать</a> </span>
                                        </div>
                                        <div class="order-info-block shop-comments">Цена: <span>{{ $my_bets->price }} &#8381;</span>
                                            <div class="order-info-block shop-comments">Старая цена: <span>{{ $my_bets->old_price }} &#8381;</span>
                                            </div>
                                        </div>
                                        <div class="actions-buttons">
                                            @if(($orders->status == 1 OR $orders->status == 2) && $my_bets->status == 0)
                                                <a href="/auction/order/editbet?offergroup={{ $my_bets->id }}"
                                                   class="button common-button">Изменить ставку</a>
                                            @elseif($my_bets->status == 1)
                                                <div class="acepted">Принято</div>
                                            @elseif(($orders->status !== 1 OR $orders->status !== 2))
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h3>Ваших предложений нет.</h3>
                    <a class="create-bet common-button" data-open="add-bet">Сделать предложение</a>
                    <div class="reveal custom-medium-reveal large" id="add-bet" data-reveal>
                        <form action="/auction/order/savebet" id="newBetForm" method="post" data-abide novalidate>
                            <div class="medium-12 columns">
                                <div class="medium-12 columns">
                                    <h3 class="modal-h-3">ПАРАМЕТРЫ СТАВКИ</h3>
                                </div>
                                <button class="close-button" data-close aria-label="Close modal" type="button">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="medium-12 columns">
                                    <ul class="accordion create-bet-accordion" data-accordion data-allow-all-closed="true">
                                        <li class="accordion-item is-active" data-accordion-item>
                                            <a href="#" class="accordion-title">Список товаров</a>
                                            <div class="accordion-content" data-tab-content>
                                                <table class="items-table unstriped">
                                                    <thead>
                                                    <tr>
                                                        <th>№</th>
                                                        <th>Товары</th>
                                                        <th>Количество</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>
                                                                <div class="medium-3 columns">
                                                                    <img class="order-photo"
                                                                         src="{{ url(array_first($orders->product->files)) }}"
                                                                         alt="item image">
                                                                </div>
                                                                <div class="medium-9 columns">{{ $orders->product->name }}</div>
                                                            </td>
                                                            <td>
                                                                <div class="medium-9 columns">{{ $orders->quantity }}</div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="medium-12 columns">
                                    <label>Количество <span class="red-star">*</span>
                                        <input type="number" name="quantity" min="{{ $orders->quantity }}" max="{{ $orders->quantity }}"
                                               placeholder="Количество товара" required value="{{ $orders->quantity }}">
                                    </label>
                                    <label>Цена <span class="red-star">*</span>@if(count($bets)>0) (Лучшее предложение -- {{ $bets->first()->price }} &#8381;) @endif
                                        <input type="text" name="price" placeholder="Сумма к оплате"
                                               pattern="[0-9]+(\.[0-9]{0,2})?%?" required>
                                    </label>
                                    <span class="gifts-info">Доставки</span>
                                    <span class="form-add-button float-right flash-link" id="delivery-add-button">Добавить доставку</span>
                                    <hr>
                                    <div class="row forms-add" id="delivery-form">
                                    </div>
                                </div>
                                <div class="medium-12 columns">
                                    <span class="gifts-info">Подарки</span>
                                    <span class="form-add-button float-right flash-link" id="gift-add-button">Добавить подарок</span>
                                    <hr>
                                    <div class="row forms-add" id="gifts-form">
                                    </div>
                                </div>
                            </div>
                            <div class="medium-12 columns modal-footer">
                                <div class="medium-12 columns">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="order_id"
                                           value="{{ $orders->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden"
                                           name="orderitem_id" value="">
                                    <input class="hidefield" hidden="hidden" type="hidden"
                                           name="product_id" value="{{ $orders->product->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="items_count"
                                           value="1">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="medium-12 columns">
                                        {{--<button type="submit" class="save-button">ОТПРАВИТЬ ПРЕДЛОЖЕНИЕ</button>--}}
                                        <button type="submit" id="newBetFormSubmit"  style="display: none;"></button>
                                        <a class="save-button button common-button button-accept submit-button" id="openConfirmButton">ОТПРАВИТЬ
                                            ПРЕДЛОЖЕНИЕ</a>
                                        <div class="reveal" id="confirm-add-bet" data-reveal style="padding: 2rem;">
                                            <h3 class="modal-h-3" id="modalTitle">Вы действительно хотите сделать предложение?</h3>
                                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <p>После подтверждения Ваше предложение появится в списке предложений
                                                заказа.</p>
                                            <hr>
                                            <button class="button common-button button-accept" id="addBetConfirmButton"
                                                    style="margin-bottom: 0;">СОГЛАСЕН
                                            </button>
                                            <a data-open="add-bet" class="button common-button button-decline">Отменить</a>
                                        </div>
                                        <button class="cancel-button button common-button button-decline" data-close
                                                aria-label="Close modal"
                                                type="button">Отменить
                                        </button>
                                        <span class="info float-right">*Обязательные для заполнения поля</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
                @if(count($bets)>0)
                    <h3 class="order-title">Ставки конкурентов:</h3>
                        <div class="medium-12 columns orders-wraper" id="items-table-block">
                            <div class="row">
                                @foreach ($bets as $bet)
                                    <div class="medium-12 columns order-block">
                                        <div class="row">
                                            <div class="medium-2 columns order-column order-logo">
                                                {{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
                                            </div>
                                            <div class="medium-7 columns order-column order-details">
                                                <div class="order-title">
                                                    @if($orders->status == 3)
                                                        <span>{{ $bet->shops->first()->name }}</span>
                                                    @else
                                                        <span>Название магазина скрыто
                                                        <span style="color:gray; border-bottom: 0px;" data-tooltip
                                                              aria-haspopup="true" class="has-tip top"
                                                              data-disable-hover="false" tabindex="1"
                                                              title="Данные будут открыты после завершения аукциона">(?)</span>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="order-description">
                                                    <div>Предложение выставлено: <span>{{ $bet->created_at }}</span></div>

                                                    @if(count($bet->deliveries)>0)
                                                        <div>Доставка: <span>{{ count($bet->deliveries) }} вариант(а/ов). Самый быстрый - {{ $bet->deliveries->min('delivery_time')  }} дней. <a
                                                                        class="flash-link"
                                                                        data-open="offerDeliveres-1">(Подробнее)</a></span></div>
                                                        <div class="reveal custom-medium-reveal" id="offerDeliveres-1" data-reveal>
                                                            <div class="medium-12 columns">
                                                                <span>Доставки:</span>
                                                                <table class="items-table unstriped">
                                                                    <tr>
                                                                        <th>Тип</th>
                                                                        <th>Срок доставки (дней)</th>
                                                                        <th>Подробности</th>
                                                                    </tr>
                                                                    @foreach ($bet->deliveries as $delivery)
                                                                        <tr>
                                                                            <td>{{ $delivery->type }}</td>
                                                                            <td>{{ $delivery->delivery_time }}</td>
                                                                            <td><a class="flash-link"
                                                                                   data-open="offerDeliveriesDetails-1">(Подробности)</a>
                                                                            </td>
                                                                            <div class="reveal custom-medium-reveal"
                                                                                 id="offerDeliveriesDetails-1" data-reveal>
                                                                                <div class="medium-12 columns">
                                                                                    <span>Подробности доставки:</span>
                                                                                    <div>
                                                                                        {{ $delivery->description }}
                                                                                    </div>
                                                                                </div>
                                                                                <button class="close-button" data-close
                                                                                        data-open="offerDeliveres-1"
                                                                                        aria-label="Close reveal" type="button">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                        </tr>
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div>Доставки: <span>нет</span>
                                                        </div>
                                                    @endif
                                                    @if(count($bet->gifts)>0)
                                                        <div>Подарки: <span>{{ count($bet->gifts) }} вариант(а/ов)<a class="flash-link"
                                                                                                                         data-open="offerGifts-1">(Подробнее)</a></span>
                                                        </div>
                                                        <div class="reveal custom-medium-reveal" id="offerGifts-1" data-reveal>
                                                            <div class="medium-12 columns">
                                                                <span>Подарки:</span>
                                                                <table class="items-table unstriped">
                                                                    <tr>
                                                                        <th>Тип</th>
                                                                        <th>Название</th>
                                                                        <th>Описание</th>
                                                                    </tr>
                                                                    @foreach ($bet->gifts as $gift)
                                                                        <tr>
                                                                            <td>Подарок</td>
                                                                            <td>Утюг в подарок</td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    <tr>
                                                                        <td>{{ $gift->type }}</td>
                                                                        <td>{{ $gift->name }}</td>
                                                                        <td>{{ $gift->description }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div>Подарки: <span>нет</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="medium-3 columns order-column order-actions">
                                                <div class="order-info-block shop-rate">Рейтинг:
                                                    <span>
                                                        {{ $bet->shops->first()->offerrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                          aria-haspopup="true" class="has-tip top"
                                                          data-disable-hover="false" tabindex="1"
                                                          title="Проведено аукционов">(?)</span>
                                                        /
                                                        {{ $bet->shops->first()->dealrate }}<span style="color:gray; border-bottom: 0px;" data-tooltip
                                                         aria-haspopup="true" class="has-tip top"
                                                         data-disable-hover="false" tabindex="1"
                                                         title="Успешных продаж">(?)</span>
                                                    </span>
                                                </div>

                                                    @if($orders->status == 3)
                                                    <div class="order-info-block shop-comments">Отзывы: <span>{{ $bet->shops->first()->commentcount }} <a href="{{url('shopdetails')}}?shop={{ $bet->shops->first()->id }}#recall" class="flash-link">Читать</a> </span></div>
                                                    @else
                                                        <div class="order-info-block shop-comments">Отзывы скрыты <span style="color:gray; border-bottom: 0px;" data-tooltip
                                                                                                                       aria-haspopup="true" class="has-tip top"
                                                                                                                       data-disable-hover="false" tabindex="1"
                                                                                                                       title="Данные будут открыты после завершения аукциона">(?)</span>
                                                            </span></div>
                                                    @endif

                                                <div class="order-info-block shop-comments">Цена: <span>{{ $bet->price }} &#8381;</span>
                                                </div>
                                                <div class="order-info-block shop-comments">Старая цена: <span>{{ $bet->old_price }} &#8381;</span>
                                                </div>
                                                <div class="actions-buttons">
                                                    @if(($orders->status == 1 OR $orders->status == 2) && $bet->status == 0)
                                                    @elseif($bet->status == 1)
                                                        <div class="acepted">Принято</div>
                                                    @elseif(($orders->status !== 1 OR $orders->status !== 2))
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    <h3>Ставок конкурентов пока нет.</h3>
                @endif
        </div>
    </div>
</div>
@endsection
