@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-editbet-page.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
    <script src="{{ asset('js/auctionFormAdd.js') }}"></script>
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
    <script src="{{ asset('js/editBetConfirm.js') }}"></script>
@endsection
@section('title')
    Подробности аукциона
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar_for_admins')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (count($my_bet)>0)
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12">
                        <a class="flash-link" href="/auction/order?order={{ $my_bet->order_id }}">Назад</a>
                        <h3>Изменение предложения</h3>
                    </div>
                </div>
                <div class="medium-12 columns">
                    @if (session('message') !== null)
                        <div class="success callout" data-closable="slide-out-right">
                            <h5>Успешно.</h5>
                            <p>Предложение успешно изменено.</p>
                            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="reveal" id="deliveryAdd" data-reveal>
                        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-h-3">Добавить доставку</h3>
                        <form action="/auction/order/delivery_create" id="addDeliveryForm" method="post" data-abide novalidate>
                            <div class="medium-12 columns form-block">
                                <label>Тип <span class="red-star">*</span><br>
                                    <select name="delivery_type" required>
                                        <option value="">Тип</option>
                                        <option value="Самовывоз">Самовывоз</option>
                                        <option value="Доставка">Доставка</option>
                                    </select>
                                </label>
                                <label>Срок доставки (дней) <span class="red-star">*</span>
                                    <input type="number" name="delivery_time" placeholder="Срок доставки в днях" required>
                                </label>
                                <label>Подробности доставки <span class="red-star">*</span>
                                    <textarea name="delivery_description" placeholder="Детально опишите свои условия доставки (Транспортные компании, сроки, стоимость и т.д.)" required></textarea>
                                </label>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="groupId" value="{{ $my_bet->id }}">

                            <a class="button common-button button-accept" id="openConfirmAddDelivery">Добавить</a>
                            <a class="button common-button button-decline" data-close>Отменить</a>
                            <div class="reveal" id="confirm-add-delivery" data-reveal>
                                <h3 class="modal-h-3">Вы действительно хотите добавить доставку?</h3>
                                <p>После подтверждения доставка появится в Вашем предложение.</p>
                                <hr>
                                <a class="button common-button button-accept" id="AddDeliveryConfirmButton" style="margin-bottom: 0;">СОГЛАСЕН</a>
                                <a data-open="deliveryAdd" class="button common-button button-decline">ОТМЕНИТЬ</a>
                            </div>
                            <button type="submit" style="display: none;"></button>
                        </form>
                    </div>
                    <div class="reveal" id="giftAdd" data-reveal>
                        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-h-3">Добавить подарок</h3>
                        <form action="/auction/order/gift_create" method="post" id="addGiftForm" data-abide novalidate>
                            <div class="medium-12 columns form-block">
                                <label>Тип <span class="red-star">*</span><br>
                                    <select name="option_type" required>
                                        <option value="">Тип</option>
                                        <option value="Товар">Товар</option>
                                        <option value="Скидка">Скидка</option>
                                    </select>
                                </label>
                                <label>Название <span class="red-star">*</span>
                                    <input type="text" name="option_name" placeholder="Название" required>
                                </label>
                                <label>Описание <span class="red-star">*</span>
                                    <textarea rows="5" cols="45" name="option_description" placeholder="Описание" required></textarea>
                                </label>
                            </div>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="groupId" value="{{ $my_bet->id }}">

                            <a class="button common-button button-accept" id="openConfirmAddGift">Добавить</a>
                            <a class="button common-button button-decline" data-close>Отменить</a>
                            <div class="reveal" id="confirm-add-gift" data-reveal>
                                <h3 class="modal-h-3">Вы действительно хотите добавить подарок?</h2>
                                <p>После подтверждения подарок появится в Вашем предложение.</p>
                                <hr>
                                <a class="button common-button button-accept" id="AddGiftConfirmButton" style="margin-bottom: 0;">СОГЛАСЕН</a>
                                <a data-open="giftAdd" class="button common-button button-decline">ОТМЕНИТЬ</a>
                            </div>
                            <button type="submit" style="display: none;"></button>
                        </form>
                    </div>
                    <form action="/auction/order/saveeditedbet" method="post" id="editBetForm" data-abide novalidate>
                        <div class="medium-6 columns modal-left">
                            <div class="medium-12 columns">
                                @if(null !== $orders->delivery)
                                <span class="item-article">Город доставки: {{$orders->delivery->first()->city}}</span><br>
                                @else
                                    <span class="item-article">Доставка: Не указана</span><br>
                                @endif
                                <span class="item-article">Начало аукциона: {{ $orders->created_at }}</span><br>
                                <span class="item-color">Окончание аукциона: {{ $orders->active_till }}</span><br>
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <label>Цена(без учёта доставки) <span class="red-star">*</span>
                                <input type="number" name="price" max="{{ $my_bet->price }}" placeholder="Сумма к оплате" required value="{{ $my_bet->price }}">
                            </label>
                        </div>

                        <div class="medium-12 columns" >
                            <ul class="accordion create-bet-accordion" data-accordion data-allow-all-closed="true">
                                <li class="accordion-item is-active" data-accordion-item>
                                    <a href="#" class="accordion-title">Список товаров</a>
                                    <div class="accordion-content" data-tab-content>
                                        <table class="items-table unstriped">
                                            <thead>
                                            <tr>
                                                <th>№</th>
                                                <th>Товары</th>
                                                <th>Количество</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <div class="medium-3 columns"><img class="order-photo" src="{{ url(array_first($orders->product->files)) }}" alt="item image"></div>
                                                        <div class="medium-9 columns">{{ $orders->product->name }}</div>
                                                    </td>
                                                    <td><input type="number" name="quantity" min="{{ $orders->quantity }}" max="{{ $orders->quantity }}"
                                                               placeholder="Количество товара" required value="{{ $orders->quantity }}">
                                                    </td>
                                                </tr>
                                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $orders->product->id }}][product_id]" value="{{ $orders->product->id }}">

                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="medium-12 columns">
                            <h3 class="order-title">Доставки <a data-open="deliveryAdd" class="form-add-button flash-link">Добавить доставку</a></h3>
                            @if(count($my_bet->deliveries)>0)
                                <table class="items-table unstriped">
                                    <tr>
                                        <th>№</th>
                                        <th>Тип</th>
                                        <th>Срок</th>
                                        <th>Подробности</th>
                                        <th>Действие</th>
                                    </tr>
                                    @php($i = 1)
                                    @foreach ($my_bet->deliveries as $delivery_item)
                                        <tr id="delivery_row_{{ $delivery_item->id }}">
                                            <td>{{ $i }}</td>
                                            <td>
                                                <?php
                                                $delivery_type_arr = array();
                                                $delivery_type_arr[] = 'Самовывоз';
                                                $delivery_type_arr[] = 'Доставка';
                                                ?>
                                                <select style="height: 50px;width: 150px;" name="delivery[{{  $delivery_item->id }}][type]">
                                                    <option value="">Тип</option>
                                                    @foreach ($delivery_type_arr as $delivery_type)
                                                        <option  @if ($delivery_item->type == $delivery_type) selected @endif value="{{ $delivery_type }}">{{ $delivery_type }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input style="height: 50px;width: 50px;" type="number" min="0" name="delivery[{{ $delivery_item->id }}][delivery_time]" placeholder="Срок доставки в днях" required value="{{ $delivery_item->delivery_time }}">
                                            </td>
                                            <td>
                                                <textarea style="height: 50px;width: 500px;" name="delivery[{{ $delivery_item->id }}][description]" placeholder="Детально опишите свои условия доставки (Транспортные компании, сроки, стоимость и т.д.)" required>{!! $delivery_item->description !!}</textarea>
                                            </td>
                                            <td><a class="button delete-button common-button button-decline" onclick="deliveryDelete(this, '{{ $delivery_item->id }}')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                        </tr>

                                        <input class="hidefield" hidden="hidden" type="hidden" name="delivery[{{ $delivery_item->id }}][delivery_id]" value="{{ $delivery_item->id }}">
                                        @php($i++)
                                    @endforeach
                                </table>
                            @else
                                <p>Доставки не указаны</p>
                            @endif
                            <br>
                            <h3 class="order-title">Подарки <a data-open="giftAdd" class="form-add-button flash-link">Добавить подарок</a></h3>
                            @if(count($my_bet->gifts)>0)
                                <table class="items-table unstriped">
                                    <tr>
                                        <th>№</th>
                                        <th>Тип</th>
                                        <th>Название</th>
                                        <th>Описание</th>
                                        <th>Действие</th>
                                    </tr>
                                    @php($i = 1)
                                    @foreach ($my_bet->gifts as $option_item)
                                        <tr  id="gift_row_{{ $option_item->id }}">
                                            <td>{{ $i }}</td>
                                            <td>
                                                <?php
                                                $option_type_arr = array();
                                                $option_type_arr[] = 'Товар';
                                                $option_type_arr[] = 'Скидка';
                                                ?>
                                                <select style="height: 50px;width: 100px;" name="options[{{ $option_item->id }}][type]">
                                                    <option value="">Тип</option>
                                                    @foreach ($option_type_arr as $option_type)
                                                        <option @if ($option_item->type == $option_type) selected @endif value="{{ $option_type }}">{{ $option_type }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input style="height: 50px;width: 200px;" type="text" name="options[{{ $option_item->id }}][name]" placeholder="Название" required value="{{ $option_item->name }}">
                                            </td>
                                            <td>
                                                <textarea style="height: 50px;width: 400px;" name="options[{{ $option_item->id }}][description]" placeholder="Описание">{{ $option_item->description }}</textarea>
                                            </td>
                                            <td><a class="button delete-button common-button button-decline" onclick="giftDelete(this, '{{ $option_item->id }}')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                        </tr>

                                        <input class="hidefield" hidden="hidden" type="hidden" name="options[{{ $option_item->id }}][option_id]" value="{{ $option_item->option_id }}">
                                        @php($i++)
                                    @endforeach
                                </table>
                            @else
                                <p>Подарки не указаны</p>
                            @endif
                        </div>
                        <div class="medium-12 columns">
                            <div class="medium-12 columns modal-footer custom-footer">
                                <div class="medium-12 columns">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="group_id" value="{{ $my_bet->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="order_id" value="{{ $orders->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="shop_id" value="{{ $my_bet->shop_id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="old_price" value="{{ $my_bet->old_price }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <a class="button save-button common-button button-accept" id="openConfirmEditBet">СОХРАНИТЬ</a>
                                    <div class="reveal" id="confirm-edit-bet" data-reveal>
                                        <h2 id="modalTitle">Вы действительно хотите изменить предложение?</h2>
                                        <p>После подтверждения Ваше предложение будет обновлено.</p>
                                        <hr>
                                        <a class="button common-button button-accept" id="editBetConfirmButton" style="margin-bottom: 0;">СОГЛАСЕН</a>
                                        <a data-close class="button common-button button-decline">ОТМЕНИТЬ</a>
                                    </div>
                                    <button style="display: none;" type="submit"></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="medium-12 columns" >
                    <span>Заказов в подписанных категориях не найдено.</span>
                </div>
            @endif
        </div>
    </div>
@endsection

