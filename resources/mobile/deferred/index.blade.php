@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-deferred-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/cart.index.js') }}"></script>
@endsection
@section('title')
    ОТЛОЖЕННЫЕ ТОВАРЫ
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (isset($products))
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12">
                        <h3>ОТЛОЖЕННЫЕ ТОВАРЫ</h3>
                    </div>
                </div>
                <div class="medium-12 columns orders-wraper" id="items-table-block">
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="medium-12 columns order-block" id="deferred_prod_row_{{ $product->id }}">
                                <div class="row">
                                    <div class="large-2 medium-12 small-12 columns order-column order-logo">
                                        <a href="{{ url('show', $product->id) }}"><img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image"></a>
                                    </div>
                                    <div class="large-7 medium-12 small-12 columns order-column order-details">
                                        <div class="order-title">
                                            <a href="{{ url('show', $product->id) }}">{{ $product->name }}</a>
                                        </div>
                                        <div class="order-description">
                                            <div>Производитель: <span>{{ $product->name }}}</span></div>
                                        </div>
                                    </div>
                                    <div class="large-3 medium-12 small-12 columns order-column order-actions">
                                        <div class="actions-offers">

                                        </div>
                                        <div class="actions-best-price">

                                        </div>
                                        <div class="actions-buttons">
                                            <a href="#" class="button common-button button-accept hide-button" id="deferred_to_cart" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_to_cart">В корзину</a>
                                            <a href="#" class="button common-button button-decline hide-button" id="deferred_del" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_del">Убрать из отложенного</a><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="medium-12 columns" >
                    <span>Отложенных товаров нет</span>
                </div>
            @endif
        </div>
    </div>
@endsection
