@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
@endsection
@section('title')
    Профиль
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            <?php $user = Auth::user(); ?>
            @if($user->type == 'shop_admin')
                    @include('partials.sidebarShopWrapper')
            @else
                    @include('partials.profile_sidebar')
            @endif

            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Подписки моего магазина</h1>
                @if (Session::has('msg'))
                    <div class="callout {{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" data-closable="slide-out-right">
                        <p>
                            {{ Session::get("msg") }}
                        </p>
                        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form action="/shop_subscriptions/update" method="post">
                    {{ csrf_field() }}

                    <label for="name">Категории</label>
                    <select name="category[]" multiple size="30">
                        @include('partials.nested_select')
                    </select>
                    <div class="medium-8 columns">
                        <button type="submit" class="common-button button-accept">Сохранить</button>
                        <a href="/shop_subscriptions" class="button common-button button-decline">Отменить</a>
                    </div>
                    <div class="medium-4 columns">
                        <a href="/howeditsubscriptions" class="flash-link float-right">Как редактировать подписки?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


