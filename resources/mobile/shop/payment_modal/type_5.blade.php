<div class="medium-12 columns">
    <label>Название сервиса<span class="red-star">*</span>
        <input type="text" placeholder="Название сервиса" name="name" required>
    </label>
</div>
<div class="medium-12 columns">
    <label>№ счета<span class="red-star">*</span>
        <input type="number" placeholder="№ счета" name="account" required>
    </label>
</div>
<div class="medium-12 columns">
    <label>Описание<span class="red-star">*</span>
        <textarea name="description"></textarea>
    </label>
</div>
