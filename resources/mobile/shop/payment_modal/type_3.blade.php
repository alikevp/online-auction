<div class="medium-12 columns">
    <label>Банк<span class="red-star">*</span>
        <input type="text" placeholder="Название банка" name="name" required>
    </label>
</div>
<div class="medium-12 columns">
    <label>№ карты<span class="red-star">*</span>
        <input type="number" placeholder="№ карты" name="account" required>
    </label>
</div>
<div class="medium-12 columns">
    <label>Описание<span class="red-star">*</span>
        <textarea name="description"></textarea>
    </label>
</div>
