<div class="medium-12 columns">
    <label>Счет<span class="red-star">*</span>
        <select name="bank_data_id">
            @foreach($bank_data_list as $row)
                <option value="{{ $row->id }}">{{ $row->bank_name }} - {{ $row->account }}</option>
            @endforeach
        </select>
    </label>
</div>
<div class="medium-12 columns">
    <label>Описание<span class="red-star">*</span>
        <textarea name="description"></textarea>
    </label>
</div>

