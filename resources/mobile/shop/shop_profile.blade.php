@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-profile-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Профиль
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            <?php $user = Auth::user(); ?>
            @if($user->type == 'shop_admin')
                    @include('partials.sidebarShopWrapper')
            @else
                    @include('partials.profile_sidebar')
            @endif

            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Данные моего магазина</h1>
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="medium-12 columns">
                            <div class="{{ $msg }} callout" data-closable="slide-out-right">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="large-6 medium-12 small-12 columns dashed-divs">
                    <div class="medium-12 columns dashed-block">
                        <h2>Основные данные</h2>
                        <table class="unstriped personal-data-table" id="user-info">
                            <tr>
                                <td><span>Название:</span></td>
                                <td><span>{{ $shop_data->name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>Юридическое название:</span></td>
                                <td><span>{{ $shop_data->le_name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>Полное юридическое название:</span></td>
                                <td><span>{{ $shop_data->full_le_name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ФИО директора:</span></td>
                                <td><span>{{ $shop_data->director_fio }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ИНН:</span></td>
                                <td><span>{{ $shop_data->inn }}</span></td>
                            </tr>
                            <tr>
                                <td><span>КПП:</span></td>
                                <td><span>{{ $shop_data->kpp }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ОГРН:</span></td>
                                <td><span>{{ $shop_data->ogrn }}</span></td>
                            </tr>
                        </table>
                        <a class="flash-link" data-open="personal-data-edit">Изменить данные</a>
                        <div class="reveal custom-medium-reveal" id="personal-data-edit" data-reveal>
                            <form action="/shop_data_update" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">ОСНОВНЫЕ ДАННЫЕ</h1> 
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="medium-6 columns form-column-left">
                                        <div class="medium-12 columns input-column">
                                            <label>Название<span class="red-star">*</span>
                                                <input type="text" placeholder="" name="shop_name" value="{{ $shop_data->name }}" required>
                                            </label>
                                        </div>
                                        <div class="medium-12 columns input-column">
                                            <label>Юридическое название<span class="red-star">*</span>
                                                <input type="text" placeholder="" name="le_name" value="{{ $shop_data->le_name }}" required>
                                            </label>
                                        </div>
                                        <div class="medium-12 columns input-column">
                                            <label>Полное юридическое название<span class="red-star">*</span>
                                                <input type="text" id="name_f" placeholder="" name="full_le_name" value="{{ $shop_data->full_le_name }}" required>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="medium-6 columns form-column-right">
                                        <div class="medium-12 columns input-column">
                                            <label>ФИО директора<span class="red-star">*</span>
                                                <input type="text" placeholder="" name="director_fio" value="{{ $shop_data->director_fio }}" required>
                                            </label>
                                        </div>
                                        <div class="medium-12 columns input-column">
                                            <label>ИНН<span class="red-star">*</span>
                                                @if(isset($shop_data->inn))
                                                    <input disabled type="text" placeholder="" maxlength="12" name="shop_inn" value="{{ $shop_data->inn }}" required>
                                                    @else
                                                <input type="text" placeholder="" maxlength="12" name="shop_inn" value="{{ $shop_data->inn }}" required>
                                                    @endif
                                            </label>
                                        </div>
                                        <div class="medium-12 columns input-column">
                                            <label>КПП<span class="red-star">*</span>
                                                <input type="text" placeholder=""  maxlength="9" name="kpp" value="{{ $shop_data->kpp }}" required>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="medium-6 columns form-column-right">
                                        <div class="medium-12 columns input-column">
                                            <label>ОГРН<span class="red-star">*</span>
                                                @if(isset($shop_data->ogrn))
                                                    <input disabled type="text" placeholder="" maxlength="13" name="ogrn" value="{{ $shop_data->ogrn }}">

                                                @else
                                                <input type="text" placeholder="" maxlength="13" name="ogrn" value="{{ $shop_data->ogrn }}" required>
                                                    @endif
                                            </label>
                                        </div>
                                    </div>
                                        <br>
                                </div>
                                <div class="medium-12 columns modal-footer">
                                    <div class="medium-12 columns">
                                        <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                        <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                        <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="medium-12 columns dashed-block">
                        <h2>Адреса</h2>
                        @foreach($addresses as $address)
                            <span class="info">

                            </span>
                                <table class="unstriped personal-data-table" id="user-info">
                                    <tr>
                                        <td>
                                            <span>
                                            @if($address->type == 1)
                                                Юридический
                                            @elseif($address->type == 2)
                                                Почтовый
                                            @else
                                                Не указан
                                            @endif
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td>{{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}</td>
                                    </tr>
                                </table>
                            <br>
                        @endforeach

                            @if(count($addresses) > 0)
                                <a class="address-add" data-open="address-edit">РЕДАКТИРОВАТЬ</a>
                            @else
                                <a class="address-add" data-open="address-add">ДОБАВИТЬ АДРЕС</a>
                            @endif

                        <div class="reveal custom-medium-reveal" id="address-add" data-reveal>
                            <form action="/shop_address_add" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">АДРЕС</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4>Юридический адрес</h4>
                                    <input type="hidden"  name="address_data[1][type]" value="1">
                                    <div class="medium-12 columns">
                                        <label>Страна<span class="red-star">*</span>
                                            <input type="text" placeholder="Ваша страна" name="country" required>
                                        </label>
                                        <label>Населенный пункт/Город<span class="red-star">*</span>
                                            <input id="dadata_city" type="text" placeholder="Ваш город" name="address_data[1][city]" required>
                                        </label>
                                        <label>Улица<span class="red-star">*</span>
                                            <input id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[1][street]" required>
                                        </label>
                                    </div>
                                    <div class="medium-4 columns">
                                        <label>Дом<span class="red-star">*</span>
                                            <input id="dadata_house" type="text" placeholder="Номер вашего дома" name="address_data[1][house]" required>
                                        </label>
                                    </div>
                                    <div class="medium-4 columns">
                                        <label>Квартира<span class="red-star"></span>
                                            <input id="dadata_flat" type="text" placeholder="Номер вашей квартиры" name="address_data[1][apartment]" >
                                        </label>
                                    </div>
                                    <div class="medium-4 columns">
                                        <label>Почтовый индекс<span class="red-star">*</span>
                                            <input id="dadata_postal_code" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="address_data[1][postcode]" required>
                                        </label>
                                    </div>
                                    <div id="post-address-block">
                                        <div class="medium-12 columns">
                                            <h4>Почтовый адрес</h4>
                                            <input class="big-checkbox left" id="post_address_equal" type="checkbox" name="post_address_equal" value="true" checked @change="showPostAddressInputs = !showPostAddressInputs">
                                            <label for="post_address_equal">Почтовый адрес совпадает с юритическим</label>
                                        </div>
                                        <postaddressinputs v-if="showPostAddressInputs"></postaddressinputs>
                                    </div>
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="reveal custom-medium-reveal" id="address-edit" data-reveal>
                            <form action="/shop_address_edit" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">РЕДАКТИРОВАНИЕ АДРЕСА</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4>Юридический адрес</h4>
                                    @foreach($addresses as $address)
                                        @if($address->type == 1)
                                            <input type="hidden"  name="address_data[1][type]" value="{{$address->type}}">
                                            <div class="medium-12 columns">
                                                <label>Страна<span class="red-star">*</span>
                                                    <input type="text" placeholder="Ваша страна" name="address_data[1][country]" required value="{{$address->country}}">
                                                </label>
                                                <label>Населенный пункт/Город<span class="red-star">*</span>
                                                    <input id="dadata_city" type="text" placeholder="Ваш город" name="address_data[1][city]" required value="{{$address->city}}">
                                                </label>
                                                <label>Улица<span class="red-star">*</span>
                                                    <input id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[1][street]" required value="{{$address->street}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Дом<span class="red-star">*</span>
                                                    <input id="dadata_house" type="text" placeholder="Номер вашего дома" name="address_data[1][house]" required value="{{$address->house}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Квартира<span class="red-star"></span>
                                                    <input id="dadata_flat" type="text" placeholder="Номер вашей квартиры" name="address_data[1][apartment]" value="{{$address->apartment}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Почтовый индекс<span class="red-star">*</span>
                                                    <input id="dadata_postal_code" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="address_data[1][post_index]" required value="{{$address->post_index}}">
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach

                                        <div class="medium-12 columns">
                                            <h4>Почтовый адрес</h4>
                                        </div>
                                    @foreach($addresses as $address)
                                        @if($address->type == 2)
                                            <input type="hidden"  name="address_data[2][type]" value="{{$address->type}}">
                                            <div class="medium-12 columns">
                                                <label>Страна<span class="red-star">*</span>
                                                    <input type="text" placeholder="Ваша страна" name="address_data[2][country]" required value="{{$address->country}}">
                                                </label>
                                                <label>Населенный пункт/Город<span class="red-star">*</span>
                                                    <input id="dadata_city" type="text" placeholder="Ваш город" name="address_data[2][city]" required value="{{$address->city}}">
                                                </label>
                                                <label>Улица<span class="red-star">*</span>
                                                    <input id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[2][street]" required value="{{$address->street}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Дом<span class="red-star">*</span>
                                                    <input id="dadata_house" type="text" placeholder="Номер вашего дома" name="address_data[2][house]" required value="{{$address->house}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Квартира<span class="red-star"></span>
                                                    <input id="dadata_flat" type="text" placeholder="Номер вашей квартиры" name="address_data[2][apartment]" value="{{$address->apartment}}">
                                                </label>
                                            </div>
                                            <div class="medium-4 columns">
                                                <label>Почтовый индекс<span class="red-star">*</span>
                                                    <input id="dadata_postal_code" type="text" maxlength="6" placeholder="Ваш почтовый индекс" name="address_data[2][post_index]" required value="{{$address->post_index}}">
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="large-6 medium-12 small-12 columns dashed-divs">
                    <div class="medium-12 columns dashed-block">
                        <h2>Номер телефона</h2>
                        <span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
                        <span class="number">
                            {{ $shop_data->phone }}
                        </span>
                        <a class="flash-link" data-open="telefone-edit">Изменить номер телефона</a>
                        <div class="reveal custom-medium-reveal" id="telefone-edit" data-reveal>
                            <form action="/shop_phone_update" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">ТЕЛЕФОН</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
                                        <input type="text" id="checktel" placeholder="Ваш телефон" name="shop_phone" value="{{ $shop_data->phone }}" required>
                                    </label>
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="medium-12 columns dashed-block">
                        <h2>Адрес электронной почты</h2>
                        <span class="e-mail">{{ $shop_data->e_mail }}</span>
                    </div>
                    <div class="medium-12 columns dashed-block">
                        <h2>Банковские реквизиты</h2>
                        @foreach($bank_data as $row)
                            <table class="unstriped personal-data-table" id="user-info">
                                <tr>
                                    <td><span>
                                            @if($row->type == 1)
                                                Для расчета с физ.лицами
                                            @elseif($row->type == 2)
                                                Для расчета с ООО "ДЖИННМАРТ"
                                            @else
                                                Не указан
                                            @endif
                                            </span></td>
                                </tr>
                                <tr>
                                    <td><span>Наименование банка:</span></td>
                                    <td><span>{{ $row->bank_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Адрес банка:</span></td>
                                    <td><span>{{ $row->bank_address }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>БИК:</span></td>
                                    <td><span>{{ $row->bank_bik }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Кор. счет:</span></td>
                                    <td><span>{{ $row->kor_account }}</span></td>
                                </tr>
                                
                                <tr>
                                    <td><span>Счет:</span></td>
                                    <td><span>{{ $row->account }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Получатель:</span></td>
                                    <td><span>{{ $row->recipient }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Назначение платежа:</span></td>
                                    <td><span>{{ $row->purpose }}</span></td>
                                </tr>
                            </table>
                            <br>
                        @endforeach
                        @if(count($bank_data) > 0)
                            <a class="address-add" data-open="bank-data-edit">РЕДАКТИРОВАТЬ</a>
                        @else
                            <a class="address-add" data-open="bank-data-add">ДОБАВИТЬ СЧЕТ</a>
                        @endif
                        <div class="reveal custom-medium-reveal" id="bank-data-add" data-reveal>
                            <form action="/shop_bank_data_add" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">СЧЕТ</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="medium-12 columns">
                                        <h4>Для расчеста с физ.лицами</h4>
                                        <input type="hidden"  name="bank_data[1][type]" value="1">
                                    </div>
                                    <div class="medium-12 columns">
                                        <label>БИК (для поиска дадаты)<span class="red-star">*</span>
                                            <input id="bank_search" class="not-letter" type="text" placeholder="БИК" maxlength="9" requiered>
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>Сокращенное наименование банка
                                            <input id="short_name" type="text" placeholder="Сокращенное наименование банка" name="bank_data[1][bank_name]">
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>Полное наименование банка<span class="red-star">*</span>
                                            <input id="full_name" type="text" placeholder="Наименование банка" name="bank_data[1][bank_full_name]" required>
                                        </label>
                                    </div>
                                    <div class="medium-12 columns">
                                        <label>Адрес банка<span class="red-star">*</span>
                                            <input id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[1][bank_address]" required>
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>БИК<span class="red-star">*</span>
                                            <input id="bank_bik" class="not-letter" type="text" placeholder="БИК" maxlength="9" name="bank_data[1][bank_bik]" required>
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>Кор. счет<span class="red-star">*</span>
                                            <input id="kor_account" class="not-letter" type="text" placeholder="Кор. счет" maxlength="20" name="bank_data[1][kor_account]" required>
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>Счет<span class="red-star">*</span>
                                            <input id="" class="not-letter" type="text" maxlength="20" placeholder="Счет" name="bank_data[1][account]" required>
                                        </label>
                                    </div>
                                    <div class="medium-6 columns">
                                        <label>Получатель<span class="red-star">*</span>
                                            <input id="firm_recipient" class="not-letter" type="text" maxlength="20" placeholder="Получатель" name="bank_data[1][recipient]" required>
                                        </label>
                                    </div>
                                    <div id="bank-inputs-block">
                                        <div class="medium-12 columns">
                                            <h4>Для расчетов с ООО "ДЖИНМАРТ"</h4>
                                            <input class="big-checkbox left" id="bank_data_equal" type="checkbox" name="bank_data_equal" value="true" checked @change="showBankAccountInputs = !showBankAccountInputs">
                                            <label for="bank_data_equal">Счет совпадает с счетом для физ.лиц</label>
                                        </div>
                                        <bankaccountinputs v-if="showBankAccountInputs"></bankaccountinputs>
                                    </div>
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="reveal custom-medium-reveal" id="bank-data-edit" data-reveal>
                            <form action="/shop_bank_data_edit" method="post" data-abide novalidate data-live-validate="true">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">РЕДАКТИРОВАНИЕ СЧЕТОВ</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    @foreach($bank_data as $row)
                                        @if($row->type == 1)
                                            <div class="medium-12 columns">
                                                <h4>Для расчеста с физ.лицами</h4>
                                                <input type="hidden"  name="bank_data[1][type]" value="1">
                                            </div>
                                            <div class="medium-12 columns">
                                                <label>БИК (для поиска дадаты)<span class="red-star">*</span>
                                                    <input id="bank_search" class="not-letter" type="text" placeholder="БИК" maxlength="9" value="{{$row->bank_bik}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>Наименование банка<span class="red-star">*</span>
                                                    <input id="short_name" type="text" placeholder="Наименование банка" name="bank_data[1][bank_name]" required value="{{$row->bank_name}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>Полное наименование банка<span class="red-star">*</span>
                                                    <input id="full_name" type="text" placeholder="Наименование банка" name="bank_data[1][bank_full_name]" required value="{{$row->bank_full_name}}">
                                                </label>
                                            </div>
                                            <div class="medium-12 columns">
                                                <label>Адрес банка<span class="red-star">*</span>
                                                    <input id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[1][bank_address]" required value="{{$row->bank_address}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>БИК<span class="red-star">*</span>
                                                    <input id="bank_bik" class="not-letter" type="text" placeholder="БИК" maxlength="9" name="bank_data[1][bank_bik]" required value="{{$row->bank_bik}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>Кор. счет<span class="red-star">*</span>
                                                    <input id="kor_account" class="not-letter" type="text" placeholder="Кор. счет" maxlength="20" name="bank_data[1][kor_account]" required value="{{$row->kor_account}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>Счет<span class="red-star">*</span>
                                                    <input id="" class="not-letter" type="text" maxlength="20" placeholder="Счет" name="bank_data[1][account]" required value="{{$row->account}}">
                                                </label>
                                            </div>
                                            <div class="medium-6 columns">
                                                <label>Получатель<span class="red-star">*</span>
                                                    <input id="" class="not-letter" type="text" maxlength="20" placeholder="Получатель" name="bank_data[1][recipient]" required value="{{$row->recipient}}">
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                    <div id="bank-inputs-block">
                                        @foreach($bank_data as $row)
                                            @if($row->type == 2)
                                                <div class="medium-12 columns">
                                                    <h4>Для расчетов с ООО "ДЖИНМАРТ"</h4>
                                                    <input type="hidden"  name="bank_data[2][type]" value="2">
                                                </div>
                                                <div class="medium-12 columns">
                                                    <label>БИК (для поиска дадаты)<span class="red-star">*</span>
                                                        <input id="bank_search" class="not-letter" type="text" placeholder="БИК" maxlength="9" value="{{$row->bank_bik}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>Наименование банка<span class="red-star">*</span>
                                                        <input id="short_name" type="text" placeholder="Наименование банка" name="bank_data[2][bank_name]" required value="{{$row->bank_name}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>Полное наименование банка<span class="red-star">*</span>
                                                        <input id="full_name" type="text" placeholder="Наименование банка" name="bank_data[2][bank_full_name]" required value="{{$row->bank_full_name}}">
                                                    </label>
                                                </div>
                                                <div class="medium-12 columns">
                                                    <label>Адрес банка<span class="red-star">*</span>
                                                        <input id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[2][bank_address]" required value="{{$row->bank_address}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>БИК<span class="red-star">*</span>
                                                        <input id="bank_bik" class="not-letter" type="text" placeholder="БИК" maxlength="9" name="bank_data[2][bank_bik]" required value="{{$row->bank_bik}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>Кор. счет<span class="red-star">*</span>
                                                        <input id="kor_account" class="not-letter" type="text" placeholder="Кор. счет" maxlength="20" name="bank_data[2][kor_account]" required value="{{$row->kor_account}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>Счет<span class="red-star">*</span>
                                                        <input id="" class="not-letter" type="text" maxlength="20" placeholder="Счет" name="bank_data[2][account]" required value="{{$row->account}}">
                                                    </label>
                                                </div>
                                                <div class="medium-6 columns">
                                                    <label>Получатель<span class="red-star">*</span>
                                                        <input id="" class="not-letter" type="text" maxlength="20" placeholder="Получатель" name="bank_data[2][recipient]" required value="{{$row->recipient}}">
                                                    </label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="medium-12 columns dashed-block">
                        <h2>Способы оплаты</h2>
                        @foreach($payment_data as $row)
                            <span>
                                @if($row['type'] == 1)
                                    Наличные при получении
                                @elseif($row['type'] == 2)
                                    Карта при получении
                                @elseif($row['type'] == 3)
                                    Карта(перевод)
                                @elseif($row['type'] == 4)
                                    Банковский перевод
                                @elseif($row['type'] == 5)
                                    Электронная коммерция
                                @else
                                    Не указан
                                @endif
                            </span>
                            <table class="unstriped personal-data-table" id="user-info">
                                    @if($row['type'] == 1)
                                        <tr>
                                            <td><span>Описание:</span></td>
                                            <td><span>{{ $row['description'] }}</span></td>
                                        </tr>
                                    @elseif($row['type'] == 2)
                                        <tr>
                                            <td><span>Описание:</span></td>
                                            <td><span>{{ $row['description'] }}</span></td>
                                        </tr>
                                    @elseif($row['type'] == 3)
                                        <tr>
                                            <td><span>Банк:</span></td>
                                            <td><span>{{ $row['name'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>№ карты:</span></td>
                                            <td><span>{{ $row['account'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Описание:</span></td>
                                            <td><span>{{ $row['description'] }}</span></td>
                                        </tr>
                                    @elseif($row['type'] == 4)
                                        <tr>
                                            <td><span>Наименование банка:</span></td>
                                            <td><span>{{ $row['bank_data']['bank_name'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Адрес банка:</span></td>
                                            <td><span>{{ $row['bank_data']['bank_address'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>БИК:</span></td>
                                            <td><span>{{ $row['bank_data']['bank_bik'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Кор. счет:</span></td>
                                            <td><span>{{ $row['bank_data']['kor_account'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>ИНН:</span></td>
                                            <td><span>{{ $row['bank_data']['inn'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>КПП:</span></td>
                                            <td><span>{{ $row['bank_data']['kpp'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>ОКТМО:</span></td>
                                            <td><span>{{ $row['bank_data']['oktmo'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>КБК:</span></td>
                                            <td><span>{{ $row['bank_data']['kbk'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Счет:</span></td>
                                            <td><span>{{ $row['bank_data']['account'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Получатель:</span></td>
                                            <td><span>{{ $row['bank_data']['recipient'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Назначение платежа:</span></td>
                                            <td><span>{{ $row['bank_data']['purpose'] }}</span></td>
                                        </tr>
                                    @elseif($row['type'] == 5)
                                        <tr>
                                            <td><span>Сервис:</span></td>
                                            <td><span>{{ $row['name'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Счет:</span></td>
                                            <td><span>{{ $row['account'] }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><span>Описание:</span></td>
                                            <td><span>{{ $row['description'] }}</span></td>
                                        </tr>
                                    @else
                                        Не указан
                                    @endif

                            </table>
                            <br>
                        @endforeach
                        <a class="address-add payment_add_btn" data-open="payment-add">ДОБАВИТЬ ОПЛАТУ</a>
                        <div class="reveal custom-medium-reveal" id="payment-add" data-reveal>
                            <form action="/payment_type_add" method="post" data-abide novalidate="novalidate">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="medium-12 columns">
                                    <h1 class="modal-h-3">СПОСОБ ОПЛАТЫ</h1>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <p>Способов оплаты может быть несколько. Добавляются по одному.</p>
                                    <div class="medium-6 columns">
                                        <label>Тип<span class="red-star">*</span>
                                            <select class="payment_type_select" name="type" required>
                                                <option value="null" selected disabled>Выберите тип</option>
                                                <option value="1">Наличные при получении</option>
                                                <option value="2">Карта при получении</option>
                                                <option value="3">Карта(перевод)</option>
                                                <option value="4">Банковский перевод</option>
                                                <option value="5">Электронная коммерция</option>
                                            </select>
                                        </label>
                                        <label>Пример
                                            <input type="text" name="tex" required>
                                        </label>
                                    </div>
                                    <div id="payment_modal_content"></div>
                                    <div class="medium-12 columns modal-footer">
                                        <div class="medium-12 columns">
                                            <button class="common-button button-accept submit-button" type="submit">СОХРАНИТЬ</button>
                                            <button class="common-button button-decline" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>
                                            <span class="info"><span class="red-star">*</span>Обязательные для заполнения поля</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#bank_search").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);

                var data = suggestion.data;
                var all_name_shop = $("#name_f").val();
                if (!data)
                    return;
                $("#short_name").val(data.name.short);
                $("#full_name").val(data.name.full);
                $("#bank_bik").val(data.bic);
                $("#kor_account").val(data.correspondent_account);
                $("#bank_address").val(data.address.value);
                $("#firm_recipient").val(all_name_shop);

            }
        });

             /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('not-letter').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
