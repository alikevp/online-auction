@extends('layouts.master')

@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Регистрация
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">
            @include('partials.profile_sidebar')
            <div class="large-7 medium-12 small-12 columns form-column">
                @include('errors.validation')
                <h1>Регистрация</h1>

                <form action="{{ url('/register/register_shop') }}" method="POST"data-abide novalidate data-live-validate="true">

                    {{ csrf_field() }}

                    <h3>Магазин</h3>
                    <div class="row">
                        <div class="medium-12 columns input-column">
                            <label>Название или ИНН<span class="red-star">*</span>
                                <input id="dadata_party" type="text" placeholder="" name="party"  required>
                            </label>
                        </div>
                    </div>
                    <div class="medium-6 columns form-column-left">


                        <div class="medium-12 columns input-column">
                            <label>Юридическое название<span class="red-star">*</span>
                                <input id="name_short"  type="hidden" name="le_name">
                                <input id="name_short_hidden" disabled type="text" placeholder="" name="" required>

                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Полное юридическое название<span class="red-star">*</span>
                                <input id="name_full"  type="hidden" name="full_le_name">
                                <input id="name_full_hidden" disabled type="text" placeholder="" name="" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Адресс<span class="red-star">*</span>
                                <input id="name_address" type="text" placeholder="" name="shop_address" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>ИНН<span class="red-star">*</span>
                                <input id="name_inn"  type="hidden" name="shop_inn">
                                <input id="name_inn_hidden" disabled type="text" placeholder="" name="" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Дата регистрации<span class="red-star">*</span>
                                <input id="date_registered"  type="hidden" name="shop_registered">
                                <input id="date_registered_hidden"  type="text" placeholder="" name="" required>
                            </label>
                        </div>

                    </div>
                    <div class="medium-6 columns form-column-right">
                        <div class="medium-12 columns input-column">
                            <label>ФИО директора<span class="red-star">*</span>
                                <input id="director_fio" type="text" placeholder="" name="director_fio" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>E-mail<span class="red-star">*</span>
                                <input type="text" placeholder="" name="shop_email" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Телефон<span class="red-star">*</span>
                                <input type="text" placeholder="" maxlength="15" name="shop_phone" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>КПП<span class="red-star">*</span>
                                <input id="name_kpp"  type="text" maxlength="9" placeholder="" name="kpp" required>
                            </label>
                        </div>
                    </div>
                    <div class="medium-6 columns form-column-left">

                    </div>

                    <div class="medium-6 columns form-column-right">

                        <div class="medium-12 columns input-column">
                            <label>ОГРН<span class="red-star">*</span>
                                <input id="name_ogrn"  type="hidden" name="ogrn">
                                <input id="name_ogrn_hidden" disabled type="text" placeholder="" name="ogrn" required>
                            </label>
                        </div>
                    </div>

                    <h3>Основной администратор</h3>
                    <div class="medium-6 columns form-column-left">
                        <div class="medium-12 columns input-column">
                            <label>E-mail<span class="red-star">*</span>
                                <input type="email" placeholder="" name="email" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Пароль<span class="red-star">*</span>
                                <input type="password" placeholder="" name="password" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Повторите пароль<span class="red-star">*</span>
                                <input type="password" name="password_confirmation" required>
                            </label>
                        </div>
                    </div>
                    <div class="medium-6 columns form-column-right">
                        <div class="medium-12 columns input-column">
                            <label>Имя<span class="red-star">*</span>
                                <input type="text" placeholder="" name="name" required>
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Фамилия
                                <input type="text" placeholder="" name="surname" pattern="^[A-Za-zА-Яа-яЁё0-9$/\s]+$">
                            </label>
                        </div>
                        <div class="medium-12 columns input-column">
                            <label>Телефон<span class="red-star">*</span>
                                <input type="text" placeholder="" maxlength="15" name="telefone" required>
                            </label>
                        </div>
                        <div class="medium-12 columns date-column">
                            <label>Дата рождения<span class="red-star">*</span> <br></label>
                            <div class="row collapse date" id="" data-date-format="dd-MM-yyyy" data-start-view="year" data-min-view="month_rus">
                                <div class="medium-1 columns">
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="medium-11 columns">
                                    <input size="16" id="dpMonths" type="text" name="date_of_birth" value="" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="medium-12 columns sex-columns">
                        <div class="medium-12 columns sex">
                            <div class="medium-1 columns sex-columns-label">
                               <span>
                                   Пол<span class="red-star">*</span>
                               </span>
                            </div>
                            <div class="medium-5 columns">
                                <input id="sex1" type="radio" name="sex" value="m" checked="checked"><label for="sex1">Мужской</label>
                            </div>
                            <div class="medium-5 columns">
                                <input id="sex2" type="radio" name="sex" value="f"><label for="sex2">Женский</label>
                            </div>
                        </div>
                    </div>

                    <div class="medium-12 columns reg-columns">
                        <div class="medium-11 medium-centered columns">
                            <div class="medium-6 columns">
                                <button type="submit" class="submit-button">Зарегистрироваться</button>
                            </div>
                            <div class="medium-6 columns reg-notice-column">
                                <span class="reg-notice">
                                    Обязательные поля помечены символом <span class="red-star">*</span>
                                </span>
                                <br>
                                <span class="reg-notice">
                                    Нажимая "ЗАРЕГИСТРИРОВАТЬСЯ", Вы соглашаетесь с <a href="{{ url('public_offer') }}" class="flash-link">Публичной офертой</a>,
                                    <a href="{{ url('page/personal-data-agreement') }}" class="flash-link">Согласие на обработку персональных данный.</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="large-3 medium-12 hide-for-small-only columns">
                <div class="medium-12 columns reg-info">
                    Указывайте только реальную информацию,
                    т.к. она будет использована для проверки!
                    <br><br>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>

    <script type="text/javascript">

        function join(arr) {
            var separator = arguments.length > 1 ? arguments[1] : ", ";
            return arr.filter(function (n) {
                return n
            }).join(separator);
        }

        function showSuggestion(suggestion) {
            console.log(suggestion);
            var data = suggestion.data;
            if(data.name)
            $('#name_short').val(join([data.opf && data.opf.short || "", data.name.short || data.name.full]," "));
            $('#name_short_hidden').val(join([data.opf && data.opf.short || "", data.name.short || data.name.full]," "));
            $('#name_full').val(join([data.opf && data.opf.short || "", data.name.full_with_opf]," "));
            $('#name_full_hidden').val(join([data.opf && data.opf.short || "", data.name.full_with_opf]," "));
            $('#name_inn').val(join([data.inn]));
            $('#name_inn_hidden').val(join([data.inn]));
            $('#name_ogrn').val(join([data.ogrn]));
            $('#name_ogrn_hidden').val(join([data.ogrn]));
            $('#name_kpp').val(join([data.kpp]));
            $('#director_fio').val(join([data.management.name]));
            $('#name_address').val(join([data.address.unrestricted_value]));
            var ticks = data.state.registration_date;
            var registered_date = new Date(ticks);
            var yy = registered_date.getFullYear();
            var mm = registered_date.getMonth()+ 1;
            if (mm < 10) mm = '0' + mm;
            var dd = registered_date.getDate();
            if (dd < 10) dd = '0' + dd;
            var formated_date = dd + '.' + mm + '.' + yy;
            var formated_date_sql = yy + '-' + mm + '-' + dd + ' 00:00:00';
            $('#date_registered').val(formated_date_sql);
            $('#date_registered_hidden').val(formated_date);
        }

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: showSuggestion
        });




    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection