@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Регистрация
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.profile_sidebar')

            <div class="large-10 medium-12 small-12 columns form-column">
                <h1>Регистрация</h1>
                <div class="callout success">
                    <p>
                        <h4>Для завершения регистрации необходимо подтвердить аккаунт. </h4>
                        На указанный Вами адрес было отправлено письмо. <br>
                        Перейдите по ссылке из этого письма для подтверждения аккаунта.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection