@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
@endsection


<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        @include('partials.profile_sidebar')
        <div class="medium-7 columns form-column">
            <h3>Восстановление пароля</h3>
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}" data-abide novalidate data-live-validate="true">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Для восстановления пароля, введите ваш E-mail:</label>
                        <input id="email" type="email" placeholder="Ваш E-mail" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="button common-button submit-button">
                        Отправить ссылку для восстановления
                    </button>
                </form>
            </div>
        </div>
        <div class="medium-3 columns">
            <div class="medium-12 columns reg-info">
                На указанный e-mail будет выслано письмо с дальнейшими инструкциями.
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection