@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-profile-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Профиль
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @if(Auth::user()->type == 'shop_admin')
                    @include('partials.sidebarShopWrapper')
                    @include('partials.profile.content_for_shopadmins')
            @else
                    @include('partials.profile_sidebar')
                    @include('partials.profile.content_for_buyers')
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">


        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

             /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
