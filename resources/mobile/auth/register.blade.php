@extends('layouts.master')

@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Регистрация
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">
            @include('partials.profile_sidebar')
            <div class="large-7 medium-12 small-12 columns form-column">
                @include('errors.validation')
                <h1>Регистрация</h1>
                <form action="{{ url('/register') }}" method="POST" data-abide novalidate data-live-validate="true">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">E-mail<span class="red-star">*</span></label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <input type="email" placeholder="" name="email" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">Пароль<span class="red-star">*</span></label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <input type="password" placeholder="" name="password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">Повторите пароль<span class="red-star">*</span></label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <input type="password" name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">Имя<span class="red-star">*</span></label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <input type="text" placeholder="" name="name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">Фамилия</label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <input type="text" placeholder="" name="surname" pattern="^[A-Za-zА-Яа-яЁё0-9$/\s]+$">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-3 columns input-column">
                            <label class="text-left middle">Дата рождения<span class="red-star">*</span> <br></label>
                        </div>
                        <div class="medium-9 columns input-column">
                            <div class="row collapse date" id="" data-date-format="dd-MM-yyyy" data-start-view="year" data-min-view="month_rus">
                                <div class="medium-1 columns">
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="medium-11 columns">
                                    <input size="16" id="dpMonths" type="text" name="date_of_birth" value="" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="medium-12 columns sex-columns">
                        <div class="medium-12 columns sex">
                            <div class="medium-1 columns sex-columns-label">
                               <span>
                                   Пол<span class="red-star">*</span>
                               </span>
                            </div>
                            <div class="medium-5 columns">
                                <input id="sex1" type="radio" name="sex" value="m" checked="checked"><label for="sex1">Мужской</label>
                            </div>
                            <div class="medium-5 columns">
                                <input id="sex2" type="radio" name="sex" value="f"><label for="sex2">Женский</label>
                            </div>
                        </div>
                    </div>
                    @if(isset($referred_by))
                        <input type="hidden" name="referred_by" value="{{ $referred_by }}">
                    @endif
                    <div class="medium-12 columns reg-columns">
                        <div class="medium-11 medium-centered columns">
                            <div class="medium-6 columns">
                                <button type="submit" class="submit-button">Зарегистрироваться</button>
                            </div>
                            <div class="medium-6 columns reg-notice-column">
                                <span class="reg-notice">
                                    Обязательные поля помечены символом <span class="red-star">*</span>
                                </span>
                                <br>
                                <span class="reg-notice">
                                    Нажимая "ЗАРЕГИСТРИРОВАТЬСЯ", Вы соглашаетесь с <a href="{{ url('public_offer') }}" class="flash-link">Публичной офертой</a>,
                                    <a href="{{ url('page/personal-data-agreement') }}" class="flash-link">Согласие на обработку персональных данный.</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="large-3 medium-12 hide-for-small-only columns">
                <div class="medium-12 columns reg-info">
                    Указывайте только реальную информацию,
                    т.к. она будет использована для проверки!
                    <br><br>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
   <script src="{{ asset('js/regDate.js') }}"></script>
   <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection