@extends('layouts.master')

@section('style')
    <link href="{{asset('owlcarousel')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Подтверждение регистрации
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">
            @include('partials.profile_sidebar')
            <div class="large-7 medium-12 small-12 columns allert-column">
                <h1>Подтверждение регистрации</h1>
                <div class="success callout" data-closable="slide-out-right">
                    <p>
                        Поздравляем! Вы успешно зарегистрировались на нашем сайте. <br>
                        Теперь вы можете авторизоваться с новым логином и паролем.
                    </p>
                    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert callout" data-closable>
                    <h5>This is Important!</h5>
                    <p>But when you're done reading it, click the close button in the corner to dismiss this alert.</p>
                    <p>I'm using the default <code>data-closable</code> parameters, and simply fade out.</p>
                    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection