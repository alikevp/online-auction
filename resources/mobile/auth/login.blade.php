@extends('layouts.master')

@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Авторизация
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.profile_sidebar')

            <div class="large-10 medium-12 small-12 columns form-column">

                @include('errors.validation')
                <form action="{{ url('/login') }}" method="POST" data-abide novalidate data-live-validate="true" >
                    {{ csrf_field() }}
                    <div class="large-7 medium-12 small-12 float-left columns form-column-left">
                        <h1>Авторизация</h1>
                        <div class="small-12 columns">
                            <div class="row">
                                <div class="medium-3 columns input-column">
                                    <label class="text-left middle">E-mail<span class="red-star">*</span></label>
                                </div>
                                <div class="medium-9 columns input-column">
                                    <input type="email" placeholder="" name="email" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="medium-3 columns input-column">
                                    <label class="text-left middle">Пароль<span class="red-star">*</span></label>
                                </div>
                                <div class="medium-9 columns input-column">
                                    <input type="password" placeholder="" name="password" required>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="submit-button login-enter">Войти</button>
                        <div class="medium-6 small-6 columns text-left"><a class = "flash-link" href="/register">Регистрация</a></div>
                        <div class="medium-6 small-6 columns text-right"><a class = "flash-link" href="/password/reset">Забыли пароль?</a></div>
                    </div>
                    <div class="large-4 medium-12 hide-for-small-only columns">
                        <div class="medium-12 columns reg-info login-info">
                            После авторизации вы сможете воспользоваться всеми сервисами сайта в полном объеме.
                        </div>

                    </div>
                </form>


            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
