@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Авторизация
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.profile_sidebar')

            <div class="large-10 medium-12 small-12 columns form-column">
                <h1>Авторизация</h1>
                <div class="callout success" data-closable="slide-out-right">
                    <p>
                        Поздравляем ваш аккаунт успешно подтвержден. Теперь вы можете авторизоваться с данными, введенными при регистрации.
                    </p>
                    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @include('errors.validation')
                <form action="{{ url('/login') }}" method="POST" data-abide novalidate>
                    {{ csrf_field() }}
                    <div class="medium-7 float-left columns form-column-left">
                        <h1>Авторизация</h1>
                        <div class="row">
                            <div class="medium-3 columns input-column">
                                <label class="text-left middle">E-mail<span class="red-star">*</span></label>
                            </div>
                            <div class="medium-9 columns input-column">
                                <input type="email" placeholder="" name="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-3 columns input-column">
                                <label class="text-left middle">Пароль<span class="red-star">*</span></label>
                            </div>
                            <div class="medium-9 columns input-column">
                                <input type="password" placeholder="" name="password" required>
                            </div>
                        </div>
                        <button type="submit" class="submit-button login-enter">Войти</button>
                        <div class="medium-6 columns text-left"><a class = "flash-link" href="/register">Регистрация</a></div>
                        <div class="medium-6 columns text-right"><a class = "flash-link" href="/password/reset">Забыли пароль?</a></div>
                    </div>
                    <div class="medium-4 columns">
                        <div class="medium-12 columns reg-info login-info">
                            После авторизации вы сможете воспользоваться всеми сервисами сайта в полном объеме.
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection