@component('mail::message')

<p>Уважаемый {{ $user->name }}!</p>
<br>
Вы успешно зарегистрировались на нашем сайте и для подтверждения регистрации необходимо подтвердить ваш email

@component('mail::button', ['url' => $url])
Подтвердить email
@endcomponent


С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
