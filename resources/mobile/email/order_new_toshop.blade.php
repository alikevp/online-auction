<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Title</title>
</head>
<body leftmargin="0" marginheight="0" marginwidth="0" style="background-color: #f5f5f5; width: 100%;" topmargin="0">
<table cellpadding="0" cellspacing="0" style="background-color: white; border: 1px solid #d7d7d7; border-radius: 8px; margin-left: auto; margin-right: auto; margin-top: 10px; table-layout: fixed;" wborder="0" width="640px">
    <tr>
        <td style="text-align: center;">
            <p style="font-size: 10px; font-weight: bold;"><a href="#" style="color: #636363; text-decoration: none;"> Письмо отображается некорректно? </a></p>
            <hr color="#cd4daa" noshade size="2" /><a href="http://mag.amediatest.ru/" target="_blank"> <img alt="logo" src="http://mag.amediatest.ru/images/logo.png" style="margin: 10px;" /> </a></td>
    </tr>
    <tr style="background-color: #d7d7d7; height: 90px;">
        <td>
            <table border="0" cellpadding="0" cellspacing="0" height="75px" width="100%">
                <tr>
                    <td style="border-right: 1px solid #cd4daa; font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Телефоны</a></td>
                    <td style="border-right: 1px solid #cd4daa; font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Ноутбуки, планшеты, компьютеры</a></td>
                    <td style="border-right: 1px solid #cd4daa; font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Телевизоры и видео</a></td>
                    <td style="border-right: 1px solid #cd4daa; font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Малая бытовая техника</a></td>
                    <td style="border-right: 1px solid #cd4daa; font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Крупная бытовая техника</a></td>
                    <td style="font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Фото и видео</a></td>
                    <td style="font-weight: bold; padding: 5px; text-align: center;"><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;">Другие категории</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 20px; padding-left: 30px; padding-right: 30px; padding-top: 20px;">
            <p style="font-weight: bold;"> Уважаемый человек!<br />
                <br /> Вы успешно зарегистрировались на нашем сайте и для подтверждения регистрации необходимо перейти по ссылке: </p><a href="#" style="background-color: #ce43da; color: white; display: block; font-size: 20px; height: 60px; line-height: 60px; text-align: center; text-decoration: none; width: 305px;"> Подтвердить регистрацию </a></td>
    </tr>
    <tr>
        <td>
            <hr color="#cd4daa" noshade size="2" />
        </td>
    </tr>
    <tr>
        <td style="padding: 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Телевизоры и цифровое ТВ</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Ноутбуки</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Планшеты</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Apple</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Смартфоны и сотовые телефоны</p>
                                    </td>
                                </tr>
                            </table></a></td>
                </tr>
                <tr>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Электронные книги</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Фото и видео</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Аудио</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Красота и здоровье</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Малая бытовая техника</p>
                                    </td>
                                </tr>
                            </table></a></td>
                </tr>
                <tr>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Крупная бытовая техника</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Автотехника</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Игры и развлечения</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Компьютерные программы</p>
                                    </td>
                                </tr>
                            </table></a></td>
                    <td><a class="category-link" href="#" style="color: black; font-size: 14px; text-decoration: none;"><table border="0" cellpadding="0" cellspacing="0" width="120px">
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;"><img alt="category" src="https://placehold.it/100x100" /></td>
                                </tr>
                                <tr>
                                    <td class="category-bottom-td" style="text-align: center;">
                                        <p style="color: #333333; font-size: 12px; font-weight: bold; height: 50px;">Аксессуары</p>
                                    </td>
                                </tr>
                            </table></a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p style="color: #c4c4c4; font-size: 12px; font-weight: bold; text-align: center;"> Вы можете отписаться и не получать информацию об акциях, распродажах и спецпредложениях.<br />Для этого <a href="#" style="color: #c4c4c4; font-size: 12px; text-align: center;">перейдите по ссылке.</a><br />Данное сообщение отправлено автоматически и не требует ответа.<br />Цены и скидки на товары действительны на момент отправки данного письма.<br />Ваш любимы магазин:<br /> www.blablabla.ru<br />Волгоград, ул. Уличная, д1, 4000000 </p>
</body>
</html>
