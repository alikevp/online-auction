@component('mail::message')
# {{ $user->name }}

Статус вашего заказа изменён.<br>
Вы можете просмотреть его в личном кабинете или перейдя по ссылке ниже.

@component('mail::panel')

@component('mail::table')

| Заказ | Товар | Статус |  |
| ------------- |-------------:| -------------:| --------:|

| {{ $order->id }} | {{ $order->product->name }} | @if($order->status == 0) Завершён @elseif($order->status == 1) Активен @elseif($order->status == 2) Просрочен @elseif($order->status == 3) Принят @endif  | <a href="https://jinnmart.ru/orderdetails?order={{ $order->id }}">Просмотреть</a> |

@endcomponent

@endcomponent

С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
