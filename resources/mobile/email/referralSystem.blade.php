@component('mail::message')
    # {{ $user->name }}

    Знаете ли Вы, что на нашем сайте существует реферальная система? <br>
    Узнайте больше прямо сейчас: <a href=" {{ url('/referral_system') }}">Реферальная система</a>.

    С уважением,<br>
    {{ config('app.name') }}
    <p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что
        зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
    <a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
