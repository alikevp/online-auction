@component('mail::message')
# {{ $shop->name }}

Ваша ставка успешно сохранена.<br>
Вы всегда можете просмотреть статус заказов в личном кабинете или перейдя по ссылке ниже.

@component('mail::panel')

@component('mail::table')

| Заказ | Товар |  |
| ------------- |-------------: |--------:|
| {{ $order->id }} | {{ $order->product->limit_name }} | <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}">Просмотреть</a> |

@endcomponent

@endcomponent

С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
