@component('mail::message')
# {{ $user->name }}

Аукцион, в котором вы принимали участие истек.<br>
Дождитесь действий покупателя в отношении аукциона.<br>
Вы всегда можете просмотреть статус заказов в личном кабинете или перейдя по ссылке ниже.

@component('mail::panel')

@component('mail::table')

| Заказ | Товар |  |
| ------------- |-------------:| --------:|
@foreach($orders as $order)
    | {{ $order['order_id'] }} | @foreach($order['products'] as $product) {{ $product['name'] }}<br> @endforeach | <a href="https://jinnmart.ru/auction/order?order={{ $order['order_id'] }}">Просмотреть</a> |
@endforeach

@endcomponent

@endcomponent

С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
