@component('mail::message')
# {{ $user->name }}

Новая конкурирующая ставка к аукциону {{ $offer->id }}<br>
Вы можете посмотреть её в кабинете, или перейдя по ссылке ниже.

@component('mail::panel')

@component('mail::table')

| Заказ | Товар | Магазин |  |
| ------------- |-------------: |-------------:| --------:|
| {{ $order->id }} | @foreach($order->items as $item) {{ $item->product->limit_name }}<br> @endforeach | {{ $order->shop_name }} | <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}">Просмотреть</a> |


@endcomponent

@endcomponent
С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
