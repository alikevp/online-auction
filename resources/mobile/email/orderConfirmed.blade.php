@component('mail::message')
# Поздравляем {{ $user->name }}

Ваш заказ сохранен.
Вы можете просмотреть его в личном кабинете или перейти по ссылке ниже.
@component('mail::panel')

@component('mail::table')
| Товар | Количество | Цена |
| ------------- |:-------------:| --------:|
@foreach($products as $product)
| {{str_limit($product->name, 30)}} | Centered      | {{$product->price}} |
@endforeach
@endcomponent

@endcomponent

С уважением,<br>
    {{ config('app.name') }}
@endcomponent
