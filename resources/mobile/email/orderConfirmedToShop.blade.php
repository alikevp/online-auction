@component('mail::message')
# {{ $user->name }}

Поступили новые заказы на категории которых Вы подписаны.<br>
Вы можете просмотреть их в личном кабинете или перейдя по ссылке ниже.

@component('mail::panel')

@component('mail::table')

| Заказ | Товар |   |
| ----- |:-----:| -:|
@foreach($orders as $order)
    | {{ $loop->index+1 }} | {{ $order->product->name }} | <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}">Просмотреть</a> |
@endforeach

@endcomponent

@endcomponent

С уважением,<br>
{{ config('app.name') }}
<p style="font-size: 12px; color: #ccc; text-align: center;">Вы получили данное сообщение, потому что зарегистрировались и осуществили подписку на рассылку соотвествующей информации.</p>
<a style="font-size: 12px; color: #ccc; text-align: center; display: block;" href=" {{ url('mail/unsubscribe') }}">Отписаться</a>
@endcomponent
