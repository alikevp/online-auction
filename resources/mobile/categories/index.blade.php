@extends('layouts.master')

@section('style')
    {{--<link href="{{ asset('owlcarousel/sidebar.owlcarousel') }}" rel="stylesheet" type="text/owlcarousel">--}}
    <link href="{{ asset('owlcarousel') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row" id="categories_manager">
        <div class="medium-12 columns">
            @include('partials.cabinet.sidebar')
            <div class="medium-10 columns">
                <div class="medium-12 columns" id="search">
                    <h1 class="category-page-lable">РАБОТА С КАТЕГОРИЯМИ</h1>
                    <div class="search-bar-div">
                        <div class="row search-row collapse">
                            <div class="medium-11 search-column columns">
                                <input type="text"
                                       class="search-input"
                                       placeholder="Поиск категорий"
                                       autocomplete="off"
                                       v-model="query"
                                       @keyup.enter="find">
                            </div>
                            <div class="medium-1 button-column columns">
                                <button type="submit"
                                        class="button search-button"
                                @click="find">
                                Найти
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="medium-12 columns dashed-block" v-show="result">
                        <div class="medium-12 columns">
                            <div class="medium-12 columns category-name-block">
                                <h2 class="category-h2">
                                    Результаты поиска: <a href="#"
                                                          class="show-hide-button"
                                                          v-if="categories != null">Скрыть</a>
                                </h2>
                            </div>
                        </div>
                        <div class="medium-12 columns category-search-result">
                            <category class="category"
                                      :model="categories"
                                      :current="'{!! $category->id !!}'"></category>
                        </div>
                    </div>
                    <div class="medium-12 columns dashed-block">
                        @include('partials.category_dash')
                    </div>

                    <div class="medium-12 columns category-tree">
                        <ul class="accordion" data-accordion data-allow-all-closed="true">
                            <li class="accordion-item is-active" data-accordion-item>
                                <a href="#" class="accordion-title">
                                    Древо категорий: <span class="show-hide-button">показать/скрыть</span>
                                </a>
                                <div class="accordion-content" data-tab-content>
                                    <div class="medium-12 tree-column columns">
                                        <ul class="vertical menu" data-accordion-menu>
                                            @include('partials.nested')
                                        </ul>
                                    </div>
                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        let searchAPI = "{{ URL::to('api/category/find') }}";
        let setRootAPI = "{{ URL::to('api/category/root') }}";
        let selectCategoryAPI = "{{ URL::to('api/category/select') }}";
    </script>
    <script src="{{ asset('js/category_manager.js') }}"></script>
@endsection