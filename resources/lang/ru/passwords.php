<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'match' => 'Пароли должны совпадать.',
    'password' => 'Пароль должн быть не менее 6 символов.',
    'reset' => 'Ваш пароль был восстановлен!',
    'sent' => 'Мы отправили вам по электронной почте ссылку для сброса пароля!',
    'token' => 'Эта ссылка сброса пароля недействительна.',
    'user' => "Мы не можем найти пользователя с таким адресом электронной почты.",

];
