<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Извините, но мы не нашли пользователя с такими данными.',
    'confirmation' => 'Вы не подтвердили e-mail. На ваш e-mail было выслано сообщение с дальнейшими инструкциями.',
    'throttle' => 'Слишком много попыток входа, попробуйте через :seconds секунд.',

];
