@extends('layouts.master')

@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/dropzone.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('scripts')
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
@endsection
@section('title')
    Форма загрузки фото
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.sidebarBuyerWrapper')

            <div class="medium-10 columns form-column">

                @include('errors.validation')

                <form action="{{ url('fileupload/upload')}}" class="dropzone" id="my-awesome-dropzone">{{ csrf_field() }}</form>







            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
