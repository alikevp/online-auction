<!DOCTYPE html>
<html lang="ru">
<head>
    @include('header.meta')
    @include('header.head')
</head>
<body id="body">
@if(Auth::check())
    @if(count(auth()->user()->roles)>0)
        <div class="admin-panel-fixed">
            <ul class="admin-panel-menu">
                <li><a href="/admin">Панель</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Записи <b class="caret"></b></a>
                    <ul class="dropdown-menu sub-menu">
                        <li><a href="/admin/posts">Записи</a></li>
                        <li class="divider"></li>
                        <li><a href="/admin/posts/create">Новый пост</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Страницы <b class="caret"></b></a>
                    <ul class="dropdown-menu sub-menu">
                        <li><a href="/admin/pages">Страницы</a></li>
                        <li class="divider"></li>
                        <li><a href="/admin/pages/create">Новый пост</a></li>
                    </ul>
                </li>
                @if(isset($post))
                    @if($post->post_type == 'page')
                        <li><a href="/admin/pages/{{$post->id}}/edit">Редактировать страницу</a></li>
                    @endif
                @endif
            </ul>
        </div>
    @endif
@endif

{{--<div id="ajax_error" style="display: none"></div>--}}
<header>
    <div class="upper-nav-wrapper">
        @if(Auth::check())
            @if(Auth::user()->type == 'shop_admin')
                @include('partials.upperNavWrapperForShops')
            @else
                @include('partials.upperNavWrapperForBuyers')
            @endif
        @else
            @include('partials.upperNavWrapper')
        @endif
    </div>
    <div class="main-nav-wrapper">
        @include('partials.mainNavWrapper')
    </div>
</header>
<div class="page-wrapper main-page-wrapper">
    <div class="container">
	@if(!isset($_COOKIE['enter_id']))
		<style type="text/css">
			.modal-open .modal.modal-center {
                display: flex !important;
                align-items: center !important;
            }
				.modal-dialog {
					flex-grow: 1;
				}
        </style>
		<div class="modal fade modal-center" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				Рекламные материалы
			  </div>
			</div>
		  </div>
		</div>
	@endif
        <div class="row" id="app">
            @yield('breadcrumbs')
            @yield('content')
            @include('partials.supportForm')
        </div>
    </div>
</div>
<footer>
    @include('footer.footerWrapper')
</footer>
<div id="ajax-loading-gif"
     style="display: none;position: fixed;left: 0;right: 0;top: 0;bottom: 0;background: rgba(194, 202, 206, 0.48);z-index: 99999;">
    <img src="{{ asset('images/loading.gif') }}" style="position: absolute;top: 40%;left: 47%">
</div>


<script src="{{ asset('js/dropdown-sidebar-fix.js') }}"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/jasny-bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/jquery.nivo.slider.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/page_scripts/supportForm.js') }}"></script>
@if(!isset($_COOKIE['enter_id']))
{{--<script type="text/javascript">--}}
	{{--$(window).on('load',function(){--}}
        {{--$('#promoModal').modal('show');--}}
    {{--});--}}
{{--</script>--}}
@endif
@yield('footer_scripts')
@include('footer.footer_script')
</body>
</html>