@extends('layouts.master')

@section('style')
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker-standalone.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/profile.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('headScripts')
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.js') }}"></script>
@endsection
@section('footer_scripts')
    <script src="{{ asset('js/jm_v_1.1/page_scripts/shopProfile.js') }}"></script>
    <script src="{{ asset('js/jm_v_1.1/page_scripts/paymetTypeAdd.js') }}"></script>
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#bank_search").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);

                var data = suggestion.data;
                var all_name_shop = $("#name_f").val();
                if (!data)
                    return;
                $("#short_name").val(data.name.short);
                $("#full_name").val(data.name.full);
                $("#bank_bik").val(data.bic);
                $("#kor_account").val(data.correspondent_account);
                $("#bank_address").val(data.address.value);
                $("#firm_recipient").val(all_name_shop);

            }
        });

        /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('not-letter').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection

@section('title')
    Данные магазина
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Профиль</h1>
    </div>
    @php $user = Auth::user(); @endphp
    @if($user->type == 'shop_admin')
        @include('partials.sidebarShopWrapper')
    @else
        @include('partials.sidebarBuyerWrapper')
    @endif
    <div class="col-sm-9 content-wrapper" id="right-block">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="col-xs-12">
                    <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ Session::get('alert-' . $msg) }}</p>
                    </div>
                </div>
            @endif
        @endforeach
        <div class="col-sm-12">
            <div class="row">
                <div class="col-lg-6 col-sm-12 col-sm-12 dashed-divs">
                    <div class="col-sm-12 columns dashed-block">
                        <h2>Основные данные</h2>
                        <table class="personal-data-table" id="user-info">
                            <tr>
                                <td><span>Название:</span></td>
                                <td><span>{{ $shop_data->name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>Юридическое название:</span></td>
                                <td><span>{{ $shop_data->le_name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>Полное юридическое название:</span></td>
                                <td><span>{{ $shop_data->full_le_name }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ФИО директора:</span></td>
                                <td><span>{{ $shop_data->director_fio }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ИНН:</span></td>
                                <td><span>{{ $shop_data->inn }}</span></td>
                            </tr>
                            <tr>
                                <td><span>КПП:</span></td>
                                <td><span>{{ $shop_data->kpp }}</span></td>
                            </tr>
                            <tr>
                                <td><span>ОГРН:</span></td>
                                <td><span>{{ $shop_data->ogrn }}</span></td>
                            </tr>
                        </table>
                        <a class="flash-link" data-toggle="modal" data-target="#personal-data-edit">Изменить данные</a>
                        <div class="modal fade" id="personal-data-edit" tabindex="-1" role="dialog" aria-labelledby="personal-data-editModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Данные магазина</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_data_update" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">Название</span><span class="red-star">*</span>
                                                            <input class="form-control" type="text" placeholder="" name="shop_name" value="{{ $shop_data->name }}" required>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">Юридическое название</span><span class="red-star">*</span>
                                                            <input class="form-control" type="text" placeholder="" name="le_name" value="{{ $shop_data->le_name }}" required>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">Полное юридическое название</span><span class="red-star">*</span>
                                                            <input class="form-control" type="text" id="name_f" placeholder="" name="full_le_name" value="{{ $shop_data->full_le_name }}" required>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">ФИО директора</span><span class="red-star">*</span>
                                                            <input class="form-control" type="text" placeholder="" name="director_fio" value="{{ $shop_data->director_fio }}" required>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">ИНН</span><span class="red-star">*</span>
                                                            @if(isset($shop_data->inn))
                                                                <input class="form-control" disabled type="text" placeholder="" minlength="10" maxlength="12" pattern="\d*" name="shop_inn" value="{{ $shop_data->inn }}" required>
                                                            @else
                                                                <input class="form-control" type="text" placeholder="" minlength="10" maxlength="12" pattern="\d*" name="shop_inn" value="{{ $shop_data->inn }}" required>
                                                            @endif
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label><span class="bold-text">КПП</span><span class="red-star">*</span>
                                                            <input class="form-control" type="text" placeholder="" minlength="9" maxlength="9" pattern="\d*" name="kpp" value="{{ $shop_data->kpp }}" required>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label><span class="bold-text">ОГРН</span><span class="red-star">*</span>
                                                    @if(isset($shop_data->ogrn))
                                                        <input class="form-control" disabled type="text" placeholder="" maxlength="13" name="ogrn" value="{{ $shop_data->ogrn }}">

                                                    @else
                                                        <input class="form-control" type="text" placeholder="" maxlength="13" name="ogrn" value="{{ $shop_data->ogrn }}" required>
                                                    @endif
                                                </label>
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 dashed-block">
                        <h2>Адреса</h2>
                        @foreach($addresses as $address)
                            <span class="info"></span>
                            <table class="personal-data-table" id="user-info">
                                <tr>
                                    <td>
                                                <span>
                                                @if($address->type == 1)
                                                        Юридический
                                                    @elseif($address->type == 2)
                                                        Почтовый
                                                    @else
                                                        Не указан
                                                    @endif
                                                </span></td>
                                </tr>
                                <tr>
                                    <td>{{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}</td>
                                </tr>
                            </table>
                            <br>
                        @endforeach

                        @if(count($addresses) > 0)
                            <a class="address-add" data-toggle="modal" data-target="#address-edit">РЕДАКТИРОВАТЬ</a>
                        @else
                            <a class="address-add" data-toggle="modal" data-target="#address-add">ДОБАВИТЬ АДРЕС</a>
                        @endif
                        <div class="modal fade" id="address-add" tabindex="-1" role="dialog" aria-labelledby="address-addModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">АДРЕС</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_address_add" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <h4>Юридический адрес</h4>
                                            <input type="hidden"  name="address_data[1][type]" value="1">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Страна</span><span class="red-star">*</span>
                                                        <input class="form-control" type="text" placeholder="Ваша страна" name="country" required>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Населенный пункт/Город</span><span class="red-star">*</span>
                                                        <input class="form-control" id="dadata_city" type="text" placeholder="Ваш город" name="address_data[1][city]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="width-100"><span class="bold-text">Улица</span><span class="red-star">*</span>
                                                        <input class="form-control" id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[1][street]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label><span class="bold-text">Дом</span><span class="red-star">*</span>
                                                        <input class="form-control" id="dadata_house" type="text" placeholder="Номер дома" name="address_data[1][house]" required>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label><span class="bold-text">Квартира</span>
                                                        <input class="form-control" id="dadata_flat" type="text" placeholder="Номер квартиры" name="address_data[1][apartment]" pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$">
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label><span class="bold-text">Индекс</span><span class="red-star">*</span>
                                                        <input class="form-control" id="dadata_postal_code" type="text" maxlength="6" placeholder="Почтовый индекс" name="address_data[1][post_index]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label><span class="bold-text">Офис</span>
                                                        <input class="form-control" type="text" placeholder="офис" name="address_data[1][office]">
                                                    </label>
                                                </div>
                                            </div>
                                            <h4>Почтовый адрес</h4>

                                            <label for="post_address_equal"><input class="big-checkbox left" id="post_address_equal" type="checkbox" name="post_address_equal" value="true" checked @change="showPostAddressInputs = !showPostAddressInputs"> Почтовый адрес совпадает с юридическим</label>

                                            <div id="post-address-block">
                                            </div>
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="address-edit" tabindex="-1" role="dialog" aria-labelledby="address-editModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Редактирование адреса</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_address_edit" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <h4>Юридический адрес</h4>
                                            @foreach($addresses as $address)
                                                @if($address->type == 1)
                                                    <input type="hidden"  name="address_data[1][type]" value="{{$address->type}}">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Страна</span><span class="red-star">*</span>
                                                                <input class="form-control" type="text" placeholder="Ваша страна" name="country" value="{{$address->country}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Населенный пункт/Город</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_city" type="text" placeholder="Ваш город" name="address_data[1][city]" value="{{$address->city}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label class="width-100"><span class="bold-text">Улица</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[1][street]" value="{{$address->street}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Дом</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_house" type="text" placeholder="Номер дома" name="address_data[1][house]" value="{{$address->house}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Квартира</span>
                                                                <input class="form-control" id="dadata_flat" type="text" placeholder="Номер квартиры" name="address_data[1][apartment]" value="{{$address->apartment}}" pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$">
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Индекс</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_postal_code" type="text" maxlength="6" placeholder="Почтовый индекс" name="address_data[1][post_index]" value="{{$address->post_index}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><span class="bold-text">Офис</span>
                                                                <input class="form-control" type="text" placeholder="офис" name="address_data[1][office]" value="{{$address->office}}" >
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="col-sm-12">
                                                <h4>Почтовый адрес</h4>
                                            </div>
                                            @foreach($addresses as $address)
                                                @if($address->type == 2)
                                                    <input type="hidden"  name="address_data[2][type]" value="{{$address->type}}">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Страна</span><span class="red-star">*</span>
                                                                <input class="form-control" type="text" placeholder="Ваша страна" name="country" value="{{$address->country}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Населенный пункт/Город</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_city" type="text" placeholder="Ваш город" name="address_data[2][city]" value="{{$address->city}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label class="width-100"><span class="bold-text">Улица</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_street" type="text" placeholder="Ваша улица" name="address_data[2][street]" value="{{$address->street}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Дом</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_house" type="text" placeholder="Номер дома" name="address_data[2][house]" value="{{$address->house}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Квартира</span>
                                                                <input class="form-control" id="dadata_flat" type="text" placeholder="Номер квартиры" name="address_data[2][apartment]" value="{{$address->apartment}}" pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$">
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><span class="bold-text">Индекс</span><span class="red-star">*</span>
                                                                <input class="form-control" id="dadata_postal_code" type="text" maxlength="6" placeholder="Почтовый индекс" name="address_data[2][post_index]" value="{{$address->post_index}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><span class="bold-text">Офис</span>
                                                                <input class="form-control" type="text" placeholder="офис" name="address_data[2][office]" value="{{$address->office}}" >
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-xs-12 dashed-divs">
                    <div class="col-sm-12 dashed-block">
                        <h2>Номер телефона</h2>
                        <span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
                        <span class="number">{{ $shop_data->phone }}</span>
                        <a class="flash-link" data-toggle="modal" data-target="#telefone-edit">Изменить номер телефона</a>
                        <div class="modal fade" id="telefone-edit" tabindex="-1" role="dialog" aria-labelledby="telefone-editModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Изменение телефона</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_phone_update" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label><span class="bold-text">Укажите свой действующий номер</span><span class="red-star">*</span>
                                                    <input class="form-control" type="text" id="checktel" placeholder="Ваш телефон" name="shop_phone" value="{{ Auth::user()->telefone }}" required>
                                                </label>
                                            </div>
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 dashed-block">
                        <h2>Адрес электронной почты</h2>
                        <span class="e-mail">{{ $shop_data->e_mail }}</span>
                    </div>
                    <div class="col-sm-12 dashed-block">
                        <h2>Банковские реквизиты</h2>
                        @foreach($bank_data as $row)
                            <table class="personal-data-table" id="user-info">
                                <tr>
                                    <td><span>
                                                @if($row->type == 1)
                                                Для расчета с физ.лицами
                                            @elseif($row->type == 2)
                                                Для расчета с ООО "ДЖИННМАРТ"
                                            @else
                                                Не указан
                                            @endif
                                                </span></td>
                                </tr>
                                <tr>
                                    <td><span>Наименование банка:</span></td>
                                    <td><span>{{ $row->bank_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Адрес банка:</span></td>
                                    <td><span>{{ $row->bank_address }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>БИК:</span></td>
                                    <td><span>{{ $row->bank_bik }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Кор. счет:</span></td>
                                    <td><span>{{ $row->kor_account }}</span></td>
                                </tr>

                                <tr>
                                    <td><span>Счет:</span></td>
                                    <td><span>{{ $row->account }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Получатель:</span></td>
                                    <td><span>{{ $row->recipient }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Назначение платежа:</span></td>
                                    <td><span>{{ $row->purpose }}</span></td>
                                </tr>
                            </table>
                            <br>
                        @endforeach
                        @if(count($bank_data) > 0)
                            <a class="address-add" data-toggle="modal" data-target="#bank-data-edit">РЕДАКТИРОВАТЬ</a>
                        @else
                            <a class="address-add" data-toggle="modal" data-target="#bank-data-add">ДОБАВИТЬ СЧЕТ</a>
                        @endif

                        <div class="modal fade" id="bank-data-add" tabindex="-1" role="dialog" aria-labelledby="bank-data-addLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Счет</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_bank_data_add" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4>Для расчеста с физ.лицами</h4>
                                                    <input type="hidden"  name="bank_data[1][type]" value="1">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="width-100"><span class="bold-text">БИК (для автоматического поиска)</span><span class="red-star">*</span>
                                                        <input class="form-control" id="bank_search" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*" requiered>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Сокращенное наименование банка</span>
                                                        <input class="form-control" id="short_name" type="text" placeholder="Сокращенное наименование банка" name="bank_data[1][bank_name]">
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Полное наименование банка</span><span class="red-star">*</span>
                                                        <input class="form-control" id="full_name" type="text" placeholder="Наименование банка" name="bank_data[1][bank_full_name]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="width-100"><span class="bold-text">Адрес банка</span><span class="red-star">*</span>
                                                        <input class="form-control" id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[1][bank_address]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">БИК</span><span class="red-star">*</span>
                                                        <input class="form-control" id="bank_bik" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*" name="bank_data[1][bank_bik]" required>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Кор. счет</span><span class="red-star">*</span>
                                                        <input class="form-control" id="kor_account" type="text" placeholder="Кор. счет" minlength="20" maxlength="20" pattern="\d*" name="bank_data[1][kor_account]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Счет</span><span class="red-star">*</span>
                                                        <input class="form-control" id="" type="text" maxlength="20" placeholder="Счет" name="bank_data[1][account]" required>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label><span class="bold-text">Получатель</span><span class="red-star">*</span>
                                                        <input class="form-control" id="firm_recipient" type="text" maxlength="20" placeholder="Получатель" name="bank_data[1][recipient]" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4>Для расчетов с ООО "ДЖИНМАРТ"</h4>
                                                <label for="bank_data_equal"><input class="big-checkbox left" id="bank_data_equal" type="checkbox" name="bank_data_equal" value="true" checked @change="showBankAccountInputs = !showBankAccountInputs"> Счет совпадает с счетом для физ.лиц</label>
                                            </div>
                                            <div id="bank-inputs-block">
                                            </div>
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="bank-data-edit" tabindex="-1" role="dialog" aria-labelledby="bank-data-editLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Банковские реквизиты</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/shop_bank_data_edit" method="post" data-abide novalidate data-live-validate="true">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4>Для расчеста с физ.лицами</h4>
                                                    <input type="hidden"  name="bank_data[1][type]" value="1">
                                                </div>
                                            </div>
                                            @foreach($bank_data as $row)
                                                @if($row->type == 1)
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label class="width-100"><span class="bold-text">БИК (для автоматического поиска)</span><span class="red-star">*</span>
                                                                <input class="form-control" id="bank_search" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*"  value="{{$row->bank_bik}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Сокращенное наименование банка</span>
                                                                <input class="form-control" id="short_name" type="text" placeholder="Сокращенное наименование банка" name="bank_data[1][bank_name]" value="{{$row->bank_name}}">
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Полное наименование банка</span><span class="red-star">*</span>
                                                                <input class="form-control" id="full_name" type="text" placeholder="Наименование банка" name="bank_data[1][bank_full_name]" value="{{$row->bank_full_name}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label class="width-100"><span class="bold-text">Адрес банка</span><span class="red-star">*</span>
                                                                <input class="form-control" id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[1][bank_address]" value="{{$row->bank_address}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">БИК</span><span class="red-star">*</span>
                                                                <input class="form-control" id="bank_bik" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*" name="bank_data[1][bank_bik]" value="{{$row->bank_bik}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Кор. счет</span><span class="red-star">*</span>
                                                                <input class="form-control" id="kor_account" type="text" placeholder="Кор. счет" minlength="20" maxlength="20" pattern="\d*" name="bank_data[1][kor_account]" value="{{$row->kor_account}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Счет</span><span class="red-star">*</span>
                                                                <input class="form-control" id="" type="text" maxlength="20" placeholder="Счет" name="bank_data[1][account]" value="{{$row->account}}" required>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label><span class="bold-text">Получатель</span><span class="red-star">*</span>
                                                                <input class="form-control" id="firm_recipient" type="text" maxlength="20" placeholder="Получатель" name="bank_data[1][recipient]" value="{{$row->recipient}}" required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div id="bank-inputs-block">
                                                @foreach($bank_data as $row)
                                                    @if($row->type == 2)
                                                        <div class="col-sm-12">
                                                            <h4>Для расчетов с ООО "ДЖИНМАРТ"</h4>
                                                            <input type="hidden"  name="bank_data[2][type]" value="2">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="width-100"><span class="bold-text">БИК (для автоматического поиска)</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="bank_search" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*"  value="{{$row->bank_bik}}" required>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">Сокращенное наименование банка</span>
                                                                    <input class="form-control" id="short_name" type="text" placeholder="Сокращенное наименование банка" name="bank_data[2][bank_name]" value="{{$row->bank_name}}">
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">Полное наименование банка</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="full_name" type="text" placeholder="Наименование банка" name="bank_data[2][bank_full_name]" value="{{$row->bank_full_name}}" required>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="width-100"><span class="bold-text">Адрес банка</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="bank_address" type="text" placeholder="Адрес банка" name="bank_data[2][bank_address]" value="{{$row->bank_address}}" required>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">БИК</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="bank_bik" type="text" placeholder="БИК" minlength="9" maxlength="9" pattern="\d*" name="bank_data[2][bank_bik]" value="{{$row->bank_bik}}" required>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">Кор. счет</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="kor_account" type="text" placeholder="Кор. счет" minlength="20" maxlength="20" pattern="\d*" name="bank_data[2][kor_account]" value="{{$row->kor_account}}" required>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">Счет</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="" type="text" maxlength="20" placeholder="Счет" name="bank_data[2][account]" value="{{$row->account}}" required>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label><span class="bold-text">Получатель</span><span class="red-star">*</span>
                                                                    <input class="form-control" id="firm_recipient" type="text" maxlength="20" placeholder="Получатель" name="bank_data[2][recipient]" value="{{$row->recipient}}" required>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 dashed-block">
                        <h2>Способы оплаты</h2>
                        @foreach($payment_data as $row)
                            <span>
                                    @if($row['type'] == 1)
                                    Наличные при получении
                                @elseif($row['type'] == 2)
                                    Карта при получении
                                @elseif($row['type'] == 3)
                                    Карта(перевод)
                                @elseif($row['type'] == 4)
                                    Банковский перевод
                                @elseif($row['type'] == 5)
                                    Электронная коммерция
                                @else
                                    Не указан
                                @endif
                                </span>
                            <table class="personal-data-table" id="user-info">
                                @if($row['type'] == 1)
                                    <tr>
                                        <td><span>Описание:</span></td>
                                        <td><span>{{ $row['description'] }}</span></td>
                                    </tr>
                                @elseif($row['type'] == 2)
                                    <tr>
                                        <td><span>Описание:</span></td>
                                        <td><span>{{ $row['description'] }}</span></td>
                                    </tr>
                                @elseif($row['type'] == 3)
                                    <tr>
                                        <td><span>Банк:</span></td>
                                        <td><span>{{ $row['name'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>№ карты:</span></td>
                                        <td><span>{{ $row['account'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Описание:</span></td>
                                        <td><span>{{ $row['description'] }}</span></td>
                                    </tr>
                                @elseif($row['type'] == 4)
                                    <tr>
                                        <td><span>Наименование банка:</span></td>
                                        <td><span>{{ $row['bank_data']['bank_name'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Адрес банка:</span></td>
                                        <td><span>{{ $row['bank_data']['bank_address'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>БИК:</span></td>
                                        <td><span>{{ $row['bank_data']['bank_bik'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Кор. счет:</span></td>
                                        <td><span>{{ $row['bank_data']['kor_account'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>ИНН:</span></td>
                                        <td><span>{{ $row['bank_data']['inn'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>КПП:</span></td>
                                        <td><span>{{ $row['bank_data']['kpp'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>ОКТМО:</span></td>
                                        <td><span>{{ $row['bank_data']['oktmo'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>КБК:</span></td>
                                        <td><span>{{ $row['bank_data']['kbk'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Счет:</span></td>
                                        <td><span>{{ $row['bank_data']['account'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Получатель:</span></td>
                                        <td><span>{{ $row['bank_data']['recipient'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Назначение платежа:</span></td>
                                        <td><span>{{ $row['bank_data']['purpose'] }}</span></td>
                                    </tr>
                                @elseif($row['type'] == 5)
                                    <tr>
                                        <td><span>Сервис:</span></td>
                                        <td><span>{{ $row['name'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Счет:</span></td>
                                        <td><span>{{ $row['account'] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Описание:</span></td>
                                        <td><span>{{ $row['description'] }}</span></td>
                                    </tr>
                                @else
                                    Не указан
                                @endif

                            </table>
                            <br>
                        @endforeach
                        <a class="address-add payment_add_btn" data-toggle="modal" data-target="#payment-add">ДОБАВИТЬ ОПЛАТУ</a>
                        <div class="modal fade" id="payment-add" tabindex="-1" role="dialog" aria-labelledby="payment-addModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Добавление оплаты</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/payment_type_add" method="post" data-abide novalidate="novalidate">
                                        <div class="modal-body">
                                            <p>Способов оплаты может быть несколько. Добавляются по одному.</p>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Тип<span class="red-star">*</span>
                                                        <select class=" form-control payment_type_select" name="type" required>
                                                            <option value="null" selected disabled>Выберите тип</option>
                                                            <option value="1">Наличные при получении</option>
                                                            <option value="2">Карта при получении</option>
                                                            <option value="3">Карта(перевод)</option>
                                                            <option value="4">Банковский перевод</option>
                                                            <option value="5">Электронная коммерция</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="payment_modal_content"></div>
                                            <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                            <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection