@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
@endsection
@section('title')
    Профиль
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Подписки на категории</h1>
    </div>
    @include('partials.sidebarShopWrapper')
    <div class="col-sm-9 content-wrapper" id="right-block">
        <div class="medium-12 columns">
            <div class="large-10 medium-12 small-12 columns right-block">
                <h1>Подписки моего магазина</h1>
                @if (Session::has('msg'))
                    <div class="callout {{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" data-closable="slide-out-right">
                        <p>
                            {{ Session::get("msg") }}
                        </p>
                        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form action="/shop_subscriptions/update" method="post">
                    {{ csrf_field() }}

                    <label for="name">Категории</label>
                    <select name="category[]" multiple size="30" class="form-control">
                        @include('partials.nested_select')
                    </select>
					<div class="row" style="padding-top:15px;">
						<div class="col-md-8 row">
							<div class="col-md-4">
								<input type="submit" name="mail_subscription_submit" class="btn gradient-green width-100" value="Сохранить">
							</div>
							<div class="col-md-4">
								<a href="/shop_subscriptions" class="btn border-red-btn width-100">Отменить</a>
							</div>
						</div>
						<div class="col-md-4">
							<a href="{{ url('/page/instruktsii/kabinet-magazina/dannye-magazina/podpiski-magazina') }}"
							   class="flash-link float-right">Как редактировать подписки?</a>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
@endsection


