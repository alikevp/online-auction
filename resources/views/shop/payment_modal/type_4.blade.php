<div class="row">
    <div class="col-sm-12">
        <label><span class="bold-text">Счет</span><span class="red-star">*</span>
            <select class="form-control" name="bank_data_id">
                @foreach($bank_data_list as $row)
                    <option value="{{ $row->id }}">{{ $row->bank_name }} - {{ $row->account }}</option>
                @endforeach
            </select>
        </label>
    </div>
    <div class="col-sm-12">
        <label><span class="bold-text">Описание</span><span class="red-star">*</span>
            <textarea class="form-control" name="description"></textarea>
        </label>
    </div>
</div>

