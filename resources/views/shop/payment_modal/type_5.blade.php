<div class="row">
    <div class="col-sm-12">
        <label><span class="bold-text">Название сервиса</span><span class="red-star">*</span>
            <input class="form-control" type="text" placeholder="Название сервиса" name="name" required>
        </label>
    </div>
    <div class="col-sm-12">
        <label><span class="bold-text">№ счета</span><span class="red-star">*</span>
            <input class="form-control" type="number" placeholder="№ счета" name="account" required>
        </label>
    </div>
    <div class="col-sm-12">
        <label><span class="bold-text">Описание</span><span class="red-star">*</span>
            <textarea class="form-control" name="description"></textarea>
        </label>
    </div>
</div>