@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <!-- header here -->
        @endcomponent
    @endslot

    {{-- Body --}}
    <div class="main-block">
        <table class="grid header">
            <tr>
                <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
                <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
                <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
            </tr>
        </table>

        <hr class="green">
        {{--содержимое письма--}}
        <table class="grid content-table">
            <tr>
                <td>
                    <h1>Внимание!!!</h1>
{{--                    <h4>{{ $user->name }},</h4>--}}
                    <br>
                    <p class="gray" style="text-align: center">У нас для Вас новый покупатель</p>
                    <p class="gray" style="text-align: center">10 минут назад к нам поступил запрос на покупку:</p>

                </td>
            </tr>
        </table>

        <hr class="green">

        <table class="grid items">
            <tr>
                <td>
                    <h1>Товары в заказе</h1>
                    <table class="items-table" style="margin-bottom: 30px;">
                        <tr>
                            <th>Заказ</th>
                            <th>Товар</th>
                            <th></th>
                        </tr>
                        @foreach($orders as $order)
                        <tr>
                            <td class="bold">
                                {{ $loop->index+1 }}
                            </td>
                            <td class="bold">
                                {{ $order->product->name }}
                            </td>
                            <td class="link">
                                <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}" class="green">Перейти к заказу</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>

        <hr class="green">

        <table class="grid content-table">
            <tr>
                <td>
                    <p class="gray">Если данный товар есть у Вас в ассортименте, то переходите по ссылке и предложите свои условия потенциальному покупателю.</p>
                    <br><br>
                    <p class="gray">Джинмарт - это единый портал для продажи любых товаров, где Покупатели со всей России, ищут необходимые товары на выгодных условиях, открывая по ним Аукцион.</p>
                    <p class="gray">Если Вы ищете источник дополнительной прибыли, то скорее регистрируйтесь в качестве продавца на портале jinnmart.ru, участвуйте в аукционах, предлагая свой товар по самым привлекательным ценам.</p>
                    <br><br>
                    <p class="gray">По всем интересующим Вас вопросам просим обращаться по телефону 8-902-364-22-14 или написать нам письмо egorkina@jinnmart.ru</p>
                </td>
            </tr>
        </table>
        {{--содержимое письма конец--}}
        <hr class="green">

        <table class="grid footer-table">
            <tr>
                <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. это может быть вам интересно.
                        Вы всегда можете отказаться от рассылки нажав <a href="{{ url('mail/unsubscribe') }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                    <br>
                    <br>
                    <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
                </td>
            </tr>
        </table>

    </div>
{{-- Body here-- }}

{{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent