@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <!-- header here -->
        @endcomponent
    @endslot

    {{-- Body --}}
    <div class="main-block">
        <table class="grid header">
            <tr>
                <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
                <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
                <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
            </tr>
        </table>

        <hr class="green">
        {{--содержимое письма--}}
        <table class="grid content-table">
            <tr>
                <td>
                    <h1>Магазин {{ $shop->name }} успешно зарегистрирован.</h1>

                    <span class="gray">
                    Теперь вы можете просматривать заказы на интересуещие Вас категории товаров, однако для выставления предложений необходима активация магазина. <br>
                    Актвация выполняется нашими менджерами.<br>
                        Для активации вам необходимо выслать на элекронную почту <a href="mailto:documents@jinnmart.ru" class="green">documents@jinnmart.ru</a> следующий пакет документов:
                </span>
                    <ol>
                        <li>Юридические лица</li>
                        <ul>
                            <li>ИНН,</li>
                            <li>ОГРН,</li>
                            <li>УСТАВ -1,2 и последняя страница с печатями,</li>
                            <li>приказ о назначении директора,</li>
                            <li>протокол собрания учредителей,</li>
                            <li>карта партнера</li>

                        </ul>
                    </ol>
                    <ol>
                        <li>Инидивуальные предпрениматели</li>
                        <ul>
                            <li>ИНН,</li>
                            <li>ОГРНИП,</li>
                            <li>ПАСПОРТ 1стр и прописка</li>
                        </ul>
                    </ol>
                </td>
            </tr>
        </table>

        <hr class="green">

        <table class="grid footer-table">
            <tr>
                <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. имеете соответствющую подписку в своем личном кабинете.
                        Вы всегда можете изменить подписки в личном кабинете или нажав <a href="{{ url('mail/unsubscribe') }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                    <br>
                    <br>
                    <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
                </td>
            </tr>
        </table>

    </div>
    {{-- Body here-- }}

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent
