@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<!-- header here -->
@endcomponent
@endslot

{{-- Body --}}
<div class="main-block">
    @include('email.parts.header')

    {{--содержимое письма--}}
    <table class="grid content-table">
        <tr>
            <td>
                <h1>Новая ставка</h1>
                <h4>Антон,</h4>
                <span class="gray">
                        Новая конкурирующая ставка к аукциону 88888<br>
Вы можете посмотреть её в кабинете, или перейдя по ссылке ниже.
                    </span>
            </td>
        </tr>
        <tr>
            <td>
                <a href="https://jinnmart.ru/auction/order?order=88888" class="green-button">Перейти к заказу</a>
            </td>
        </tr>
    </table>

    <hr class="green">

    <table class="grid items">
        <tr>
            <td>
                <h1>Товары в заказе</h1>
                <table class="items-table">
                    <tr>
                        <th>Заказ</th>
                        <th>Товар</th>
                        <th>Магазин</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td class="bold">88888</td>
                        <td class="bold">
                            Товар
                        </td>
                        <td class="bold">Магазин</td>
                        <td><a href="https://jinnmart.ru/auction/order?order=88888" class="green">Посмотреть</a></td>
                    </tr>
                    <tr>
                        <td class="bold">123</td>
                        <td class="bold">Название</td>
                        <td><a href="" class="green">Посмотреть</a></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    {{--содержимое письма конец--}}
    <hr class="green">

    <table class="grid footer-table">
        <tr>
            <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. имеете соответствющую подписку в своем личном кабинете.
                        Вы всегда можете изменить подписки в личном кабинете или нажав <a href="{{ url('mail/unsubscribe') }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                <br>
                <br>
                <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
            </td>
        </tr>
    </table>
</div>
{{-- Body here-- }}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
<!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!-- footer here -->
@endcomponent
@endslot
@endcomponent