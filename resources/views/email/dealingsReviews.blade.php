@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<!-- header here -->
@endcomponent
@endslot

{{-- Body --}}
<div class="main-block">
    <table class="grid header">
        <tr>
            <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
            <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
            <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
        </tr>
    </table>

    <hr class="green">
    {{--содержимое письма--}}
    <table class="grid content-table">
        <tr>
            <td>
                <h1>Ваши отзывы</h1>
                <h4>{{ $user->name }},</h4>
                <span class="gray">
                    Недавно Вы успешно завершили аукцион на нашем сайте.
                    Пожалуйста, оставьте свой отзыв о магазине, выигравшем аукцион,
                    о товаре, который Вы приобрели, а также о работе нашего сайта.
                    <br><br>
                    Сделать это Вы можете <a href="{{ url('/reviews/'.$dealing->id) }}" class="green">нажав сюда</a>.
                </span>
            </td>
        </tr>
    </table>

    <hr class="green">

    <table class="grid footer-table">
        <tr>
            <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. имеете соответствющую подписку в своем личном кабинете.
                        Вы всегда можете изменить подписки в личном кабинете или нажав <a href="{{ url('mail/unsubscribe') }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                <br>
                <br>
                <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
            </td>
        </tr>
    </table>

</div>
{{-- Body here-- }}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
<!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!-- footer here -->
@endcomponent
@endslot
@endcomponent
