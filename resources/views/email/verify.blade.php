@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<!-- header here -->
@endcomponent
@endslot

{{-- Body --}}
<div class="main-block">
    <table class="grid header">
        <tr>
            <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
            <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
            <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
        </tr>
    </table>

    <hr class="green">
    {{--содержимое письма--}}
    <table class="grid content-table">
        <tr>
            <td>
                <p>{{ $user->name }},</p>
                <br>
                Вы успешно зарегистрировались на нашем сайте и для подтверждения регистрации необходимо подтвердить ваш email

                <a href="{{ $url }}" class="green-button">Подтвердить email</a>


                С уважением,<br>
                {{ config('app.name') }}
            </td>
        </tr>
    </table>

    <hr class="green">

</div>
{{-- Body here-- }}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
<!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!-- footer here -->
@endcomponent
@endslot
@endcomponent