@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <!-- header here -->
        @endcomponent
    @endslot

    {{-- Body --}}
    <div class="main-block">
        <table class="grid header">
            <tr>
                <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
                <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
                <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
            </tr>
        </table>

        <hr class="green">
        {{--содержимое письма--}}
        <table class="grid content-table">
            <tr>
                <td>
                    <h1>Статус</h1>
                    <h4>{{ $user->name }},</h4>
                    <span class="gray">
                    Статус аукциона, в котором вы принимали участие изменен.<br>
                    Вы можете просмотреть его в личном кабинете или перейдя по ссылке ниже.
                </span>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}" class="green-button">Перейти к заказу</a>
                </td>
            </tr>
        </table>

        <hr class="green">

        <table class="grid items">
            <tr>
                <td>
                    <h1>Товары в заказе</h1>
                    <table class="items-table">
                        <tr>
                            <th>Заказ</th>
                            <th>Товар</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>

                        <tr>
                            <td class="bold">{{ $order->id }}</td>
                            <td class="bold">
                                    {{ $order->product->name }}<br>

                            </td>
                            <td class="bold">
                                @if($order->status == 0) Завершён
                                @elseif($order->status == 1) Активен
                                @elseif($order->status == 2) Просрочен
                                @elseif($order->status == 3) Принят
                                @endif
                            </td>
                            <td class="bold">
                                <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}" class="green">Перейти к заказу</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        {{--содержимое письма конец--}}
        <hr class="green">

        <table class="grid footer-table">
            <tr>
                <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. имеете соответствющую подписку в своем личном кабинете.
                        Вы всегда можете изменить подписки в личном кабинете или нажав <a href="{{ $subscribe }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                    <br>
                    <br>
                    <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
                </td>
            </tr>
        </table>

    </div>
{{-- Body here-- }}

{{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent