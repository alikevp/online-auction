@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<!-- header here -->
@endcomponent
@endslot

{{-- Body --}}
<div class="main-block">
    <table class="grid header">
        <tr>
            <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
            <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
            <td class="link"><span class="gray bold">8-800-888-88-88</span></td>
        </tr>
    </table>

    <hr class="green">
    {{--содержимое письма--}}
    <table class="grid content-table">
        <tr>
            <td>
                <h1>Сотрудничество</h1>
                <span class="gray">
                    Здравствуйте!.<br>
                    На нашем сайте недавно была совершена покупка товара из Вашего ассортимента. <br>
                    <a href="{{ url('register/shop') }}" class="green">Зарегистрируйте</a> Ваш магазин на нашем сайте, и получите приток новых покупателей уже сейчас!
                </span>
            </td>
        </tr>
    </table>

    {{--содержимое письма конец--}}
    <hr class="green">

    <table class="grid footer-table">
        <tr>
            <td>
                <span class="light-gray">
                    ООО «Джинмарт»
                    <br>
                    Все права защищены
                </span>
            </td>
        </tr>
    </table>

</div>
<!-- Body here -->

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
<!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!-- footer here -->
@endcomponent
@endslot
@endcomponent