@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<!-- header here -->
@endcomponent
@endslot

{{-- Body --}}
<div class="main-block">
    <table class="grid header">
        <tr>
            <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
            <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
            <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
        </tr>
    </table>

    <hr class="green">
    {{--содержимое письма--}}
    <table class="grid content-table">
        <tr>
            <td>
                <h1>Выполнена выгрузка в 1с</h1>
            </td>
        </tr>
    </table>

    <hr class="green">

    <p><b>Тип</b>: {{$report['type']}}</p>
    <p><b>Дата</b>: {{$report['date']}}</p>
    <p><b>Обработано файлов строк</b>: {{count($report["filesReport"])}}</p>

    <table class="grid items">
        <tr>
            <td>
                <h1>Файлы</h1>
                <table class="items-table">
                    <tr>
                        <th>№</th>
                        <th>Файл</th>
                        <th>Счетов</th>
                        <th>Удаление</th>
                    </tr>
                    @foreach($report["filesReport"] as $key=>$file)
                        <tr>
                            <td class="bold">{{ $key }}</td>
                            <td class="bold">{{ $file['fileName'] }}</td>
                            <td class="bold">{{ $file['billsCount'] }}</td>
                            <td class="bold">{{ $file['fileDeleteStatus'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>
    {{--содержимое письма конец--}}
    <hr class="green">

    <table class="grid footer-table">
        <tr>
            <td>
                <span class="light-gray">
                    ООО «Джинмарт»
                    <br>
                    Все права защищены
                </span>
            </td>
        </tr>
    </table>

</div>
{{-- Body here-- }}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
<!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!-- footer here -->
@endcomponent
@endslot
@endcomponent
