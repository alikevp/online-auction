{{--@component('mail::message')--}}
{{--# Поздравляем {{ $user->name }}--}}

{{--Ваш заказ сохранен.--}}
{{--Вы можете просмотреть его в личном кабинете или перейти по ссылке ниже.--}}
{{--@component('mail::panel')--}}

{{--@component('mail::table')--}}
{{--| Товар | Количество | Цена |--}}
{{--| ------------- |:-------------:| --------:|--}}
{{--@foreach($products as $product)--}}
{{--| {{str_limit($product->name, 30)}} | Centered      | {{$product->price}} |--}}
{{--@endforeach--}}
{{--@endcomponent--}}

{{--@endcomponent--}}

{{--С уважением,<br>--}}
    {{--{{ config('app.name') }}--}}
{{--@endcomponent--}}

{{--       START        --}}

{{--@component('mail::header', ['url' => '#'])--}}

{{--@endcomponent--}}

{{--@component('mail::message')--}}
{{--# Order Shipped--}}

{{--Your order has been shipped!--}}
{{--@component('mail::table')--}}
{{--| Laravel       | Table         | Example  |--}}
{{--| ------------- |:-------------:| :-------:|--}}
{{--| Col 2 is      | Centered      | $10      |--}}
{{--| Col 3 is      | Right-Aligned | $20      |--}}
{{--@endcomponent--}}

{{--@component('mail::panel')--}}
{{--This is the panel content.--}}
{{--@endcomponent--}}

{{--@component('mail::button', ['url' => '#', 'color' => 'green'])--}}
{{--Button--}}
{{--@endcomponent--}}
{{--@endcomponent--}}


@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <!-- header here -->
        @endcomponent
    @endslot

    {{-- Body --}}
    <div class="main-block">
        <table class="grid header">
            <tr>
                <td class="link"><a href="https://jinnmart.ru/login" class="gray bold">Вход в личный кабинет</a></td>
                <td class="image"><a href="https://jinnmart.ru"><img src="https://jinnmart.ru/images/logo.png" alt=""></a></td>
                <td class="link"><a href="https://jinnmart.ru?support_modal_show=true" class="gray bold">Задать вопрос</a></td>
            </tr>
        </table>

        <hr class="green">
        {{--содержимое письма--}}
        <table class="grid content-table">
            <tr>
                <td>
                    <h1>Поздравляем</h1>
                    <h4>{{ $user->name }},</h4>
                    <span class="gray">
                    Ваш заказ сохранен.
                    Вы можете просмотреть его в личном кабинете или перейти по ссылке ниже.
                </span>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="https://jinnmart.ru/auction/order?order={{ $order->id }}" class="green-button">Перейти к заказу</a>
                </td>
            </tr>
        </table>

        <hr class="green">

        <table class="grid items">
            <tr>
                <td>
                    <h1>Сохранённый заказ</h1>
                    <table class="items-table">
                        <tr>
                            <th>Товар, количество</th>
                            <th>Цена</th>
                        </tr>
                        @foreach($products as $product)
                        <tr>
                            <td class="bold">
                                     {{str_limit($product->name, 30)}}
                            </td>
                            <td class="bold">
                                    {{$product->price}}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>
        {{--содержимое письма конец--}}
        <hr class="green">

        <table class="grid footer-table">
            <tr>
                <td>
                    <span class="light-gray">
                        Вы получили данное сообщение, т.к. имеете соответствющую подписку в своем личном кабинете.
                        Вы всегда можете изменить подписки в личном кабинете или нажав <a href="{{ url('mail/unsubscribe') }}" class="green">отписаться</a> в этом сообщении.
                    </span>
                    <br>
                    <br>
                    <span class="light-gray">
                        ООО «Джинмарт»
                        <br>
                        Все права защищены
                    </span>
                </td>
            </tr>
        </table>

    </div>
{{-- Body here-- }}

{{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent
