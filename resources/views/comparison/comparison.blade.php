@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/cart.index.js') }}"></script>
@endsection
@section('title')
    Сравнение товаров
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.sidebarWrapper')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            <h1>Сравнение товаров</h1>
            <table class="table">
                <tr>
                    <th>Атрибут</th>
                    <th>Первый товар</th>
                    <th>Второй товар</th>
                </tr>
                <tr>
                    <td>Название атрибута</td>
                    <td>Атрибут первого товара</td>
                    <td>Атрибут второго товара</td>
                </tr>
            </table>
        </div>
    </div>
@endsection
