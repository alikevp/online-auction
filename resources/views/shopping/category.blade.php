@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/category.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/slider.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar-filter.css') }}" rel="stylesheet">
@endsection

@section('headScripts')
    <script src="{{ asset('js/jm_v_1.1/plugins/bootstrap-slider.js') }}"></script>
@endsection
@section('footer_scripts')
    <script src="{{ asset('js/jm_v_1.1/page_scripts/catFilters.js') }}"></script>
    <script>
        $("#costSlider").slider().on('slide', function(sliderData){
            $('#sliderOutput1').attr('value', sliderData['value'][0]);
            $('#sliderOutput1').val(sliderData['value'][0]);
            $('#sliderOutput2').attr('value', sliderData['value'][1]);
            $('#sliderOutput2').val(sliderData['value'][1]);
        });
    </script>
@endsection
@section('title')
    Shopping Cart
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    @include('partials.breadcrumbs')
    <div class="col-sm-12">
        <h1>{{ $category->name }}</h1>
    </div>
    <div class="col-sm-12 category-wrapper">
        @include('partials.sidebar')
        @include('partials.products')
    </div>
@endsection