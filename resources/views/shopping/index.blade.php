@extends('layouts.master')

@section('style')
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/main-page.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/mediaqueries/main-page-mediaqueries.css') }}" rel="stylesheet">
@endsection
@section('head_scripts')
@endsection

@section('content')
    <div class="col-md-3 col-sm-6 d-none d-sm-block left-sidebar-wrapper">
        @include('partials.sidebarWrapper')
    </div>
    <div class="col-md-9 col-sm-6 col-xs-12 content-wrapper" id="right-block">
        <div class="serach-wrapper">
            <form class="serachForm" method="get" action="{{ action('ShoppingController@search') }}">
                <div class="input-group">
                    <div class="input-group-btn search-panel">
                        <select class="form-control" id="search-where" name="searchWhere">
                            <option selected>Везде</option>
                            <option>Contains</option>
                        </select>
                    </div>
                    <input name="search"
                           id="search-what"
                           type="text"
                           list="suggestions"
                           class="search-input form-control"
                           @keyup="find"
                           v-model="query"
                           placeholder="Поиск по сайту..."
                           autocomplete="off">
                    <div class="suggestion-div col-sm-5Z">
                        <datalist id="suggestions">
                            <option v-for="suggest in suggestion">
                                {{--<a href="#">--}}
                                @{{ suggest }}
                                {{--</a>--}}
                            </option>
                            {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                        </datalist>
                        {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                    </div>
                    <span class="input-group-btn">
                                    <input class="btn gradient-green" type="submit" id="search-submit" name="searchSubmit" value="Найти" @click="find">
                                </span>
                </div>
            </form>
        </div>
        <div class="col-sm-12 products-wraper">
            <div class="slider-wrapper theme-light">
                <div id="slider" class="nivoSlider">
                    <a href=""><img src="{{ asset('images/slider/banner.jpg') }}" alt="" /></a>
                    <a href=""><img src="{{ asset('images/slider/banner.jpg') }}" alt="" /></a>
                </div>
            </div>
            <div class="products-header">
                <h4>Хиты продаж</h4>
            </div>
            @foreach($products->chunk(4) as $offers)
                <div class="row products-block">
                    @foreach($offers as $product)
                        <div class="col-md-3 col-sm-6 product">
                            <a class="product-link decoration-green" href="{{ url('show', ['id' => $product->id]) }}">
                                <div>
                                    <div class="product-img">
                                        <img src="{{ URL::to(array_first($product->files)) }}">
                                    </div>
                                    <div class="product-name">
                                        <span>{{ $product->name }}</span>
                                    </div>
                                    <div class="product-price">
                                        @if($product->price == "" || $product->price == 0)
                                            <span>Цена не указана</span>
                                        @else
                                            <span>~{{ $product->price }} руб.</span>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@endsection
@section('footer_scropts')
    <script>
        $(document).ready(function(e){
            $('.search-panel .dropdown-menu').find('a').click(function(e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#","");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>

    <script type="text/javascript">
        $(window).on('load', function() {
            $('#slider').nivoSlider({
                effect: 'random', //Specify sets like: 'fold,fade,sliceDown'
                slices: 15,
                animSpeed: 500,
                pauseTime: 3000,
                startSlide: 0,
                pauseOnHover: true,
            })
        });
    </script>
@endsection