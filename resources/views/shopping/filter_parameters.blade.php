{{--<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">--}}
{{--@foreach($attributes as $attribute)--}}
    {{--@if(preg_match("/[a-zа-яё0-9]+/i",$attribute->first()->value) )--}}
        {{--<li class="accordion-item  @if(isset($selected['attributes'][$attribute->first()->slug])) is-active @endif" data-accordion-item>--}}
            {{--<a href="#" class="accordion-title">{{ $attribute->first()->name }}</a>--}}
            {{--<div class="accordion-content" data-tab-content>--}}
                {{--<div class="brand-div">--}}
                    {{--@foreach($attribute as $key=>$at)--}}
                        {{--<div>--}}
                            {{--<input type="checkbox"--}}
                                   {{--id="{{ $at->slug }}_{{ $key }}"--}}
                                   {{--value="{{ $at->value }}"--}}
                                   {{--name="attributes[{{ $at->slug }}][]"--}}
                                   {{--class="filter_parameter filter_parameter_attributes"--}}
                                   {{--data-filter_parameter-slug="{{ $at->slug }}"--}}
                                   {{--@if(isset($selected['attributes'][$at->slug]))--}}
                                    {{--@if(in_array(trim($at->value), array_map('trim', $selected['attributes'][$at->slug]))) checked @endif--}}
                                    {{--@endif--}}
                            {{-->--}}
                            {{--<label for="{{ $at->slug }}_{{ $key }}">--}}
                                {{--{{ trim($at->value) }}--}}
                            {{--</label>--}}
                        {{--</div>--}}
                        {{--<br>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</li>--}}
    {{--@endif--}}
{{--@endforeach--}}
{{--</ul>--}}

@foreach($attributes as $attribute)
    @if(preg_match("/[a-zа-яё0-9]+/i",$attribute->first()->value) )
        <div id="{{ $attribute->first()->slug }}accordion" role="tablist" aria-multiselectable="false">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0">
                        <a data-toggle="collapse" data-parent="#{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}accordion" href="#{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}Collaps" aria-expanded="false" aria-controls="brandCollaps">
                            {{ $attribute->first()->name }}
                        </a>
                    </h5>
                </div>

                <div id="{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}Collaps" class="collapse @if(isset($selected['attributes'][$attribute->first()->slug])) show @endif" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-block scroll">
                        @foreach($attribute as $key=>$at)
                            <input type="checkbox"
                                   id="{{ $at->slug }}_{{ $key }}"
                                   value="{{ $at->value }}"
                                   name="attributes[{{ $at->slug }}][]"
                                   class="filter_parameter filter_parameter_attributes"
                                   data-filter_parameter-slug="{{ $at->slug }}"
                                   @if(isset($selected['attributes'][$at->slug]))
                                   checked @if(in_array(trim($at->value), array_map('trim', $selected['attributes'][$at->slug]))) checked @endif
                                    @endif
                            >
                            <label for="{{ $at->slug }}_{{ $key }}">
                                {{ $at->value }}
                            </label>
                            <br>
                        @endforeach
                        <br>
                    </div>
                    <a href="#" class="green green-dotted float-right" onclick="$('#{{ $attribute->first()->slug }}Collaps').collapse()">Свернуть</a>
                </div>
            </div>
        </div>
    @endif
@endforeach