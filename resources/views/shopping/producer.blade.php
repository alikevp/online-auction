@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/brand-page.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar-filter.css') }}" rel="stylesheet">
@endsection

@section('title')
    {{ $producer  }}
@endsection

@section('content')


    <div class="col-sm-12">
        <h1>Товары от производителя: {{ $producer }}</h1>
    </div>
    <hr>
    <br>

    <div class="brand-wrapper">
        @include('partials.sidebar')
        @include('partials.products')
    </div>

@endsection
