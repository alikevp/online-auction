@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/category.css') }}" rel="stylesheet">
@endsection

@section('title')
    Поиск
@endsection

@section('breadcrumbs')

@endsection

@section('content')


    <div class="row category-wrapper">
		@include('partials.sidebar')
        @if(count($products) > 0 )
            @include('partials.products')
        @else
            @include('partials.itemAddRequestForm')
        @endif
    </div>

@endsection

@section('scripts')
	{{-- <script> --}}
	{{-- 	let urlFilter = "{{ URL::to('api/filter') }}"; --}}
	{{-- 	let category = null; --}}
	{{-- 	let search = "{{ Request::input('search') }}"; --}}
	{{-- 	$('a.revealLink').trigger('click'); --}}
    {{-- </script> --}}
    {{-- <script src="{{ asset('js/filters.js') }}"></script> --}}
@endsection