@foreach($products->chunk(6) as $chunk)
    <div class="row">
        @foreach($chunk as $product)
            <div class="col-sm-4 item">
                <div class="col-xs-12 item-img">
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        <img src="{{ URL::to(array_first($product->files)) }}" class="item-img" alt=""><br>
                    </a>
                </div>
                <div class="col-xs-12">
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        <span class="item-name">{{ $product->name }}</span><br>
                    </a>
                </div>
                <div class="col-xs-12 item-cost">
                    <a href="{{ url('show', ['id' => $product->id]) }}">
                        @if($product->price == "" || $product->price == 0)
                            <span>Цена не указана</span>
                        @else
                            <span>~{{ $product->price }} руб.</span>
                        @endif
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endforeach
<br>
<div class="row">
    {{ $products->appends(Request::only('search'))->links('vendor.pagination.bootstrap-4') }}
</div>