@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/plugins/owlcarousel/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/main-page.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/mediaqueries/main-page-mediaqueries.css') }}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/jm_v_1.1/pages/desktop/productPage.css')}}" rel="stylesheet">

    <link type="text/css" href="{{asset('css/jm_v_1.1/pages/mobile/mediaqueries/show-page-mediaqueries.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/owl.carousel.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.single-foto-gallery').owlCarousel({
            items:1,
            loop:true,
            margin:10,
            URLhashListener:true,
            autoplayHoverPause:true,
            startPosition: 0,
            nav:true,
            navText: ["<i class='fa fa-chevron-left slide-nav slide-previous' aria-hidden='true'></i>","<i class='fa fa-chevron-right slide-nav slide-next' aria-hidden='true'></i>"],
        });
        $('.multi-foto-gallery').owlCarousel({
            items:5,
            loop:false,
            center:false,
            margin:10,
            URLhashListener:true,
            autoplayHoverPause:true,
            startPosition: 0,
            nav:true,
            navText: ["<i class='fa fa-chevron-left slide-nav slide-previous' aria-hidden='true'></i>","<i class='fa fa-chevron-right slide-nav slide-next' aria-hidden='true'></i>"],

        });
        $('.similar-products').owlCarousel({
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:6,
                    nav:true,
                    loop:false
                }
            },
            loop:false,
            URLhashListener:false,
            autoplayHoverPause:true,
            stageClass:'owl-stage row',
            itemClass:'owl-item col-xs-12 product',
            startPosition: 0,
            nav:true,
            navText: ["<i class='fa fa-chevron-left slide-nav slide-previous' aria-hidden='true'></i>","<i class='fa fa-chevron-right slide-nav slide-next' aria-hidden='true'></i>"],
        });
    </script>
@endsection

@section('title')
    {{  $product->name  }}
@endsection

@section('breadcrumbs')


@endsection

@section('content')
    @include('partials.catalogAndSearch')
    @include('partials.breadcrumbs')
    <div class="col-sm-12 page-title">
        <h1>{{$product->name}}</h1>
    </div>
    <div class="col-sm-12 content-wrapper">
        <div class="row product-control-wrapper">
            <div class="col-lg-4 col-md-12 col-sm-12 product-fotos-wrapper">
                <div class="owl-carousel owl-theme single-foto-gallery">
                    @foreach($photos as $key => $photo)
                        @if($key == 0)
                            <div class="item" id="product_foto_0" data-hash="{{ $key }}"><img src="{{ URL::to($photo) }}"></div>
                        @else
                            <div class="item" data-hash="{{ $key }}"><img src="{{ URL::to($photo) }}"></div>
                        @endif
                    @endforeach
                </div>
                <div class="owl-carousel owl-theme multi-foto-gallery">
                    @foreach($photos as $key => $photo)
                        <div class="item" data-hash="{{ $key }}"><a href="#{{ $key }}"><img src="{{ URL::to($photo) }}"></a></div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="product-page-price">
                    @if($product->price == 0)
                        <span class="bold-text">Цена не указана @include('partials.priceTooltip')</span>
                    @else
                        <span class="bold-text">~{{ $product->price }} Руб. @include('partials.priceTooltip')</span>
                    @endif
                </div>
                <div class="product-page-rating">
                    <div class="star-rating">
                        <div class="star-rating__wrap">
                            <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled @if($rating == 5) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="Отлично"></label>
                            <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" disabled @if($rating == 4) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="Хорошо" ></label>
                            <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled @if($rating == 3) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="Приемлимо"></label>
                            <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled @if($rating == 2) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="Плохо"></label>
                            <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled @if($rating == 1) checked="checked" @endif>
                            <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="Очень плохо"></label>
                        </div>
                    </div>
                    <a href="#recall" class="recalls-link grey-text decoration-green">Отзывов: {{ count($comments) }}</a>
                </div>
                <div class="product-buttons">
                    <a href="#" class="btn gradient-green width-50 in_cart_add" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                    <br><br>
                    <a href="#" class="btn border-green-btn width-50 fast_order" data-fast-order-prod-id="{{ $product->id }}" data-fast-order-url="/fast_order">НАЧАТЬ АУКЦИОН</a>
                    <br><br>
                    <a href="#" class="btn border-red-btn width-50 deferred_add" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_add"><i class="fa fa-heart-o" aria-hidden="true"></i>Отложить</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="col-sm-12 columns brand-div text-center">
                    <img src="/images/catalog/producer/{{ $product->producer }}.gif" alt="brand" id="brand-img"><br>
                    <a href="/brand/{{ $product->producer  }}" class="flash-link">Все товары бренда</a>
                </div>
            </div>
        </div>

        <div class="row product-tabs-wrapper">
            <ul class="col-sm-12 nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active regular-text" href="#desc" role="tab" data-toggle="tab">Описание</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link regular-text" href="#attr" role="tab" data-toggle="tab">Характеристики</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link regular-text" href="#recall" role="tab" data-toggle="tab">Отзывы <span class="value">{{ count($comments) }}</span></a>
                </li>
            </ul>
            <div class="col-sm-12 tab-content">
                <div role="tabpanel" class="tab-pane active" id="desc">
                    <div class="col-sm-12">
                        <h4>Коротко о товаре:</h4>
                        @if($product->description != '')
                            @php
                                $description = str_replace("<div>", "", $product->description);
                                $description = str_replace("</div>", "", $description);
                            @endphp
                            <p>{!! $description !!}</p>
                        @else
                            <p>Описание отсутствует</p>
                        @endif
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="attr">
                    @foreach($product->attributes as $attribute)
                        <span class="product-attr">
                            <span class="product-attr-name grey-text bold-text">{{ $attribute->name }}:</span>
                            <span class="product-attr-value">{{ $attribute->value  }}</span>
                        </span>
                        <br>
                    @endforeach
                </div>
                <div role="tabpanel" class="tab-pane" id="recall">
                    @include('partials.commentsOutput')
                </div>
            </div>
        </div>
    </div>
    <div class="products-similar-products">
        <div class="products-header">
            <h4>Похожие товары</h4>
        </div>
        <div class="row owl-carousel similar-products">
            @foreach($similarProducts as $similarProduct)
                    <a class="product-link decoration-green" href="{{ url('show', ['id' => $similarProduct->id]) }}">
                        <div>
                            <div class="product-img">
                                <img src="{{ URL::to(array_first($similarProduct->files)) }}">
                            </div>
                            <div class="product-name">
                                <span>{{ $similarProduct->name }}</span>
                            </div>
                            <div class="product-price">
                                @if($similarProduct->price == "" || $similarProduct->price == 0)
                                    <span>Цена не указана</span>
                                @else
                                    <span>~{{ $similarProduct->price }}</span> <span class="currency">руб.</span>
                                @endif
                            </div>
                        </div>
                    </a>
            @endforeach
        </div>
    </div>
@endsection