<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
    @foreach($categories as $category)
        @if($category->isLeaf())
            <li>
                <a href="{{ route('category', $category->id) }}" class="bold-text regular-text decoration-green">{{ $category->name }}</a>
            </li>
        @else
            <li class="dropdown-submenu">
                <a tabindex="-1" href="{{ route('category', $category->id) }}" class="bold-text regular-text decoration-green">{{ $category->name }}</a>
                <ul class="dropdown-menu">
                    @include('partials.category', ['categories' => $category->children])
                </ul>
            </li>
        @endif
    @endforeach
</ul>