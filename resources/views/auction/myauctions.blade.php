@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
	<link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Подробности аукциона
@endsection

@section('content')
    @include('partials.sidebarShopWrapper')

    <div class="col-sm-9" id="right-block">
        @if (session('message') !== null)
            <div class="col-sm-12">
                <div class="success callout" data-closable="slide-out-right">
                    <h5>Успешно.</h5>
                    <p>Предложение успешно сделано.</p>
                    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        @if (count($orders)>0)
            <div class="col-sm-12 orders-wraper" id="items-table-block">
                <div class="row">
                    @foreach ($orders as $order)
                        <div class="col-sm-12 order-block">
                            <div class="row">
                                <div class="col-sm-2 order-column order-logo">
                                    @if(isset($order->product->files[0]))
                                        <img src="../{{ $order->product->files[0] }}" alt="" class="order-photo">
                                    @endif
                                </div>
                                <div class="col-sm-7 order-column order-details">
                                    <div class="order-title">
                                        <span>{{ $order->product->name }}</span>
                                    </div>
                                    <div class="order-description">
                                        <div>№ заказа: <span>{{ $order->id }}</span></div>
                                        <div>Производитель: <span>{{ $order->product->producer }}</span></div>
                                        <div>Количество: <span>{{ $order->quantity }}</span></div>
                                        <div>Активен до: <span>{{ $order->active_till }}</span></div>
                                        @if(null !== $order->delivery)
                                            <div>Город: <span>{{$order->delivery->city}}</span></div>
                                            <div>Тип доставки:
                                                @foreach($order->delivery->types as $type)
                                                    @if($type->slug == 'delivery')
                                                        <span>Доставка.</span>
                                                    @elseif($type->slug == 'pickup')
                                                        <span>Самовывоз.</span>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @else
                                            <div>Доставка: <span>Не указана</span></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-3 order-column order-actions">
                                    <?php $my_bet = $order->order_offer_groups->where(
                                        'shop_id',
                                        Auth::user()->connection_id
                                    )->first();?>
                                    <div class="actions-offers">
                                        Предложений: <span>{{ count($order->order_offer_groups) }}</span>
                                    </div>
                                    <div class="actions-best-price">
                                        {{--{{dd(Auth::user()->connection_id)}}--}}
                                        {{--{{dd($order->order_offer_groups)}}--}}
                                        @if(count($order->order_offer_groups)>1)
                                            @if($order->order_offer_groups->last()->shop_id == Auth::user()->connection_id)
                                                Ваша лучшая цена:
                                                <span>{{ $order->order_offer_groups->min('price') }}</span>
                                            @else
                                                Лучшая цена:
                                                <span>{{ $order->order_offer_groups->min('price') }}</span>
                                            @endif

                                        @else
                                            @if($order->order_offer_groups->last()->shop_id == Auth::user()->connection_id)
                                                Ваша лучшая цена:
                                                <span>{{ $order->order_offer_groups->first()->price }}</span>
                                            @else
                                                Лучшая цена:
                                                <span>{{ $order->order_offer_groups->first()->price }}</span>
                                            @endif



                                        @endif
                                    </div>
                                    <div class="hidden-sm actions-offers">
                                        @if(($order->status == 1 OR $order->status == 2) && $my_bet->status == 0)
                                        @elseif($my_bet->status == 1)
                                            <div class="acepted">Победа!</div>
                                        @elseif(($order->status !== 1 OR $order->status !== 2))
                                        @endif
                                    </div>
                                    <div class="actions-buttons">
                                        <a href="/auction/order?order={{ $order->id }}"
                                           class="btn gradient-green">Подробности</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-sm-12">
                <span>Заказов с указанными параметрами не найдено.</span>
            </div>
        @endif
        <hr>
        {{ $orders->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
@endsection