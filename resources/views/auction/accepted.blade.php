@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-auctions-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
@endsection
@section('title')
    Ваши принятые аукционы.
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.sidebarShopWrapper')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            @if (count($orders)>0)
                <div class="medium-12 columns" id="buy-steps-block">
                    <div class="medium-12 columns">
                        <h3>Мои выигранные аукционы</h3>
                        <hr>
                    </div>
                </div>
                <div class="medium-12 columns orders-wraper" id="items-table-block">
                    <div class="row">
                        @foreach ($orders as $order)
                            <div class="medium-12 columns order-block">
                                <div class="row">
                                    <div class="medium-2 columns order-column order-logo">
                                        @if(isset($order->product->files[0]))
                                            <img src="../{{ $order->product->files[0] }}" alt="" class="order-photo">
                                        @endif
                                    </div>
                                    <div class="medium-7 columns order-column order-details">
                                        <div class="order-title">
                                            <span>{{ $order->product->name }}</span>
                                        </div>
                                        <div class="order-description">
                                            <div>№ заказа: <span>{{ $order->id }}</span></div>
                                            <div>Производитель: <span>{{ $order->product->producer }}</span></div>
                                            <div>Количество: <span>{{ $order->quantity }}</span></div>
                                            <div>Активен до: <span>{{ $order->active_till }}</span></div>
                                            <div>Город: <span>{{$order->delivery->city}}</span></div>
                                            <div>Тип доставки:
                                                @foreach($order->delivery->types as $type)
                                                    @if($type->slug == 'delivery')
                                                        <span>Доставка.</span>
                                                    @elseif($type->slug == 'pickup')
                                                        <span>Самовывоз.</span>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="medium-3 columns order-column order-actions">
                                        <?php $my_bet = $order->order_offer_groups->where('shop_id', Auth::user()->connection_id)->first();?>
                                        <div class="actions-offers">
                                            Предложений: <span>{{ count($order->order_offer_groups) }}</span>
                                        </div>
                                        <div class="actions-best-price">
                                            @if(count($order->order_offer_groups)>1)
                                                Лучшая цена: <span>{{ $order->order_offer_groups->min('price') }}</span>
                                            @else
                                                Лучшая цена: <span>{{ $order->order_offer_groups->first()->price }}</span>
                                            @endif
                                        </div>
                                        <div class="actions-offers hide-for-medium-only hide-for-small-only">
                                            @if(($order->status == 1 OR $order->status == 2) && $my_bet->status == 0)
                                            @elseif($my_bet->status == 1)
                                                <div class="acepted">Победа!</div>
                                            @elseif(($order->status !== 1 OR $order->status !== 2))
                                            @endif
                                        </div>
                                        <div class="actions-buttons">
                                            <a href="/auction/order?order={{ $order->id }}"
                                               class="button common-button">Подробности</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="medium-12 columns" >
                    <span>Заказов с указанными параметрами не найдено.</span>
                </div>
            @endif
            <hr>
            {{ $paging->links('vendor.pagination.foundation') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
@endsection