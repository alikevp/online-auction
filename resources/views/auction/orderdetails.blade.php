@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/auction-details-page.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">

@endsection

@section('footer_scripts')
    {{--<script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>--}}
    <script src="{{ asset('js/jm_v_1.1/auctionFormAdd.js') }}"></script>
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
    <script src="{{ asset('js/addBetConfirm.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection
@section('title')
    Подробности аукциона
@endsection

@section('content')
    <div class="col-sm-12 page-title">
        <h1>Подробности аукциона</h1>
    </div>
    @include('partials.sidebarShopWrapper')

    <div class="col-sm-9" id="right-block">
        <div class="col-sm-12">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="col-xs-12">
                        <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p>{{ Session::get('alert-' . $msg) }}</p>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="row">
                <div class="col-sm-12"><h3>{{ $orders->product->name }}</h3></div>
                <div class="col-sm-6 shadow-block">
                    @if(count($my_bets)>0)
                        @if(count($bets)>0)
                            @if($my_bets->price < $bets->first()->price)
                                <div class="order-info-block order-best-price">Ваша цена лучшая: <span> {{ $my_bets->price }} &#8381;</span></div>

                            @else
                                <div class="order-info-block order-best-price">Лучшая цена: <span> {{ $bets->first()->price }} &#8381;</span></div>
                            @endif
                        @else

                            <div class="order-info-block order-best-price">Ваша цена лучшая: <span> {{ $my_bets->price }}
                                    &#8381;</span></div>
                        @endif
                    @else
                        @if(count($bets)>0)
                            <div class="order-info-block order-best-price">Лучшая цена: <span> {{ $bets->first()->price }} &#8381;</span></div>
                        @else
                            <div class="order-info-block order-best-price">Лучшая цена: <span> Ставок пока нет</span></div>
                        @endif
                    @endif
                    <div class="order-info-block order-status">Статус аукциона:
                        <span>
                            @if($orders->status == 0)
                                Отменен
                            @elseif($orders->status == 1)
                                Активен
                            @elseif($orders->status == 2)
                                Истек
                            @elseif($orders->status == 3)
                                Принят
                            @endif
                        </span>
                    </div>
                </div>
                <div class="col-sm-6 shadow-block">
                    @if(null !== $orders->delivery)
                        @foreach($orders->delivery->types as $type)
                            @if($type->slug == 'delivery')
                                <div class="order-info-block order-city">
                                    Доставка:
                                    <button type="button" class="flash-link" data-toggle="modal" data-target="#delyveryModal{{ $orders->delivery->id }}">
                                        Показать
                                    </button>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="delyveryModal{{ $orders->delivery->id }}" tabindex="-1" role="dialog" aria-labelledby="delyveryModal{{ $orders->delivery->id }}Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Доставка</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <span><span class="bold-text">Город:</span> <span class="grey-text">{{ $orders->delivery->city }}</span></span>
                                                <br>
                                                <span><span class="bold-text">Улица:</span> <span class="grey-text">{{ $orders->delivery->street }}</span></span>
                                                <br>
                                                <span><span class="bold-text">Дом:</span> <span class="grey-text">{{ $orders->delivery->house }}</span></span>
                                                <br>
                                                @if($orders->status == 3)
                                                        <span><span class="bold-text">Квартира:</span> <span class="grey-text">{{ $orders->delivery->apartment }}</span></span>
                                                @else
                                                    <span><span class="bold-text">Квартира стрыта @include('partials.auctionEndTooltip')</span></span>
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Закрыть</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($type->slug == 'pickup')
                                <div class="order-info-block order-city">Самовывоз: <span>{{ $orders->delivery->city }}</span></div>
                            @endif
                        @endforeach
                    @else
                        <div>Доставка: <span>Не указана</span></div>
                    @endif
                    <div class="order-info-block order-start">Начало аукциона:
                        <span>{{ $orders->created_at->formatLocalized('%d.%m.%Y, %H:%M') }}</span>
                    </div>
                    <div class="order-info-block order-finish">До окончания осталось:
                        @if($orders->status == 1)
                            <span>{{ $time_last }}</span>
                        @else
                            <span> 0 </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="row">
                @if($orders->status == 3)
                    <h3 class="order-title">Данные покупателя:</h3>
                    <div class="col-sm-12 orders-wraper" id="items-table-block">
                        <div class="row">
                            <table class="table personal-data-table" id="user-info">
                                <tr>
                                    <td><span>ФИО:</span></td>
                                    <td><span>
									@if(isset($user->surname))
										{{ $user->surname }} {{ $user->name }} {{ $user->patronymic }}
									@else
										Пользователь удален либо еще не создан
									@endif
									</span></td>
                                </tr>
                                <tr>
                                    <td><span>E-mail: </span></td>
                                    <td>
                                        <span>
										@if(isset($user->e_mail))
											{{ $user->e_mail }}
										@else
											Информация отсутствует
										@endif
										</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Дата рождения:</span></td>
                                    <td><span>
										@if(isset($user->date_of_birth))
											{{ $user->date_of_birth }}
										@else
											Информация отсутствует
										@endif	
									</span></td>
                                </tr>                            <tr>
                                    <td><span>Пол:</span></td>
                                    <td>
                                    <span>
										@if(isset($user->sex))
											@if($user->sex == "m")
												Мужской
											@elseif($user->sex == "f")
												Женский
											@else
												Не указан
											@endif
										@else
											Информация отсутствует
										@endif
                                    </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Телефон:</span></td>
                                    <td><span>
										@if(isset($user->telefone))
											{{ $user->telefone }}
										@else
											Информация отсутствует
										@endif
									</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                @else
                    <h3 class="order-title">Покупатель скрыт. @include('partials.auctionEndTooltip')</h3>
                @endif
            </div>
            <div class="row">
                @if(count($my_bets)>0)
                    <h3 class="order-title">Моя ставка:</h3>
                    <div class="col-sm-12 orders-wraper" id="items-table-block">
                        <div class="row">
                            <div class="col-sm-12 order-block">
                                <div class="row">
                                    <div class="col-sm-2 order-column order-logo">
                                        {{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
                                    </div>
                                    <div class="col-sm-7 order-column order-details">
                                        <div class="order-title">
                                            <span>{{ $my_bets->shops->first()->name }}</span>
                                        </div>
                                        <div class="order-description">
                                            <div>Предложение выставлено: <span>{{ $my_bets->created_at }}</span></div>

                                            @if(count($my_bets->deliveries)>0)
                                                <div>Доставка:
                                                    <span>
                                                    {{ count($my_bets->deliveries) }} вариант(а/ов). Самый быстрый - {{ $my_bets->deliveries->min('delivery_time')  }} дней.
                                                </span>
                                                </div>
                                                <!-- Button trigger modal -->
                                                <button type="button" class="flash-link" data-toggle="modal" data-target="#offerDeliveres-1">
                                                    (Подробнее)
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="offerDeliveres-1" tabindex="-1" role="dialog" aria-labelledby="offerDeliveres-1Label" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="offerDeliveres-1Label">Доставки</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table items-table unstriped">
                                                                    <tr>
                                                                        <th>Тип</th>
                                                                        <th>Срок доставки (дней)</th>
                                                                        <th>Подробности</th>
                                                                    </tr>
                                                                    @foreach ($my_bets->deliveries as $delivery)
                                                                        <tr>
                                                                            <td>{{ $delivery->type }}</td>
                                                                            <td>{{ $delivery->delivery_time }}</td>
                                                                            <td>
                                                                                <button type="button" class="flash-link" data-toggle="modal" data-target="#offerDeliveriesDetails-{{ $delivery->id }}">
                                                                                    (Подробности)
                                                                                </button>

                                                                                <!-- Modal -->
                                                                                <div class="modal fade" id="offerDeliveriesDetails-{{ $delivery->id }}" tabindex="-1" role="dialog" aria-labelledby="offerDeliveriesDetails-{{ $delivery->id }}Label" aria-hidden="true">
                                                                                    <div class="modal-dialog" role="document">
                                                                                        <div class="modal-content">
                                                                                            <div class="modal-header">
                                                                                                <h5 class="modal-title" id="offerDeliveriesDetails-{{ $delivery->id }}Label">Подробности доставки:</h5>
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                    <span aria-hidden="true">&times;</span>
                                                                                                </button>
                                                                                            </div>
                                                                                            <div class="modal-body">
                                                                                                <div class="col-sm-12">
                                                                                                    <div>
                                                                                                        {{ $delivery->description }}
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="modal-footer">
                                                                                                <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Закрыть</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Закрыть</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @else
                                                <div>Доставки: <span>нет</span>
                                                </div>
                                            @endif
                                            @if(count($my_bets->gifts)>0)
                                                <div>Подарки:
                                                    <span>{{ count($my_bets->gifts) }} вариант(а/ов)
                                                        <!-- Button trigger modal -->
                                                    <button type="button" class="flash-link" data-toggle="modal" data-target="#offerGifts-1">
                                                        (Подробнее)
                                                    </button>
                                                </span>
                                                </div>

                                                <!-- Modal -->
                                                <div class="modal fade" id="offerGifts-1" tabindex="-1" role="dialog" aria-labelledby="offerGifts-1Label" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="offerGifts-1Label">Подробности доставки:</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="col-sm-12">
                                                                    <table class="items-table table unstriped">
                                                                        <tr>
                                                                            <th>Тип</th>
                                                                            <th>Название</th>
                                                                            <th>Описание</th>
                                                                        </tr>
                                                                        @foreach ($my_bets->gifts as $gift)
                                                                            <tr>
                                                                                <td>{{ $gift->type }}</td>
                                                                                <td>{{ $gift->name }}</td>
                                                                                <td>{{ $gift->description }}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Закрыть</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div>Подарки:
                                                    <span>нет</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-3 order-column order-actions">
                                        <div class="order-info-block shop-rate">Рейтинг:
                                            <span>
                                                {{ $my_bets->shops->first()->offerrate }} @include('partials.auctionsCountTooltip')
                                                /
                                                {{ $my_bets->shops->first()->dealrate }} @include('partials.salesCountTooltip')
                                            </span>
                                        </div>
                                        <div class="order-info-block shop-comments">Отзывы: <span>{{ $my_bets->shops->first()->commentcount }} <a href="/shopdetails?shop={{ $my_bets->shops->first()->id }}#recall/"
                                                                                                                                                  class="flash-link">Читать</a> </span>
                                        </div>
                                        <div class="order-info-block shop-comments">Цена: <span>{{ $my_bets->price }} &#8381;</span>
                                            <div class="order-info-block shop-comments">Старая цена: <span>{{ $my_bets->old_price }} &#8381;</span>
                                            </div>
                                        </div>
                                        <div class="actions-buttons">
                                            @if(($orders->status == 1 OR $orders->status == 2) && $my_bets->status == 0)
                                                <a href="/auction/order/editbet?offergroup={{ $my_bets->id }}"
                                                   class="btn gradient-green width-100">Изменить ставку</a>
                                            @elseif($my_bets->status == 1)
                                                <div class="acepted">Принято</div>
                                            @elseif(($orders->status !== 1 OR $orders->status !== 2))
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h3>Ваших предложений нет.</h3>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn gradient-green width-100" data-toggle="modal" data-target="#add-bet">
                        Сделать предложение
                    </button>
                    @if($shop->activity)
                        <!-- Modal -->
                        <div class="modal fade" id="add-bet" tabindex="-1" role="dialog" aria-labelledby="add-betLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="add-betLabel">ПАРАМЕТРЫ СТАВКИ</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="/auction/order/savebet" id="newBetForm" method="post" data-abide novalidate>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <div id="accordionItems" role="tablist" aria-multiselectable="true">
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                                    <a data-toggle="collapse" data-parent="#accordionItems" href="#collapseItems" aria-expanded="true" aria-controls="collapseItems">
                                                                        Список товаров
                                                                    </a>
                                                                </h5>
                                                            </div>
                                                            <div id="collapseItems" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-block">
                                                                    <table class="table items-table unstriped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>№</th>
                                                                            <th>Товары</th>
                                                                            <th>Количество</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>
                                                                                <div class="col-sm-3">
                                                                                    <img class="order-photo"
                                                                                         src="{{ url(array_first($orders->product->files)) }}"
                                                                                         alt="item image">
                                                                                </div>
                                                                                <div class="col-sm-9">{{ $orders->product->name }}</div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="col-sm-9">{{ $orders->quantity }}</div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>Количество <span class="red-star">*</span>
                                                        <input type="number" class="form-control" name="quantity" min="{{ $orders->quantity }}" max="{{ $orders->quantity }}"
                                                               placeholder="Количество товара" required value="{{ $orders->quantity }}">
                                                    </label>
                                                    <label>Цена <span class="red-star">*</span>@if(count($bets)>0) (Лучшее предложение -- {{ $bets->first()->price }} &#8381;) @endif
                                                        <input type="text" class="form-control" name="price" placeholder="Сумма к оплате"
                                                               pattern="[0-9]+(\.[0-9]{0,2})?%?" required>
                                                    </label>
                                                    <span class="gifts-info">Доставки</span>
                                                    <span class="form-add-button float-right flash-link" id="delivery-add-button">Добавить доставку</span>
                                                    <hr>
                                                    <div class="row forms-add" id="delivery-form">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <span class="gifts-info">Подарки</span>
                                                    <span class="form-add-button float-right flash-link" id="gift-add-button">Добавить подарок</span>
                                                    <hr>
                                                    <div class="row forms-add" id="gifts-form">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 modal-footer">
                                                <div class="col-sm-12">
                                                    <input class="hidefield" hidden="hidden" type="hidden" name="order_id"
                                                           value="{{ $orders->id }}">
                                                    <input class="hidefield" hidden="hidden" type="hidden"
                                                           name="orderitem_id" value="">
                                                    <input class="hidefield" hidden="hidden" type="hidden"
                                                           name="product_id" value="{{ $orders->product->id }}">
                                                    <input class="hidefield" hidden="hidden" type="hidden" name="items_count"
                                                           value="1">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                </div>
                                            </div>
                                            <div class="alert alert-danger alert-dismissible fade" id="validation-error-wrapper" role="alert">
                                                <button id="validation-error-wrapper-close" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div id="validation-error"></div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" id="newBetFormSubmit"  style="display: none;"></button>
                                        <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отмена</button>
                                        <button type="button" class="btn gradient-green width-50" id="openConfirmButton">
                                            Отправить предложение
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="modal fade" id="openConfirm" tabindex="-1" role="dialog" aria-labelledby="openConfirmLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="#openConfirmLabel">Вы действительно хотите сделать предложение?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>После подтверждения Ваше предложение появится в списке предложений заказа.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn gradient-green width-50" id="addBetConfirmButton">СОГЛАСЕН</button>
                                            <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @else
                    <!-- Modal -->
                        <div class="modal fade" id="add-bet" tabindex="-1" role="dialog" aria-labelledby="add-betLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="add-betLabel">ПРЕДУПРЕЖДЕНИЕ</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <span>
                                        Дождитесь проверки документов и активации Вашего магазина.
                                        <br><br>
                                        Если Вы еще не выслали необходимый пакет документов - сделайте это как можно быстрее.
                                        <br><br>
                                        Для активации Вам необходимо выслать на элекронную почту documents@jinnmart.ru следующий пакет документов:
                                        <br><br>
                                        1. Юридические лица
                                        <br><br>
                                        <ul>
                                            <li>ИНН,</li>
                                            <li>ОГРН,</li>
                                            <li>УСТАВ -1,2 и последняя страница с печатями,</li>
                                            <li>приказ о назначении директора,</li>
                                            <li>протокол собрания учредителей,</li>
                                            <li>карта партнера</li>
                                        </ul>

                                        2. Индивидуальные предприниматели
                                        <br><br>
                                        <ul>
                                            <li>ИНН,</li>
                                            <li>ОГРНИП,</li>
                                            <li>ПАСПОРТ 1 стр и прописка</li>
                                        </ul>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
            <div class="row">
                @if(count($bets)>0)
                    <h3 class="order-title">Ставки конкурентов:</h3>
                    <div class="col-sm-12 orders-wraper" id="items-table-block">
                        <div class="row">
                            @foreach ($bets as $bet)
                                <div class="col-sm-12 order-block">
                                    <div class="row">
                                        <div class="col-sm-2 order-column order-logo">
                                            {{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
                                        </div>
                                        <div class="col-sm-7 order-column order-details">
                                            <div class="order-title">
                                                @if($orders->status == 3)
                                                    <span>{{ $bet->shops->first()->name }}</span>
                                                @else
                                                    <span>Название магазина скрыто @include('partials.auctionEndTooltip')
                                                @endif
                                            </div>
                                            <div class="order-description">
                                                <div>Предложение выставлено: <span>{{ $bet->created_at }}</span></div>

                                                @if(count($bet->deliveries)>0)
                                                    <div>
                                                        Доставка:
                                                        <span>{{ count($bet->deliveries) }} вариант(а/ов). Самый быстрый - {{ $bet->deliveries->min('delivery_time')  }} дней.
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="flash-link" data-toggle="modal" data-target="#offerDeliveres-1">
                                                              (Подробнее)
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="offerDeliveres-1" tabindex="-1" role="dialog" aria-labelledby="offerDeliveres-1Label" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="offerDeliveres-1Label">Доставки:</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <table class="table items-table unstriped">
                                                                        <tr>
                                                                            <th>Тип</th>
                                                                            <th>Срок доставки (дней)</th>
                                                                            <th>Подробности</th>
                                                                        </tr>
                                                                        @foreach ($bet->deliveries as $delivery)
                                                                            <tr>
                                                                                <td>{{ $delivery->type }}</td>
                                                                                <td>{{ $delivery->delivery_time }}</td>
                                                                                <td>
                                                                                    <button type="button" class="flash-link" data-toggle="modal" data-target="#offerDeliveriesDetails-{{$delivery->id}}">
                                                                                        (Подробности)
                                                                                    </button>
                                                                                    <!-- Modal -->
                                                                                    <div class="modal fade" id="offerDeliveriesDetails-{{$delivery->id}}" tabindex="-1" role="dialog" aria-labelledby="offerDeliveriesDetails-{{$delivery->id}}Label" aria-hidden="true">
                                                                                        <div class="modal-dialog" role="document">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <h5 class="modal-title" id="offerDeliveriesDetails-{{$delivery->id}}Label">Подробности доставки:</h5>
                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                        <span aria-hidden="true">&times;</span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div>
                                                                                                        {{ $delivery->description }}
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div>Доставки: <span>нет</span>
                                                    </div>
                                                @endif
                                                @if(count($bet->gifts)>0)
                                                    <div>
                                                        Подарки:
                                                        <span>{{ count($bet->gifts) }} вариант(а/ов)
                                                            <button type="button" class="flash-link" data-toggle="modal" data-target="#offerGifts-1">
                                                              (Подробнее)
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="offerGifts-1" tabindex="-1" role="dialog" aria-labelledby="offerGifts-1Label" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="offerGifts-1Label">Подарки:</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="col-sm-12">
                                                                        <span>Подарки:</span>
                                                                        <table class="table items-table unstriped">
                                                                            <tr>
                                                                                <th>Тип</th>
                                                                                <th>Название</th>
                                                                                <th>Описание</th>
                                                                            </tr>
                                                                            @foreach ($bet->gifts as $gift)
                                                                                <tr>
                                                                                    <td>{{ $gift->type }}</td>
                                                                                    <td>{{ $gift->name }}</td>
                                                                                    <td>{{ $gift->description }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn gradient-green width-50" data-dismiss="modal">Закрыть</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div>Подарки: <span>нет</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3 order-column order-actions">
                                            <div class="order-info-block shop-rate">Рейтинг:
                                                <span>
                                                        {{ $bet->shops->first()->offerrate }} @include('partials.auctionsCountTooltip')
                                                        /
                                                    {{ $bet->shops->first()->dealrate }} @include('partials.salesCountTooltip')
                                                    </span>
                                            </div>

                                            @if($orders->status == 3)
                                                <div class="order-info-block shop-comments">Отзывы: <span>{{ $bet->shops->first()->commentcount }} <a href="{{url('shopdetails')}}?shop={{ $bet->shops->first()->id }}#recall" class="flash-link">Читать</a> </span></div>
                                            @else
                                                <div class="order-info-block shop-comments">Отзывы скрыты @include('partials.auctionEndTooltip')</div>
                                            @endif

                                            <div class="order-info-block shop-comments">Цена: <span>{{ $bet->price }} &#8381;</span>
                                            </div>
                                            <div class="order-info-block shop-comments">Старая цена: <span>{{ $bet->old_price }} &#8381;</span>
                                            </div>
                                            <div class="actions-buttons">
                                                @if(($orders->status == 1 OR $orders->status == 2) && $bet->status == 0)
                                                @elseif($bet->status == 1)
                                                    <div class="acepted">Принято</div>
                                                @elseif(($orders->status !== 1 OR $orders->status !== 2))
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
            </div>
                @else
                    <h3>Ставок конкурентов пока нет.</h3>
                @endif
            </div>
    </div>
@endsection
