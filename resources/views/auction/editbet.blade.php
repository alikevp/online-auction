@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
	<link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-editbet-page.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('head_scripts')
    <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
    <script src="{{ asset('js/auctionFormAdd.js') }}"></script>
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
    <script src="{{ asset('js/editBetConfirm.js') }}"></script>
@endsection
@section('title')
	Изменение предложения
@endsection

@section('content')
	<div class="col-sm-12 page-title">
		<h1>Изменение предложения</h1>
	</div>
        @include('partials.sidebarShopWrapper')

        <div class="col-sm-9" id="right-block">
            @if (count($my_bet)>0)
                <div class="col-sm-12" id="buy-steps-block">
                    <div class="col-sm-12">
                        <a class="flash-link" href="/auction/order?order={{ $my_bet->order_id }}">Назад</a>
                    </div>
                </div>
                <div class="col-sm-12">
                    @if (session('message') !== null)
                        <div class="success callout" data-closable="slide-out-right">
                            <h5>Успешно.</h5>
                            <p>Предложение успешно изменено.</p>
                            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="/auction/order/saveeditedbet" method="post" id="editBetForm" data-abide novalidate>
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									@if(null !== $orders->delivery)
									<span class="item-article">Город доставки: {{$orders->delivery->first()->city}}</span><br>
									@else
										<span class="item-article">Доставка: Не указана</span><br>
									@endif
									<span class="item-article">Начало аукциона: {{ $orders->created_at }}</span><br>
									<span class="item-color">Окончание аукциона: {{ $orders->active_till }}</span><br>
								</div>
								<div class="col-sm-6">
									<label>Цена(без учёта доставки) <span class="red-star">*</span>
										<input type="number" name="price" max="{{ $my_bet->price }}" placeholder="Сумма к оплате" required value="{{ $my_bet->price }}" class="form-control">
									</label>
								</div>
							</div>
						</div>
						<div class="col-sm-12" style="padding-top:20px;">
						<a class="flash-link" data-toggle="collapse" href="#collapseItems" aria-expanded="false" aria-controls="collapseItems">Список товаров</a>
						<div class="collapse" id="collapseItems">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>№</th>
									<th>Товары</th>
									<th>Количество</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>1</td>
									<td>
										<div class="row">
											<div class="col-sm-3"><img class="order-photo" src="{{ url(array_first($orders->product->files)) }}" alt="item image" style="width:150px;"></div>
											<div class="col-sm-9">{{ $orders->product->name }}</div>
										</div>
									</td>
									<td><input type="number" name="quantity" min="{{ $orders->quantity }}" max="{{ $orders->quantity }}"
											   placeholder="Количество товара" required value="{{ $orders->quantity }}">
									</td>
								</tr>
								<input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $orders->product->id }}][product_id]" value="{{ $orders->product->id }}">
								</tbody>
							</table>
						</div><br><br>
                        </div>
                        <div class="col-sm-12">
							<h3 class="order-title">Доставки <a class="form-add-button flash-link" data-toggle="modal" data-target="#deliveryAdd">Добавить доставку</a></h3>
                            @if(count($my_bet->deliveries)>0)
                                <table class="items-table unstriped">
                                    <tr>
                                        <th>№</th>
                                        <th>Тип</th>
                                        <th>Срок</th>
                                        <th>Подробности</th>
                                        <th>Действие</th>
                                    </tr>
                                    @php($i = 1)
                                    @foreach ($my_bet->deliveries as $delivery_item)
                                        <tr id="delivery_row_{{ $delivery_item->id }}">
                                            <td>{{ $i }}</td>
                                            <td>
                                                <?php
                                                $delivery_type_arr = array();
                                                $delivery_type_arr[] = 'Самовывоз';
                                                $delivery_type_arr[] = 'Доставка';
                                                ?>
                                                <select style="height: 50px;width: 150px;" name="delivery[{{  $delivery_item->id }}][type]">
                                                    <option value="">Тип</option>
                                                    @foreach ($delivery_type_arr as $delivery_type)
                                                        <option  @if ($delivery_item->type == $delivery_type) selected @endif value="{{ $delivery_type }}">{{ $delivery_type }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input style="height: 50px;width: 50px;" type="number" min="0" name="delivery[{{ $delivery_item->id }}][delivery_time]" placeholder="Срок доставки в днях" required value="{{ $delivery_item->delivery_time }}">
                                            </td>
                                            <td>
                                                <textarea style="height: 50px;width: 500px;" name="delivery[{{ $delivery_item->id }}][description]" placeholder="Детально опишите свои условия доставки (Транспортные компании, сроки, стоимость и т.д.)" required>{!! $delivery_item->description !!}</textarea>
                                            </td>
                                            <td><a class="button delete-button common-button button-decline" onclick="deliveryDelete(this, '{{ $delivery_item->id }}')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                        </tr>

                                        <input class="hidefield" hidden="hidden" type="hidden" name="delivery[{{ $delivery_item->id }}][delivery_id]" value="{{ $delivery_item->id }}">
                                        @php($i++)
                                    @endforeach
                                </table>
                            @else
                                <p>Доставки не указаны</p>
                            @endif
                            <br>
                            <h3 class="order-title">Подарки <a class="form-add-button flash-link" data-toggle="modal" data-target="#giftAdd">Добавить подарок</a></h3>
                            @if(count($my_bet->gifts)>0)
                                <table class="items-table unstriped">
                                    <tr>
                                        <th>№</th>
                                        <th>Тип</th>
                                        <th>Название</th>
                                        <th>Описание</th>
                                        <th>Действие</th>
                                    </tr>
                                    @php($i = 1)
                                    @foreach ($my_bet->gifts as $option_item)
                                        <tr  id="gift_row_{{ $option_item->id }}">
                                            <td>{{ $i }}</td>
                                            <td>
                                                <?php
                                                $option_type_arr = array();
                                                $option_type_arr[] = 'Товар';
                                                $option_type_arr[] = 'Скидка';
                                                ?>
                                                <select style="height: 50px;width: 100px;" name="options[{{ $option_item->id }}][type]">
                                                    <option value="">Тип</option>
                                                    @foreach ($option_type_arr as $option_type)
                                                        <option @if ($option_item->type == $option_type) selected @endif value="{{ $option_type }}">{{ $option_type }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input style="height: 50px;width: 200px;" type="text" name="options[{{ $option_item->id }}][name]" placeholder="Название" required value="{{ $option_item->name }}">
                                            </td>
                                            <td>
                                                <textarea style="height: 50px;width: 400px;" name="options[{{ $option_item->id }}][description]" placeholder="Описание">{{ $option_item->description }}</textarea>
                                            </td>
                                            <td><a class="button delete-button common-button button-decline" onclick="giftDelete(this, '{{ $option_item->id }}')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                        </tr>

                                        <input class="hidefield" hidden="hidden" type="hidden" name="options[{{ $option_item->id }}][option_id]" value="{{ $option_item->option_id }}">
                                        @php($i++)
                                    @endforeach
                                </table>
                            @else
                                <p>Подарки не указаны</p>
                            @endif
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-12 modal-footer custom-footer">
                                <div class="col-sm-12">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="group_id" value="{{ $my_bet->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="order_id" value="{{ $orders->id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="shop_id" value="{{ $my_bet->shop_id }}">
                                    <input class="hidefield" hidden="hidden" type="hidden" name="old_price" value="{{ $my_bet->old_price }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<span role="button"  class="btn gradient-green width-50 d-block mx-auto" data-toggle="modal" data-target="#verifySave">СОХРАНИТЬ</span>
                                </div>
                            </div>
                        </div>
						<div class="modal fade" id="verifySave" tabindex="-1" role="dialog" aria-labelledby="verifySave" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Подтвердите действие</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<strong>Вы действительно хотите изменить предложение?</strong><br>
										После подтверждения Ваше предложение будет обновлено.
									</div>
									<div class="modal-footer">
										<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">ОТМЕНИТЬ</button>
										<button type="submit" class="btn gradient-green width-50">СОГЛАСЕН</button>
									</div>
								</div>
							</div>
						</div>
                    </form>
                </div>


				<div class="modal fade" id="deliveryAdd" tabindex="-1" role="dialog" aria-labelledby="deliveryAddLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Добавить доставку</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="/auction/order/delivery_create" id="addDeliveryForm" method="post" data-abide novalidate>
								<div class="modal-body">
									<div class="col-sm-12 columns form-block">
										<label>Тип <span class="red-star">*</span><br>
											<select name="delivery_type" required class="form-control">
												<option value="">Тип</option>
												<option value="Самовывоз">Самовывоз</option>
												<option value="Доставка">Доставка</option>
											</select>
										</label>
										<label>Срок доставки (дней) <span class="red-star">*</span>
											<input type="number" name="delivery_time" placeholder="Срок доставки в днях" required class="form-control">
										</label>
										<label>Подробности доставки <span class="red-star">*</span>
											<textarea name="delivery_description" placeholder="Детально опишите свои условия доставки (Транспортные компании, сроки, стоимость и т.д.)" required class="form-control"></textarea>
										</label>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="groupId" value="{{ $my_bet->id }}">
								</div>
								<div class="alert alert-danger alert-dismissible fade" id="validation-error-wrapper" role="alert">
									<button id="validation-error-wrapper-close" type="button" class="close" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<div id="validation-error"></div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
									<button id="openConfirmAddDelivery" type="button" class="btn gradient-green width-50">Добавить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal fade" id="confirmAddDelivery" tabindex="-1" role="dialog" aria-labelledby="confirmAddDelivery" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Вы действительно хотите добавить доставку?</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								После подтверждения доставка появится в Вашем предложении.
							</div>
							<div class="modal-footer">
								<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
								<button id="AddDeliveryConfirmButton" type="submit" class="btn gradient-green width-50">СОГЛАСЕН</button>
							</div>
						</div>
					</div>
				</div>


					<div class="modal fade" id="giftAdd" tabindex="-1" role="dialog" aria-labelledby="giftAddLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Добавить подарок</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="/auction/order/gift_create" method="post" id="addGiftForm" data-abide novalidate>
									<div class="modal-body">
										<div class="col-sm-12 columns form-block">
											<label>Тип <span class="red-star">*</span><br>
												<select name="option_type" required class="form-control">
													<option value="">Тип</option>
													<option value="Товар">Товар</option>
													<option value="Скидка">Скидка</option>
												</select>
											</label>
											<label>Название <span class="red-star">*</span>
												<input type="text" name="option_name" placeholder="Название" required class="form-control">
											</label>
											<label>Описание <span class="red-star">*</span>
												<textarea rows="5" cols="45" name="option_description" placeholder="Описание" required class="form-control"></textarea>
											</label>
										</div>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="groupId" value="{{ $my_bet->id }}">
									</div>
									<div class="alert alert-danger alert-dismissible fade" id="validation-error-wrapper" role="alert">
										<button id="validation-error-wrapper-close" type="button" class="close" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<div id="validation-error"></div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
										<button id="openConfirmAddGift" type="button" class="btn gradient-green width-50">Добавить</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade" id="confirmAddGift" tabindex="-1" role="dialog" aria-labelledby="confirmAddGift" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="delyveryModal{{ $orders->delivery->id }}Label">Вы действительно хотите добавить подарок?</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									После подтверждения подарок появится в Вашем предложении.
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
									<button id="AddGiftConfirmButton" type="submit" class="btn gradient-green width-50">СОГЛАСЕН</button>
								</div>
							</div>
						</div>
					</div>
            @else
                <div class="col-sm-12" >
                    <span>Заказов в подписанных категориях не найдено.</span>
                </div>
            @endif
        </div>
@endsection

