@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
@endsection

@section('title')
    Список заказов из подписки
@endsection
@section('content')
    @include('partials.sidebarShopWrapper')

    <div class="col-sm-9" id="right-block">

        @if (isset($orders))
            <div class="col-sm-12 orders-wraper" id="items-table-block">
                <div class="row">
                    @foreach ($orders as $order)
                        <div class="col-sm-12 order-block">
                            <div class="row">
                                <div class="col-sm-2 order-column order-logo">
                                    @if(isset($order->product->files[0]))
                                        <img src="../{{ $order->product->files[0] }}" alt="" class="order-photo">
                                    @endif
                                </div>
                                <div class="col-sm-7 order-column order-details">
                                    <div class="order-title">
                                        <span>{{ $order->product->name }}</span>
                                    </div>
                                    <div class="order-description">
                                        <div>№ заказа: <span>{{ $order->id }}</span></div>
                                        <div>Производитель: <span>{{ $order->product->producer }}</span></div>
                                        <div>Количество: <span>{{ $order->quantity }}</span></div>
                                        <div>Активен до: <span>{{ $order->active_till }}</span></div>
                                        @if(null !== $order->delivery)
                                            <div>Город: <span>{{$order->delivery->city}}</span></div>
                                            <div>Тип доставки:
                                                @foreach($order->delivery->types as $type)
                                                    @if($type->slug == 'delivery')
                                                        <span>Доставка.</span>
                                                    @elseif($type->slug == 'pickup')
                                                        <span>Самовывоз.</span>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @else
                                            <div>Доставка: <span>Не указана</span></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-3 order-column order-actions">
                                    <div class="actions-offers">
                                        Предложений: <span>{{ count($order->order_offer_groups) }}</span>
                                    </div>
                                    <div class="actions-best-price">
                                        @if(count($order->order_offer_groups)>0)
                                            @if($order->order_offer_groups->last()->shop_id == Auth::user()->connection_id)
                                                Ваша лучшая цена:
                                                <span>{{ $order->order_offer_groups->min('price') }}</span>
                                            @else
                                                Лучшая цена:
                                                <span>{{ $order->order_offer_groups->min('price') }}</span>
                                            @endif
                                        @else
                                        @endif
                                    </div><br><br>
                                    <div class="actions-buttons">
                                        <a href="/auction/order?order={{ $order->id }}"
                                           class="btn gradient-green  width-100">Подробности</a><br><br>

                                        <button type="button" class="btn border-red-btn width-100 hide-button" data-toggle="modal" data-target="#hideConfirm{{ $order->id }}">
                                            Скрыть
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="hideConfirm{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="hideConfirm{{ $order->id }}Label" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="hideConfirm{{ $order->id }}Label">Скрыть заказ</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form method="post" action="/auction/hide">
                                                        <div class="modal-body">
                                                                <h3 class="modal-h-3" id="modalTitle">Вы действительно
                                                                    хотите скрыть заказ?</h3>
                                                                <p>После подтверждения заказ пропадёт из списка
                                                                    доступных.</p>
                                                                <input type="hidden" name="order_id"
                                                                       value="{{ $order->id }}">
                                                                <input type="hidden" name="_token"
                                                                       value="{{ csrf_token() }}">
                                                        </div>
                                                        <div id="validation-error"></div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn gradient-green width-50">СОГЛАСЕН</button>
                                                            <button type="button" class="btn border-red-btn" data-dismiss="modal">ОТМЕНИТЬ</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-sm-12" >
                <span>Заказов в подписанных категориях не найдено.</span>
            </div>
        @endif
        <hr>
        {{ $orders->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
@endsection