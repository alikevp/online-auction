@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
@endsection

@section('title')
    Скрытые заказы
@endsection

@section('content')
	@include('partials.sidebarShopWrapper')
    
	<div class="col-sm-9" id="right-block">
        @if (count($orders)>0)
			<div class="col-sm-12" id="buy-steps-block">
				<div class="col-sm-12">
					<h3>Список скрытых аукционов</h3>
					<hr>
				</div>
			</div>
			<div class="col-sm-12 orders-wraper" id="items-table-block">
				<div class="row">
					@foreach ($orders as $order)
						<div class="col-sm-12 order-block">
							<div class="row">
								<div class="col-sm-2 order-column order-logo">
									@if(isset($order->product->files[0]))
										<img src="../{{ $order->product->files[0] }}" alt="" class="order-photo">
									@endif
								</div>
								<div class="col-sm-7 order-column order-details">
									<div class="order-title">
										<span>{{ $order->product->name }}</span>
									</div>
									<div class="order-description">
										<div>№ заказа: <span>{{ $order->id }}</span></div>
										<div>Производитель: <span>{{ $order->product->producer }}</span></div>
										<div>Количество: <span>{{ $order->quantity }}</span></div>
										<div>Активен до: <span>{{ $order->active_till }}</span></div>
										<div>Город: <span>{{$order->delivery->city}}</span></div>
										<div>Тип доставки:
											@foreach($order->delivery->types as $type)
												@if($type->slug == 'delivery')
													<span>Доставка.</span>
												@elseif($type->slug == 'pickup')
													<span>Самовывоз.</span>
												@endif
											@endforeach
										</div>
									</div>
								</div>
								<div class="col-sm-3 order-column order-actions">
									<div class="actions-offers">
										Предложений: <span>{{ count($order->order_offer_groups) }}</span>
									</div>
									<div class="actions-best-price">
										@if(count($order->order_offer_groups)>0)
											@if($order->order_offer_groups->last()->shop_id == Auth::user()->connection_id)
												Ваша лучшая цена:
												<span>{{ $order->order_offer_groups->min('price') }}</span>
											@else
												Лучшая цена:
												<span>{{ $order->order_offer_groups->min('price') }}</span>
											@endif
										@else
										@endif
									</div>
									<div class="actions-buttons">
										<a href="/auction/order?order={{ $order->id }}"
										   class="btn gradient-green width-100">Подробности</a>
										<a class="btn border-green-btn width-100" data-toggle="modal" data-target="#hideConfirm{{ $order->id }}">Вернуть отображение</a>

										<div class="modal fade" id="hideConfirm{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="hideConfirm{{ $order->id }}Label" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="hideConfirm{{ $order->id }}Label">Скрыть заказ</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<form method="post" action="/auction/unhide">
														<div class="modal-body">
															<h3 class="modal-h-3" id="modalTitle">Вы действительно хотите вернуть отображение заказа?</h2>
																<p>После подтверждения заказ снова будет в списке аукционов.</h3>
															<input type="hidden" name="order_id"
																   value="{{ $order->id }}">
															<input type="hidden" name="_token"
																   value="{{ csrf_token() }}">
														</div>
														<div class="modal-footer">
															<button class="btn gradient-green width-50" type="submit" style="margin-bottom: 0;">СОГЛАСЕН</button>
															<<button type="button" class="btn border-red-btn" data-dismiss="modal">ОТМЕНИТЬ</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@else
			<div class="col-sm-12" >
				<span>Скрытых аукционов найдено не было.</span>
			</div>
		@endif
            <hr>
            {{ $orders->links('vendor.pagination.foundation') }}
        </div>
    </div>
@endsection
{{--@section('content')--}}
{{--<div class="row" id="content">--}}

{{--@include('partials.profile_sidebar_for_admins')--}}

{{--<div class="col-sm-10" id="right-block">--}}

{{--@if (count($orders))--}}
{{--<div class="col-sm-12" id="buy-steps-block">--}}
{{--<div class="col-sm-12">--}}
{{--<h3>Список заказов из подписки</h3>--}}
{{--<hr>--}}
{{--<span>Сортировать по: <br></span>--}}
{{--<a href="orderlist?orderby=date_up" class="button {{ app('request')->input('orderby') == '' || app('request')->input('orderby') == 'date_up' ? '' : 'hollow' }}">Дате (возраст.)</a> <a href="orderlist?orderby=date_down" class="button {{ app('request')->input('orderby') == 'date_down' ? '' : 'hollow' }}">Дате (убыв.)</a> <a href="orderlist?orderby=status" class="button {{ app('request')->input('orderby') == 'status' ? '' : 'hollow' }}">Статусу</a> <a href="orderlist?orderby=id" class="button {{ app('request')->input('orderby') == 'id' ? '' : 'hollow' }}">id</a>   <a href="orderlist?orderby=delivery" class="button {{ app('request')->input('orderby') == 'delivery' ? '' : 'hollow' }}">Городу доставки</a>   <a href="orderlist?orderby=fio" class="button {{ app('request')->input('orderby') == 'fio' ? '' : 'hollow' }}">ФИО покупателя</a>--}}
{{--<a data-open="filter">Фильтр</a>--}}
{{--@if(app('request')->input())--}}
{{--@if(!(count(app('request')->input()) == 1 && app('request')->input('page')))--}}
{{--<a href="orderlist">(сбросить)</a>--}}
{{--@endif--}}
{{--@endif--}}
{{--<div class="reveal" id="filter" data-reveal>--}}
{{--<h1 class="modal-h-1">Фильтр</h1>--}}
{{--<form action="">--}}
{{--<div class="col-sm-12">--}}
{{--<legend>Поиск по id: </legend>--}}
{{--</div>--}}
{{--<div class="col-sm-9">--}}
{{--<input type="text" name="search_id" placeholder="id заказа" value="{{ app('request')->input('search_id') == "" ? "" : app('request')->input('search_id')}}">--}}
{{--</div>--}}
{{--<div class="col-sm-3">--}}
{{--<button type="submit" class="button float-right">Найти</button>--}}
{{--</div>--}}
{{--</form>--}}
{{--<div class="col-sm-12">--}}
{{--<hr>--}}
{{--</div>--}}
{{--<form action="">--}}
{{--<div class="col-sm-12">--}}
{{--<legend>Статус: </legend>--}}
{{--<input id="checkbox1" name="state_active" type="checkbox" {{ app('request')->input('state_active') == "on" || !app('request')->input() || (count(app('request')->input()) == 1 && app('request')->input('page'))  ? "checked" : ""}}><label for="checkbox1">Активные</label>--}}
{{--<input id="checkbox2" name="state_end" type="checkbox" {{ app('request')->input('state_end') == "on" ? "checked" : ""}}><label for="checkbox2">Завершенные</label>--}}
{{--<input id="checkbox3" name="state_late" type="checkbox" {{ app('request')->input('state_late') == "on" ? "checked" : ""}}><label for="checkbox3">Просроченные</label>--}}
{{--<input id="checkbox4" name="state_accept" type="checkbox" {{ app('request')->input('state_accept') == "on" ? "checked" : ""}}><label for="checkbox4">Принятые</label>--}}
{{--<hr>--}}
{{--</div>--}}
{{--<div class="col-sm-12">--}}
{{--<label>Город:--}}
{{--<input type="text" name="city" placeholder="Город доставки" value="{{ app('request')->input('city') == "" ? "" : app('request')->input('city')}}">--}}
{{--</label>--}}
{{--<hr>--}}
{{--</div>--}}
{{--<div class="col-sm-12">--}}
{{--<legend>Ваше участие в аукционе:</legend>--}}
{{--<input id="checkbox5" name="participation" type="checkbox" {{ app('request')->input('participation') == "on" ? "checked" : ""}}><label for="checkbox5">Я учавствую</label>--}}
{{--<input id="checkbox6" name="notparticipation" type="checkbox" {{ app('request')->input('notparticipation') == "on"  || !app('request')->input() || (count(app('request')->input()) == 1 && app('request')->input('page')) ? "checked" : ""}}><label for="checkbox6">Я не учавствую</label>--}}
{{--<hr>--}}
{{--</div>--}}
{{--<div class="col-sm-12">--}}
{{--<legend>Категории, на которые Вы подписаны:</legend>--}}
{{--@php($i = 7)--}}
{{--@php($j = 0)--}}
{{--@foreach($subscription_list as $subscription_item)--}}
{{--<input id="checkbox{{ $i }}" name="categories[]" value="{{ $subscription_item->id }}" type="checkbox" {{ !app('request')->input() || (count(app('request')->input()) == 1 && app('request')->input('page')) || (isset(app('request')->input('categories')[$j]) && app('request')->input('categories')[$j] == $subscription_item->id)  ? "checked" : "" }} ><label for="checkbox{{ $i }}">{{ $subscription_item->name }}</label>--}}
{{--<br>--}}
{{--@php($i++)--}}
{{--@if(isset(app('request')->input('categories')[$j]) && app('request')->input('categories')[$j] == $subscription_item->id)--}}
{{--@php($j++)--}}
{{--@endif--}}
{{--@endforeach--}}
{{--<hr>--}}
{{--</div>--}}
{{--<div class="col-sm-12">--}}
{{--<button type="submit" class="button">Фильтровать</button>--}}
{{--</div>--}}
{{--</form>--}}
{{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
{{--<span aria-hidden="true">&times;</span>--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-sm-12" id="items-table-block">--}}
{{--<table class="items-table unstriped">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th>№</th>--}}
{{--<th>Номер заказа</th>--}}
{{--<th>Статус</th>--}}
{{--<th>Товары</th>--}}
{{--<th>Кол-во</th>--}}
{{--<th>Город</th>--}}
{{--<th>Начало</th>--}}
{{--<th>Окончание</th>--}}
{{--<th>Действие</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}

{{--@foreach ($orders as $order)--}}
{{--<tr id="cart_prod_row_{{ $order->product->id }}">--}}
{{--<th>{{ $loop->index+1 }}</th>--}}
{{--<td>{{ $order->id }}</td>--}}
{{--<td>--}}
{{--@if($product_item['status'] == 0)--}}
{{--Завершён--}}
{{--@elseif($product_item['status'] == 1)--}}
{{--Активен--}}
{{--@elseif($product_item['status'] == 2)--}}
{{--Истёк--}}
{{--@endif--}}
{{--</td>--}}
{{--<td>--}}
{{--<div class="col-sm-5">--}}
{{--<img src="{{ url(array_first($order->product->files)) }}" class="order-photo" alt="">--}}
{{--</div>--}}
{{--<div class="col-sm-7">--}}
{{--<span class="item-name">{{ $order->product->name }}</span><br>--}}
{{--<span class="item-producer">Производитель: {{ $order->product->producer }}</span><br>--}}
{{--</div>--}}
{{--</td>--}}
{{--<td>{{ $order->quantity }}</td>--}}
{{--<td>{{ $order->delivery->first()->city }}</td>--}}
{{--<td>{{ $order->created_at }}</td>--}}
{{--<td>{{ $order->active_till }}</td>--}}
{{--<th>--}}
{{--<a href="/auction/order?order={{ $order->id }}"--}}
{{--class="button order-control-button">Аукцион</a>--}}
{{--<a data-open="hideConfirm{{ $order_number }}" class="button order-control-button">Скрыть</a>--}}
{{--<div class="reveal" id="hideConfirm{{ $order_number }}" data-reveal>--}}
{{--<form method="post" action="/auction/hide">--}}
{{--<h2 id="modalTitle">Вы действительно хотите скрыть заказ?</h2>--}}
{{--<p>После подтверждения заказ пропадёт из списка доступных.</p>--}}
{{--<hr>--}}
{{--<button class="button" type="submit" style="margin-bottom: 0;">СОГЛАСЕН--}}
{{--</button>--}}
{{--<input type="hidden" name="order_id"--}}
{{--value="{{ $product_item['order_id'] }}">--}}
{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--<a data-close="hideConfirm{{ $order_number }}" class="button"--}}
{{--style="margin-bottom: 0;">ОТМЕНИТЬ</a>--}}
{{--</form>--}}
{{--</div>--}}
{{--</th>--}}
{{--</tr>--}}
{{--@endforeach--}}
{{--</tbody>--}}
{{--</table>--}}
{{--</div>--}}
{{--@else--}}
{{--<div class="col-sm-12">--}}
{{--<span>Заказов в подписанных категориях не найдено.</span>--}}
{{--</div>--}}
{{--@endif--}}
{{--<hr>--}}
{{--{{ $orders->links('vendor.pagination.foundation') }}--}}
{{--</div>--}}
{{--</div>--}}
{{--@endsection--}}

@section('scripts')
    <script src="{{ asset('js/getOrderCount.js') }}"></script>
@endsection