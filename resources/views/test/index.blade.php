@if (isset($menu_array))
    <ul class="dropdown menu category-menu" data-dropdown-menu>
    @foreach ($menu_array as $menu_1_level_data)
            <li>
                <a href="#">{{ $menu_1_level_data['name'] }}</a>
                @if (count($menu_1_level_data['childes']>0))
                    <ul class="menu">
                    @foreach ($menu_1_level_data['childes'] as $menu_2_level_data)
                        <li>
                            <a href="#">{{ $menu_2_level_data['name'] }}</a>
                            @if (count($menu_2_level_data['childes']>0))
                                <ul class="menu">
                                @foreach ($menu_2_level_data['childes'] as $menu_3_level_data)
                                    <li><a href="#">{{ $menu_3_level_data['name'] }}</a></li>
                                @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                    </ul>
                @endif
            </li>
    @endforeach
    </ul>
@endif
