@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    <script src="{{ asset('js/page_scripts/cart.index.js') }}"></script>
@endsection
@section('title')
    ОТЛОЖЕННЫЕ ТОВАРЫ
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Отложенные товары</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-sm-9" id="right-block">
        @if (isset($products))
            <div class="col-sm-12 orders-wraper" id="items-table-block">
                @foreach ($products as $product)
                    <div class="medium-12 columns order-block" id="deferred_prod_row_{{ $product->id }}">
                        <div class="row">
                            <div class="col-sm-2 columns order-column order-logo">
                                <a href="{{ url('show', $product->id) }}"><img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image"></a>
                            </div>
                            <div class="col-sm-7 order-column order-details">
                                <div class="order-title">
                                    <a href="{{ url('show', $product->id) }}">{{ $product->name }}</a>
                                </div>
                                <div class="order-description">
                                    <div>Производитель: <span>{{ $product->name }}}</span></div>
                                </div>
                            </div>
                            <div class="col-sm-3 order-column order-actions">
                                <div class="actions-offers">

                                </div>
                                <div class="actions-best-price">

                                </div>
                                <div class="actions-buttons">
                                    <a href="#" class="button common-button button-accept hide-button" id="deferred_to_cart" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_to_cart">В корзину</a>
                                    <a href="#" class="button common-button button-decline hide-button" id="deferred_del" data-deferred-prod-id="{{ $product->id }}" data-deferred-url="/deferred_del">Убрать из отложенного</a><br>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="col-sm-12" >
                <span>Отложенных товаров нет</span>
            </div>
        @endif
    </div>
@endsection
