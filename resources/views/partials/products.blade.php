<div class="col-sm-9 products" id="right-block">
    <div id="sticky-content">
        @if(isset($products))
            <div class="col-sm-12 text-right">
                <select name="order" id="order_type_select">
                    <option value="price_desc"
                            @if(isset($_GET['order'])) @if($_GET['order'] == 'price_desc') selected @endif @endif>Цена
                        &#8595;</option>
                    <option value="price_asc" @if(isset($_GET['order'])) @if($_GET['order'] == 'price_asc') selected
                            @endif @else selected @endif>Цена &#8593;
                    </option>
                    <option value="name_desc"
                            @if(isset($_GET['order'])) @if($_GET['order'] == 'name_desc') selected @endif @endif>Название
                        &#8595;</option>
                    <option value="name_asc"
                            @if(isset($_GET['order'])) @if($_GET['order'] == 'name_asc') selected @endif @endif>Название
                        &#8593;
                    </option>
                </select>
            </div>
            <div class="col-sm-12">
                <div class="products-filter-block">
                    @foreach($products->chunk(6) as $chunk)
                        <div class="row">
                            @foreach($chunk as $product)
                                <div class="col-sm-4 item">
                                    <div class="col-xs-12 item-img">
                                        <a href="{{ url('show', ['id' => $product->id]) }}">
                                            <img src="{{ URL::to(array_first($product->files)) }}" class="item-img" alt=""><br>
                                        </a>
                                    </div>
                                    <div class="col-xs-12">
                                        <a href="{{ url('show', ['id' => $product->id]) }}">
                                            <span class="item-name">{{ $product->name }}</span><br>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 item-cost">
                                        <a href="{{ url('show', ['id' => $product->id]) }}">
                                            @if($product->price == "" || $product->price == 0)
                                                <span>Цена не указана</span>
                                            @else
                                                <span>~{{ $product->price }} руб.</span>
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    <br>
                    <div class="row">
                        {{ $products->appends(Request::only('search'))->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>