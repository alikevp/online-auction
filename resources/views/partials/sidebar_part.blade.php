@foreach($categories as $category)
    @if($category->isChild())
        <li>
            <a href="{{ url('category', ['id' =>$category->id])}}" class="sidebar-element no-chevron">{{ $category->name }}</a>
            <ul class="menu sidebar-element submenu is-dropdown-submenu">
                @include("partials.sidebar_part",["categories" => $category->children])
            </ul>
        </li>
    @elseif($category->isLeaf())
        <li>
            <a class="sidebar-element no-chevron" href="{{ url('category', ['id' =>$category->id])}}">{{ $category->name }}</a>
        </li>
    @endif
@endforeach