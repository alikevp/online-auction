@include('partials.navMobileMenu')
<div class="container d-none d-sm-block">
    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 upper-nav-tabs-wrapper">
            <div  
			@if((\Request::route()->getName() == "login") and (isset($_GET['company'])))
				class="upper-nav-tab busines">
			@else
				class="upper-nav-tab active">
			@endif
                <a href="/login" class="navigation-link decoration-green">
                    <span class="bold-text">Джинмарт</span><br>
                    <span class="grey-text">Магазин</span>
                </a>
            </div>
            <div  
			@if((\Request::route()->getName() == "login") and (isset($_GET['company'])))
				class="upper-nav-tab active">
			@else
				class="upper-nav-tab busines">
			@endif
                <a href="/login?company" class="navigation-link decoration-green">
                    <span class="bold-text">Джинмарт Бизнес</span><br>
                    <span class="grey-text">Юридические лица</span>
                </a>
            </div>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 text-right upper-nav-links-wrapper">
            <div class="row">
                <a href="/register/shop" class="navigation-link decoration-green">
                    <span class="regular-text">Хотите продавать у нас?</span>
                </a>
                <a href="/page/instruktsii" class="navigation-link decoration-green">
                    <span class="regular-text">Помощь</span>
                </a>
                <a href="/deferred" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-heart-o" aria-hidden="true"></i> Отложенное
                    <span class="deferred-items-count">(<span id="deferred-items-count-value"></span>)</span>
                </a>
                <a href="/cart" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-shopping-cart" aria-hidden="true"></i> Корзина
                    <span class="cart-items-count">(<span id="cart-items-count-value"></span>)</span>
                </a>
                <a href="/register" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-user-plus" aria-hidden="true"></i>
                    <span class="regular-text">Регистрация</span>
                </a>
                <a href="/login" class="navigation-link decoration-green">
                    <span class="fa fa-sign-in" aria-hidden="true"></span> <span class="regular-text">Вход</span>
                </a>
            </div>
        </div>
    </div>
</div>