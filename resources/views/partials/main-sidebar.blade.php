<div class="title-bar mobile-triger" data-responsive-toggle="sidebar-block" data-hide-for="large"
     style="background: #8bc63e; margin-bottom: 10px;">
    <button class="menu-icon" type="button" data-toggle="sidebar-block"></button>
    <div class="title-bar-title">Категории</div>
</div>
<div class="sidebar-block" id="sidebar-block">
    <div class="row collapse">
        <div class="small-12 columns logo-div">
            <a href="/">
                <img src="{{ asset('images/logo.png') }}" class="logo-img" alt="logo">
            </a>
        </div>
        <div class="small-12 columns logo-text">
            <span class="shop-name">аукци<span>on-line</span></span>
        </div>
        <div class="small-12 columns categories">
            <div class="icon-bar">
                @include('cached_views.cache.CategoriesSidebar')
                {{--{{ dd($categories->toArray()) }}--}}
                {{--@foreach($categories as $category)--}}
                {{--<ul class="vertical dropdown menu" data-dropdown-menu data-closing-time="50">--}}
                {{--<li>--}}
                {{--<a href="{{ route('category', $category->id) }}">--}}
                {{--<div class="icon-div">--}}
                {{--<img src="{{ asset('images/J.png') }}" alt="" class="cat-icon">--}}
                {{--</div>--}}
                {{--<div class="text-div">--}}
                {{--<span class="cat-text">--}}
                {{--{{ $category->name }}--}}
                {{--</span>--}}
                {{--</div>--}}
                {{--</a>--}}
                {{--@include('partials.category', ['categories' => $category->children])--}}
                {{--<ul class="vertical dropdown menu is-dropdown-submenu" data-dropdown-menu>--}}
                {{--@include('partials.category', ['categories' => $category->children])--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--@endforeach--}}
            </div>
        </div>
    </div>
</div>