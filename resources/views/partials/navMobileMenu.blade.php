<div class="mobile-header d-block d-sm-none">
    <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
        <div class="col-sm-4 logo-img">
            <a class="navmenu-brand" href="/"><img src="{{asset('images/jinnmart-logo.png')}}"></a>
        </div>
        <ul class="nav navmenu-nav">
            <li class="active">
                <a href="/register/shop" class="navigation-link decoration-green">
                    <span class="regular-text">Хотите продавать у нас?</span>
                </a>
            </li>
            <li>
                <a href="/page/instruktsii" class="navigation-link decoration-green">
                    <span class="regular-text">Помощь</span>
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <a href="/deferred" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-heart-o" aria-hidden="true"></i> Отложенное
                    <span class="deferred-items-count-mobile">(<span id="deferred-items-count-value-mobile"></span>)</span>
                </a>
            </li>
            <li>
                <a href="/cart" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-shopping-cart" aria-hidden="true"></i> Корзина
                    <span class="cart-items-count-mobile">(<span id="cart-items-count-value-mobile"></span>)</span>
                </a>
            </li>
            <li>
                <a href="/register" class="navigation-link decoration-green">
                    <i class="fa header-menu-icon fa-user-plus" aria-hidden="true"></i>
                    <span class="regular-text">Регистрация</span>
                </a>
            </li>
            <li>
                <a href="/login" class="navigation-link decoration-green">
                    <span class="fa fa-sign-in" aria-hidden="true"></span> <span class="regular-text">Вход</span>
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <a href="/page/instruktsii" class="navigation-link decoration-green">
                    <span class="bold-text">Как это работает?</span>
                </a>
            </li>
            <li>
                <a href="/page/instruktsii/kak-sdelat-zakaz" class="navigation-link decoration-green">
                    <span class="bold-text">Заказ</span>
                </a>
            </li>
            <li>
                <a href="/page/sposoby-oplaty" class="navigation-link decoration-green">
                    <span class="bold-text">Оплата</span>
                </a>
            </li>
            <li>
                <a href="/page/dostavka" class="navigation-link decoration-green">
                    <span class="bold-text">Получение</span>
                </a>
            </li>
            <li>
                <a href="/page/vozvrat-tovara" class="navigation-link decoration-green">
                    <span class="bold-text">Гарантия и возврат</span>
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <a href="#" class="navigation-link decoration-green">
                    <span class="bold-text">Каталог:</span>
                </a>
            </li>
            <li class="catalog">
                @include('cached_views.cache.CategoriesSidebar')
            </li>
        </ul>
    </nav>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="col-xs-6">
            <button type="button" class="btn navbar-toggle gradient-green" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
                <i class="fa fa-bars" aria-hidden="true"></i>
                Меню
            </button>
        </div>
        <div class="col-xs-6 logo-text">
            <span class="text">аукци<span>on-line</span></span>
        </div>
    </div>
</div>