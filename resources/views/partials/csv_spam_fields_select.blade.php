<form method="get" action="#">
    <select name='name'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для названия</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='direction'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для направления</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='category_upload'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для категории</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='city'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для города</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='address'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для адреса</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='tel'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для телефона</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='fax'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для факса</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='email'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для почты</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <br>
    <select name='www'>
        <option value="" disabled="disabled" selected="selected">Выберите поле для сайта</option>
        @foreach ($headings as $heading)
            <option value="<?php echo $heading;?>"><?php echo $heading;?></option>
        @endforeach
    </select>
    <input type="submit" name="spam_upload_submit" value="Загрузить">
</form>