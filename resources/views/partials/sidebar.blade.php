<div class="col-sm-3 sidebar-filter">
    <div id="sticky-sidebar">
            <div id="col-xs-12">
                <span class="bold-text">Категории</span>
            </div>
                <div class="catalog-list">
                    @include('partials.nav')
                </div>

        <?php $brand_page = preg_match('/^brand/', Request::path());?>
        @if(Request::path() !== 'search')
            @if($brand_page !== 1)
                @if(isset($attributes) OR isset($producers) OR (isset($min_price) AND isset($max_price)))
                    <form action="{{ url('filter') }}" method="get">
                        <div class="col-xs-12">
                            <div id="costAccordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#costAccordion" href="#costCollaps" aria-expanded="true" aria-controls="brandCollaps">
                                                Цена
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="costCollaps" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="card-block">
                                            <table>
                                                <tr>
                                                    <td id="input-1-td">
                                                        <input value="@if(isset($_GET['min'])){{$_GET['min']}}@else{{$min_price}}@endif"
                                                               type="number" id='sliderOutput1' name="min"
                                                               class="filter_parameter filter_parameter_price filter_parameter_min_price">
                                                    </td>
                                                    <td id="dash-td">—</td>
                                                    <td id="input-2-td">
                                                        <input value="@if(isset($_GET['max'])){{$_GET['max']}}@else{{$max_price}}@endif"
                                                               type="number" id='sliderOutput2' name="max"
                                                               class="filter_parameter filter_parameter_price filter_parameter_max_price">
                                                    </td>
                                                </tr>
                                            </table>
                                            <input id="costSlider"
                                                   type="text"
                                                   name="somename"
                                                   data-provide="slider"
                                                   data-slider-min="@if(isset($_GET['min'])){{ $_GET['min']}}@else{{ $min_price }}@endif"
                                                   data-slider-max="@if(isset($_GET['max'])){{ $_GET['max']}}@else{{ $max_price }}@endif"
                                                   data-slider-value="[{{ $min_price }},{{ $max_price }}]"
                                                   data-slider-step="1"
                                                   data-slider-range="true"
                                                   data-slider-tooltip="always"/><br/>

                                        </div>
                                        <a href="#" class="green green-dotted float-right" onclick="$('#costCollaps').collapse()">Свернуть</a>
                                    </div>
                                </div>
                            </div>
                            {{--<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">--}}
                                {{--@if(isset($min_price) AND isset($max_price))--}}
                                    {{--<li class="accordion-item is-active" data-accordion-item>--}}
                                        {{--<a href="#" class="accordion-title">Цена, р</a>--}}
                                        {{--<div class="accordion-content" id="price-content-div" data-tab-content>--}}
                                            {{--<table>--}}
                                                {{--<tr>--}}
                                                    {{--<td id="input-1-td">--}}
                                                        {{--<input value="@if(isset($_GET['min'])) {{ $_GET['min'] }} @else {{ $min_price }} @endif" type="number" id='sliderOutput1' name="min" class="filter_parameter filter_parameter_min_price">--}}
                                                    {{--</td>--}}
                                                    {{--<td id="dash-td">—</td>--}}
                                                    {{--<td id="input-2-td">--}}
                                                        {{--<input  value="@if(isset($_GET['max'])) {{ $_GET['min'] }} @else {{ $max_price }} @endif" type="number" id='sliderOutput2' name="max" class="filter_parameter filter_parameter_max_price">--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--</table>--}}
                                            {{--<div class="slider" id="priceSlider" data-slider--}}
                                                 {{--@if(isset($_GET['min']))--}}
                                                     {{--data-initial-start='{{ $_GET['min'] }}'--}}
                                                     {{--data-start='{{ $min_price }}'--}}
                                                     {{--aria-valuenow="{{ $_GET['min'] }}"--}}
                                                 {{--aria-valuemax = "{{ $max_price }}"--}}
                                                 {{--aria-valuemin = "{{ $min_price }}"--}}
                                                 {{--@else--}}
                                                     {{--data-initial-start='{{ $min_price }}'--}}
                                                     {{--data-start='{{ $min_price }}'--}}
                                                    {{--aria-valuenow="{{ $min_price }}"--}}
                                                 {{--aria-valuemax = "{{ $max_price }}"--}}
                                                 {{--aria-valuemin = "{{ $min_price }}"--}}
                                                 {{--@endif--}}
                                                 {{--@if(isset($_GET['max']))--}}
                                                     {{--data-initial-end='{{ $_GET['max'] }}'--}}
                                                     {{--data-end='{{ $max_price }}'--}}
                                                 {{--aria-valuenow="{{ $_GET['max'] }}"--}}
                                                 {{--aria-valuemax = "{{ $max_price }}"--}}
                                                 {{--aria-valuemin = "{{ $min_price }}"--}}
                                                 {{--@else--}}
                                                     {{--data-initial-end='{{ $max_price }}'--}}
                                                     {{--data-end='{{ $max_price }}'--}}
                                                 {{--aria-valuenow="{{ $max_price }}"--}}
                                                 {{--aria-valuemax = "{{ $max_price }}"--}}
                                                 {{--aria-valuemin = "{{ $min_price }}"--}}
                                                 {{--@endif--}}
                                                {{-->--}}
                                                {{--<span class="slider-handle filter_parameter"--}}
                                                      {{--data-slider-handle role="slider"--}}
                                                      {{--data-changed-delay="2000"--}}
                                                      {{--tabindex="1"--}}
                                                      {{--aria-controls='sliderOutput1'></span>--}}
                                                {{--<span class="slider-fill" data-slider-fill></span>--}}
                                                {{--<span class="slider-handle filter_parameter"--}}
                                                      {{--data-slider-handle role="slider"--}}
                                                      {{--data-changed-delay="2000"--}}
                                                      {{--tabindex="1"--}}
                                                      {{--aria-controls='sliderOutput2'></span>--}}
                                            {{--</div>--}}
                                            {{--<span onclick="$('#price-content-div').parent().parent().foundation('up', $('#price-content-div'));"--}}
                                                  {{--class="togle float-right">Свернуть</span>--}}
                                            {{--<br><br>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {{--@endif--}}
                                    {{--<div>--}}
                                        {{--<input type="checkbox" id="no_price"--}}
                                               {{--value="true" name="no_price"--}}
                                               {{--class="filter_parameter filter_parameter_no-price"--}}
                                               {{--@if(isset($_GET['no_price']))checked @endif--}}
                                        {{-->--}}
                                        {{--<label for="no_price">Показывать товары без цены</label>--}}
                                    {{--</div>--}}
                            {{--</ul>--}}
                            @if(isset($producers))
                                <div id="brandAccordion" role="tablist" aria-multiselectable="true">
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#brandAccordion" href="#brandCollaps" aria-expanded="true" aria-controls="brandCollaps">
                                                    Производитель
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="brandCollaps" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="card-block scroll">
                                                @foreach($producers as $producer)
                                                    <input type="checkbox" id="{{ $producer->slug }}"
                                                           value="{{ $producer->slug }}" name="producer[]"
                                                           class="filter_parameter filter_parameter_producer"
                                                           @if(isset($_GET['producer']))
                                                           @if(in_array($producer->slug, $_GET['producer'])) checked @endif
                                                            @endif
                                                    >
                                                    <label for="{{ $producer->slug }}">{{ $producer->name }}</label>
                                                    <br>
                                                @endforeach
                                                <br>
                                            </div>
                                            <a href="#" class="green green-dotted float-right" onclick="$('#brandCollaps').collapse()">Свернуть</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($attributes))
                                <div id="filter_parameters_wraper">
                                    @foreach($attributes as $attribute)
                                        @if(preg_match("/[a-zа-яё0-9]+/i",$attribute->first()->value) )
                                            <div id="{{ $attribute->first()->slug }}accordion" role="tablist" aria-multiselectable="false">
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" data-parent="#{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}accordion" href="#{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}Collaps" aria-expanded="false" aria-controls="brandCollaps">
                                                                {{ $attribute->first()->name }}
                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="{{ preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","",$attribute->first()->slug) }}Collaps" class="collapse @if(isset($_GET['attributes'][$attribute->first()->slug])) show @endif" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="card-block scroll">
                                                            @foreach($attribute as $key=>$at)
                                                                <input type="checkbox"
                                                                       id="{{ $at->slug }}_{{ $key }}"
                                                                       value="{{ $at->value }}"
                                                                       name="attributes[{{ $at->slug }}][]"
                                                                       class="filter_parameter filter_parameter_attributes"
                                                                       data-filter_parameter-slug="{{ $at->slug }}"
                                                                       @if(isset($_GET['attributes'][$at->slug]))
                                                                       @if(in_array(trim($at->value), array_map('trim', $_GET['attributes'][$at->slug]))) checked @endif
                                                                        @endif
                                                                >
                                                                <label for="{{ $at->slug }}_{{ $key }}">
                                                                    {{ $at->value }}
                                                                </label>
                                                                <br>
                                                            @endforeach
                                                            <br>
                                                        </div>
                                                        <a href="#" class="green green-dotted float-right" onclick="$('#{{ $attribute->first()->slug }}Collaps').collapse()">Свернуть</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endif

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="category"  class="filter_parameter_category" value="{{ session()->get('category')->id }}">
                            <input type="hidden" name="order" id="order_type_input" value="price_asc">
                            <a href="/category/{{ session()->get('category')->id }}" class="green clear-filter">Сбросить фильтры</a>
                        </div>
                    </form>
                @endif
            @endif
        @endif
    </div>
</div>
