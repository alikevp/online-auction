@foreach($categories as $category)
    @if ($category->leafs_offers_count !== 0 OR $category->offers_count !== 0)
        <div class="parent-category parent-category-main">
            <a href="{{ route('category', $category->id) }}">{{ $category->name }}</a>
            @include('partials.sidebar_parent',['categories' => $category->children])
        </div>
    @endif
@endforeach