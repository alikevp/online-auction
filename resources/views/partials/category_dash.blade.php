<div class="medium-12 columns category-name-block">
    <div class="medium-12 columns">
        <h2 class="category-h2">
            Непроверенная категория - {{ $category->name }}
        </h2>
    </div>
</div>
<div class="medium-12 columns category-content items-row ">
    <ul class="accordion" data-accordion data-allow-all-closed="true">
        <li class="accordion-item is-active" data-accordion-item>
            <a href="#" class="accordion-title">Категория содержит: <span
                        class="show-hide-button">показать/скрыть</span></a>
            <div class="accordion-content" data-tab-content>
                <div class="row">
                    Родительская категория - <b>{{ $category->parent->name }}</b>
                </div>
                <div class="row itemsrow">
                    @if($products != null)
                        @foreach($products as $product)
                            <div class="medium-4 columns">
                                <img src="{{ URL::to(array_first($product->files)) }}"
                                     alt=""><br>
                                <span class="itembrand">{{ $product->producers->name }}</span><br>
                                <a href="#">
                                    <span class="itemname">{{ $product->name }}</span><br>
                                </a>
                                <span class="itemcost">{{ $product->price }}</span>
                            </div>
                        @endforeach
                    @else
                        Данная категория совершенна пуста, тут нет ничего
                    @endif
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="medium-12 columns category-similar">
    <ul class="accordion" data-accordion data-allow-all-closed="true">
        <li class="accordion-item is-active" data-accordion-item>
            <a href="#" class="accordion-title">Похожие категории: <span
                        class="show-hide-button">показать/скрыть</span></a>
            <div class="accordion-content" data-tab-content>
                @foreach($similar as $item)
                    <a href="{{ url('category/select', $item->id) }}" class="category-link">
                        {{ $item->name }}&#160;({{ $item->products->count() }})
                    </a>
                @endforeach
            </div>
        </li>
    </ul>
</div>
<div class="medium-12 columns category-actions">
    <h2 class="category-h2">Действия:</h2>
    <div class="medium-12 columns">
        <a href="#" data-open="confirm" class="category-action-button">Сделать рутовой</a>
        <a data-open="subsidiary" class="category-action-button">
            Сделать дочерней
        </a>
        <a data-open="merge" class="category-action-button">
            Объединить
        </a>
        <a data-open="rename" class="category-action-button">
            Изменить название
        </a>
        <a data-open="show-in-menu" href="#" class="category-action-button">
            Отображение в меню
        </a>
        <a data-open="associate" href="#" class="category-action-button">
            Ассоциация
        </a>
    </div>
    <div class="reveal" id="confirm" data-reveal>
        <h1>Вы уверены?</h1>
        <p class="lead">Вы собираетесь сделать категорию "{{ $category->name }}" как родительскую</p>
        <p>После подтверждения категория станет родительской, подтвердите действие</p>
        <button class="button" @click="setRoot('{!! $category->id !!}')" data-close type="button">
        <span aria-hidden="true">Подтвердить</span>
        </button>
        <button class="button secondary" data-close type="button">
            <span aria-hidden="true">Отмена</span>
        </button>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @include('partials.category_modal', [
                    'modal_title' => 'Сделать дочерней',
                    'target' => 'subsidiary'])
    @include('partials.category_modal', [
                    'modal_title' => 'Объеденить',
                    'target' => 'merge'])
    <div class="reveal" id="rename" data-reveal>
        <h1 class="modal-h1">Переименовать</h1>
        <div class="medium-12 columns">
            <div class="search-bar-div">
                <form method="get" action="/">
                    <div class="row search-row collapse">
                        <div class="medium-8 search-column columns">
                            <input name="search"
                                   type="text"
                                   class="search-input"
                                   placeholder="Новое название"
                                   autocomplete="off"
                                   value="Старое название">
                        </div>
                        <div class="medium-4 button-column columns">
                            <button type="submit"
                                    class="button search-button float-right">
                                Переименовать
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="reveal" id="show-in-menu" data-reveal>
        <h1 class="modal-h1">Отображение в меню:</h1>
        <div class="medium-12 columns">
            <form action="/">
                <div class="medium-12 columns show">
                    <div class="medium-6 columns">
                        <label><input type="radio" name="toggle" value="on"
                                      checked="checked"><span>Показывать</span></label>
                    </div>
                    <div class="medium-6 columns">
                        <label><input type="radio" name="toggle"
                                      value="of"><span>Не показывать</span></label>
                    </div>
                    <div class="medium-12 columns">
                        <br>
                        <button type="submit" class="button float-right">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="reveal" id="associate" data-reveal>
        <h1 class="modal-h1">Отображение в меню:</h1>
        <div class="medium-12 columns">
            <form action="{{ URL::to('api/category/associate') }}" method="post">
                {{ csrf_field() }}
                <div class="medium-12 columns show">
                    <input type="hidden"
                           id="category_id"
                           name="category_id"
                           value="{{ $category->id }}">
                    <input type="hidden"
                           id="category_slug"
                           name="category_slug"
                           value="{{ $category->slug }}">
                    <div class="medium-6 columns medium-centered">
                        <label for="name">Имя</label>
                        <input type="text" id="name" name="name">
                        <label for="shop_id">ID магазина</label>
                        <input type="text" id="shop_id" name="shop_id">
                    </div>
                    <div class="medium-12 columns">
                        <br>
                        <button type="submit" class="button float-right">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
