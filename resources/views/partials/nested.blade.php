@foreach($categories as $node)
    @if($node->isRoot())
        <li>
            <a class="category-controls-name" href="#"><span>{{ $node->name }} ({{ ($node->checked != 0) ? 'Не проверено' : 'Проверено' }})</span></a>
            <div class="category-controls">
                <a href="#">Сделать главной</a>
                <a href="#">Объединить с текущей</a>
                <a href="#">Редактировать</a>
                <a href="{{ url('category/select', $node->id) }}">Выбрать</a>
                <a data-open="associate" href="#" class="category-action-button">Ассоциация</a>
            </div>
            <ul class="menu vertical nested">
                @include('partials.nested', ['categories' => $node->children])
            </ul>
        </li>
    @elseif($node->isLeaf())
        <li>
            <a class="category-controls-name" href="#"><span>{{ $node->name }} ({{ ($node->checked != 0) ? 'Не проверено' : 'Проверено' }})</span></a>
            <div class="category-controls">
                <a href="#">Сделать главной</a>
                <a href="#">Объединить с текущей</a>
                <a href="#">Редактировать</a>
                <a href="{{ url('category/select', $node->id) }}">Выбрать</a>
                <a data-open="associate" href="#" class="category-action-button">Ассоциация</a>
            </div>
        </li>
    @elseif ($node->isChild())
        <li>
            <a class="category-controls-name" href="#"><span>{{ $node->name }} ({{ ($node->checked != 0) ? 'Не проверено' : 'Проверено' }})</span></a>
            <div class="category-controls">
                <a href="#">Сделать главной</a>
                <a href="#">Объединить с текущей</a>
                <a href="#">Редактировать</a>
                <a href="{{ url('category/select', $node->id) }}">Выбрать</a>
                <a data-open="associate" href="#" class="category-action-button">Ассоциация</a>
            </div>
            <ul class="menu vertical nested">
                @include('partials.nested', ['categories' => $node->children])
            </ul>
        </li>
    @endif
@endforeach
