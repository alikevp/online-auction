<div class="container d-none d-sm-block">
    <div class="row">
        <div class="col-md-4 d-none d-md-block logo-wrapper">
            <div class="row">
                <div class="col-sm-4 logo-img">
                    <a href="/"><img src="{{asset('images/jinnmart-logo.png')}}"></a>
                </div>
                <div class="col-sm-8 logo-text">
                    <span class="text">аукци<span>on-line</span></span>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12 text-right main-nav-links-wrapper">
            <a href="/page/instruktsii" class="navigation-link decoration-green">
                <span class="bold-text">Как это работает?</span>
            </a>
            <a href="/page/instruktsii/kak-sdelat-zakaz" class="navigation-link decoration-green">
                <span class="bold-text">Заказ</span>
            </a>
            <a href="/page/sposoby-oplaty" class="navigation-link decoration-green">
                <span class="bold-text">Оплата</span>
            </a>
            <a href="/page/dostavka" class="navigation-link decoration-green">
                <span class="bold-text">Получение</span>
            </a>
            <a href="/page/vozvrat-tovara" class="navigation-link decoration-green">
                <span class="bold-text">Гарантия и возврат</span>
            </a>
        </div>
    </div>
</div>