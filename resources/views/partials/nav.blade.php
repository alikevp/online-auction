<div class="sidebar-holder">
    @foreach($categories as $category)
        @if ($category->isLeaf())
            @if ($category->leafs_offers_count !== 0 OR $category->offers_count !== 0)
                <div class="category-holder">
                    <a href="{{ route('category', $category->id) }}">
                        <div class="main-category last">
                            {{ $category->name }}
                        </div>
                    </a>
                </div>
            @endif
        @else
            @if ($category->leafs_offers_count !== 0 OR $category->offers_count !== 0)
                <div class="category-holder">
                    <a href="{{ route('category', $category->id) }}">
                        <div class="main-category">
                            {{ $category->name }}
                            <i class="fa fa-play left-arrow" aria-hidden="true"></i>
                        </div>
                    </a>
                    <div class="sub-category">
                        @include('partials.sidebar_main_parent',['categories'=>$category->children])
                    </div>
                </div>
            @endif
        @endif
    @endforeach
</div>

