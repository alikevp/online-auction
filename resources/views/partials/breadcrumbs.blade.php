<div class="col-sm-12">
    <nav aria-label="You are here:" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
            </li>
            @include('partials.bcpart', ['category' => $category])
            <li>
                <span>&nbsp;/ {{ $category->name }}</span>
            </li>
        </ol>
    </nav>
</div>