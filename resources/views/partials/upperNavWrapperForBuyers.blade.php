@include('partials.navMobileMenuForBuyers')
<div class="container d-none d-sm-block">
    <div class="row">
        <div class="col-lg-4 col-md-12 upper-nav-tabs-wrapper">
            <div class="upper-nav-tab active">
                <a href="" class="navigation-link decoration-green">
                    <span class="bold-text">Джинмарт</span><br>
                    <span class="grey-text">Магазин</span>
                </a>
            </div>
        </div>
        <div class="col-lg-8 col-md-12 text-right upper-nav-links-wrapper">
            <a href="/register/shop" class="navigation-link decoration-green">
                <span class="regular-text">Хотите продавать у нас?</span>
            </a>
            <a href="/page/instruktsii" class="navigation-link decoration-green">
                <span class="regular-text">Помощь</span>
            </a>
            <a href="/deferred" class="navigation-link decoration-green">
                <i class="fa header-menu-icon fa-heart-o" aria-hidden="true"></i> Отложенное
                <span class="deferred-items-count">(<span id="deferred-items-count-value"></span>)</span>
            </a>
            <a href="/cart" class="navigation-link decoration-green">
                <i class="fa header-menu-icon fa-shopping-cart" aria-hidden="true"></i> Корзина
                <span class="cart-items-count">(<span id="cart-items-count-value"></span>)</span>
            </a>
            <div class="dropdown show">
                <a class="navigation-link decoration-green" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="regular-text"> <i class="fa header-menu-icon fa-user-o" aria-hidden="true"></i> {{Auth::check() ? Auth::user()->surname." ".Auth::user()->name." ".Auth::user()->patronymic  : 'Вход'}}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="/profile">Профиль</a>
                    <a class="dropdown-item" href="/my-orders">Мои аукционы</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('profile/my_bonus') }}">Бонусы:
                        @if(null !== Auth::user()->finances()->latest()->first())
                            {{ Auth::user()->finances()->latest()->first()->total }}
                        @else
                            0
                        @endif
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/logout">Выход</a>
                </div>
            </div>
        </div>
    </div>
</div>