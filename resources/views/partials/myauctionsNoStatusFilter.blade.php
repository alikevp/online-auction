<form action="">
    <div class="col-xs-12">
        <label>Город:
            <input type="text" name="city" placeholder="Город доставки" class="form-control" value="{{ app('request')->input('city') == "" ? "" : app('request')->input('city')}}">
        </label>
        <hr>
    </div>
    <div class="col-xs-12">
        <label>Id аукциона:
            <input type="text" name="id" placeholder="Id аукциона" class="form-control" value="{{ app('request')->input('id') == "" ? "" : app('request')->input('id')}}">
        </label>
        <hr>
    </div>
    <div class="col-xs-12">
        <button type="submit" class="btn gradient-green" style="width: 100%;">Фильтровать</button>
    </div>
</form>