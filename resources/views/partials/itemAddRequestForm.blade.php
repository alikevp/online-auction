<div class="col-sm-12">
    <h1>Товары не найдены</h1>
    <span>
                    По Вашему запросу ничего не найдено. Уточните запрос,
                    или <a href="#" onclick="return false;" class="flash-link" data-toggle="modal" data-target="#personalPhotoAvatar">оставьте запрос</a>
                    на добавление отсутствующего товара.
                </span>
    <div class="modal fade" id="personalPhotoAvatar" tabindex="-1" role="dialog" aria-labelledby="personalPhotoAvatarModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Запрос на добавление товара</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="support_form" method="post" @submit.prevent="itemAddUpdateRequest">
                                <div class="modal-body">
									<div class="row">
										<div class="col-sm-6">
											<label>Фамилия
												<input type="text" placeholder="Иванов"
													   name="middle"
													   pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$" v-model="middle"  class="form-control"/>
											</label>
										</div>
										<div class="col-sm-6">
											<label>Имя
												<input type="text" placeholder="Иван" name="name" pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$" v-model="name" required class="form-control"/>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<label>Е-mail для связи
												<input type="email" placeholder="test@mail.ru" name="email" v-model="email" required class="form-control"/>

											</label>
										</div>
										<div class="col-sm-6">
											<label>Телефон
												<input type="text" maxlength="15" placeholder="8(999)999-99-99" name="tel"
													   pattern="^[\s0-9+()-]+$" v-model="tel" class="form-control"/>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label>Причина
												<select name="type" v-model="type_id" required class="form-control">
													<option value="item-add" selected>Добавление товара</option>
												</select>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label>Название товара
												<input type="text" placeholder="Название отсутствующего товара" name="theme"
												   v-model="theme"
												   required class="form-control"/>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label>Дополнительная информация
												<textarea placeholder="Укажите необходимую информацию о товаре" rows="10" name="message"
												  v-model="message" required class="form-control"></textarea>
											</label>
										</div>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="user_id"  v-model="user_id" />
								</div>
                                <div class="modal-footer">
                                        <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                        <button type="button" class="btn gradient-green width-50">Отправить</button>
                                </div>
                            </form>
                        </div>
                    </div>
    </div>
</div>
@section('head_scripts')
    @if(Auth::check())
        <script>
            var middle = '{{trim(Auth::user()->surname)}}';
            var name = '{{trim(Auth::user()->name)}}';
            var email = '{{trim(Auth::user()->email)}}';
            var tel = '{{trim(Auth::user()->telefone)}}';
            var type_id = '18';
        </script>
    @else
        <script>
            var middle = '';
            var name = '';
            var email = '';
            var tel = '';
            var type = '';
        </script>
    @endif
@endsection