@foreach($categories_tree as $node)
    @if($node->isRoot())
        <option value="{{ $node->id }}" disabled="disabled" style="color:#001dff;"><span>+</span>{{ $node->name }}</option>
        @include('partials.nested_select_unlight', ['categories_tree' => $node->children])
    @elseif($node->isLeaf())
        <option value="{{ $node->id }}">{{ $node->name }}</option>
    @elseif ($node->isChild())
        <option value="{{ $node->id }}" disabled="disabled"  style="color:#ff0050;"><span>++</span>{{ $node->name }}</option>
        @include('partials.nested_select_unlight', ['categories_tree' => $node->children])
    @endif
@endforeach
