<div class="row collapse user-navigation">
    <div class="medium-4 small-12 header-menu-left columns">
        <a href="/deferred"><i class="fa header-menu-icon fa-heart-o" aria-hidden="true"></i> Отложенное<span
                    class="deferred-items-count"></span></a><br>
    </div>
    <div class="medium-4 small-12 header-menu-right columns">
        <a href="{{action('CartController@index')}}"><i class="fa header-menu-icon fa-shopping-cart"
                                                        aria-hidden="true"></i> Корзина <span
                    class="cart-items-count">(<span id="cart-items-count-value">0</span>)</span></a>
    </div>
    <div class="medium-4 small-12 header-menu-right columns">
        <ul class="dropdown menu" data-dropdown-menu>
            <li>
                <a href="#"><i class="fa header-menu-icon fa-user-o" aria-hidden="true"></i> {{Auth::check() ? Auth::user()->surname." ".Auth::user()->name." ".Auth::user()->patronymic  : 'Вход'}}</a>
                <ul class="menu is-dropdown-submenu">
                    @if(Auth::check())
                        <li><a class="dropdown-item" href="/profile">Профиль</a></li>
                        <li><a class="dropdown-item" href="/my-orders">Мои аукционы</a></li>
                        <hr class="menu-devider">
                        <li>
                            <a class="dropdown-item" href="{{ url('profile/my_bonus') }}">Бонусы:
                                @if(null !== Auth::user()->finances()->latest()->first())
                                    {{ Auth::user()->finances()->latest()->first()->total }}
                                @else
                                    0
                                @endif
                            </a>
                        </li>
                        <hr class="menu-devider">
                        <li><a class="dropdown-item" href="/logout">Выход</a></li>
                    @else
                        <li><a class="dropdown-item" href="/login">Вход</a></li>
                        <li><a class="dropdown-item" href="/register">Регистрация</a></li>
                        <li><a class="dropdown-item" href="/password/reset">Восстановить пароль</a></li>
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</div>