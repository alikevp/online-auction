f@extends('layouts.master')

@section('content')
    <form data-abide novalidate action="{{ action('SendMailController@send') }}" method="post">
        {{ csrf_field() }}
        <div data-abide-error class="alert callout" style="display: none;">
            <p><i class="fi-alert"></i> Упс, что-то пошло не так!</p>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Email
                    <input type="email"
                           name="email"
                           placeholder="your@email.com"
                           aria-describedby="exampleHelpText"
                           required
                           pattern="email">
                </label>
                <p class="help-text" id="exampleHelpText">Не-а, это не похоже на email</p>
            </div>
        </div>
        <div class="row">
            <fieldset class="large-6 columns">
                <button class="button" type="submit" value="Submit">Поехали!!!</button>
            </fieldset>
        </div>
    </form>
@endsection