@if($category->isChild())
    @include('partials.bcpart', ['category' => $category->parent])
    <li class="breadcrumb-item"><a href="{{ route('category', ['id' => $category->parent->id]) }}">{{ $category->parent->name }}</a></li>
@endif