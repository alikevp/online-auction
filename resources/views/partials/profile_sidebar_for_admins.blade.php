<div class="col-sm-2">
    <ul class="left-menu menu vertical">
        @if(Auth::check())
            <?php $user = Auth::user(); ?>
            @if($user->type != "shop_admin")
                <li><a class="{{ Request::path() == 'my-orders' ? 'left-menu-active' : '' }}" href="/my-orders"><i class="fa fa-bars" aria-hidden="true"></i>Мои заказы</a></li>
                <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Моя корзина</a></li>
            @endif
            @if( $user->type == 'shop_admin')
                <li>
                    <a class="{{ Request::path() == 'auction/orderlist' ? 'left-menu-active' : '' }}" href="/auction/orderlist"><i class="fa fa-bars" aria-hidden="true"></i>Доступные заказы
                        @include('partials.orderCounter')
                    </a>
                </li>
                <li><a class="{{ Request::path() == 'my-auctions' ? 'left-menu-active' : '' }}" href="/my-auctions"><i class="fa fa-gavel" aria-hidden="true"></i>Мои аукционы</a></li>
                <li>
                    <a class="{{ Request::path() == 'auction/accepted' ? 'left-menu-active' : '' }}" href="/auction/accepted"><i class="fa fa-check" aria-hidden="true"></i>Выигранные аукционы
                        @include('partials.winCounter')
                    </a>
                </li>
                    <li><a class="{{ Request::path() == 'hided_products' ? 'left-menu-active' : '' }}" href="/auction/hided_products"><i class="fa fa-eye-slash" aria-hidden="true"></i>Скрытые аукционы</a></li>
            @endif
            <hr>
            <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Выход</a></li>
            @if(Request::path() == 'auction/orderlist')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                        <a class="flash-link" href="orderlist">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.orderlistFilter')
                <br>
            @endif

            @if(Request::path() == 'my-auctions')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('pages')))
                        <a class="flash-link" href="my-auctions">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.myauctionsFilter')
                <br>
            @endif

            @if(Request::path() == 'auction/accepted' OR Request::path() == 'auction/hided_products')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                        <a class="flash-link" href="/{{ Request::path() }}">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.myauctionsNoStatusFilter')
                <br>
            @endif

            @if(Request::path() == 'my-orders')
                <br>
                <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                @if(app('request')->input())
                    @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                        <a class="flash-link" href="my-orders">(сбросить)</a>
                    @endif
                @endif
                <hr>
                @include('partials.myOrdersFilter')
                <br>
            @endif
        @else
            <li><a class="{{ Request::path() == 'login' ? 'left-menu-active' : '' }}" href="/login"><i class="fa fa-user" aria-hidden="true"></i> Вход кабинет</a></li>
            <li><a class="{{ Request::path() == 'register' ? 'left-menu-active' : '' }}" href="/register"><i class="fa fa-user-plus" aria-hidden="true"></i> Регистрация</a></li>
            <li><a class="{{ Request::path() == 'password/reset' ? 'left-menu-active' : '' }}" href="/password/reset"><i class="fa fa-refresh" aria-hidden="true"></i> Восстановить пароль</a></li>
            <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Моя корзина</a></li>
        @endif
    </ul>
</div>