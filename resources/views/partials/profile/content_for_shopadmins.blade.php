<div class="col-sm-12">
    <h1>Мои данные</h1>
    @if (Session::has('msg'))
        <div class="alert alert-{{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" role="alert">
            {{ Session::get("msg") }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6 col-sm-12 dashed-divs">
            <div class="col-sm-12 dashed-block">
                <h2>Персональные данные</h2>
                <div class="modal fade" id="personalPhotoAvatar" tabindex="-1" role="dialog" aria-labelledby="personalPhotoAvatarModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Аватар</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/set_avatar" method="post" data-abide novalidate data-live-validate="true">
                                <div class="modal-body">
                                    <div class="col-sm-6 modal-left avatar-bloc">
                                        @foreach($avatars as $avatar)
                                            <label>
                                                <input type="radio" name="ava" value="{{asset('default/avatars').'/'.$avatar}}" required>
                                                <img src="{{asset('default/avatars').'/'.$avatar}}" width="200" height="150">
                                            </label>
                                        @endforeach
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                    <button type="submit" class="btn gradient-green width-50">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <table class="personal-data-table" id="user-info">
                    <tr class="tr_avatar">
                        <td><img src="{{Auth::user()->url_avatar}}" width="120"></td>
                        <td>
                            <a class="avatar-add" data-toggle="modal" data-target="#personalPhotoAvatar">
                                {{ (empty(Auth::user()->url_avatar)?'Добавить аватарку' : 'Изменить аватарку') }}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><span>ФИО:</span></td>
                        <td><span>{{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->patronymic }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td><span>E-mail: </span></td>
                        <td>
                            <span>{{ Auth::user()->email }}</span>
                            @if(!Auth::user()->verified)
                                <div class="arrow-text float-right">
                                    <a href="#" v-on:click="sendMail({{ Auth::user()->id }})">Подтвердить</a>
                                </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><span>Дата рождения:</span></td>
                        <td><span>{{ Auth::user()->date_of_birth }}</span></td>
                    </tr>
                    <tr>
                        <td><span>Пол:</span></td>
                        <td>
                        <span>
                            @if(Auth::user()->sex == "m")
                                Мужской
                            @elseif(Auth::user()->sex == "f")
                                Женский
                            @else
                                Не указан
                            @endif
                        </span>
                        </td>
                    </tr>
                    <tr>
                        <td><span>ИНН:</span></td>
                        <td><span>{{ Auth::user()->inn }}</span></td>
                    </tr>
                </table>
                <a class="flash-link" data-toggle="modal" data-target="#personalDataEdit">Изменить персональные данные</a>
                <div class="modal fade" id="personalDataEdit" tabindex="-1" role="dialog" aria-labelledby="personalDataEditModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Персональные данные</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/personal_data_edit" method="post" data-abide novalidate data-live-validate="true">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label><span class="bold-text">Имя</span> <span class="red-star">*</span>
                                                <input class="form-control" type="text" placeholder="Ваше имя" name="name" value="{{ Auth::user()->name }}" required>
                                            </label>
                                            <label><span class="bold-text">Фамилия</span> <span class="red-star">*</span>
                                                <input class="form-control" type="text" placeholder="Ваша фамилия" name="surname" value="{{ trim(Auth::user()->surname) }}" required>
                                            </label>
                                            <label><span class="bold-text">Отчество</span> <span class="red-star">*</span>
                                                <input class="form-control" type="text" placeholder="Ваше отчество" name="patronymic" value="{{ Auth::user()->patronymic }}" required>
                                            </label>
                                            <div class="col-sm-12 sex float-left">
                                                <span class="bold-text">Пол</span><span class="red-star">*</span>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label><input type="radio" value="m" name="toggle" {{ Auth::user()->sex == "m" ? "checked" : ""}}><span>Мужской</span></label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label><input type="radio" value="f" name="toggle" {{ Auth::user()->sex == "f" ? "checked" : ""}}><span>Женский</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-sm-12">
                                                <label><span class="bold-text">Дата рождения</span>
                                                    <input class="form-control" size="16" type="text" id="checkdata" name="dob" data-date-format="YYYY-MM-DD" value="{{ Auth::user()->date_of_birth != NULL ? Auth::user()->date_of_birth : "" }}">
                                                </label>
												<label><span class="bold-text">ИНН</span> <span class="red-star">*</span>
                                                <input class="form-control" type="text" minlength="10" maxlength="12" pattern="\d*" placeholder="ИНН" name="inn" value="{{ Auth::user()->inn }}" required>
                                            </label>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                    <button type="submit" class="btn gradient-green width-50">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12 dashed-divs">
            <div class="col-sm-12 dashed-block">
                <h2>Номер мобильного телефона</h2>
                <span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
                <span class="number">{{ Auth::user()->telefone }}</span>
                <a class="flash-link" data-toggle="modal" data-target="#telefone-edit">Изменить номер телефона</a>
                <div class="modal fade" id="telefone-edit" tabindex="-1" role="dialog" aria-labelledby="telefone-editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Изменение телефона</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/telefone_edit" method="post" data-abide novalidate data-live-validate="true">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
                                            <input class="form-control" type="text" id="checktel" placeholder="Ваш телефон" name="telefone" value="{{ Auth::user()->telefone }}" required>
                                        </label>
                                    </div>
                                    <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                    <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 dashed-block">
                <h2>Основной адрес электронной почты</h2>
                <span class="e-mail">{{ Auth::user()->email }}</span>
                <h2>Адрес для оповещений</h2>
                <span class="e-mail">{{ Auth::user()->e_mail }}</span>
                <a class="flash-link" data-toggle="modal" data-target="#email-edit" href="#">Смена email</a>
                <div class="modal fade" id="email-edit" tabindex="-1" role="dialog" aria-labelledby="email-editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Изменение E-mail</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="form-change-password" role="form" method="POST" action="email_deal" data-abide novalidate data-live-validate="true">
                                <div class="modal-body">
                                    <label for="email_deal">Введите E-mail для рассылки <span class="red-star">*</span></label>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email_deal" name="email_deal" placeholder="E-mail" value="{{ Auth::user()->e_mail }}" required>
                                    </div>
                                    <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="modal-footer">
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                    <button type="submit" class="btn gradient-green width-50">Изменить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 dashed-block">
                <h2>Пароль</h2>
                <span class="info">Здесь вы можете изменить свой пароль для входа в личный кабинет</span>
                <a class="flash-link" data-toggle="modal" data-target="#password-edit" href="#">Смена пароля</a>
                <div class="modal fade" id="password-edit" tabindex="-1" role="dialog" aria-labelledby="password-editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Изменение пароля</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="form-change-password" role="form" method="POST" action="password_edit" data-abide novalidate data-live-validate="true">
                                <div class="modal-body">
                                    <label for="current-password">Текущий пароль <span class="red-star">*</span></label>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Пароль" required>
                                    </div>
                                    <label for="password">Новый пароль <span class="red-star">*</span></label>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" required>
                                    </div>
                                    <label for="password_confirmation">Повторите новый пароль <span class="red-star">*</span></label>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Новый пароль еще раз" required>
                                    </div>
                                    <span class="form-notice">Обязательные поля помечены символом <span class="red-star">*</span></span>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="col-xs-12 modal-footer">
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                    <button type="submit" class="btn gradient-green width-50">Добавить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
