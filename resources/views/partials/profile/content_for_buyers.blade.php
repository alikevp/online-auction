<div class="col-sm-12">
	<h1>Мои данные</h1>
	@if (Session::has('msg'))
		@if (Session::has('msg'))
			<div class="alert alert-{{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}"
			     role="alert">
				{{ Session::get("msg") }}
			</div>
		@endif
	@endif
	<div class="row">
		<div class="col-lg-6 col-sm-12 dashed-divs">
			<div class="col-sm-12 dashed-block">
				<h2>Персональные данные</h2>
				<div class="modal fade" id="personalPhotoAvatar" tabindex="-1" role="dialog"
				     aria-labelledby="personalPhotoAvatarModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Аватар</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="/set_avatar" method="post" data-abide novalidate data-live-validate="true">
								<div class="modal-body">
									<div class="col-sm-12 modal-left avatar-bloc">
										<div class="row">
											@foreach($avatars as $avatar)
												<div class="col-sm-4">
													<label>
														<input type="radio" name="ava"
														       value="{{asset('default/avatars').'/'.$avatar}}"
														       required>
														<img src="{{asset('default/avatars').'/'.$avatar}}" width="120"
														     height="120">
													</label>
												</div>
											@endforeach
										</div>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Сохранить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6">
							<img src="{{Auth::user()->url_avatar}}" width="120">
						</div>
						<div class="col-sm-6" style="padding:15px;">
							<a class="avatar-add" data-toggle="modal" data-target="#personalPhotoAvatar">
								{{ (empty(Auth::user()->url_avatar)?'Добавить аватарку' : 'Изменить аватарку') }}
							</a>
						</div>
						<div class="col-sm-6">
							<span>ФИО:</span>
						</div>
						<div class="col-sm-6">
							<span>{{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->patronymic }}</span>
						</div>
						<div class="col-sm-6">
							<span>E-mail: </span>
						</div>
						<div class="col-sm-6">
							<span>{{ Auth::user()->email }}</span>
							@if(!Auth::user()->verified)
								<div class="arrow-text float-right">
									<a href="#" v-on:click="sendMail({{ Auth::user()->id }})">Подтвердить</a>
								</div>
							@endif
						</div>
						<div class="col-sm-6">
							<span>Дата рождения:</span>
						</div>
						<div class="col-sm-6">
							<span>{{ Auth::user()->date_of_birth }}</span>
						</div>
						<div class="col-sm-6">
							<span>Пол:</span>
						</div>
						<div class="col-sm-6">
							<span>
								@if(Auth::user()->sex == "m")
									Мужской
								@elseif(Auth::user()->sex == "f")
									Женский
								@else
									Не указан
								@endif
							</span>
						</div>
						<div class="col-sm-6">
							<span>ИНН:</span>
						</div>
						<div class="col-sm-6">
							<span>{{ Auth::user()->inn }}</span>
						</div>
					</div>
				</div>
				<a class="flash-link" data-toggle="modal" data-target="#personalDataEdit">Изменить персональные
					данные</a>
				<div class="modal fade" id="personalDataEdit" tabindex="-1" role="dialog"
				     aria-labelledby="personalDataEditModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Персональные данные</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="/personal_data_edit" method="post" data-abide novalidate
							      data-live-validate="true">
								<div class="modal-body">
									<div class="row">
										<div class="col-sm-6">
											<label><span class="bold-text">Имя</span> <span class="red-star">*</span>
												<input class="form-control" type="text" placeholder="Ваше имя"
												       name="name" value="{{ Auth::user()->name }}" required>
											</label>
											<label><span class="bold-text">Фамилия</span> <span
														class="red-star">*</span>
												<input class="form-control" type="text" placeholder="Ваша фамилия"
												       name="surname" value="{{ trim(Auth::user()->surname) }}"
												       required>
											</label>
											<label><span class="bold-text">Отчество</span> <span
														class="red-star">*</span>
												<input class="form-control" type="text" placeholder="Ваше отчество"
												       name="patronymic" value="{{ Auth::user()->patronymic }}"
												       required>
											</label>
											<div class="col-sm-12 sex float-left">
												<span class="bold-text">Пол</span><span class="red-star">*</span>
												<div class="row">
													<div class="col-sm-6">
														<label><input type="radio" value="m"
														              name="toggle" {{ Auth::user()->sex == "m" ? "checked" : ""}}><span>Мужской</span></label>
													</div>
													<div class="col-sm-6">
														<label><input type="radio" value="f"
														              name="toggle" {{ Auth::user()->sex == "f" ? "checked" : ""}}><span>Женский</span></label>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
                                            <span class="gifts-info">
                                              Чтобы получать подарки и сюрпризы, пожалуйста, укажите свои данные. <br><br>
                                            </span>
											<div class="col-sm-12">
												<label><span class="bold-text">Дата рождения</span>
													<input class="form-control" size="16" type="text" id="checkdata"
													       name="dob" data-date-format="YYYY-MM-DD"
													       value="{{ Auth::user()->date_of_birth != null ? Auth::user()->date_of_birth : "" }}">
												</label>
											</div>
											<div class="col-sm-12">
												<label><span class="bold-text">ИНН</span><span class="red-star">*</span>
													<input class="form-control" type="text" id="checkinn"
													       placeholder="Ваш ИНН" name="inn" minlength="10" maxlength="12" pattern="\d*"
													       value="{{ trim(Auth::user()->inn) }}" required>
												</label>

											</div>
										</div>
									</div>
									<span class="form-notice">Обязательные поля помечены символом <span
												class="red-star">*</span></span>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Сохранить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 dashed-block">
				<h2>Адреса доставки</h2>
				@foreach($addresses as $key => $address)
					<span class="info">{{ $address->country }}, {{ $address->city }}, {{ $address->street }}
						, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}</span>
					<a class="flash-link" data-toggle="modal" data-target="#address-update{{ $key }}">Изменить адрес</a>
					<a class="flash-link" data-toggle="modal" data-target="#address-delete{{ $key }}"
					   style="margin-left: 30px">Удалить адрес</a>
					<hr>
					<div class="modal fade" id="address-update{{ $key }}" tabindex="-1" role="dialog"
					     aria-labelledby="address-update{{ $key }}ModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">обновление адреса</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="/address_update/{{ $address->id }}" method="post" data-abide novalidate
								      data-live-validate="true">
									<div class="modal-body">
										<div class="row">
											<div class="col-sm-6">
												<label><span class="bold-text">Страна</span><span
															class="red-star">*</span>
													<input class="form-control" type="text" placeholder="Ваша страна"
													       name="country" value="{{ $address->country }}" required>
												</label>
											</div>
											<div class="col-sm-6">
												<label><span class="bold-text">Населенный пункт/Город</span><span
															class="red-star">*</span>
													<input class="form-control" id="dadata_city" type="text"
													       placeholder="Ваш город" name="city"
													       value="{{ $address->city }}" required>
												</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<label class="width-100"><span class="bold-text">Улица</span><span
															class="red-star">*</span>
													<input class="form-control" id="dadata_street" type="text"
													       placeholder="Ваша улица" name="street"
													       value="{{ $address->street }}" required>
												</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<label><span class="bold-text">Дом</span><span class="red-star">*</span>
													<input class="form-control" id="dadata_house" type="text"
													       placeholder="Номер вашего дома" name="house"
													       value="{{ $address->house }}" required>
												</label>
											</div>
											<div class="col-sm-4">
												<label><span class="bold-text">Квартира</span>
													<input class="form-control" id="dadata_flat" type="text"
													       placeholder="Номер вашей квартиры" name="apartment"
													       pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$"
													       value="{{ $address->apartment }}">
												</label>
											</div>
											<div class="col-sm-4">
												<label><span class="bold-text">Индекс</span><span
															class="red-star">*</span>
													<input class="form-control" id="dadata_postal_code" type="text"
													       maxlength="6" placeholder="Ваш почтовый индекс"
													       name="post_index" value="{{ $address->post_index }}"
													       required>
												</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<label><span class="bold-text">Офис</span><span
															class="red-star">*</span>
													<input class="form-control" type="text" placeholder="офис"
													       name="office" value="{{ $address->office }}" required>
												</label>
											</div>
										</div>
										<span class="form-notice">Обязательные поля помечены символом <span
													class="red-star">*</span></span>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									<div class="modal-footer">
										<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
											Отменить
										</button>
										<button type="submit" class="btn gradient-green width-50">Сохранить</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade" id="address-delete{{ $key }}" tabindex="-1" role="dialog"
					     aria-labelledby="address-delete{{ $key }}ModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Удаление адреса</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="/address_delete/{{ $address->id }}" method="post" data-abide novalidate
								      data-live-validate="true">
									<div class="modal-body">
										<div class="col-sm-12">
											<span class="info">{{ $address->country }}, {{ $address->city }}
												, {{ $address->street }}, {{ $address->house }}
												, {{ $address->apartment }}, {{ $address->post_index }}</span>
										</div>
										<hr>
										<div class="col-sm-12">
											<span>Вы уверены, что хотите удалить этот адрес?</span>
										</div>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									<div class="modal-footer">
										<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
											Отменить
										</button>
										<button type="submit" class="btn gradient-green width-50">Удалить</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<br>
				@endforeach
				<a class="address-add" data-toggle="modal" data-target="#address-add">ДОБАВИТЬ АДРЕС</a>
				<div class="modal fade" id="address-add" tabindex="-1" role="dialog"
				     aria-labelledby="address-addModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Добавление адреса</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="/address_edit" method="post" data-abide novalidate data-live-validate="true">
								<div class="modal-body">
									<div class="row">
										<div class="col-sm-6">
											<label><span class="bold-text">Страна</span><span class="red-star">*</span>
												<input class="form-control" type="text" placeholder="Ваша страна"
												       name="country" required>
											</label>
										</div>
										<div class="col-sm-6">
											<label><span class="bold-text">Населенный пункт/Город</span><span
														class="red-star">*</span>
												<input class="form-control" id="dadata_city" type="text"
												       placeholder="Ваш город" name="city"												       required>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label class="width-100"><span class="bold-text">Улица</span><span
														class="red-star">*</span>
												<input class="form-control" id="dadata_street" type="text"
												       placeholder="Ваша улица" name="street"
												       required>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<label><span class="bold-text">Дом</span><span class="red-star">*</span>
												<input class="form-control" id="dadata_house" type="text"
												       placeholder="Номер вашего дома" name="house"
												       required>
											</label>
										</div>
										<div class="col-sm-4">
											<label><span class="bold-text">Квартира</span>
												<input class="form-control" id="dadata_flat" type="text"
												       placeholder="Номер вашей квартиры" name="apartment"
												       pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$">
											</label>
										</div>
										<div class="col-sm-4">
											<label><span class="bold-text">Индекс</span><span class="red-star">*</span>
												<input class="form-control" id="dadata_postal_code" type="text"
												       maxlength="6" placeholder="Ваш почтовый индекс" name="post_index"
												       required>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<label><span class="bold-text">Офис</span><span class="red-star">*</span>
												<input class="form-control" type="text" placeholder="офис" name="office"
												       required>
											</label>
										</div>
									</div>
									<span class="form-notice">Обязательные поля помечены символом <span
												class="red-star">*</span></span>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Добавить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-12 dashed-divs">
			<div class="col-sm-12 dashed-block">
				<h2>Количество просроченных аукционов: {{ Auth::user()->auctions_overdue }}</h2>
			</div>
			<div class="col-sm-12 dashed-block">
				<h2>Номер мобильного телефона</h2>
				<span class="info">Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
				<span class="number">{{ Auth::user()->telefone }}</span>
				<a class="flash-link" data-toggle="modal" data-target="#telefone-edit">Изменить номер телефона</a>
				<div class="modal fade" id="telefone-edit" tabindex="-1" role="dialog"
				     aria-labelledby="telefone-editModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Изменение телефона</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="/telefone_edit" method="post" data-abide novalidate data-live-validate="true">
								<div class="modal-body">
									<div class="form-group">
										<label>Укажите свой действующий мобильный номер<span class="red-star">*</span>
											<input class="form-control" type="text" id="checktel"
											       placeholder="Ваш телефон" name="telefone"
											       value="{{ Auth::user()->telefone }}" required>
										</label>
									</div>
									<span class="form-notice">Обязательные поля помечены символом <span
												class="red-star">*</span></span>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Изменить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 dashed-block">
				<h2>Основной адрес электронной почты</h2>
				<span class="e-mail">{{ Auth::user()->email }}</span>
				<h2>Адрес для оповещений</h2>
				<span class="e-mail">{{ Auth::user()->e_mail }}</span>
				<a class="flash-link" data-toggle="modal" data-target="#email-edit" href="#">Смена email</a>
				<div class="modal fade" id="email-edit" tabindex="-1" role="dialog"
				     aria-labelledby="email-editModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Изменение E-mail</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form id="form-change-password" role="form" method="POST" action="email_deal" data-abide
							      novalidate data-live-validate="true">
								<div class="modal-body">
									<label for="email_deal">Введите E-mail для рассылки <span class="red-star">*</span></label>
									<div class="form-group">
										<input type="email" class="form-control" id="email_deal" name="email_deal"
										       placeholder="E-mail" value="{{ Auth::user()->e_mail }}" required>
									</div>
									<span class="form-notice">Обязательные поля помечены символом <span
												class="red-star">*</span></span>
								</div>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Изменить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 dashed-block">
				<h2>Пароль</h2>
				<span class="info">Здесь вы можете изменить свой пароль для входа в личный кабинет</span>
				<a class="flash-link" data-toggle="modal" data-target="#password-edit" href="#">Смена пароля</a>
				<div class="modal fade" id="password-edit" tabindex="-1" role="dialog"
				     aria-labelledby="password-editModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Изменение пароля</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form id="form-change-password" role="form" method="POST" action="password_edit" data-abide
							      novalidate data-live-validate="true">
								<div class="modal-body">
									<label for="current-password">Текущий пароль <span class="red-star">*</span></label>
									<div class="form-group">
										<input type="password" class="form-control" id="current-password"
										       name="current-password" placeholder="Пароль" required>
									</div>
									<label for="password">Новый пароль <span class="red-star">*</span></label>
									<div class="form-group">
										<input type="password" class="form-control" id="password" name="password"
										       placeholder="Новый пароль" required>
									</div>
									<label for="password_confirmation">Повторите новый пароль <span
												class="red-star">*</span></label>
									<div class="form-group">
										<input type="password" class="form-control" id="password_confirmation"
										       name="password_confirmation" placeholder="Новый пароль еще раз" required>
									</div>
									<span class="form-notice">Обязательные поля помечены символом <span
												class="red-star">*</span></span>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="col-xs-12 modal-footer">
									<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">
										Отменить
									</button>
									<button type="submit" class="btn gradient-green width-50">Добавить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
