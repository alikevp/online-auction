@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-referrals-page.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Мои приглашения
@endsection

@section('content')
	@include('partials.sidebarBuyerWrapper')
	
    <div class="col-sm-9" id="right-block">
		<div class="col-sm-12 right-block">
			<h1>Мои приглашения</h1>
			<div class="col-sm-12 orders-wraper" id="items-table-block">
				<div class="cur_balance">
					<div>Ссылка для приглашения:</div>
					<span id="guest-referral">{{ route('referral', Auth::user()->id) }}</span>
					<a id="btn" data-clipboard-text="{{ route('referral', Auth::user()->id) }}" class="flash-link">
						<i class="fa fa-copy"></i>
					</a>
					<script type="text/javascript" src="{{asset('js/plugins/clipboard/clipboard.min.js')}}"></script>
					<script type="text/javascript">
						var btn = document.getElementById('btn');
						var clipboard = new Clipboard(btn);
						clipboard.on('success', function(e) {
							alert('Ссылка успешно скопированна в буфер обмена');
						});
					</script>
					<p class="description">Чтобы получать бонусы, сообщите эту ссылку своим знакомым для их регистрации на сайте. <a class="flash-link" href="/page/bonusnaya-sistema">Подробнее</a></p>
				</div>
			</div>
			<br>
			<h3 class="order-title">История приглашений:</h3>
			<div class="col-sm-12 orders-wraper" id="items-table-block">
				<div class="row" style="padding:10px;">
					@if(count($referrals)>0)
						<table class="unstriped personal-data-table" id="user-info" style="width:100%;">
							<thead style="font-weight:bold;background-color:#f4f4f4;">
								<tr>
									<td>Дата регистрации</td>
									<td>ФИО</td>
									<td>Первая покупка</td>
									<td>Списания</td>
									<td>ИТОГ</td>
								</tr>
							</thead>
							@foreach($referrals as $item)
								<tr>
									<td>{{ $item->created_at }}</td>
									<td>{{ $item->surname }} {{ $item->name }} {{ $item->patronymic }}</td>
									<td>
										@if($item->bought == true)
											Состоялась
										@else
											Нет
										@endif
									</td>
								</tr>
							@endforeach
						</table>
					@else
						<div class="row cur_balance">Вы еще не пригласили ни одного друга.</span></div>
					@endif
				</div>
			</div>
		</div>
	</div>

@endsection