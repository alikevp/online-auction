@foreach($categories as $category)
    @if ($category->leafs_offers_count !== 0 OR $category->offers_count !== 0)
        <div class="child-category">
            <a href="{{ route('category', $category->id) }}">{{ $category->name }}</a>
        </div>
    @endif
@endforeach