<div class="mobile-header d-block d-sm-none">
    <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
        <div class="col-sm-4 logo-img">
            <a class="navmenu-brand" href="/"><img src="{{asset('images/jinnmart-logo.png')}}"></a>
        </div>
        <ul class="nav navmenu-nav">
            <li>
                <a href="/page/instruktsii" class="navigation-link decoration-green">
                    <span class="regular-text">Помощь</span>
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <a class="navigation-link decoration-green" href="/profile">Профиль администратора</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="/mail_subscription">Оповещения</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="/shop_profile">Данные магазина</a>
            </li>
            <li>
                <a href="/shop_subscriptions" class="navigation-link decoration-green">Подписки магазина</a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <a class="navigation-link decoration-green" href="{{ url('auction/orderlist') }}">Доступные заказы</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="{{ url('my-auctions') }}">Мои аукционы</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="{{ url('auction/accepted') }}">Выигранные аукционы</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="{{ url('auction/hided_products') }}">Скрытые аукционы</a>
            </li>
            <li>
                <a class="navigation-link decoration-green" href="/logout">Выход</a>
            </li>
        </ul>
    </nav>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="col-xs-6">
            <button type="button" class="btn navbar-toggle gradient-green" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
                <i class="fa fa-bars" aria-hidden="true"></i>
                Меню
            </button>
        </div>
        <div class="col-xs-6 logo-text">
            <span class="text">аукци<span>on-line</span></span>
        </div>
    </div>
</div>