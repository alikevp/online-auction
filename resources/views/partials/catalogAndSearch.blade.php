<div class="col-sm-3 d-none d-sm-block">
    <div class="catalog-wrapper dropdown">
        <div class="catalog-header gradient-green" id="catalog" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
            <span>Каталог товаров</span>
            <span class="fa fa-bars" aria-hidden="true"></span>
        </div>
        <div class="dropdown-menu fixed-bottom" aria-labelledby="catalog">
            <div class="catalog-list">
                @include('partials.nav',['categories'=>$main_categories])
            </div>
        </div>
    </div>
</div>

<div class="col-sm-9 col-xs-12 left-sidebar-wrapper">
    <div class="serach-wrapper">
        <form class="serachForm" method="get" action="{{ action('ShoppingController@search') }}">
            <div class="input-group">
                <div class="input-group-btn search-panel">
                    <select class="form-control" id="search-where" name="searchWhere">
                        <option selected>Везде</option>
                        <option>Contains</option>
                    </select>
                </div>
                <input name="search"
                       id="search-what"
                       type="text"
                       list="suggestions"
                       class="search-input form-control"
                       @keyup="find"
                       v-model="query"
                       placeholder="Поиск по сайту..."
                       autocomplete="off">
                <div class="suggestion-div col-sm-5Z">
                    <datalist id="suggestions">
                        <option v-for="suggest in suggestion">
                            {{--<a href="#">--}}
                            @{{ suggest }}
                            {{--</a>--}}
                        </option>
                        {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                    </datalist>
                    {{--<a class="suggestion" href="search=>@{{ suggest.name }}">@{{ query }}</a>--}}
                </div>
                <span class="input-group-btn">
                                    <input class="btn gradient-green" type="submit" id="search-submit" name="searchSubmit" value="Найти" @click="find">
                                </span>
            </div>

        </form>
    </div>
</div>
@section('footer_scropts')
    <script>
        $(document).ready(function(e){
            $('.search-panel .dropdown-menu').find('a').click(function(e) {
                e.preventDefault();
                var param = $(this).attr("href").replace("#","");
                var concept = $(this).text();
                $('.search-panel span#search_concept').text(concept);
                $('.input-group #search_param').val(param);
            });
        });
    </script>
@endsection