@if(count($comments) == 0)
    <div class="medium-12 columns">
        <p>Отзывов нет.</p>
    </div>
@else
    @foreach($comments as $comment)
        <div class="col-sm-12 comment">
            <span class="commenter-name"><p>{{ $comment->user_name }}</p></span>
            <span class="rating"><p class="title">Рейтинг: </p><p class="value">{{ $comment->rating }}</p></span>
            <span class="advantages"><p class="title">Достоинства: </p><p class="value">{{ $comment->advantages }}</p></span>
            <span class="disadvantages"><p class="title">Недостатки: </p><p class="value">{{ $comment->disadvantages }}</p></span>
            <span class="text"><p class="title">Комментарий: </p><br><p class="value">{{ $comment->text }}</p></span>
            <span class="date"><p>{{ $comment->created_at }}</p></span>
        </div>
    @endforeach
@endif
<hr>
    <div class="row">
        <div class="col-sm-6">

        </div>
        <div class="col-sm-6 flex-horizontal-right">
            <a class="flash-link" data-toggle="modal" data-target="#recall-rules-modal">Правила оформления отзывов</a>
        </div>
        <div class="modal fade" id="recall-rules-modal" tabindex="-1" role="dialog" aria-labelledby="recallRulesModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Правила оформления отзывов</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="lead">Подробно расскажите о своём опыте использования товара. Обратите внимание на качество, удобство модели, её соответствие заявленным характеристикам.</p>
                        <p>Мы не публикуем отзывы, которые содержат оскорбления и ненормативную лексику.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn gradient-green" data-dismiss="modal">Понятно</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<button type="button" class="btn gradient-green" data-toggle="collapse" data-target="#comment-form"> Написать отзыв</button>
<div id="comment-form" class="collapse comment-form">
    <form class="form-horizontal" role="form" action="comment&={{ $id  }}" method="post">
            <div class="form-group row">
                <label for="name" class="col-sm-2 control-label">Имя<span class="red-star">*</span></label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="Ваше имя" name="name" @if(Auth::check()) value="{{ Auth::user()->name  }}" @endif required>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 control-label">E-mail<span class="red-star">*</span></label>
                <div class="col-sm-10">
                    <input class="form-control" type="email" placeholder="Ваш email" name="email" @if(Auth::check()) value="{{ Auth::user()->email  }}" @endif required>
                </div>
            </div>
            <div class="form-group row">
                <label for="rating" class="col-sm-2 control-label">Оценка<span class="red-star">*</span></label>
                <div class="col-sm-10">
                    <select class="form-control" name="rating" required>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected>5</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="advantages" class="col-sm-2 control-label">Достоинства</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="advantages" placeholder="Достоинства товара"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="disadvantages" class="col-sm-2 control-label">Недостатки</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="disadvantages" placeholder="Недостатки товара"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="text" class="col-sm-2 control-label">Комментарий</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="text" placeholder="Произвольный текст комментария"></textarea>
                </div>
            </div>
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="col-sm-12">
                    <button class="btn gradient-green" type="submit">Оставить отзыв</button>
                </div>
            </div>
    </form>
</div>