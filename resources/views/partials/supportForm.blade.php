<a href="#" data-toggle="modal" data-target="#helpModal" id="popup__toggle" onclick="return false;">
	<div class="circlephone" style="transform-origin: center;"></div>
	<div class="circle-fill" style="transform-origin: center;"></div>
	<div class="img-circle" style="transform-origin: center;">
		<div class="img-circleblock" style="transform-origin: center;"><i class="fa fa-question" aria-hidden="true"></i>
		</div>
	</div>
</a>
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Форма для связи с поддержкой</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="support_form" method="post" action="/api/support/save_request">
				<!--@submit.prevent="saveRequest"-->
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="middle">Фамилия</label>
								<input class="form-control" type="text" placeholder="Иванов" id="middle" name="middle"
								       pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$"
								       value="{{auth()->check() ? auth()->user()->surname : ''}}"
								       v-model="middle"/>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="name">Имя<span class="red-star">*</span></label>
								<input class="form-control" type="text" placeholder="Иван" id="name" name="name"
								       pattern="^[A-Za-zА-Яа-яЁё0-9$\s/$-]+$"
								       value="{{auth()->check() ? auth()->user()->name : ''}}"
								       v-model="name" required/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="email">Е-mail для связи<span
											class="red-star">*</span></label>
								<input class="form-control" type="email"
								       value="{{auth()->check() ? auth()->user()->e_mail : ''}}"
								       placeholder="test@mail.ru"
								       id="email" name="email"
								       v-model="email" required/>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="tel">Телефон</label>
								<input class="form-control"
								       type="text" maxlength="15" placeholder="8(999)999-99-99"
								       id="tel" name="tel"
								       value="{{auth()->check() ? auth()->user()->telefone : ''}}"
								       pattern="^[\s0-9+()-]+$"
								       v-model="tel"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="type">Причина</label>
								<select class="form-control" id="type" name="type" v-model="type" required>
									<option value="" disabled selected>Выберите причину</option>
									@foreach($problem_types as $problem_type)
										<option value="{{ $problem_type->label }}">{{ $problem_type->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="theme">Тема</label>
								<input class="form-control" type="text" placeholder="Тема вашего сообщения" id="theme"
								       name="theme" v-model="theme" required/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="message">Сообщение</label>
								<textarea class="form-control" placeholder="Опишите полный текст проблемы" rows="10"
								          id="message" name="message" v-model="message"></textarea>
							</div>
						</div>

					</div>
					<div class="row" style="margin: 0 0 5px 1rem;">
						Обязательные поля помечены символом <span class="red-star">*</span>
					</div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					{{--<input type="hidden" name="user_id"  v-model="user_id" />--}}
				</div>
				<div id="ajax_support_update" class="col-sm-12">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
					<button type="submit" class="btn gradient-green width-50">Отправить</button>
				</div>
			</form>
		</div>
	</div>
</div>
