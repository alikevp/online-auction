<div class="row">
    <div class="large-12 medium-12 hide-for-small-only columns big-ad">
        <div class="medium-4 columns">
            <div class="banner">
                <img src="{{ asset('images/banner1.png') }}" class="article-image" alt="main article-image">
                <div class="step">шаг<br><span>1</span></div>
                <div class="step-info">
                    ВЫБЕРИ ТОВАР<br>ИЛИ ВВЕДИ ЕГО<br>НАЗВАНИЕ/АРТИКУЛ
                </div>
            </div>
        </div>
        <div class="medium-4 columns">
            <div class="banner">
                <img src="{{ asset('images/banner2.png') }}" class="article-image" alt="main article-image">
                <div class="step">шаг<br><span>2</span></div>
                <div class="step-info">
                    ПРОВЕДИ<br>СВОЙ ЛИЧНЫЙ<br>АУКЦИОН
                </div>
            </div>
        </div>
        <div class="medium-4 columns">
            <div class="banner">
                <img src="{{ asset('images/banner3.png') }}" class="article-image" alt="main article-image">
                <div class="step">шаг<br><span>3</span></div>
                <div class="step-info">
                    ЖДИ ПРЕДЛОЖЕНИЙ<br>И ВЫБЕРИ ЛУЧШЕГО<br>ПОСТАВЩИКА
                </div>
            </div>
        </div>
    </div>
</div>

<hr>