@include('partials.navMobileMenuForShops')
<div class="container d-none d-sm-block">
    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 upper-nav-tabs-wrapper">
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 text-right upper-nav-links-wrapper">
            <a href="/page/instruktsii" class="navigation-link decoration-green">
                <span class="regular-text">Помощь</span>
            </a>
            <div class="dropdown show">
                <a class="navigation-link decoration-green" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="regular-text"> <i class="fa header-menu-icon fa-user-o" aria-hidden="true"></i> {{Auth::check() ? Auth::user()->surname." ".Auth::user()->name." ".Auth::user()->patronymic  : 'Вход'}}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="/profile">Профиль</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/auction/orderlist">Доступные заказы</a>
                    <a class="dropdown-item" href="/my-auctions">Мои аукционы</a>
                    <a class="dropdown-item" href="/auction/accepted">Принятые аукционы</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/logout">Выход</a>
                </div>
            </div>
        </div>
    </div>
</div>