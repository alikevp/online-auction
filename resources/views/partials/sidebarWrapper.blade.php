<div id="sticky-sidebar">
    <div class="catalog-wrapper d-none d-sm-block">
        <div class="catalog-header gradient-green">
            <span>Каталог товаров</span>
            <span class="fa fa-bars" aria-hidden="true"></span>
        </div>
        <div class="catalog-list">
            @include('partials.nav')
        </div>
    </div>
    @yield('cmsSidebarMenu')
    <div class="banners-wrapper d-none d-sm-block">
        <div class="banner">
            <a href="#">
                <img class="banner-img" src="{{ asset('images/banners/баннер.jpg') }}">
            </a>
        </div>
        <div class="banner">
            <a href="#">
                <img class="banner-img" src="{{ asset('images/banners/баннер.jpg') }}">

            </a>
        </div>
    </div>
</div>