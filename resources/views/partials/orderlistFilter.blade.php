<form action="">
    <div class="col-xs-12">
        <legend>Статус: </legend>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" id="checkbox1" name="status[]" value="1" type="checkbox" @if(null == app('request')->input('status')) checked @endif @if(null !== app('request')->input('status'))@if(in_array(1,app('request')->input('status'))) checked @endif @endif>
                Активные
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" id="checkbox2" name="status[]" value="0" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(0,app('request')->input('status'))) checked @endif @endif>
                Отмененные
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" id="checkbox3" name="status[]" value="2" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(2,app('request')->input('status'))) checked @endif @endif>
                Просроченные
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" id="checkbox4" name="status[]" value="3" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(3,app('request')->input('status'))) checked @endif @endif>
                Принятые
            </label>
        </div>
        {{--<input id="checkbox1" name="status[]" value="1" type="checkbox" @if(null == app('request')->input('status')) checked @endif @if(null !== app('request')->input('status'))@if(in_array(1,app('request')->input('status'))) checked @endif @endif><label for="checkbox1">Активные</label><br>--}}
        {{--<input id="checkbox2" name="status[]" value="0" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(0,app('request')->input('status'))) checked @endif @endif><label for="checkbox2">Отмененные</label><br>--}}
        {{--<input id="checkbox3" name="status[]" value="2" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(2,app('request')->input('status'))) checked @endif @endif}}><label for="checkbox3">Просроченные</label><br>--}}
        {{--<input id="checkbox4" name="status[]" value="3" type="checkbox" @if(null !== app('request')->input('status'))@if(in_array(3,app('request')->input('status'))) checked @endif @endif}}><label for="checkbox4">Принятые</label><br>--}}
        <hr>
    </div>
    {{--<div class="medium-12">--}}
        {{--<legend>Показывать скрытые: </legend>--}}
        {{--<input id="checkbox5" name="show_hide" type="checkbox" {{ app('request')->input('show_hide') == "on" ? "checked" : ""}}><label for="checkbox5">Да</label><br>--}}
        {{--<hr>--}}
    {{--</div>--}}
    <div class="col-xs-12">
        <label>Город:
            <input type="text" name="city" placeholder="Город доставки" class="form-control" value="{{ app('request')->input('city') == "" ? "" : app('request')->input('city')}}">
        </label>
        <hr>
    </div>
    <div class="col-xs-12">
        <label>Id аукциона:
            <input type="text" name="id" placeholder="Id аукциона" class="form-control" value="{{ app('request')->input('id') == "" ? "" : app('request')->input('id')}}">
        </label>
        <hr>
    </div>
    <div class="col-xs-12">
        <button type="submit" class="btn gradient-green" style="width: 100%;">Фильтровать</button>
    </div>
</form>