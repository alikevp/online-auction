@foreach($categories_tree as $node)
    @if($node->isRoot())
        <optgroup label="+ {{ $node->name }}">
			@include('partials.nested_select', ['categories_tree' => $node->children])
		</optgroup>
    @elseif($node->isLeaf())
        <option value="{{ $node->id }}" @if(count($subscription)>0) @if(in_array($node->id, $subscription)) selected @endif @endif>{{ $node->name }}</option>
    @elseif ($node->isChild())
        <optgroup label="++ {{ $node->name }}">
			@include('partials.nested_select', ['categories_tree' => $node->children])
		</optgroup>
    @endif
@endforeach
