<div class="col-md-3 d-none d-md-block" id="left-block">
    <div id="sticky-sidebar">
        @yield('cmsSidebarMenu')
        <ul class="left-menu">
            @if(Auth::check())
                <?php $user = Auth::user(); ?>
                <li><a class="{{ Request::path() == 'profile' ? 'left-menu-active' : '' }}" href="/profile"><i class="fa fa-user" aria-hidden="true"></i>Профиль</a></li>
                <li><a class="{{ Request::path() == 'mail_subscription' ? 'left-menu-active' : '' }}" href="/mail_subscription"><i class="fa fa-envelope" aria-hidden="true"></i>Оповещения</a></li>
                @if($user->type != "shop_admin")
                    <li><a class="{{ Request::path() == 'my-orders' ? 'left-menu-active' : '' }}" href="/my-orders"><i class="fa fa-bars" aria-hidden="true"></i>Мои аукционы</a></li>
                    <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Моя корзина</a></li>
                    <li><a href="{{ url('referrals') }}" class="{{ Request::path() == 'referrals' ? 'left-menu-active' : '' }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Мои приглашения</a></li>
                    <li><a href="{{ url('profile/my_bonus') }}" class="{{ Request::path() == 'my_bonus' ? 'left-menu-active' : '' }}"><i class="fa fa-money" aria-hidden="true"> </i> Бонусы</a></li>
                @endif
                <hr>
                <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Выход</a></li>

                @if(Request::path() == 'my-orders')
                    <br>
                    <span><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</span>
                    @if(app('request')->input())
                        @if(!(count(app('request')->input()) == 1 && app('request')->input('page')))
                            <a class="flash-link" href="my-orders">(сбросить)</a>
                        @endif
                    @endif
                    <hr>
                    @include('partials.myOrdersFilter')
                    <br>
                @endif
            @else
                <li><a class="{{ Request::path() == 'login' ? 'left-menu-active' : '' }}" href="/login"><i class="fa fa-user" aria-hidden="true"></i> Вход кабинет</a></li>
                <li><a class="{{ Request::path() == 'register' ? 'left-menu-active' : '' }}" href="/register"><i class="fa fa-user-plus" aria-hidden="true"></i> Регистрация</a></li>
                <li><a class="{{ Request::path() == 'password/reset' ? 'left-menu-active' : '' }}" href="/password/reset"><i class="fa fa-refresh" aria-hidden="true"></i> Восстановить пароль</a></li>
                <li><a href="/cart" class="{{ Request::path() == 'cart' ? 'left-menu-active' : '' }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Моя корзина</a></li>
            @endif
        </ul>
    </div>
</div>