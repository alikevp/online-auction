<div class="medium-12 medium-centered columns category-info">
    <span class="category-name">{{ $category->name }}</span>
    <hr class="category-hr">
    <span class="category-tag">НОВИНКИ</span>
</div>
<div class="orbit" role="region" aria-label="Pictures" data-orbit>
    <ul class="orbit-container">
        <button class="orbit-previous">
            <span class="show-for-sr">Previous Slide</span>
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
        <button class="orbit-next">
            <span class="show-for-sr">Next Slide</span>
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </button>
        @foreach($category->products->chunk(4) as $chunk)
            <li class="is-active orbit-slide">
                <div class="medium-12 columns">
                    @foreach($chunk as $product)
                        <div class="medium-3 columns item-column">
                            <a href="{{ url('show', ['id' => $product->id]) }}">
                                <img src="{{ URL::to(array_first($product->files)) }}" class="item-img" alt=""><br>
                            </a>
                            @if($product->producer == "" || $product->producer == "0")
                                <span class="item-brand">Производитель не указан</span><br>
                            @else
                                <span class="item-brand">{{ strtoupper($product->producer) }}</span><br>
                            @endif
                            <a href="{{ url('show', ['id' => $product->id]) }}">
                                @if($product->name == "" || $product->name == "0")
                                    <span class="item-name">Название не указано</span><br>
                                @else
                                    <span class="item-name">{{ $product->name }}</span><br>
                                @endif
                            </a>
                            <a href="{{ url('show', ['id' => $product->id]) }}">
                                @if($product->price == "" || $product->price == 0)
                                    <span class="item-cost">Цена не указана</span>
                                @else
                                    <span class="item-cost">~{{ $product->price }} руб.</span>
                                @endif
                            </a>
                            <br>
                            <a href="#" class="button add-item-to-basket-button in_cart_add" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_add">В КОРЗИНУ</a>
                            <br>
                        </div>
                    @endforeach
                </div>
            </li>
        @endforeach
    </ul>
</div>
<hr class="items-hr">