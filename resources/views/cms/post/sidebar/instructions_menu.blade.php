<div class="cms-sidebar-menu-wrapper">
    <h5>Навигация</h5>
    <hr>
    <ul class="left-menu">
        @php
            $args = array(
                'slug'=>array('instruktsii')
            );
            $posts = PostHelper::get_post_children($args);
        @endphp
        @if(count($posts)>0)
            @php
                function findKey($array, $keySearch)
                {
                    foreach ($array as $item) {
                        if (isset($item->id)) {
                            if ($item->id == $keySearch) {
                                return true;
                            }
                            else {
                                if (is_object($item->children)) {
                                    if (findKey($item->children, $keySearch)) {
                                        return true;
                                    }
                                }
                            }
                        }
                        else {
                            if (is_object($item->children)) {
                                if (findKey($item->children, $keySearch)) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                }
            @endphp
            @include('cms.post.menu.tree_menu', ['posts' => $posts,'post' => $post])
        @endif
    </ul>
</div>