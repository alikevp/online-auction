@extends('layouts.master')

@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    {{$post->title}}
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.sidebarWrapper')

            <div class="medium-10 columns form-column">

                <h1>{{$post->title}}</h1>
                <div class="post-content">{!! $post->content !!}</div>


            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
