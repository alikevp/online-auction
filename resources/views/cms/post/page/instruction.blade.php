@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    {{$post->title}}
@endsection
@section('cmsSidebarMenu')
    @include('cms.post.sidebar.instructions_menu')
@endsection
@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>{{$post->title}}</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')

    <div class="col-md-9 col-sm-12 col-xs-12 content-wrapper" id="right-block">
        <h1>{{$post->title}}</h1>
        <div class="post-content">{!! $post->content !!}</div>
    </div>
@endsection
