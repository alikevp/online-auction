@foreach($posts as $node)
    @if($node->isRoot())
        <li id="post_menu_{{ $node->id }}">
            <a role="button" data-toggle="collapse" href="#collapse_{{$node->id}}" aria-expanded="true" aria-controls="collapse_{{$node->id}}">{{ $node->title }}</a>
            <ul id="collapse_{{ $node->id }}" class="panel-collapse collapse @if(findKey($node->children,$post->id)) show @endif" role="tabpanel" aria-labelledby="heading_{{ $node->id }}">
                @include('cms.post.menu.tree_menu', ['posts' => $node->children,'post' => $post])
            </ul>
        </li>
    @elseif($node->isLeaf())
        <li id="post_menu_{{ $node->id }}"><a href="{{$node->url}}" class="@if($node->id == $post->id) left-menu-active @endif">{{ $node->title }}</a></li>
    @elseif ($node->isChild())
        <li id="post_menu_{{ $node->id }}">
            <a role="button" data-toggle="collapse" href="#collapse_{{$node->id}}" aria-expanded="true" aria-controls="collapse_{{$node->id}}">{{ $node->title }}</a>
            <ul id="collapse_{{ $node->id }}" class="panel-collapse collapse @if(findKey($node->children,$post->id)) show @endif" role="tabpanel" aria-labelledby="heading_{{ $node->id }}">
                @include('cms.post.menu.tree_menu', ['posts' => $node->children,'post' => $post])
            </ul>
        </li>
    @endif
@endforeach