@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/profile.css') }}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet">
@endsection

@section('title')
    Мои бонусы
@endsection

@section('scripts')
    <script src="{{ asset('js/withdrawBonusesConfirm.js') }}"></script>
@endsection

@section('content')
    @include('partials.sidebarBuyerWrapper')

    <div class="col-sm-9 right-block">
        @if(session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <h1>Мои бонусы</h1>

        <div class="col-sm-12 orders-wraper" id="items-table-block">

            <div class="row cur_balance">Текущий баланс:
                <span>
                    @if(count($bonuses)>0)
                        {{ $bonuses->first()->total }}
                        @if( $bonuses->first()->total > 0)
                            <!-- Button trigger modal -->
                            <button type="button" class="flash-link" data-toggle="modal" data-target="#withdraw_bonuses">
                              Вывести средства
                            </button>
                            <br>

                            <!-- Modal -->
                            <div class="modal fade" id="withdraw_bonuses" tabindex="-1" role="dialog" aria-labelledby="withdraw_bonusesLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="withdraw_bonusesLabel">Вывод средств</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    @if( empty(Auth::user()->inn) )
                                          <span>
                                                Для вывода средств вы должны указать свой ИНН.
                                                <br>
                                                Пожалуйста, укажите свой ИНН в <a href="/profile">профиле</a>.
                                            </span>
                                      @else
                                          <form action="withdraw_bonuses" method="post" id="newWithdrawBonusesForm">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <button type="submit" id="withdraw_bonusesSubmit"  style="display: none;"></button>
                                              <div class="row">
                                                <div class="col-sm-12">
                                                  <label>Получатель
                                                    <input class="form-control" type="text" name="person" placeholder="Получатель" required/>
                                                  </label>
                                                    <label>Банк
                                                    <input class="form-control" type="text" name="bank" placeholder="Банк" required/>
                                                  </label>
                                                  <label>№ карты
                                                    <input class="form-control" type="number" name="card_number" placeholder="Номер карты" required/>
                                                  </label>
                                                  <label>№ счёта
                                                    <input class="form-control" type="text" name="account_number" placeholder="Номер счёта" required/>
                                                  </label>
                                                  <label>Сумма
                                                    <input class="form-control" type="number" name="amount" min="1" max="{{ $bonuses->first()->total }}" placeholder="Сумма" required/>
                                                  </label>
                                                </div>
                                              </div>
                                            </form>
                                      @endif
                                  </div>
                                  <div class="modal-footer">
                                      <a type="button" id="openConfirmButton" class="btn gradient-green width-50" data-toggle="modal" data-target="#confirm-withdraw-bonuses">
                                          ПОДТВЕРДИТЬ
                                      </a>
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">ОТМЕНИТЬ</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="confirm-withdraw-bonuses" tabindex="-1" role="dialog" aria-labelledby="confirm-withdraw-bonusesLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="confirm-withdraw-bonusesLabel">Вы уверены что хотите отправить заявку на вывод бонусов?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn gradient-green width-50" id="WithdrawBonusesConfirmButton">Отправить</button>
                                    <button type="button" class="btn border-red-btn width-50" data-dismiss="modal">Отменить</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endif
                    @else
                        0
                    @endif
                </span>
            </div>
        </div>

        <h3 class="order-title" style="padding-top:20px;">История операций:</h3>
        @if(count($bonuses)>0)
            <div class="col-sm-12 orders-wraper" id="items-table-block">
                <div class="row">
                    <table class="table unstriped personal-data-table" id="user-info">
                        <thead>
                        <td>Дата</td>
                        <td>Входящий баланс</td>
                        <td>Начисления</td>
                        <td>Списания</td>
                        <td>ИТОГ</td>
                        </thead>
                        @foreach($bonuses as $item)
                            <tr>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->balance }}</td>
                                <td>{{ $item->debit }}</td>
                                <td>{{ $item->credit }}</td>
                                <td>{{ $item->total }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @else
            <h3>Операций нет.</h3>
        @endif

    </div>
@endsection

@section('scripts')

@endsection
