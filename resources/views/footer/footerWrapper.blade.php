<div class="container">
    <div class="row footer-upper-line">
        <div class="col-md-7 col-sm-12 col-xs-12 footer-menu-wrapper">
            <a href="/page/o-nas" class="navigation-link decoration-green">
                <span class="bold-text">О Джинмарт</span>
            </a>
            <a href="/page/contacty" class="navigation-link decoration-green">
                <span class="bold-text">Контакты</span>
            </a>
            <a href="/page/page/voprosy-i-otvety" class="navigation-link decoration-green">
                <span class="bold-text">Вопрос/Ответ</span>
            </a>
            <a href="/page/pravila-prodaji" class="navigation-link decoration-green">
                <span class="bold-text">Магазинам</span>
            </a>
            <a href="/page/instruktsii/kak-sdelat-zakaz" class="navigation-link decoration-green">
                <span class="bold-text">Покупателям</span>
            </a>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12 footer-socials-wrapper">
            <a href="https://vk.com/club151902522" class="social-icon decoration-green">
                <span class="fa fa-vk" aria-hidden="true"></span>
            </a>
            <a href="https://www.instagram.com/jinnmart/" class="social-icon decoration-green">
                <span class="fa fa-instagram" aria-hidden="true"></span>
            </a>
            <a href="https://ok.ru/group/55441853644830" class="social-icon decoration-green">
                <span class="fa fa-odnoklassniki" aria-hidden="true"></span>
            </a>
            <a href="" class="social-icon decoration-green">
                <span class="fa fa-youtube" aria-hidden="true"></span>
            </a>
        </div>
    </div>
    <div class="row footer-lower-line">
        <div class="col-sm-7 footer-copyright-wrapper">
            <span class="grey-text">2016-2017 © jinnmart.ru - Электронная торговая площадка потребителей.</span><br>
            <span class="grey-text">Все права защищены. Доставка по всей России.</span>
        </div>
        <div class="col-sm-5 footer-notifications-wrapper">
            <span class="icon18">18+</span>
        </div>
    </div>
</div>