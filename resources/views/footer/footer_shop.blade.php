<div class="row" id="footer">

    <hr class="main-hr">
    <div class="medium-12 footer columns">
        <div class="large-2 medium-6 small-6 columns">

        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>Магазин</p>
            <div class="medium-12 footer-column columns">
                <a href="/shop_profile" class="footer-link">Профиль магазина</a><br>
                <a href="/shop_subscriptions" class="footer-link">Подписки</a><br>
                <a href="/auction/orderlist" class="footer-link">Доступные аукционы</a><br>
                <a href="/my-auctions" class="footer-link">Мои аукционы</a><br>
                <a href="/auction/accepted" class="footer-link">Выигранные аукционы</a>
            </div>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>Поддержка</p>
            <div class="medium-12 footer-column columns">
                @php
                    $args = array(
                        'tag_in'=>array('podval-2-prodavets'),
                        'order_by'=>array('created_at','desc'),
                    );
                    $posts = PostHelper::get_posts($args);
                @endphp
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <a href="{{ $post->url }}" class="footer-link">{{ $post->title }}</a><br>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>О компании</p>
            <div class="medium-12 footer-column columns">
                @php
                    $args = array(
                        'tag_in'=>array('podval-3-prodavets'),
                        'order_by'=>array('created_at','desc'),
                    );
                    $posts = PostHelper::get_posts($args);
                @endphp
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <a href="{{ $post->url }}" class="footer-link">{{ $post->title }}</a><br>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="large-2 medium-6 small-6 columns">
            <p>Присоединяйтесь</p>
        </div>
        <div class="medium-12 copyright-div columns">
                    <span class="copyright">
                        2016-2017 © jinnmart.ru - Электронная торговая площадка потребителей. <br>
                        Все права защищены. Доставка по всей России.
                    </span>
        </div>
    </div>
</div>