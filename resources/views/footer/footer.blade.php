<div class="row" id="footer">

    <hr class="main-hr">
    <div class="medium-12 footer columns">
        <div class="large-2 medium-6 small-6 columns">
            <div class="row">
                <a href="{{action('CartController@index')}}">
                    <div class="large-5 medium-2 small-2 footer-icon-div columns hide-for-small-only">
                        <i class="fa fa-3x footer-icon fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                    <div class="large-7 medium-10 small-12 columns"><p id="footer-basket-link">Корзина</p></div>
                </a>

            </div>
            {{--<div class="row">--}}
                {{--<a href="#">--}}
                    {{--<div class="medium-5 footer-icon-div columns">--}}
                        {{--<i class="fa fa-3x footer-icon fa-map-marker" aria-hidden="true"></i>--}}
                    {{--</div>--}}
                    {{--<div class="medium-7 columns"><p id="footer-deliver-link">Пункты самовывоза</p></div>--}}
                {{--</a>--}}
            {{--</div>--}}
        </div>
        <div class="large-4 medium-6 small-6 columns">
            <p>Магазин</p>
            <div class="medium-6 footer-column columns">
                @php
                    $args = array(
                        'tag_in'=>array('podval-1-bez-avtorizatsii'),
                        'order_by'=>array('created_at','desc'),
                    );
                    $posts = PostHelper::get_posts($args);
                @endphp
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <a href="{{ $post->url }}" class="footer-link">{{ $post->title }}</a><br>
                    @endforeach
                @endif
            </div>
            <div class="medium-6 footer-column columns">
                @php
                    $args = array(
                        'tag_in'=>array('podval-2-bez-avtorizatsii'),
                        'order_by'=>array('created_at','desc'),
                    );
                    $posts = PostHelper::get_posts($args);
                @endphp
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <a href="{{ $post->url }}" class="footer-link">{{ $post->title }}</a><br>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>О компании</p>
            <div class="medium-12 footer-column columns">
                @php
                    $args = array(
                        'tag_in'=>array('podval-3-bez-avtorizatsii'),
                        'order_by'=>array('created_at','desc'),
                    );
                    $posts = PostHelper::get_posts($args);
                @endphp
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <a href="{{ $post->url }}" class="footer-link">{{ $post->title }}</a><br>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>Партнерам</p>
            <div class="medium-12 footer-column columns">
                <a href="/register/shop" class="footer-link">Регистрация магазина</a><br>
            </div>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <p>Присоединяйтесь</p>
        </div>
        <div class="medium-12 copyright-div columns">
                    <span class="copyright">
                        2016-2017 © jinnmart.ru - Электронная торговая площадка потребителей.<br>
                        Все права защищены. Доставка по всей России.
                    </span>
        </div>
    </div>

</div>
