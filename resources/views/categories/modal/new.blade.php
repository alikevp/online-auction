<div class="reveal" id="createNew" data-reveal>
    <h1 class="modal-h1">Добавить новую вложенную:</h1>
    <div class="medium-12 columns">
        <form action="{{ URL::to('api/category/create') }}" method="post">
            {{ csrf_field() }}
            <div class="medium-12 columns show">
                <input type="hidden"
                       name="parent_id"
                       v-bind:value="parent_id">
                <div class="medium-6 columns medium-centered">
                    <label for="name">Имя</label>
                    <input type="text" id="name" name="name">
                </div>
                <div class="medium-12 columns">
                    <br>
                    <button type="submit" class="button float-right">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>