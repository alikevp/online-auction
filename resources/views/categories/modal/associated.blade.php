<div class="reveal" id="associate" data-reveal>
    <h1 class="modal-h1">Ассоциация:</h1>
    <div class="medium-12 columns">
        <form action="{{ URL::to('api/category/associate') }}" method="post">
            {{ csrf_field() }}
            <div class="medium-12 columns show">
                <input type="hidden" id="category_id" name="category_id" :value="cat_id">
                <input type="hidden" id="category_slug" name="category_slug" :value="cat_slug">
                <div class="medium-6 columns medium-centered">
                    <label for="name">Имя</label>
                    <input type="text" id="name" name="name">
                    <label for="shop_id">ID магазина</label>
                    <input type="text" id="shop_id" name="shop_id">
                </div>
                <div class="medium-12 columns">
                    <br>
                    <button type="submit" class="button float-right">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>