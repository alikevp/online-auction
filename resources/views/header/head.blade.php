<?php
use Jenssegers\Agent\Agent as Agent;
$Agent = new Agent();
?>
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/bootstrap-reboot.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/bootstrap-grid.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/bootstrap.min.css') }}" rel="stylesheet">
<!-- jasny-bootstrap -->
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/jasny-bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom -->
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/font-awesome.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/general.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/mediaqueries/general-mediaqueries.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/nivo/nivo-slider.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/plugins/nivo/light.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/admin-panel-fixed.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/mobile/offcanvas-main-menu.css') }}" rel="stylesheet">

@yield('style')
@if(Auth::check())
    @if(count(auth()->user()->roles)>0)
        <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/whenAdmin.css') }}" rel="stylesheet">
    @endif
@endif

{{--<script src="{{ mix('js/vendor/vue.js') }}"></script>--}}
{{--<script src="{{ mix('js/menu.js') }}"></script>--}}

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/popper.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/page_scripts/cart.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jm_v_1.1/main.js') }}"></script>

@yield('scripts')

<link href="https://cdn.jsdelivr.net/jquery.suggestions/17.2/css/suggestions.css" type="text/css" rel="stylesheet" />
<!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/17.2/js/jquery.suggestions.min.js"></script>
@yield('headScripts')
<script>
    window.Laravel =<?php echo json_encode([
        'csrfToken' => csrf_token(),
        'base_url'  => url(),
    ]); ?>
</script>
@if(Auth::check())
    <script>
        var middle = '{{trim(Auth::user()->surname)}}';
        var name = '{{trim(Auth::user()->name)}}';
        var email = '{{trim(Auth::user()->email)}}';
        var tel = '{{trim(Auth::user()->telefone)}}';
        var type = '18';
    </script>
@else
    <script>
        var middle = '';
        var name = '';
        var email = '';
        var tel = '';
        var type = '';
    </script>
@endif
@if ($Agent->isMobile())
@else
    <script src="{{ asset('js/jquery.sticky-kit.min.js') }}"></script>
@endif
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
