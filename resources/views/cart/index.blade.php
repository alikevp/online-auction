@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/cart.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">

    <link type="text/css" href="{{asset('css/jm_v_1.1/pages/mobile/mediaqueries/cart-page-mediaqueries.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $(document).ready(function() {
            $('.minus').click(function () {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        function checkCart(removable){
            var prodid = document.getElementsByClassName('cartProd');
            var totalPr = document.getElementById('total-price-value').innerHTML;
            var removablePr = document.getElementById('product-amount_' + removable).innerHTML;
            if(prodid.length == 1){
                document.getElementById('order-block').style.display = 'none';
                document.getElementById('order-block-empty').style.display = 'block';
            }
            document.getElementById('total-price-value').innerHTML = totalPr-removablePr;
        }
    </script>
@endsection
@section('title')
    Корзина
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Корзина</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 content-wrapper"  id="right-block">
        @if (isset($products))
            <div class="col-sm-12 d-none d-lg-block">
                <div class="row text-center" id="buy-steps">
                    <div class="buy-step col-sm-2">
                        <a href="#">
                            <div class="buy-step-div buy-step-div-active">
                                <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span class="step-name step-name-active">Корзина</span>
                        </a>
                    </div>
                    <div class="col-sm-1">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step col-sm-2">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Доставка</span>
                        </a>
                    </div>
                    <div class="col-sm-1">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step col-sm-3">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Подтверждение</span>
                        </a>
                    </div>
                    <div class="col-sm-1">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step col-sm-2">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Завершение</span>
                        </a>
                    </div>
                </div>
            </div>
            <form method="post" action="/order/delivery">
                <div class="col-sm-12" id="items-table-block">
                    <table class="items-table">
                        <thead>
                        <tr>
                            <th>Товар</th>
                            <th>Цена@include('partials.priceTooltip')</th>
                            <th>Количество</th>
                            <th>Сумма@include('partials.priceTooltip')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_price = 0; ?>
                        @foreach ($products as $product)
                            <tr id="cart_prod_row_{{ $product->id }}" class="cartProd">
                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][id]" value="{{ $product->id }}">
                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][name]" value="{{ $product->name }}">
                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][foto]" value="{{ URL::to($product->files[0]) }}">
                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][price]" value="{{ $product->price }}">
                                <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product->id }}][category_id]" value="{{ $product->category_id }}">

                                <td class="item-info">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-2 col-xs-12">
                                            <a href="{{ url('show', $product->id) }}"><img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image"></a>
                                        </div>
                                        <div class="col-lg-9 col-sm-10 col-xs-12">
                                            <a href="{{ url('show', $product->id) }}"><span class="item-name">{{ $product->name }}</span><br></a>
                                            <a href="#" class="btn border-red-btn width-50" id="in_cart_del" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-url="/in_cart_del" onClick="javascript:checkCart('{{ $product->id }}');">Убрать</a>
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle;text-align: center;" id="product-price_{{ $product->id }}">{{ $product->price }}</td>
                                <td style="vertical-align: middle;text-align: center;" id="product-quantity_{{ $product->id }}">
                                    <div class="number">
                                        <span class="minus">-</span>
                                        <input type="text" class="item-amount-input product_count_{{ $product->id }}" id="product_count_value" name="products[{{ $product->id }}][count]" value="{{ $counts[$product->id] }}" data-prod-cur-count="{{ $counts[$product->id] }}" data-in-cart-prod-id="{{ $product->id }}" data-in-cart-prod-price="{{ $product->price }}" data-in-cart-url="/in_cart_recount">
                                        <span class="plus">+</span>

                                        <div style="clear:both"></div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle;text-align: center;" id="product-amount_{{ $product->id }}">{{ ($product->price) * ($counts[$product->id]) }}</td>
                            </tr>

                            <?php
                            $cur_price = ($product->price) * ($counts[$product->id]);
                            $total_price = $cur_price + $total_price;
                            ?>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="order-wraper col-sm-12">
                        <div id="order-block" class="row">
                            <div class="col-sm-12">
                                <div class="row"><div class="col-sm-6 order-title-big"><span>Оформление заказа</span></div></div>
                                <!--<div class="row"><div class="medium-6 columns order-title-medium"><span>Проверьте стоимость покупки</span></div></div>-->
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12 col-xs-12" id="price-block">
                                        <div class="row">
                                            <div class="col-sm-6"><span id="price-description">Ориентировачная стоимость товара:<br><span class="price_text" style="font-weight: 400;font-size: 14px;">Указана ориентировачная стоимость. Окончательная стоимость будет выбрана вами в результате торгов.</span></span></div>
                                            <div class="col-sm-6"><span id="price-value"><span id="total-price-value">{{ $total_price }}</span>@include('partials.priceTooltip')</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12 col-xs-12">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if(Auth::check())
                                            <input type="submit" name="order_submit" class="btn gradient-green" value="ПЕРЕЙТИ К ОФОРМЛЕНИЮ АУКЦИОНА">
                                        @else
                                            <input type="submit" name="order_submit" class="btn gradient-green" value="АВТОРИЗОВАТЬСЯ И ПРОДОЛЖИТЬ">
                                        @endif
                                    </div>
                                </div>
                                <div class="row"><div class="col-sm-6 order-agreement"><span>Оформляя заказ, вы соглашаетесь с условиями <a href="/uploads/files/documents/Оферта для покупателя.pdf" class="flash-link" target="_blank">Публичной офертой</a>.</span></div></div>
                            </div>
                        </div>
                        <div id="order-block-empty" style="display:none;" class="row">
                            <div class="col-sm-12">
                                <div class="row"><div class="col-sm-12 order-title-big" style="padding:25px;"><span>Корзина пуста</span></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @else
            <div class="col-sm-12" >
                <span>Корзина пуста</span>
            </div>
        @endif
    </div>
@endsection
