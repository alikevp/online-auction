<div class="">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>{{ Session::get('alert-' . $msg) }}</p>
            </div>
        @endif
    @endforeach
    <h4>Навигация / Выбор категории</h4>
    <a href="{{ url('/admin/categories?page=add_category') }}">
        <button type="button" class="btn btn-default">Добавить категорию</button>
    </a>

    <hr>
</div>


