
<ul>
<?php
$level = 1;
function tplMenu($category, $level){?>
        <li>
                <span class="cat-tree-li-name" title="{{ $category->name }}" data-toggle="collapse" data-target="#cat-ul-{{ $category->id }}">{{ $category->name }}</span>
            <?php if(count($category['children'])>0):
                $level++;

            ?>
            <span class="glyphicon glyphicon-chevron-down"></span>
                <ul class="collapse cat-tree cat-tree-level-{{ $level }}" id="cat-ul-{{ $category->id }}">
                    <?php showCat($category['children'], $level); ?>
                </ul>
             <?php else: ?>
                <span class="cat-tree-li-name" title="{{ $category->name }}" data-toggle="collapse" data-target="#cat-ul-{{ $category->id }}">{{ $category->name }}({{ $category->attributesCountRelation }})</span>
                <a href="?category={{ $category->id }}"><button type="button" class="btn btn-primary" data-dismiss="modal">Редактировать атрибуты</button></a>
             <?php endif; ?>
        </li>
<?php }
         /**
          * Рекурсивно считываем наш шаблон
          **/
         function showCat($data, $level){
             $string = '';
             foreach($data as $item){
                 $string .= tplMenu($item,$level);
             }
             return $string;
         }

         //Получаем HTML разметку
         $cat_menu = showCat($categories_tree,$level);
?>
    </ul>

