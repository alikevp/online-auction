<div class="">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>{{ Session::get('alert-' . $msg) }}</p>
            </div>
        @endif
    @endforeach
    <h4>Навигация / Выбор категории</h4>
    <a href="{{ url('/admin/offers_analises?page=uploads') }}">
        <button type="button" class="btn btn-default">Загрузки</button>
    </a>

    <hr>
</div>


<h4>Добавление новой категории</h4>

<form action="/api/category/add_category" method="post">

    <div class="form-group form-element-text ">
        <label for="name" class="control-label">Название</label>
        <input type="text" id="name" name="name" required>
    </div>

    <div class="form-group form-element-text ">
        <label for="parent_id" class="control-label">Родительская</label>
        <select id="parent_id" class="form-control" name="parent_id" size="25" required>
            <?php
            $level = 1;
            function tplMenu2($category, $level){?>

            <?php if(count($category['children']) > 0): ?>

            <option value="{{ $category->id }}" class="tree-level-{{$level}}"><span
                        class="tree-level-{{$level}}"> </span>{{ $category->name }}</option>
            <?php
            $level++;
            showCat2($category['children'], $level); ?>
            <?php else: ?>
            <option value="{{ $category->id }}"
                    class="tree-level-{{$level}} tree-level-last">{{ $category->name }}</option>
            <?php endif; ?>
            <?php }
            /**
             * Рекурсивно считываем наш шаблон
             **/
            function showCat2($data, $level)
            {
                $string = '';
                foreach ($data as $item) {
                    $string .= tplMenu2($item, $level);
                }
                return $string;
            }

            //Получаем HTML разметку
            $cat_menu = showCat2($categories_tree, $level);
            ?>
        </select>
    </div>
    {{ csrf_field() }}
    <input type="hidden" name="redirect_url" value="/admin/categories?page=add_category">
    <button type="submit" class="btn btn-primary">Выбрать</button>
</form>

