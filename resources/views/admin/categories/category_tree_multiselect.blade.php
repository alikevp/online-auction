<select id="parent_id" class="form-control" name="parent_id[]" size="25" multiple required>
    <?php
    $level = 1;
    function tplMenu2($category, $level){?>

    <?php if(count($category['children']) > 0): ?>

    <option value="{{ $category->id }}" class="tree-level-{{$level}}"><span
                class="tree-level-{{$level}}"> </span>{{ $category->name }}</option>
    <?php
    $level++;
    showCat2($category['children'], $level); ?>
    <?php else: ?>
    <option value="{{ $category->id }}"
            class="tree-level-{{$level}} tree-level-last">{{ $category->name }}</option>
    <?php endif; ?>
    <?php }
    /**
     * Рекурсивно считываем наш шаблон
     **/
    function showCat2($data, $level)
    {
        $string = '';
        foreach ($data as $item) {
            $string .= tplMenu2($item, $level);
        }
        return $string;
    }

    //Получаем HTML разметку
    $cat_menu = showCat2($categories_tree, $level);
    ?>
</select>

