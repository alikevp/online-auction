<div class="alert fade" id="ajax-result-wrapper" role="alert">
    <button id="validation-error-wrapper-close" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div id="ajax-result"></div>
</div>
<div class="col-xs-12 table-responsive">
    <form action="/category_move" id="catMoveForm" method="post">
        <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="cat_source" class="control-label">Категория-источник</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="cat_source" name="cat_source" placeholder="cat_xxxxxxxxxxxxx" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="like_name" class="control-label">Совпадения в названии</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="like_name" name="like_name" placeholder="%эл%питания%" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="cat_destination" class="control-label">Категория-назначение</label>
                                <div class="col-sm-12">
                                    <select id="cat_destination" class="form-control" name="cat_destination" size="25" required>
                                        <?php
                                        $level = 1;
                                        function tplMenu2($category, $level){?>

                                        <?php if(count($category['children']) > 0): ?>

                                        <option value="{{ $category->id }}" class="tree-level-{{$level}}" disabled><span class="tree-level-{{$level}}"> </span>{{ $category->name }}</option>
                                        <?php
                                        $level++;
                                        showCat2($category['children'], $level); ?>
                                        <?php else: ?>
                                        <option value="{{ $category->id }}" class="tree-level-{{$level}} tree-level-last">{{ $category->name }}</option>
                                        <?php endif; ?>
                                        <?php }
                                        /**
                                         * Рекурсивно считываем наш шаблон
                                         **/
                                        function showCat2($data, $level)
                                        {
                                            $string = '';
                                            foreach ($data as $item) {
                                                $string .= tplMenu2($item, $level);
                                            }
                                            return $string;
                                        }

                                        //Получаем HTML разметку
                                        $cat_menu = showCat2($categories_tree, $level);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ csrf_field() }}
            <button id="catMoveCheck" type="button" class="btn btn-default">Проверить</button>
            <button id="catMoveExecute" type="button" class="btn btn-primary">Загрузить</button>
    </form>
</div>