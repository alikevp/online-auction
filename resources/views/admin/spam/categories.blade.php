<div class="form-group form-element-text ">
    <label for="category_id" class="control-label">Категории</label>
    <select id="category_id" class="form-control" name="category_id"  size="25">
<?php
$level = 1;
function tplMenu($category, $level, $cur_spam){?>

        <?php if(count($category['children'])>0): ?>

            <option value="{{ $category['id'] }}" class="tree-level-{{$level}}"><span  class="tree-level-{{$level}}" @if(isset($cur_spam)) @if($category['id'] == $cur_spam->category_id) selected @endif @endif> </span>{{ $category['name'] }}</option>
            <?php
                $level++;
                showCat($category['children'], $level, $cur_spam); ?>
         <?php else: ?>
            <option value="{{ $category['id'] }}" class="tree-level-last" @if(isset($cur_spam)) @if($category['id'] == $cur_spam->category_id) selected @endif @endif>{{ $category['name'] }}</option>
         <?php endif; ?>
<?php }
         /**
          * Рекурсивно считываем наш шаблон
          **/
         function showCat($data, $level, $cur_spam){
             $string = '';
             foreach($data as $item){
                 $string .= tplMenu($item,$level, $cur_spam);
             }
             return $string;
         }

         //Получаем HTML разметку
         $cat_menu = showCat($categories_tree,$level, $cur_spam);
?>
    </select>
</div>

