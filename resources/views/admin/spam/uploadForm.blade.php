<div class="row">
    <div class="col-xs-12">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }} fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p>{{ Session::get('alert-' . $msg) }}</p>
                </div>
            @endif
        @endforeach

        <div class="col-xs-12 table-responsive">
            <form action="/spamBaseCsvUpload" method="post">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="form-group">
                            <label for="file" class="col-sm-2 control-label">Файл</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="file" id="file" required>
                                    @foreach($upload_files as $file)
                                        @php
                                            $name = explode('/', $file);
                                            $name = array_pop($name);
                                        @endphp
                                        <option value="{{$file}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="folder" class="col-sm-2 control-label">Категория</label>
                            <div class="col-sm-10">
                                @include('admin.categories.category_tree_multiselect')
                            </div>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Загрузить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

