
<div id="ajax-loading-gif"
     style="display: none;position: fixed;left: 0;right: 0;top: 0;bottom: 0;background: rgba(194, 202, 206, 0.48);z-index: 99999;">
    <img src="{{ asset('images/loading.gif') }}" style="position: absolute;top: 40%;left: 47%">
</div>
<table class="table table-striped table-hover">
    <thead>
        <td>Название</td>
        <td>Категория</td>
        <td>Фильтруемость</td>
    </thead>
    <tbody>
        @foreach($attributes as $slug=>$attribute_data)
            <tr>
                <td>{{ $attribute_data->first()->name }}</td>
                <td>{{ $attribute_data->first()->category->name }}</td>
                <td>
                    <input class="filter_atr_checkbox" id="filter_atr_checkbox_{{$slug}}" type="checkbox" data-filter_atr_checkbox-slug="{{$slug}}" data-filter_atr_checkbox-category="{{$attribute_data->first()->category->id}}" name="filter_atr_checkbox_{{$slug}}" @if($attribute_data->first()->filter == 1) checked  value="0" @else value="1" @endif>
                </td>
            </tr>
        @endforeach
    </tbody>

</table>
