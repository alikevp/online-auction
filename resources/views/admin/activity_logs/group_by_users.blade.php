@php
    use Carbon\Carbon;
@endphp
<div class="btn-group">
    <a href="?group_by=tables"><button type="button" class="btn btn-default">По таблицам</button></a>
    <a href="?group_by=users"><button type="button" class="btn btn-default">По пользователям</button></a>
</div>

<table class="table table-striped table-hover">
    <thead>
    <td>Пользователь</td>
    <td>Изменений всего</td>
    <td>За год</td>
    <td>За месяц</td>
    <td>За неделю</td>
    <td>За день</td>
    <td></td>
    </thead>
    <tbody>
    @foreach($activities as $activity)
        <tr>
            <td>{{ $activity->first()->causer()->first()->getFullNameAttribute() }}</td>
            <td>{{ count($activity) }}</td>
            <td>
                {{ count($activity->where('created_at','>=', Carbon::now()->startOfYear())
                    ->where('created_at','<=', Carbon::now()->endOfYear())) }}
            </td>
            <td>
                {{ count($activity->where('created_at','>=', Carbon::now()->startOfMonth())
                    ->where('created_at','<=', Carbon::now()->endOfMonth())) }}
            </td>
            <td>
                {{ count($activity->where('created_at','>=', Carbon::now()->startOfWeek())
                    ->where('created_at','<=', Carbon::now()->endOfWeek())) }}
            </td>
            <td>
                {{ count($activity->where('created_at','>=', Carbon::now()->startOfDay())
                    ->where('created_at','<=', Carbon::now()->endOfDay())) }}
            </td>
            <td>
                <a href="?user={{ $activity->first()->causer_id }}"><button type="button" class="btn btn-default">Посмотреть</button></a>
            </td>
        </tr>
    @endforeach
    </tbody>

</table>
