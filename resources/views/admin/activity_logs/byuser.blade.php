<div class="panel panel-default">
	<div class="panel-heading">
		<table  class="table table-striped">
			<thead>
				<tr>
					<th class="row-header">Действие</th>
					<th class="row-header">Субъект</th>
					<th class="row-header">Контекст</th>
					<th class="row-header">Изменения</th>
					<th class="row-header">Дата и время</th>
				</tr>
			</thead>
			<tbody>
			@foreach($activities as $activity)
				<tr>
					<td class="row-text">
					@if($activity['description'] == "created")
						Создание
					@elseif($activity['description'] == "updated")
						Правка
					@endif
					</td>
					<td class="row-text">
					@if($activity['subject_type'] == "App\Page")
						{{ $pageName[$activity['subject_id']] }}
					@elseif($activity['subject_type'] == "App\Category")
						{{ $catName[$activity['subject_id']] }}
					@endif
					</td>
					<td class="row-text">
					@if($activity['subject_type'] == "App\Page")
						Страница
					@elseif($activity['subject_type'] == "App\Category")
						Категория
					@endif
					</td>
					<td class="row-text">
					@if($activity['description'] == "created")
						<strong>Данные при создании:</strong><br>
					@elseif($activity['description'] == "updated")
						<strong>Данные после правки:</strong><br>
					@endif
					@foreach($activity['properties']['attributes'] as $key => $value)
						{{ $key }} -> {{ $value }}<br>
					@endforeach
					@if(isset($activity['properties']['old']))
						<strong>Прежние данные:</strong><br>
						@foreach($activity['properties']['old'] as $key => $value)
							{{ $key }} -> {{ $value }}<br>
						@endforeach
					@endif
					</td>
					<td class="row-text">{{ $activity['created_at'] }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>