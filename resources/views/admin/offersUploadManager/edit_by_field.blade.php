<div class="">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>{{ Session::get('alert-' . $msg) }}</p>
            </div>
        @endif
    @endforeach
    <h4>Навигация / Массовая ассоциация</h4>
    <a href="{{ url('/admin/offers_analises?page=uploads') }}"><button type="button" class="btn btn-default">Загрузки</button></a>
    <hr>
</div>


<h4>Выберите параметры</h4>

<hr>


<form action="/set_category_by_field/{{ $offerAnaliseUploadId }}" method="post">
    <div class="form-group form-element-text ">
        <label for="field" class="control-label">Поле</label>
        <select id="field" class="form-control" name="field" required>
            <option disabled selected value="">Выберите поле для выборки</option>
            <option value="cat0">cat0</option>
            <option value="cat1">cat1</option>
            <option value="cat2">cat2</option>
            <option value="cat3">cat3</option>
            <option value="cat_tree">cat_tree</option>
        </select>
    </div>
    <div class="form-group form-element-text ">
        <label for="algiritm" class="control-label">Алгоритм</label>
        <select id="algiritm" class="form-control" name="algiritm" required>
            <option disabled selected value="">Выберите алгоритм выборки</option>
            <option value="=">=</option>
            <option value="like">LIKE</option>
        </select>
    </div>
    <div class="form-group form-element-text ">
        <label for="value" class="control-label">Значение выборки</label>
        <input class="form-control" id="value" type="text" name="value" required placeholder="Текст для поиска при выборке">
    </div>
    {{--2--}}
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#btnForField2" aria-expanded="false" aria-controls="btnForField2">
        Нужно поле 2
    </button>
    <div class="collapse" id="btnForField2">
        <div class="form-group form-element-text ">
            <label for="field2" class="control-label">Поле2</label>
            <select id="field2" class="form-control" name="field2">
                <option disabled selected value="">Выберите поле для выборки</option>
                <option value="cat0">cat0</option>
                <option value="cat1">cat1</option>
                <option value="cat2">cat2</option>
                <option value="cat3">cat3</option>
                <option value="cat_tree">cat_tree</option>
            </select>
        </div>
        <div class="form-group form-element-text ">
            <label for="algiritm2" class="control-label">Алгоритм2</label>
            <select id="algiritm2" class="form-control" name="algiritm2">
                <option disabled selected value="">Выберите алгоритм выборки</option>
                <option value="=">=</option>
                <option value="like">LIKE</option>
            </select>
        </div>
        <div class="form-group form-element-text ">
            <label for="value2" class="control-label">Значение выборки2</label>
            <input class="form-control" id="value2" type="text" name="value2" placeholder="Текст для поиска при выборке">
        </div>
    </div>
    <div class="form-group form-element-text ">
        <label for="category_id" class="control-label">Категории для ассоциации</label>
        <select id="category_id" class="form-control" name="category_id"  size="25">
            <?php
            $level = 1;
            function tplMenu($category, $level){?>

            <?php if(count($category['children'])>0): ?>

            <option value="{{ $category->id }}" class="tree-level-{{$level}}"><span  class="tree-level-{{$level}}"> </span>{{ $category->name }}</option>
            <?php
            $level++;
            showCat($category['children'], $level); ?>
            <?php else: ?>
            <option value="{{ $category->id }}"
                    class="tree-level-{{$level}} tree-level-last">{{ $category->name }}</option>
            <?php endif; ?>
            <?php }
            /**
             * Рекурсивно считываем наш шаблон
             **/
            function showCat($data, $level){
                $string = '';
                foreach($data as $item){
                    $string .= tplMenu($item,$level);
                }
                return $string;
            }

            //Получаем HTML разметку
            $cat_menu = showCat($categories_tree,$level);
            ?>
        </select>
    </div>
    {{ csrf_field() }}
    <button type="submit" class="btn btn-primary">Ассоциировать</button>
</form>
