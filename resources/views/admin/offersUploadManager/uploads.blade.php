<div class="">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>{{ Session::get('alert-' . $msg) }}</p>
            </div>
        @endif
    @endforeach
    <h4>Навигация / Загрузки</h4>
    <a href="{{ url('/admin/offers_analises?page=uploads') }}"><button type="button" class="btn btn-default">Загрузки</button></a>
        
    <hr>

</div>

<div class="col-xs-12 table-responsive">
    <table class="table table-striped table-hover">
        <tr>
            <td>Файл</td>
            <td>Папка с фото</td>
            <td>Всего товаров</td>
            <td>Всего категорий</td>
            <td>Категорий найдено</td>
            <td>Категорий не найдено</td>
            <td>Моделей найдено</td>
            <td>Моделей не найдено</td>
            <td>Дата создания</td>
            <td>Действие</td>
        </tr>
        @foreach($uploads as $upload)
            <tr>
                <td>{{ $upload->file }}</td>
                <td>{{ $upload->foto_folder }}</td>
                <td>{{ $upload->total_offers }}</td>
                <td>{{ $upload->total_cats }}</td>
                <td>{{ $upload->cats_found }}</td>
                <td>{{ $upload->cats_unfound }}</td>
                <td>{{ $upload->models_found }}</td>
                <td>{{ $upload->models_unfound }}</td>
                <td>{{ $upload->created_at }}</td>
                <td>
                        <button class="btn btn-default" type="submit" data-toggle="modal" data-target="#uploadAnalisis">Анализ</button>


                    <a href="{{ url('admin/offers_analises?page=nfcat&upload_id='.$upload->id) }}" class="btn btn-default">Категории</a>

                    @if($upload->cats_unfound == 0)
                        <form action="/offer_upload/{{ $upload->id }}" method="post">
                            {{ csrf_field() }}
                            <button class="btn btn-default" type="submit">Загрузить все товары</button>
                        </form>
                    @else
                        <form action="/offer_upload/{{ $upload->id }}" method="post">
                            {{ csrf_field() }}
                            <button class="btn btn-default" type="submit">Загрузить ассоциированные товары</button>
                        </form>
                    @endif
                    <a href="/offer-analise-export?upload_id={{$upload->id}}" target="_blank"><button class="btn btn-default" type="submit">Экспорт</button></a>
                </td>
            </tr>
        @endforeach
    </table>
    <div class="modal fade" id="uploadAnalisis" tabindex="-1" role="dialog" aria-labelledby="uploadAnalisis">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="uploadAnalisis">Добавление выгрузки CSV</h4>
                </div>
                <form action="/offer_temp_analisis/{{ $upload->id }}" method="post">
                    <div class="modal-body">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="source_cat" class="col-sm-2 control-label">Исходная категория</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="source_cat" id="source_cat" required>
                                                    <option value="0">cat0</option>
                                                    <option value="1">cat1</option>
                                                    <option value="2">cat2</option>
                                                    <option value="3">cat3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Загрузить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>