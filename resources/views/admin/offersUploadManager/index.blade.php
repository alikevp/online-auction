<div class="">
    <h4>Навигация</h4>
    <a href="{{ url('/admin/offers_analises?page=uploads') }}"><button type="button" class="btn btn-default">Загрузки</button></a>

    <hr>

    <h4>Управление</h4>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#uploadModalCSV">
        Добавить выгрузку CSV
    </button>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#uploadModalXML">
        Добавить выгрузку XML
    </button>
</div>
{{--Модаль для CSV--}}
<div class="modal fade" id="uploadModalCSV" tabindex="-1" role="dialog" aria-labelledby="uploadModalCSV">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Добавление выгрузки CSV</h4>
            </div>
            <form action="/offer_temp_upload" method="post">
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="folder" class="col-sm-2 control-label">Папка загрузки</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="folder" id="folder" required>
                                                @foreach($upload_folders as $folder)
                                                    @php
                                                        $name = explode('/', $folder);
                                                        $name = array_pop($name);
                                                    @endphp
                                                    <option value="{{$folder}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="foto_folder" class="col-sm-2 control-label">Фото-папка</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="foto_folder" name="foto_folder" value="files" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Загрузить</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{--Модаль для XML--}}
<div class="modal fade" id="uploadModalXML" tabindex="-1" role="dialog" aria-labelledby="uploadModalXML">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Добавление выгрузки XML</h4>
            </div>
            <form action="/offer_temp_xml_upload" method="post">
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="folder" class="col-sm-2 control-label">Папка загрузки</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="folder" id="folder" required>
                                                @foreach($upload_folders as $folder)
                                                    @php
                                                        $name = explode('/', $folder);
                                                        $name = array_pop($name);
                                                    @endphp
                                                    <option value="{{$folder}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="foto_folder" class="col-sm-2 control-label">Фото-папка</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="foto_folder" name="foto_folder" value="files" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Загрузить</button>
                </div>
            </form>
        </div>
    </div>
</div>


