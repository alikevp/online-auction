<div class="">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>{{ Session::get('alert-' . $msg) }}</p>
            </div>
        @endif
    @endforeach
    <h4>Навигация / Не найденные категории</h4>
    <a href="{{ url('/admin/offers_analises?page=uploads') }}"><button type="button" class="btn btn-default">Загрузки</button></a>
        @if(count($offerAnalises) > 0)
        <a href="{{ url('/admin/offers_analises?page=edit_by_field&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">Массовая ассоциация</button></a>
        <hr>
        <h5>Сортировка</h5>
            <a href="{{ url('/admin/offers_analises?page=nfcat&order=name&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">Имя</button></a>
            <a href="{{ url('/admin/offers_analises?page=nfcat&order=cat0&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">cat0</button></a>
            <a href="{{ url('/admin/offers_analises?page=nfcat&order=cat1&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">cat1</button></a>
            <a href="{{ url('/admin/offers_analises?page=nfcat&order=cat2&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">cat2</button></a>
            <a href="{{ url('/admin/offers_analises?page=nfcat&order=cat3&upload_id='.$offerAnalises->first()->upload_id) }}"><button type="button" class="btn btn-default">cat3</button></a>
        @endif
</div>

<div class="col-xs-12 table-responsive">
    @if(count($offerAnalises) > 0)
        <table class="table table-striped table-hover">
            <tr>
                <td>№</td>
                <td>Название</td>
                <td>cat0</td>
                <td>cat1</td>
                <td>cat2</td>
                <td>cat3</td>
                <td>Дерево</td>
                <td>Действие</td>
            </tr>
            @php $n=1; @endphp
            @foreach($offerAnalises as $key=>$offerAnalise)
                <tr>
                    <td>{{ $n }}</td>
                    <td>
                        <a href="{{ url('/admin/offers_analises?page=edit&offer_analise='.$offerAnalise->id) }}">{{ $offerAnalise->name }}</a>
                        <a href="{{ $offerAnalise->url }}" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>
                    </td>
                    <td>{{ $offerAnalise->cat0 }}</td>
                    <td>{{ $offerAnalise->cat1 }}</td>
                    <td>{{ $offerAnalise->cat2 }}</td>
                    <td>{{ $offerAnalise->cat3 }}</td>
                    <td>{{ $offerAnalise->cat_tree }}</td>
                    <td>
                        <a href="{{ url('/admin/offers_analises?page=edit&offer_analise='.$offerAnalise->id) }}">
                            Указать категорию
                        </a>
                    </td>
                </tr>
                @php $n++; @endphp
            @endforeach
        </table>
    @else
        <span>
            Не найденных категорий нет. Можете загружать товары из этой выгрузки.
        </span>
    @endif
</div>