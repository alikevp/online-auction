@extends('layouts.master')
@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @include('partials.cabinet.sidebar')
            <div class="medium-10 columns">
                <router-link to="/">
                    <a class="button primary">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                        Список
                    </a>
                </router-link>
                <router-link to="/create">
                    <a class="button primary">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Добавить
                    </a>
                </router-link>
                <router-view></router-view>
            </div>
        </div>
    </div>

    {{--<div class="reveal custom-medium-reveal" id="removeMenuModal" data-reveal>--}}
        {{--<div class="medium-12 columns">--}}
            {{--<h1 class="modal-h-1">Удалить пункт?</h1>--}}
            {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
            {{--</button>--}}
            {{--<div class="medium-12 columns modal-left">--}}
                {{--<p>Вы хотите удалить пункт меню</p>--}}
                {{--<p>Вы уверены?</p>--}}
            {{--</div>--}}
            {{--<br>--}}
        {{--</div>--}}
        {{--<div class="medium-12 columns modal-footer">--}}
            {{--<div class="medium-12 columns">--}}
                {{--<button class="save-button button primary" @click="removeMenu">Да</button>--}}
                {{--<button class="button default" data-close aria-label="Close modal" type="button">Нет--}}
                {{--</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="reveal custom-medium-reveal" id="make-root" data-reveal>--}}
        {{--<div class="medium-12 columns">--}}
            {{--<h1 class="modal-h-1">Поднять меню выше?</h1>--}}
            {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
            {{--</button>--}}
            {{--<div class="medium-12 columns modal-left">--}}
                {{--<p>Вы хотите поднять меню на уровень выше</p>--}}
                {{--<p>Вы уверены?</p>--}}
            {{--</div>--}}
            {{--<br>--}}
        {{--</div>--}}
        {{--<div class="medium-12 columns modal-footer">--}}
            {{--<div class="medium-12 columns">--}}
                {{--<button class="save-button button primary" type="submit" data-close @click="makeMenuRoot">Да</button>--}}
                {{--<button class="button default" data-close aria-label="Close modal" type="button">Нет--}}
                {{--</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="reveal custom-medium-reveal" id="make-child" data-reveal>--}}
        {{--<div class="medium-12 columns">--}}
            {{--<h1 class="modal-h-1">Выберите что-то</h1>--}}
            {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
            {{--</button>--}}
            {{--<div class="medium-12 columns modal-left">--}}
                {{--<select name="name" required title="parentMenu" v-model="parentId">Что-то <span class="red-star">*</span>--}}
                    {{--<option value="null" disabled selected>Выберите родительское меню</option>--}}
                    {{--<option :value="menu.id" v-for="menu in menus">@{{ menu.name }}</option>--}}
                {{--</select>--}}
            {{--</div>--}}
            {{--<br>--}}
        {{--</div>--}}
        {{--<div class="medium-12 columns modal-footer">--}}
            {{--<div class="medium-12 columns">--}}
                {{--<button class="save-button button primary" data-close @click="changeParentMenu">СОХРАНИТЬ</button>--}}
                {{--<button class="cancel-button button" data-close aria-label="Close modal" type="button">ОТМЕНИТЬ</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="reveal custom-medium-reveal" id="edit-modal" data-reveal>--}}
        {{--<div class="medium-12 columns">--}}
            {{--<h1 class="modal-h-1">Редактирование пункта</h1>--}}
            {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
            {{--</button>--}}
            {{--<div class="medium-12 columns modal-left">--}}
                {{--<p>Содержимое тут</p>--}}
            {{--</div>--}}
            {{--<br>--}}
        {{--</div>--}}
        {{--<div class="medium-12 columns modal-footer">--}}
            {{--<div class="medium-12 columns">--}}
                {{--<button class="save-button button primary" type="submit" data-close @click="makeMenuRoot">Да</button>--}}
                {{--<button class="button default" data-close aria-label="Close modal" type="button">Нет--}}
                {{--</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection