@extends('layouts.master')
@section('style')
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @include('partials.cabinet.sidebar')
            <div class="medium-10 columns">
                <table>
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Имя</td>
                        <td>Email</td>
                        <td>Роль</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @foreach($user->roles as $role)
                                    {{ $role->name }}
                                @endforeach
                            </td>
                            <td>
                                <a href="#" class="button small warning" data-open="user-data-edit">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="button small alert">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin.edit_modal')
@endsection
