@extends('layouts.master')
@section('style')
    <link href="{{ asset('css/categoryedit.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="row">
        <div class="medium-12 columns">
            {{--@include('partials.cabinet.sidebar')--}}
            <div class="medium-10 columns" id="menu">
                <form action="{{ route('menu.store') }}" method="post">
                    {{ csrf_field() }}
                    <label for="name">Назвение</label>
                    <input type="text" id="name" name="name">
                    <select name="category[]" multiple size="30">
                        @include('partials.nested_select')
                    </select>
                    <button type="submit" class="button primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
@endsection