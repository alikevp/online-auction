
<div class="form-group form-element-text ">
    <label for="categories[]" class="control-label">Категории<span class="form-element-required">*</span></label>
    <select id="categories[]" class="form-control" name="categories[]" multiple="multiple" size="25">
<?php
$level = 1;
function tplMenu($category, $level){
    if($level == 1): ?>
        <option value="{{ $category['id'] }}" disabled="disabled" style="color:#001dff;"><span>+</span>{{ $category['name'] }}</option>
    <?php endif; ?>
        <?php if(count($category['children'])>0):
            $level++;
        ?>
            <option value="{{ $category['id'] }}" disabled="disabled"  style="color:#ff0050;"><span>++</span>{{ $category['name'] }}</option>
            <?php showCat($category['children'], $level); ?>
         <?php else: ?>
            <option value="{{ $category['id'] }}" @if(isset($categories_selected)) @if(in_array($category['id'], $categories_selected) == true) selected @endif @endif>{{ $category['name'] }}</option>
         <?php endif; ?>

<?php }
         /**
          * Рекурсивно считываем наш шаблон
          **/
         function showCat($data, $level){
             $string = '';
             foreach($data as $item){
                 $string .= tplMenu($item,$level);
             }
             return $string;
         }

         //Получаем HTML разметку
         $cat_menu = showCat($categories_tree,$level);
?>
    </select>
</div>

