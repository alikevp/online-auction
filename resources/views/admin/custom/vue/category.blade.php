<div class="form-group">
    <element-select-category inline-template>
        <select v-model="value">
            <option v-for="category in categories">@{{ category.name }}</option>
        </select>
    </element-select-category>
</div>