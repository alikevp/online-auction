<div class="tab-wraper">
    <form class="form-horizontal" role="form">
        <div class="form-group">
            <label for="option_name" class="col-sm-2 control-label">Название настройки</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="option_name" placeholder="Название настройки">
            </div>
        </div>
        <div class="form-group">
            <label for="value" class="col-sm-2 control-label">Значение</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="values" name="values[]" placeholder="Значение">
            </div>
        </div>
    </form>
</div>
