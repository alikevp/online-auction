<div class="admin-wraper">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#single_value_tab" data-toggle="tab">Одно значение</a></li>
        <li><a href="#multi_value_tab" data-toggle="tab">Много значений</a></li>
        <li><a href="#associatad_array_tab" data-toggle="tab">Ассоциативный массив</a></li>
        <li><a href="#multi_dimencial_array_tab" data-toggle="tab">Двумерный массив</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="single_value_tab">
            @include('admin.options.partials.single_value_tab')
        </div>
        <div class="tab-pane" id="multi_value_tab">
            @include('admin.options.partials.multi_value_tab')
        </div>
        <div class="tab-pane" id="associatad_array_tab">
            @include('admin.options.partials.associatad_array_tab')
        </div>
        <div class="tab-pane" id="multi_dimencial_array_tab">
            @include('admin.options.partials.multi_dimencial_array_tab')
        </div>
    </div>
</div>
<?php
$options = array(
    '1' => array(
        'from'=>0,
        'to'=>10000,
        'percent'=>5,
    ),
    '2' => array(
        'from'=>10001,
        'to'=>100000,
        'percent'=>3,
    ),
    '3' => array(
        'from'=>100001,
        'to'=>'',
        'percent'=>2,
    ),
);
