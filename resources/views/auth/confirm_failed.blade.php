@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Подтверждение Email
@endsection

@section('content')
    <div class="row content">
        <div class="medium-12 columns">

            @include('partials.sidebarBuyerWrapper')

            <div class="large-10 medium-12 small-12 columns form-column">
                <h1>Подтверждение Email</h1>
                <div class="callout success">
                    <p>
                        <h4>Вы не подтвердили e-mail. </h4>
                        На указанный Вами адрес было отправлено письмо. <br>
                        Перейдите по ссылке из этого письма для подтверждения аккаунта.
                    </p>
                    <form action="/send_Verification" method="POST">
                        {{ csrf_field() }}
                    <button class="button common-button">Выслать повторно</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection