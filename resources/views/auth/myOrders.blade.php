@extends('layouts.master')

@section('style')
    <link href="{{asset('owlcarousel')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Профиль
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">
            @include('partials.sidebarBuyerWrapper')
            <div class="large-6 medium-12 small-12 columns right-block">
                ...
            </div>
        </div>
    </div>
@endsection
