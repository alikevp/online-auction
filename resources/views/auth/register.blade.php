@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker-standalone.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('headScripts')
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.js') }}"></script>
@endsection
@section('footer_scripts')
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.10/js/jquery.suggestions.min.js"></script>
	<script src="{{ asset('js/regDate.js') }}"></script>
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93 ",
            type = "ADDRESS",
            $city = $("#dadata_city"),
            $street = $("#dadata_street"),
            $house = $("#dadata_house");

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement"
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street
        });

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
    </script>
    <script type="text/javascript">
        var dt = new Date();
        dt.setYear(dt.getFullYear() - 18);
        $(function () {
            $('.checkdata').datetimepicker({
                format: 'DD-MM-YYYY',
                extraFormats: [ 'DD-MM-YYYY','YYYY-MM-DD' ],
                language: "ru",
                todayBtn: true,
                pickTime: false,
                autoclose: true,
                maxDate:dt,
                defaultDate:dt,
            });
        });
    </script>
    <script type="text/javascript">
        function join(arr) {
            var separator = arguments.length > 1 ? arguments[1] : ", ";
            return arr.filter(function (n) {
                return n
            }).join(separator);
        }
        function showSuggestion(suggestion) {
            console.log(suggestion);
            var data = suggestion.data;
            if(data.name)
                $('#name_short').val(join([data.opf && data.opf.short || "", data.name.short || data.name.full]," "));
            $('#name_short_hidden').val(join([data.opf && data.opf.short || "", data.name.short || data.name.full]," "));
            $('#name_full').val(join([data.opf && data.opf.short || "", data.name.full_with_opf]," "));
            $('#name_full_hidden').val(join([data.opf && data.opf.short || "", data.name.full_with_opf]," "));
            $('#name_inn').val(join([data.inn]));
            $('#name_inn_hidden').val(join([data.inn]));
            $('#name_ogrn').val(join([data.ogrn]));
            $('#name_ogrn_hidden').val(join([data.ogrn]));
            $('#name_kpp').val(join([data.kpp]));
            $('#director_fio').val(join([data.management.name]));
            $('#name_address').val(join([data.address.unrestricted_value]));
            var ticks = data.state.registration_date;
            var registered_date = new Date(ticks);
            var yy = registered_date.getFullYear();
            var mm = registered_date.getMonth()+ 1;
            if (mm < 10) mm = '0' + mm;
            var dd = registered_date.getDate();
            if (dd < 10) dd = '0' + dd;
            var formated_date = dd + '.' + mm + '.' + yy;
            var formated_date_sql = yy + '-' + mm + '-' + dd + ' 00:00:00';
            $('#date_registered').val(formated_date_sql);
            $('#date_registered_hidden').val(formated_date);
        }
        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: showSuggestion
        });
    </script>
    <script src="{{ asset('js/regDate.js') }}"></script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
@section('title')
    Регистрация
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Регистрация</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-md-9 col-sm-12 col-xs-12 content-wrapper" id="right-block">
        @include('errors.validation')
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12 register-tabs-wrapper">
                <ul class="col-sm-12 nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active regular-text" href="#buyer" role="tab" data-toggle="tab">Покупатель</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link regular-text" href="#shop" role="tab" data-toggle="tab">Продавец</a>
                    </li>
                </ul>
                <div class="col-sm-12 tab-content">
                    <div role="tabpanel" class="tab-pane active" id="buyer">
                        <form action="{{ url('/register') }}" method="POST" role="form">
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 control-label">E-mail<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="email" placeholder="" name="email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 control-label">Пароль<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="password" placeholder="" name="password" pattern=".{6,}" required>
                                    <small>Минимум 6 символов</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-3 control-label">Повторите пароль<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="password" name="password_confirmation" pattern=".{6,}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 control-label">Имя<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" placeholder="" name="name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="surname" class="col-sm-3 control-label">Фамилия<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" placeholder="" name="surname" required pattern="^[A-Za-zА-Яа-яЁё0-9$/\s]+$">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dadata_city" class="col-sm-3 control-label">Ваш город</label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="dadata_city" type="text" name="city" placeholder="Населенный пункт" :value="address.city">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Дата рождения<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control checkdata" size="16" type="text" name="date_of_birth" value="" required data-date-format="YYYY-MM-DD">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Пол<span class="red-star">*</span></label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6 sex">
                                            <input id="sex1" type="radio" name="sex" value="m" required><label for="sex1">Мужской</label>
                                        </div>
                                        <div class="col-sm-6 sex">
                                            <input id="sex2" type="radio" name="sex" value="f" required><label for="sex2">Женский</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(isset($referred_by))
                                <input type="hidden" name="referred_by" value="{{ $referred_by }}">
                            @endif
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6 columns">
                                    <button type="submit" class="btn gradient-green">Зарегистрироваться</button>
                                </div>
                                <div class="col-sm-6 columns reg-notice-column">
                                <span class="form-notice">
                                    Обязательные поля помечены символом <span class="red-star">*</span>
                                </span>
                                    <br>
                                    <span class="form-notice">
                                    Нажимая "ЗАРЕГИСТРИРОВАТЬСЯ", Вы соглашаетесь с <a href="/uploads/files/documents/Оферта для покупателя.pdf" class="flash-link" target="_blank">Публичной офертой</a>,
                                    <a href="/uploads/files/documents/СОГЛАСИЕ.pdf" class="flash-link" target="_blank">Согласие на обработку персональных данный.</a>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="shop">
                        <form action="{{ url('/register/register_shop') }}" method="POST" role="form">
                            {{ csrf_field() }}
                            <h3>Магазин</h3>
                            <div class="col-xs-12">
                                <label>Название или ИНН<span class="red-star">*</span></label>
                                <input id="dadata_party" type="text" placeholder="" name="party" required class="form-control">
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-column-left">
                                    <div class="row">
                                        <div class="col-sm-6 input-column">
                                            <label>Юридическое название<span class="red-star">*</span></label>
                                            <input id="name_short_hidden"  type="text" placeholder="" name="le_name" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>Полное юр. название<span class="red-star">*</span></label>
                                            <input id="name_full_hidden"  type="text" placeholder="" name="full_le_name" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>Адрес<span class="red-star">*</span></label>
                                            <input id="name_address" type="text" placeholder="" name="shop_address" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>ИНН<span class="red-star">*</span></label>
                                            <input id="name_inn_hidden" type="text" minlength="10" maxlength="12" pattern="\d*" placeholder="" name="shop_inn" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>Дата регистрации<span class="red-star">*</span></label>
                                            <input id="date_registered_hidden"  type="text" placeholder="" name="shop_registered" required class="form-control checkdata" data-date-format="DD.MM.YYYY">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>ФИО директора<span class="red-star">*</span></label>
                                            <input id="director_fio" type="text" placeholder="" name="director_fio" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>E-mail<span class="red-star">*</span></label>
                                            <input type="text" placeholder="" name="shop_email" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>Телефон<span class="red-star">*</span></label>
                                            <input type="text" placeholder="" maxlength="15" name="shop_phone" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>КПП<span class="red-star">*</span></label>
                                            <input id="name_kpp" type="text" minlength="9" maxlength="9" pattern="\d*" placeholder="" name="kpp" required class="form-control">
                                        </div>
                                        <div class="col-sm-6 input-column">
                                            <label>ОГРН<span class="red-star">*</span></label>
                                            <input id="name_ogrn_hidden" minlength="13" maxlength="13" pattern="\d*" type="text" placeholder="" name="ogrn" required class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Основной администратор</h3>
                            <div class="col-sm-12 content-wrapper">
                                <div class="row">
                                    <div class="col-xs-12 form-column-left">
                                        <div class="row">
                                            <div class="col-sm-6 input-column">
                                                <label>E-mail<span class="red-star">*</span></label>
                                                <input type="email" placeholder="" name="email" required class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Пароль<span class="red-star">*</span> <small>(Минимум 6 символов)</small></label>
                                                <input type="password" placeholder="" name="password" pattern=".{6,}" required class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Повторите пароль<span class="red-star">*</span></label>
                                                <input type="password" name="password_confirmation" pattern=".{6,}" required class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Имя<span class="red-star">*</span></label>
                                                <input type="text" placeholder="" name="name" required class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Фамилия<span class="red-star">*</span></label>
                                                <input type="text" placeholder="" name="surname" pattern="^[A-Za-zА-Яа-яЁё0-9$/\s]+$" class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Телефон<span class="red-star">*</span></label>
                                                <input type="text" placeholder="" maxlength="15" name="telefone" required class="form-control">
                                            </div>
                                            <div class="col-sm-6 input-column">
                                                <label>Дата рождения<span class="red-star">*</span></label>
                                                <input class="form-control checkdata" size="16" type="text" name="dob" data-date-format="YYYY-MM-DD">
                                            </div>
                                            <div class="col-sm-12 form-group row" style="margin-top:20px;">
                                                <label class="col-sm-2 control-label">Пол<span class="red-star">*</span></label>
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-6 sex">
                                                            <input id="shop_sex1" type="radio" name="sex" value="m" required><label for="shop_sex1">Мужской</label>
                                                        </div>
                                                        <div class="col-sm-6 sex">
                                                            <input id="shop_sex2" type="radio" name="sex" value="f" required><label for="shop_sex2">Женский</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn gradient-green d-block mx-auto">Зарегистрироваться</button>
                                    </div>
                                    <div class="col-sm-6 reg-notice-column">
                                        <span class="form-notice">
                                            Обязательные поля помечены символом <span class="red-star">*</span>
                                        </span>
                                        <br>
                                        <span class="form-notice">
                                            Нажимая "ЗАРЕГИСТРИРОВАТЬСЯ", Вы соглашаетесь с <a href="/uploads/files/documents/Оферта для покупателя.pdf" class="flash-link" target="_blank">Публичной офертой</a>,
                                            <a href="/uploads/files/documents/СОГЛАСИЕ.pdf" class="flash-link" target="_blank">Согласие на обработку персональных данный.</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="col-md-12 info-wrapper">
                    Указывайте только реальную информацию, т.к. она будет использована для проверки!
                </div>
            </div>
        </div>
    </div>
@endsection