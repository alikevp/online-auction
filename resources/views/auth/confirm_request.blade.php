@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Подтверждение регистрации
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Подтверждение регистрации</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-md-9 col-sm-12 col-xs-12 content-wrapper" id="right-block">
        <div class="alert alert-success" role="alert">

            <h4>Для завершения регистрации необходимо подтвердить аккаунт. </h4>
            <p>На указанный Вами адрес было отправлено письмо. </p>
            <p>Перейдите по ссылке из этого письма для подтверждения аккаунта.</p>
        </div>
    </div>
@endsection