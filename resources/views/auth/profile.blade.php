@extends('layouts.master')

@section('style')
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker-standalone.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/profile.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('headScripts')
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jm_v_1.1/plugins/datepicker/bootstrap-datetimepicker.min.js') }}"></script>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var dt = new Date();
        dt.setYear(dt.getFullYear() - 18);
        $(function () {
            $('#checkdata').datetimepicker({
                format: 'DD-MM-YYYY',
                extraFormats: [ 'DD-MM-YYYY','YYYY-MM-DD' ],
                language: "ru",
                todayBtn: true,
                pickTime: false,
                autoclose: true,
                maxDate:dt,
                defaultDate:dt,
            });
        });
    </script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">
        new Vue({
            el: '#user-info',

            methods: {
                sendMail: function (id) {
                    console.log(id);
                    this.$http.post('api/verify', {
                        user: id
                    }).then((response) => {
                        console.log(response);
                    });
                }
            }
        });
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        };

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection

@section('title')
    Профиль
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Профиль</h1>
    </div>
    @php $user = Auth::user(); @endphp
    @if($user->type == 'shop_admin')
        @include('partials.sidebarShopWrapper')
    @else
        @include('partials.sidebarBuyerWrapper')
    @endif
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 content-wrapper" id="right-block">
        @if($user->type == 'shop_admin')
            @include('partials.profile.content_for_shopadmins')
        @else
            @include('partials.profile.content_for_buyers')
        @endif
    </div>
@endsection

