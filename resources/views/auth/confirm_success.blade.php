@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar.css') }}" rel="stylesheet">
    <link href="{{asset('css/reg.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Авторизация
@endsection

@section('content')
	@include('partials.sidebarWrapper')
	<div class="col-sm-9 right-block">
		<div class="col-sm-12 page-title">
			<h1>Авторизация</h1>
		</div>
		<span>
			Вы можете авторизоваться с данными, введенными при регистрации.
		</span>
		<p>
			Поздравляем ваш аккаунт успешно подтвержден. <br>
			Теперь вы можете не только создавать свои аукционы и очень выгодно покупать товары, но и
			зарабатывать на этом!
			Приглашайте своих знакомых на наш сайт и получаейте процент от их покупок! Подробнее читайте
			тут:
			<a href="{{ url('page/instruktsii/kabinet-pokupatelya/bonusy') }}">{{ url('page/instruktsii/kabinet-pokupatelya/bonusy') }}</a>
			.
		</p><br>
		@include('errors.validation')
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
                <form class="form-horizontal" role="form" action="{{ url('/login') }}" method="POST" data-abide novalidate data-live-validate="true" >
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 control-label">E-mail<span class="red-star">*</span></label>
                        <div class="col-sm-10">
                            <input class="form-control" type="email" placeholder="" name="email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 control-label">Пароль<span class="red-star">*</span></label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" placeholder="" name="password" required>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 flex-horizontal-right">
                            <button type="submit" class="btn gradient-green">Войти</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-6 col-sm-12 flex-horizontal-right">
                        <a href="/password/reset" class="navigation-link decoration-green">
                            <span class="regular-text">Забыли пароль?</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="col-md-12 info-wrapper">
				@if(isset($_GET['company']))
					Авторизуйтесь за Ваш магазин или пройдите процедуру регистрации.
				@else
                    После авторизации вы сможете воспользоваться всеми сервисами сайта в полном объеме.
				@endif
                </div>
            </div>
        </div>
	</form>
@endsection