@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/profile.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('footer_scripts')
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
@section('title')
    Подписки
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Профиль</h1>
    </div>
    @php $user = Auth::user(); @endphp
    @if($user->type == 'shop_admin')
        @include('partials.sidebarShopWrapper')
    @else
        @include('partials.sidebarBuyerWrapper')
    @endif
    <div class="col-sm-9 content-wrapper" id="right-block">
        @if (Session::has('msg'))
            <div class="callout {{ Session::get("msg") == "Пароль не обновлен. Проверьте правильность введённых данных." || Session::get("msg") == "Пожалуйста, укажите правильный текущий пароль." ? "alert" : "success"}}" data-closable="slide-out-right">
                <p>
                    {{ Session::get("msg") }}
                </p>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-7">
                @if(count($mail_subscription)>0)
                    <form method="post" action="/mail_subscription/sync">
                        @foreach($mail_subscription as $subscription)
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" id="{{$subscription->id}}" name="subscription[{{$subscription->id}}]" @if(isset($active_subscription)) @if(in_array($subscription->id, $active_subscription) == true) checked @endif @endif>
                                    <label for="{{$subscription->id}}">{{$subscription->name}}</label>

                                </div>
                            </div>
                        @endforeach
                        <div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <a href="/mail_subscription" class="btn border-red-btn">Отменить</a>
                            <input type="submit" name="mail_subscription_submit" class="btn gradient-green" value="ПРОДОЛЖИТЬ">
                        </div>
                    </form>
                @endif
            </div>
            <div class="col-sm-5">
                <div class="col-md-12 info-wrapper">
                    Выберите направления, по которым вы бы хотели получать оповещения от системы.
                </div>
            </div>
        </div>
    </div>
@endsection

