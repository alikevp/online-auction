@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('title')
    Требуется действие
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Авторизация</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-md-9 col-sm-12 col-xs-12 content-wrapper" id="right-block">
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
                <h4>Необходимо выйти из текущей авторизации</h4>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="col-md-12 info-wrapper">
                    Вы уже авторизованы
                    <br>
                    Для регистрации необходимо выйти из кабинета
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection