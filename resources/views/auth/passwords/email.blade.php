@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('title')
    Восстановление пароля
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Восстановление пароля</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-sm-9 content-wrapper" id="right-block">
        @include('errors.validation')
        <div class="row">
            <div class="col-sm-7">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}" data-abide novalidate data-live-validate="true">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Для восстановления пароля, введите ваш E-mail:</label>
                        <input id="email" type="email" placeholder="Ваш E-mail" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-12 flex-horizontal-right">
                            <button type="submit" class="btn gradient-green">Отправить ссылку для восстановления</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-5">
                <div class="col-md-12 info-wrapper">
                    На указанный e-mail будет выслано письмо с дальнейшими инструкциями.
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection