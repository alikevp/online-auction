@extends('layouts.master')

@section('style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-profile-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Профиль
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            @if(count($mailsubscription)>0)
                <form method="post" action="/mail/unsubscribe/{{$hash}}">
                    <h3>Вы действительно хотите отписаться от {{$mailsubscription[0]->name}}</h3>
                    <div class="medium-12 columns" id="order-control-buttons-block">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="/" class="button common-button order-submit width-50">Нет</a>
                        <input type="submit" name="mail_subscription_submit" class="button common-button order-submit submit-button width-50"
                               value="ПРОДОЛЖИТЬ">
                    </div>
                </form>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>

    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection
