@extends('layouts.master')

@section('style')
    <link href="{{asset('css/basket.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mediaqueries-cart-page.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Корзина
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.profile_sidebar')

        <div class="large-10 medium-12 small-12 columns" id="right-block">
            <div class="medium-12 columns" id="buy-steps-block">
                <div class="medium-12 columns">
                    <h3>РЕКВИЗИТЫ ДОСТАВКИ</h3>
                </div>
                <div class="large-10 large-offset-1 medium-12 hide-for-small-only columns" id="buy-steps">
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Корзина</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div buy-step-div-active">
                                <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span class="step-name step-name-active">Доставка</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-3 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Подтверждение</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Завершение</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="small-12 columns">
                {{--data-abide novalidate data-live-validate="true"--}}
                <form method="post" action="/order/confirm">
                    <div class="large-6 medium-6 small-12 columns">
                        <input type="checkbox" id="pickup" name="delivery_type[pickup]" v-model="pickup">
                        <label for="pickup">Заберу самостоятельно</label>
                    </div>
                    <div class="large-6 medium-6 small-12 columns">
                        <input type="checkbox" id="delivery" name="delivery_type[delivery]" v-model="delivery"
                        @change="showDeliveryAddressInputs = !showDeliveryAddressInputs">
                        <label for="delivery">Нужна доставка</label>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <label>Адрес доставки:</label>
                        </div>
                    </div>

                    {{--<div id="pickupBlock">--}}
                    {{--<input id="dadata_city" required type="text" name="city"--}}
                    {{--placeholder="Населенный пункт" :value="address.city">--}}
                    {{--</div>--}}


                    <input id="dadata_city" type="text" name="city"
                           placeholder="Населенный пункт" :value="address.city">
                    <deliveryaddressinputs v-if="showDeliveryAddressInputs"></deliveryaddressinputs>


                    <div class="medium-12 columns" id="order-control-buttons-block">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="/cart" class="button common-button order-submit width-50">НАЗАД В КОРЗИНУ</a>
                        <input type="submit" name="order_submit" class="button common-button order-submit submit-button width-50"
                               value="ПРОДОЛЖИТЬ">
                    </div>
                </form>
            </div>
            {{--@else--}}
            {{--<div class="medium-12 columns" >--}}
            {{--<span>Заказ не сформирован. Вернитесь в корзину и сформируйте заказ.</span>--}}
            {{--<a href="/cart" class="button order-control-button">НАЗАД В КОРЗИНУ</a>--}}
            {{--</div>--}}
            {{--@endif--}}
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93 ",
            type = "ADDRESS",
            $city = $("#dadata_city"),
            $street = $("#dadata_street"),
            $house = $("#dadata_house");

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement"
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street
        });

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
    </script>
    <script src="{{ asset('js/deactivated_button-submit_form.js') }}"></script>
@endsection