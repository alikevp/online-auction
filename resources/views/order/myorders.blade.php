@extends('layouts.master')

@section('style')
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/orderlist.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/profile.css') }}" rel="stylesheet">
@endsection
@section('footer_scripts')
    <script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
@endsection
@section('title')
    Мои аукционы
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Профиль</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-sm-9 content-wrapper" id="right-block">
        @if (session('message') !== null)
            <div class="col-xs-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5>Успешно.</h5>
                    <p>Предложение успешно сделано.</p>
                </div>
            </div>
        @endif
            @if (count($orders)>0)
                    <div class="col-sm-12 orders-wraper" id="items-table-block">
                        <div class="row">
                            @foreach ($orders as $order)
                                <div class="col-sm-12 order-block">
                                    <div class="row">
                                        <div class="col-sm-2 order-column order-logo">
                                            <img class="item-image" src="{{ url(array_first($order->product->files)) }}" alt="item image">
                                        </div>
                                        <div class=" col-sm-7 order-column order-details">
                                            <div class="order-title">
                                                <span>{{ $order->product->name }}</span>
                                            </div>
                                            <div class="order-description">
                                                <div>№ заказа: <span class="grey-text">{{ $order->id }}</span></div>
                                                <div>Производитель: <span class="grey-text">{{ $order->product->producer }}</span></div>
                                                <div>Количество: <span class="grey-text">{{ $order->quantity }}</span></div>
                                                <div>Активен до: <span class="grey-text">{{ $order->active_till }}</span></div>
                                                @if(null !== $order->delivery)
                                                    @foreach($order->delivery->types as $type)
                                                        @if($type->slug == 'delivery')
                                                            <div>Доставка:
                                                                <span>
                                                                    <a class="flash-link" data-toggle="modal" data-target="#delyveryModal{{ $order->delivery->id }}">Показать</a>
                                                                </span>
                                                            </div>
                                                            <div class="modal fade" id="delyveryModal{{ $order->delivery->id }}" tabindex="-1" role="dialog" aria-labelledby="delyveryModal{{ $order->delivery->id }}ModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Доставка</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <h3 class="modal-h-3">Доставка</h3>
                                                                            <span><span class="bold-text">Город:</span> <span class="grey-text">{{ $order->delivery->city }}</span></span>
                                                                            <br>
                                                                            <span><span class="bold-text">Улица:</span> <span class="grey-text">{{ $order->delivery->street }}</span></span>
                                                                            <br>
                                                                            <span><span class="bold-text">Дом:</span> <span class="grey-text">{{ $order->delivery->house }}</span></span>
                                                                            <br>
                                                                            <span><span class="bold-text">Квартира:</span> <span class="grey-text">{{ $order->delivery->apartment }}</span></span>
                                                                            <br>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn gradient-green width-50" data-dismiss="modal">Закрыть</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif($type->slug == 'pickup')
                                                            <div>Самовывоз: <span>{{ $order->delivery->city }}</span></div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div>Доставка: <span>Не указана</span></div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3 order-column order-actions">
                                            <div class="actions-offers">
                                                Предложений: <span>{{ count($order->order_offer_groups) }}</span>
                                            </div>
                                            <div class="actions-best-price">
                                                @if(count($order->order_offer_groups)>0)
                                                    @if(count($order->order_offer_groups)>1)
                                                        Лучшая цена: <span>{{ $order->order_offer_groups->min('price') }}</span>
                                                    @else
                                                        Лучшая цена: <span>{{ $order->order_offer_groups->first()->price }}</span>
                                                    @endif
                                                @else
                                                @endif

                                            </div>
                                            <div class="actions-offers">
                                                @if(($order->status == 1))
                                                    <div class="acepted acepted-active">Активен</div>
                                                @elseif(($order->status == 0))
                                                    <div class="acepted acepted-cancel">Отменён</div>
                                                @elseif(($order->status == 2))
                                                    <div class="acepted acepted-end">Истёк</div>
                                                @elseif(($order->status == 3))
                                                    <div class="acepted acepted-acepted">Принят</div>
                                                @endif
                                            </div>
                                            <div class="actions-buttons">
                                                <a href="/orderdetails?order={{ $order->id }}"
                                                   class="btn gradient-green width-100">Подробности</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="reveal custom-medium-reveal" id="edit-bet" data-reveal>
                            <div id="modal-content-block"></div>
                        </div>
                    </div>

                <br>
                {{ $orders->appends(Request::only('search'))->links('vendor.pagination.bootstrap-4') }}

                </div>
            @else
                <div class="col-sm-12">
                    <span>Аукционов нет.</span>
                </div>
            @endif

    </div>

@endsection
