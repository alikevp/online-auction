@extends('layouts.master')

@section('style')
    <link href="{{asset('css/sidebar_menu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/itempage.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Ваши отзывы
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">

            @include('partials.sidebarWrapper')

            <div class="medium-10 columns comment-form">
                <div class="row">
                    @foreach (['alert', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <div class="callout {{ $msg }}">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                            </div>

                            <hr>

                        @endif
                    @endforeach
                </div>
                <form action="" method="post" data-abide novalidate>
                    <div class="row">
                        <h1>Ваши отзывы</h1>
                        <span>
                            Недавно Вы успешно завершили аукцион.
                            Пожалуйста, поделитесь мнением о приобретённом товаре,
                            о магазине, который предоставил Вам этот товар, а также о работе
                            нашего сайта.
                        </span>

                        <hr>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <h2>Отзыв о магазине {{ $dealing->shop()->first()->name }}</h2>

                        <div class="medium-12 columns">
                            <label>Рейтинг
                                <select name="shop_rating" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3" selected>3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Достоинства
                                <textarea name="shop_advantages" placeholder="Достоинства магазина" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Недостатки
                                <textarea name="shop_disadvantages" placeholder="Недостатки магазина" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Комментарий
                                <textarea name="shop_text" placeholder="Комментарий" required></textarea>
                            </label>
                        </div>

                        <hr>

                        <h2>Отзыв о товаре {{ $dealing->order()->first()->product()->first()->name }}</h2>
                        <div class="medium-12 columns">
                            <label>Рейтинг
                                <select name="item_rating" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3" selected>3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Достоинства
                                <textarea name="item_advantages" placeholder="Достоинства товара" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Недостатки
                                <textarea name="item_disadvantages" placeholder="Недостатки товара" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Комментарий
                                <textarea name="item_text" placeholder="Комментарий о товаре" required></textarea>
                            </label>
                        </div>

                        <hr>

                        <h2>Отзыв о нашем сайте</h2>
                        <div class="medium-12 columns">
                            <label>Рейтинг
                                <select name="site_rating" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3" selected>3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Достоинства
                                <textarea name="site_advantages" placeholder="Достоинства сайта" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Недостатки
                                <textarea name="site_disadvantages" placeholder="Недостатки сайта" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <label>Комментарий
                                <textarea name="site_text" placeholder="Комментарий о сайте" required></textarea>
                            </label>
                        </div>
                        <div class="medium-12 columns">
                            <button class="button comment-form-button" type="submit">Отправить</button>
                        </div>
                        <section id="recall" data-magellan-target="recall">
                            <a data-open="recall-rules-modal" class="button recall-rules review">Правила оформления отзывов</a>
                        </section>
                        <div class="reveal" id="recall-rules-modal" data-reveal>
                            <h1>Правила оформления отзывов</h1>
                            <p class="lead">
                                Подробно расскажите о своём опыте использования товара. Обратите внимание на качество, удобство модели, её соответствие заявленным характеристикам.
                            </p>
                            <p>
                                Мы не публикуем отзывы, которые содержат оскорбления и ненормативную лексику.
                            </p>

                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--</div>--}}
@endsection

