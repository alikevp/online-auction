@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/cart.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('footer_scripts')
    <script src="{{ asset('js/jm_v_1.1/deactivated_button-submit_form.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $(document).ready(function(){
            var old = $(".items-table tr:nth-child(n+2)")
            var trigger = $(".full-table-trigger")
            old.hide();
            trigger.click(function(){
                event.preventDefault();
                $(".hidden_row").fadeIn();
                $(this).hide();
            });
        });
    </script>
@endsection
@section('title')
    Корзина
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h1>Корзина</h1>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-sm-9 content-wrapper" id="right-block">
        <div class="col-sm-12 text-center" id="buy-steps">
            <div class="row">
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Корзина</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Доставка</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-3">
                    <a href="#">
                        <div class="buy-step-div buy-step-div-active">
                            <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span class="step-name step-name-active">Подтверждение</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Завершение</span>
                    </a>
                </div>
            </div>
        </div>
        @if (isset($products))
            <form method="post" action="/order/finish">
                <div class="col-sm-12" id="items-table-block">
                    <div class="col-sm-12" id="total-block">
                        @if(array_has(session('delivery_type'), 'pickup'))
                            <p class="info">Самовывоз по адресу: <span class="address">{{ $delivery_data->city }}</span></p>
                        @endif
                        @if(array_has(session('delivery_type'), 'delivery'))
                            <p class="info">Доставка по адресу: <span class="address">{{ $delivery_data->full_address }}</span></p>
                        @endif
                        <p class="info">Пожалуйста, внимательно проверьте детали заказа, и нажмите "Продолжить".</p>

                        <table class="items-table">
                            <thead>
                            <tr>
                                <th>Товар</th>
                                <th>Цена@include('partials.priceTooltip')</th>
                                <th>Количество</th>
                                <th>Сумма@include('partials.priceTooltip')</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_price = 0; ?>
                            <tr id="cart_prod_row_{{ $products->first()->id }}">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <img class="item-image" src="{{ URL::to($products->first()->files[0]) }}" alt="item image">
                                        </div>
                                        <div class="col-sm-9">
                                            <span class="item-name">{{ $products->first()->name }}</span><br>
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle;text-align: center;" id="product-price_{{ $products->first()->id }}">{{ $products->first()->price }}
                                    <input class="hidefield" hidden="hidden" type="hidden" name="product_price[{{ $products->first()->id }}]" value="{{ $products->first()->price }}">
                                </td>

                                <td style="vertical-align: middle;text-align: center;" id="product-quantity_{{ $products->first()->id }}">{{ $cart_array['count'][$products->first()->id] }}
                                    <input class="hidefield" hidden="hidden" type="hidden" name="product_count[{{ $products->first()->id }}]" value="{{ $cart_array['count'][$products->first()->id] }}">
                                </td>
                                <td style="vertical-align: middle;text-align: center;" id="product-amount_{{ $products->first()->id }}">{{ ($products->first()->price) * ($cart_array['count'][$products->first()->id]) }}</td>
                            </tr>
                            <?php
                            $cur_price = ($products->first()->price) * ($cart_array['count'][$products->first()->id]);
                            $total_price = $cur_price + $total_price;
                            ?>
                            @foreach ($products as $key=>$product)
                                @if($key == 0)
                                    <?php continue; ?>
                                @endif
                                <tr id="cart_prod_row_{{ $product->id }}" class="hidden_row">
                                    <td>
                                        <div class="col-sm-3">
                                            <img class="item-image" src="{{ URL::to($product->files[0]) }}" alt="item image">
                                        </div>
                                        <div class="col-sm-9">
                                            <span class="item-name">{{ $product->name }}</span><br>
                                        </div>
                                    </td>
                                    <td id="product-price_{{ $product->id }}">{{ $product->price }}@include('partials.priceTooltip')
                                        <input class="hidefield" hidden="hidden" type="hidden" name="product_price[{{ $product->id }}]" value="{{ $product->price }}">
                                    </td>

                                    <td id="product-quantity_{{ $product->id }}">{{ $cart_array['count'][$product->id] }}
                                        <input class="hidefield" hidden="hidden" type="hidden" name="product_count[{{ $product->id }}]" value="{{ $cart_array['count'][$product->id] }}">
                                    </td>

                                    <td id="product-amount_{{ $product->id }}">{{ ($product->price) * ($cart_array['count'][$product->id]) }}@include('partials.priceTooltip')</td>
                                </tr>
                                <?php
                                $cur_price = ($product->price) * ($cart_array['count'][$product->id]);
                                $total_price = $cur_price + $total_price;
                                ?>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="order-wraper col-sm-12">
                            <div id="order-block" class="row">
                                <div class="col-sm-12">
                                    <a class="full-table-trigger flash-link" href="#">В заказе
                                        товаров: {{ count($products) }}, на общую сумму {{ $total_price }} руб.
                                        <span> (Просмотреть все)</span></a>
                                    <div class="row">
                                        <div class="col-sm-6 order-title-big">
                                            <span>Завершение оформления заказа</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 order-title-medium">
                                            <span>Проверьте товары, доставку и стоимость покупки</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12" id="price-block">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <span id="price-description">Ориентировачная стоимость товара:<br>
                                                        <span class="price_text" style="font-weight: 400;font-size: 14px;">
                                                            Указана ориентировачная стоимость. Окончательная стоимость будет выбрана вами в результате торгов.
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <span id="price-value">
                                                        <span id="total-price-value">{{ $total_price }} @include('partials.priceTooltip')</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input class="hidefield" hidden="hidden" type="hidden" name="total_count" value="{{ count($products) }}">
                                            <input class="hidefield" hidden="hidden" type="hidden" name="total_price" value="{{ $products->sum('price') }}">
                                            <input class="hidefield" hidden="hidden" type="hidden" name="total_count" value="{{ count($products) }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a href="/cart" class="btn border-green-btn width-100">НАЗАД В КОРЗИНУ</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="submit" name="order_submit" class="btn gradient-green width-100" value="НАЧАТЬ АУКЦИОН">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"><div class="col-sm-12 order-agreement"><span>Оформляя заказ, вы соглашаетесь с условиями <a href="/uploads/files/documents/Оферта для покупателя.pdf" class="flash-link" target="_blank">Публичной офертой</a>.</span></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @else
            <div class="col-sm-12">
                <span>Заказ не сформирован. Вернитесь в корзину и сформируйте заказ.</span>
                <a href="/cart" class="button order-control-button">НАЗАД В КОРЗИНУ</a>
            </div>
        @endif
    </div>
@endsection