@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
    <link href="{{asset('css/cabinet.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/itempage.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    {{ $shop_data->name }}
@endsection

@section('content')
	<?php $user = Auth::user(); ?>
	@if($user->type == 'shop_admin')
		@include('partials.sidebarShopWrapper')
	@else
		@include('partials.sidebarBuyerWrapper')
	@endif
    <div class="col-sm-9" id="right-block">
        <div class="col-sm-12">
            <div class="col-sm-12 right-block">
                <h1>Данные магазина {{ $shop_data->name }}</h1>
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="col-sm-12">
                            <div class="{{ $msg }} callout" data-closable="slide-out-right">
                                <p>{{ Session::get('alert-' . $msg) }}</p>
                                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                @endforeach
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="" id="items-button-group-top-div">
                                <div class="button-group items-button-group main-tabs"><ul class="tabs">
                                        <li class="tabs-title"><a onClick="javascript:toggleTab('desc');" href="#desc">Общие данные</a></li>
                                        <li class="tabs-title"><a onClick="javascript:toggleTab('adr');" href="#adr">Адреса</a></li>
                                        <li class="tabs-title"><a onClick="javascript:toggleTab('pay');" href="#pay">Варианты оплаты</a></li>
                                        <li class="tabs-title"><a onClick="javascript:toggleTab('recall');" href="#recall">Отзывы ({{ count($shop_data->comments) }})</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 shadow-block tabs-content" data-tabs-content="deeplinked-tabs">
                        <div class="col-sm-12 item-about-div tabs-panel" id="desc">
                            <table class="unstriped personal-data-table" id="user-info">
                                <tr>
                                    <td><span>Название:</span></td>
                                    <td><span>{{ $shop_data->name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Юридическое название:</span></td>
                                    <td><span>{{ $shop_data->le_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Полное юридическое название:</span></td>
                                    <td><span>{{ $shop_data->full_le_name }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ФИО директора:</span></td>
                                    <td><span>{{ $shop_data->director_fio }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ИНН:</span></td>
                                    <td><span>{{ $shop_data->inn }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>КПП:</span></td>
                                    <td><span>{{ $shop_data->kpp }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>ОГРН:</span></td>
                                    <td><span>{{ $shop_data->ogrn }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>Телефон:</span></td>
                                    <td><span>{{ $shop_data->phone }}</span></td>
                                </tr>
                                <tr>
                                    <td><span>E-mail:</span></td>
                                    <td><span>{{ $shop_data->e_mail }}</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12 item-about-div tabs-panel" style="display:none;" id="adr">
                            @if(count($addresses)>0)
                                @foreach($addresses as $address)
                                    <span class="info">

                                        </span>
                                    <table class="unstriped personal-data-table" id="user-info">
                                        <tr>
                                            <td>
                                                <span>
                                                @if($address->type == 1)
                                                        Основной
                                                    @elseif($address->type == 0)
                                                        Второстепенный
                                                    @else
                                                        Не указан
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ $address->country }}, {{ $address->city }}, {{ $address->street }}, {{ $address->house }}, {{ $address->apartment }}, {{ $address->post_index }}</td>
                                        </tr>
                                    </table>
                                    <br>
                                @endforeach
                            @else
                                <div class="col-sm-12">
                                    <p>Адреса не указаны.</p>
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-12 item-about-div tabs-panel" style="display:none;" id="pay">
                            @if(count($payment_data)>0)
                                @foreach($payment_data as $row)
                                    <table class="unstriped personal-data-table" id="user-info">
                                        <tr>
                                            <td colspan="2">
                                                <span>
                                                    @if($row['type'] == 1)
                                                    Наличные при получении
                                                    @elseif($row['type'] == 2)
                                                        Карта при получении
                                                    @elseif($row['type'] == 3)
                                                        Карта(перевод)
                                                    @elseif($row['type'] == 4)
                                                        Банковский перевод
                                                    @elseif($row['type'] == 5)
                                                        Электронная коммерция
                                                    @else
                                                        Не указан
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        @if($row['type'] == 1)
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 2)
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 3)
                                            <tr>
                                                <td><span>Банк:</span></td>
                                                <td><span>{{ $row['name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>№ карты:</span></td>
                                                <td><span>{{ $row['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 4)
                                            <tr>
                                                <td><span>Наименование банка:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Адрес банка:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_address'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>БИК:</span></td>
                                                <td><span>{{ $row['bank_data']['bank_bik'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Кор. счет:</span></td>
                                                <td><span>{{ $row['bank_data']['kor_account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>ИНН:</span></td>
                                                <td><span>{{ $row['bank_data']['inn'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>КПП:</span></td>
                                                <td><span>{{ $row['bank_data']['kpp'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>ОКТМО:</span></td>
                                                <td><span>{{ $row['bank_data']['oktmo'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>КБК:</span></td>
                                                <td><span>{{ $row['bank_data']['kbk'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Счет:</span></td>
                                                <td><span>{{ $row['bank_data']['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Получатель:</span></td>
                                                <td><span>{{ $row['bank_data']['recipient'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Назначение платежа:</span></td>
                                                <td><span>{{ $row['bank_data']['purpose'] }}</span></td>
                                            </tr>
                                        @elseif($row['type'] == 5)
                                            <tr>
                                                <td><span>Сервис:</span></td>
                                                <td><span>{{ $row['name'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Счет:</span></td>
                                                <td><span>{{ $row['account'] }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><span>Описание:</span></td>
                                                <td><span>{{ $row['description'] }}</span></td>
                                            </tr>
                                        @else
                                            Не указан
                                        @endif
                                    </table>
                                    <div style="clear: both"></div>
                                    <br>
                                @endforeach
                                <div style="clear: both"></div>
                            @else
                                <div class="col-sm-12">
                                    <p>Варианты оплаты не указаны.</p>
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-12 item-about-div tabs-panel" style="display:none;" id="recall">
                            @if(count($shop_data->comments) <1)
                                <div class="col-sm-12">
                                    <p>Отзывов нет.</p>
									<button type="button" class="btn gradient-green width-50"  data-toggle="modal" data-target="#commentForm">Добавить отзыв</button>
                                </div>
                            @else
                                @foreach($shop_data->comments as $comment)
                                    <div class="col-sm-12 comment">
                                        <span class="commenter-name"><p>{{ $comment->user_name }}</p></span>
                                        <span class="rating"><p class="title">Рейтинг: </p><p class="value">{{ $comment->rating }}</p></span>
                                        <span class="advantages"><p class="title">Достоинства: </p><p class="value">{{ $comment->advantages }}</p></span>
                                        <span class="disadvantages"><p class="title">Недостатки: </p><p class="value">{{ $comment->disadvantages }}</p></span>
                                        <span class="text"><p class="title">Комментарий: </p><br><p class="value">{{ $comment->text }}</p></span>
                                        <span class="date"><p>{{ $comment->created_at }}</p></span>
                                    </div>
                                @endforeach
                            @endif
                            <hr>
								<div class="modal fade" id="commentForm" tabindex="-1" role="dialog" aria-labelledby="personalPhotoAvatarModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Добавление отзыва</h5>
												<a data-toggle="modal" data-target="#commentsRules" class="button recall-rules">Правила оформления отзывов</a>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
                                                <form action="shop_comment?shop={{ $shop_data->id  }}" method="post" data-abide novalidate>
                                                    <div class="row" style="margin:10px;">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        @if(Auth::check())
                                                            <div class="col-sm-6">
                                                                <label>Имя
                                                                    <input type="text" value="{{ Auth::user()->name  }}" name="name" placeholder="Ваше имя" required disabled class="form-control">
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label>Email
                                                                    <input type="email" value="{{ Auth::user()->email  }} " name="email" placeholder="Ваш email" required disabled class="form-control">
                                                                </label>
                                                            </div>
                                                        @else
                                                            <div class="col-sm-6">
                                                                <label>Имя
                                                                    <input type="text" name="name" placeholder="Ваше имя" required class="form-control">
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label>Email
                                                                    <input type="email" name="email" placeholder="Ваш email" required class="form-control">
                                                                </label>
                                                            </div>
                                                        @endif
                                                        <div class="col-sm-5">
                                                            <label>Достоинства
                                                                <textarea name="advantages" placeholder="Достоинства магазина" required class="form-control"></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Рейтинг
                                                                <select name="rating" required class="form-control">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3" selected>3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label>Недостатки
                                                                <textarea name="disadvantages" placeholder="Недостатки магазина" required class="form-control"></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label>Комментарий
                                                                <textarea name="text" placeholder="Комментарий" required class="form-control"></textarea>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-12 center-block">
                                                            <input type="hidden" name="shop_id" value="{{ $shop_data->id  }}">
                                                            <div align="center"><button type="button" class="btn gradient-green width-50">Отправить</button></div>
                                                        </div>
                                                    </div>
                                                </form>
											</div>
										</div>
									</div>
								</div>
								<div class="modal fade" id="commentsRules" tabindex="-1" role="dialog" aria-labelledby="personalPhotoAvatarModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Правила оформления отзывов</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p class="lead">
													Подробно расскажите о своём опыте использования товара. Обратите внимание на качество, удобство модели, её соответствие заявленным характеристикам.
												</p>
												<p>
													Мы не публикуем отзывы, которые содержат оскорбления и ненормативную лексику.
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
    {{--</div>--}}
@endsection

@section('scripts')
    <script src="{{ asset('js/regDate.js') }}"></script>
    @if(Auth::user()->type == 'shop_admin')
        <script src="{{ asset('js/getOrderCount.js') }}"></script>
    @endif
    <script type="text/javascript">
		$( "#tututu" ).click(function() {
			alert('null');
		});
		function toggleTab(tab) {
			if(tab == "desc"){
				$("#" + tab).css('display', 'block');
				$("#adr").css('display', 'none');
				$("#pay").css('display', 'none');
				$("#recall").css('display', 'none');
			}else if(tab == "adr"){
				$("#" + tab).css('display', 'block');
				$("#desc").css('display', 'none');
				$("#pay").css('display', 'none');
				$("#recall").css('display', 'none');
			}else if(tab == "pay"){
				$("#" + tab).css('display', 'block');
				$("#adr").css('display', 'none');
				$("#desc").css('display', 'none');
				$("#recall").css('display', 'none');
			}else if(tab == "recall"){
				$("#" + tab).css('display', 'block');
				$("#adr").css('display', 'none');
				$("#pay").css('display', 'none');
				$("#desc").css('display', 'none');
			}
		}
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        (function($) {

            /**
             * Показывает индекс в отдельном поле
             */
            function showPostalCode(suggestion) {
                $("#dadata_postal_code").val(suggestion.data.postal_code);
            }

            /**
             * Очищает индекс
             */
            function clearPostalCode(suggestion) {
                $("#dadata_postal_code").val("");
            }
            var
                token = "04b92bad6b543b84100ad2012655143cf83fdc93",
                type  = "ADDRESS",
                $region = $("#region"),
                $area   = $("#area"),
                $city   = $("#dadata_city"),
                $settlement = $("#settlement"),
                $street = $("#dadata_street"),
                $house  = $("#dadata_house");

            // регион
            $region.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "region",
            });

            // район
            $area.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "area",
                constraints: $region
            });

            // город и населенный пункт
            $city.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "city",
                constraints: $area,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // geolocateCity($city);

            // город и населенный пункт
            $settlement.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "settlement",
                constraints: $city,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // улица
            $street.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "street",
                constraints: $settlement,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

            // дом
            $house.suggestions({
                token: token,
                type: type,
                hint: false,
                bounds: "house",
                constraints: $street,
                onSelect: showPostalCode,
                onSelectNothing: clearPostalCode
            });

        })(jQuery);


        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        /*Отмена ввода букв в инпутах*/
        document.getElementById('checkinn').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('checktel').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('checkdata').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }

        document.getElementById('dadata_postal_code').onkeypress = function (e) {
            return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
        }
    </script>
@endsection
