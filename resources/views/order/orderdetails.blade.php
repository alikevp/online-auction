@extends('layouts.master')

@section('style')
	<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/orderlist.css') }}" rel="stylesheet">
	<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/sidebar_menu.css') }}" rel="stylesheet">
	<link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/auction-details-page.css') }}" rel="stylesheet">
@endsection

@section('footer_scripts')
	<script src="{{ asset('js/page_scripts/orders.myorders.js') }}"></script>
@endsection

@section('title')
	Подробности заказа
@endsection

@section('content')
	@include('partials.sidebarBuyerWrapper')

	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-12" id="buy-steps-block">
				<div class="row">
					<div class="col-sm-12">
						<a class="flash-link" href="/my-orders">Назад</a>
						<h3>Подробности заказа</h3>
						<hr>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<h3 class="order-title">{{ $order->product->name }}</h3>
			<div class="row">
				<div class="col col-sm-6 shadow-block equivalent-height">
					@if(count($offers)>0)
						<div class="order-info-block order-best-price">Лучшая цена: <span>{{ ($offers->first()->price != null) ? $offers->first()->price : 'Предложений пока нет' }}
								&#8381;</span></div>
						<div class="order-info-block order-dest-price-shop">Магазин: <span><a class="flash-link"
						                                                                      href="/shopdetails?shop={{ $offers->first()->shops->first()->id }}">{{ $offers->first()->shops->first()->name }}</a></span>
						</div>
					@else
						<div class="order-info-block order-best-price">Предложений пока нет</div>
					@endif
					<div class="order-info-block order-status">Статус аукциона:
						<span>
                            @if($order->status == 0)
								Отменен
							@elseif($order->status == 1)
								Активен
							@elseif($order->status == 2)
								Истек
							@elseif($order->status == 3)
								Принят
							@endif
                        </span>
					</div>
					<div class="order-info-block orderdetalais-actions" style="width:50%;">
						@if($order->status == 1)
							<a class="btn border-red-btn width-50"
							   href="/order_cancel?order={{ $order->id }}">Завершить</a>
						@elseif($order->status == 2)
							<a class="btn gradient-green width-50"
							   href="/order_extend?order={{ $order->id }}">Продлить</a>
						@endif
					</div>
				</div>
				<div class="col col-sm-6 shadow-block equivalent-height">
					@if(null !== $order->delivery)
						@foreach($order->delivery->types as $type)
							@if($type->slug == 'delivery')
								<div class="order-info-block order-city">Доставка: <span><a class="flash-link"
								                                                            data-toggle="modal"
								                                                            data-target="#delyveryModal{{ $order->delivery->id }}">Показать</a></span>
								</div>
								<div class="modal fade" id="delyveryModal{{ $order->delivery->id }}" tabindex="-1"
								     role="dialog" aria-labelledby="delyveryModal{{ $order->delivery->id }}Label"
								     aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title"
												    id="delyveryModal{{ $order->delivery->id }}Label">Доставка</h5>
												<button type="button" class="close" data-dismiss="modal"
												        aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<span><span class="bold-text">Город:</span> <span
															class="grey-text">{{ $order->delivery->city }}</span></span>
												<br>
												<span><span class="bold-text">Улица:</span> <span
															class="grey-text">{{ $order->delivery->street }}</span></span>
												<br>
												<span><span class="bold-text">Дом:</span> <span
															class="grey-text">{{ $order->delivery->house }}</span></span>
												<br>
												<span><span class="bold-text">Квартира:</span> <span
															class="grey-text">{{ $order->delivery->apartment }}</span></span>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn border-red-btn width-50"
												        data-dismiss="modal">Закрыть
												</button>
											</div>
										</div>
									</div>
								</div>
							@elseif($type->slug == 'pickup')
								<div class="order-info-block order-city">Самовывоз:
									<span>{{ $order->delivery->city }}</span></div>
							@endif
						@endforeach
					@else
						<div>Доставка: <span>Не указана</span></div>
					@endif
					<div class="order-info-block order-start">Начало аукциона:
						<span>{{ $order->created_at->formatLocalized('%d %B %Y %H:%m') }}</span></div>
					<div class="order-info-block order-finish">До окончания осталось:
						@if($order->status == 1)
							<span>{{ $time_last }}</span>
						@else
							<span> 0 </span>
						@endif
					</div>
				</div>
			</div>
			<h3 class="order-title">Предложения по заказу:</h3>
			<div class="col-sm-12 orders-wraper" id="items-table-block">
				<div class="row">
					{{--@foreach ($orders as $order)--}}
					@if($offers->isNotEmpty())
						@foreach ($offers as $bet)
							<div class="col-sm-12 order-block">
								<div class="row">
									<div class="col-sm-2 order-column order-logo">
										{{--ЛОГО МАГАЗИНА ---- <img class="item-image" src="{{ url($order->products->first()['files'][0]) }}" alt="item image">--}}
									</div>
									<div class="col-sm-7 order-column order-details">
										<div class="order-title">

											<a href="/shopdetails?shop={{ $bet->shops->first()->id }}"><span>{{ $bet->shops->first()->name }}</span></a>
										</div>
										<div class="order-description">
											<div>Предложение выставлено:
												<span>{{ $bet->created_at }}</span></div>

											@if(count($bet->deliveries)>0)
												<div>Доставка: <span>{{ count($bet->deliveries) }} вариант(а/ов). Самый быстрый - {{ $bet->deliveries->min('delivery_time')  }}
														дней. <a
																class="flash-link"
																data-toggle="modal"
																data-target="#deliveryType">(Подробнее)</a></span></div>
												<div class="modal fade" id="deliveryType" tabindex="-1" role="dialog"
												     aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">
																	Доставки</h5>
																<button type="button" class="close" data-dismiss="modal"
																        aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<table class="items-table unstriped"
																       style="width:100%;">
																	<tr>
																		<th>Тип</th>
																		<th>Срок доставки (дней)</th>
																		<th>Подробности</th>
																	</tr>
																	@foreach ($bet->deliveries as $delivery)
																		<tr>
																			<td>{{ $delivery->type }}</td>
																			<td>{{ $delivery->delivery_time }}</td>
																			<td><a class="flash-link"
																			       data-open="offerDeliveriesDetails-{{ $delivery->id }}"
																			       data-toggle="modal"
																			       data-target="#offerDeliveriesDetails-{{ $delivery->id }}">(Подробности)</a>
																			</td>
																		</tr>
																	@endforeach
																</table>
															</div>
														</div>
													</div>
												</div>
												@foreach ($bet->deliveries as $deliveryModal)
													<div class="modal fade"
													     id="offerDeliveriesDetails-{{ $deliveryModal->id }}"
													     tabindex="-1" role="dialog" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">
																		Подробности доставки</h5>
																	<button type="button" class="close"
																	        data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div>
																		{{ $deliveryModal->description }}
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endforeach
											@else
												<div>Доставки: <span>нет</span></div>
											@endif
											@if(count($bet->gifts)>0)
												<div>Подарки: <span>{{ count($bet->gifts) }} вариант(а/ов)<a
																class="flash-link"
																data-toggle="modal"
																data-target="#offerGifts-1">(Подробнее)</a></span>
												</div>
												<div class="modal fade" id="offerGifts-1" tabindex="-1" role="dialog"
												     aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">
																	Подарки</h5>
																<button type="button" class="close" data-dismiss="modal"
																        aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<table class="items-table unstriped">
																	<tr>
																		<th>Тип</th>
																		<th>Название</th>
																		<th>Описание</th>
																	</tr>
																	@foreach ($bet->gifts as $gift)
																		<tr>
																			<td>{{ $gift->type }}</td>
																			<td>{{ $gift->name }}</td>
																			<td>{{ $gift->description }}</td>
																		</tr>
																	@endforeach
																</table>
															</div>
														</div>
													</div>
												</div>
											@else
												<div>Подарки: <span>нет</span>
												</div>
											@endif
										</div>
									</div>
									<div class="col-sm-3 order-column order-actions">
										<div class="order-info-block shop-rate">Рейтинг:
											<span>
                                                        {{ $bet->shops->first()->offerrate }}@include('partials.auctionsCountTooltip')
												/
												{{ $bet->shops->first()->dealrate }}
												<@include('partials.salesCountTooltip')
                                                    </span>
										</div>
										<div class="order-info-block shop-comments">Отзывы: <span> {{ $bet->shops->first()->commentcount }}
												<a href="/shopdetails?shop={{ $bet->shops->first()->id }}#recall"
												   class="flash-link">Читать</a> </span>
										</div>
										<div class="order-info-block shop-comments">Цена: <span>{{ $bet->price }}
												&#8381;</span>
										</div>
										<div class="order-info-block shop-comments">Старая цена: <span>{{ $bet->old_price }}
												&#8381;</span>
										</div>
										<div class="actions-buttons">

											@if(($order->status == 1 OR $order->status == 2) && $bet->status == 0)

												<a class="btn gradient-green width-100" data-toggle="modal"
												   data-target="#confirm-accept-{{ $bet->id }}">Принять предложение</a>

												<div class="modal fade" id="confirm-accept-{{ $bet->id }}" tabindex="-1"
												     role="dialog" aria-labelledby="confirm-accept-{{ $bet->id }}Label"
												     aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<!-- проверка на существование телефона -->
															@if( empty(Auth::user()->telefone) )
																<div class="modal-header">
																	<h5 class="modal-title"
																	    id="confirm-accept-{{ $bet->id }}Label">Не
																		указан телефон</h5>
																	<button type="button" class="close"
																	        data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>

																<form action="/telefone_edit" method="post" data-abide
																      novalidate>
																	<div class="modal-body">
																		<input type="hidden" name="_token"
																		       value="{{ csrf_token() }}">
																		<div class="col-sm-12">
																			<label>Укажите свой действующий мобильный
																				номер<span class="red-star">*</span>
																				<input class="form-control" type="text"
																				       id="checktel"
																				       placeholder="Ваш телефон"
																				       name="telefone" required>
																			</label>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button class="btn gradient-green width-50"
																		        type="submit">СОХРАНИТЬ
																		</button>
																		<button class="btn border-red-btn width-50"
																		        data-dismiss="modal" type="button">
																			ОТМЕНИТЬ
																		</button>
																	</div>
																</form>
															@else
																<div class="modal-header">
																	<h5 class="modal-title"
																	    id="confirm-accept-{{ $bet->id }}Label">Вы
																		действительно хотите принять это
																		предложение?</h5>
																	<button type="button" class="close"
																	        data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<p>После подтверждения магазин будет оповещён о том,
																		что Вы приняли его предложение.</p>
																</div>
																<div class="modal-footer">
																	<a href="\offer_accept?order={{ $order->id }}&offer_confirm={{ $bet->id }}"
																	   class="btn gradient-green width-50">Согласен</a>
																	<button class="btn border-red-btn width-50"
																	        data-dismiss="modal" type="button">ОТМЕНИТЬ
																	</button>
																</div>
															@endif
														</div>
													</div>
												</div>
													@elseif($bet->status == 1)
														<div class="acepted">Принято</div>
													@elseif(($order->status !== 1 OR $order->status !== 2))
													@endif
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div style="padding:25px;">Предложений от магазинов не поступало.</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection