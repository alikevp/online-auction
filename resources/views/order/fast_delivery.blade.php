@extends('layouts.master')

@section('style')
    <link type="text/css" href="{{ asset('css/jm_v_1.1/pages/desktop/cart.css') }}" rel="stylesheet">
    <link href="{{asset('css/jm_v_1.1/pages/desktop/sidebar.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('footer_scripts')
    <script src="{{ asset('js/jm_v_1.1/deactivated_button-submit_form.js') }}"></script>
    <script src="{{ asset('js/jm_v_1.1/page_scripts/delivery.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.10/js/jquery.suggestions.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93 ",
            type = "ADDRESS",
            $city = $("#dadata_city"),
            $street = $("#dadata_street"),
            $house = $("#dadata_house");

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement"
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street
        });

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $( "#delivery" ).prop( "checked", true );
    </script>
@endsection

@section('title')
    Корзина
@endsection

@section('content')
    @include('partials.catalogAndSearch')
    <div class="col-sm-12 page-title">
        <h3>РЕКВИЗИТЫ ДОСТАВКИ</h3>
    </div>
    @include('partials.sidebarBuyerWrapper')
    <div class="col-sm-9 content-wrapper" id="right-block">
        <div class="col-sm-12 text-center" id="buy-steps">
            <div class="row">
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Корзина</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div buy-step-div-active">
                            <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span class="step-name step-name-active">Доставка</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-3">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Подтверждение</span>
                    </a>
                </div>
                <div class="col-sm-1">
                    <hr class="buy-step-hr">
                </div>
                <div class="buy-step col-sm-2">
                    <a href="#">
                        <div class="buy-step-div">
                            <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                        </div>
                        <span>Завершение</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <form method="post" action="/order/fast_confirm" data-abide novalidate>

                @foreach ($post_products as $product)
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][id]" value="{{ $product['id'] }}">
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][name]" value="{{ $product['name'] }}">
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][foto]" value="{{ $product['foto'] }}">
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][price]" value="{{ $product['price'] }}">
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][count]" value="{{ $product['count'] }}">
                    <input class="hidefield" hidden="hidden" type="hidden" name="products[{{ $product['id'] }}][category_id]" value="{{ $product['category_id'] }}">
                @endforeach


                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <label for="pickup">
                            <input type="checkbox" id="pickup" name="delivery_type[pickup]" v-model="pickup" value="pickup">Заберу самостоятельно
                        </label>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <label>
                            <input type="checkbox" checked id="delivery" name="delivery_type[delivery]" v-model="delivery" value="delivery" @change="showDeliveryAddressInputs = !showDeliveryAddressInputs"> Нужна доставка
                        </label>
                    </div>
                </div>

                    @foreach($addresses as $address)
                        <div class="row">
                            <div class="col-lg-6">
                                <h6>Адрес доставки:</h6>
                                <label for="address_{{ $address->id }}">
                                    <input type="radio" name="delivery" value="{{ $address }}" id="address_{{ $address->id }}" class="addressDataInput" v-model="address"
                                           data-address-country="{{ $address->country }}"
                                           data-address-city="{{ $address->city }}"
                                           data-address-street="{{ $address->street }}"
                                           data-address-house="{{ $address->house }}"
                                           data-address-apartment="{{ $address->apartment }}"
                                    >{{ $address->full_address }}
                                </label>
                            </div>
                        </div>
                    @endforeach
                    <input class="form-control" id="dadata_city" type="text" name="city" placeholder="Населенный пункт" :value="address.city">
                    <div id="deliveryBlock">
                        <input class="form-control" id="dadata_street" type="text" name="street"  placeholder="Улица" :value="address.street">
                        <input class="form-control" id="dadata_house" type="text" name="house" placeholder="Дом/строение" :value="address.house">
                        <input class="form-control" id="dadata_flat" type="text" placeholder="Квартира/Офис" name="apartment" pattern="^[A-Za-zА-Яа-яЁё0-9/\s-]+$" :value="address.apartment">
                    </div>

                <div class="col-sm-12" id="order-control-buttons-block">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="/cart" class="btn border-green-btn width-100">НАЗАД В КОРЗИНУ</a>
                        </div>
                        <div class="col-sm-6">
                            <input type="submit" name="order_submit" class="btn gradient-green width-100" value="ПРОДОЛЖИТЬ">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {{--@else--}}
        {{--<div class="medium-12 columns" >--}}
        {{--<span>Заказ не сформирован. Вернитесь в корзину и сформируйте заказ.</span>--}}
        {{--<a href="/cart" class="button order-control-button">НАЗАД В КОРЗИНУ</a>--}}
        {{--</div>--}}
        {{--@endif--}}
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $("#dadata_name").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_adress").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "ADDRESS",
            params: {
                parts: ["CITY"]
            },
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        var
            token = "04b92bad6b543b84100ad2012655143cf83fdc93 ",
            type = "ADDRESS",
            $city = $("#dadata_city"),
            $street = $("#dadata_street"),
            $house = $("#dadata_house");

        // город и населенный пункт
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement"
        });

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "house",
            constraints: $street
        });

        $("#dadata_party").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_email").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "EMAIL",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
        $("#dadata_bank").suggestions({
            token: "04b92bad6b543b84100ad2012655143cf83fdc93",
            type: "BANK",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
            }
        });
    </script>
@endsection