@extends('layouts.master')

@section('style')
    <link href="{{asset('owlcarousel')}}" rel="stylesheet" type="text/css">

@endsection

@section('title')
    Корзина
@endsection

@section('content')
    <div class="row" id="content">

        @include('partials.sidebarBuyerWrapper')

        <div class="medium-10 columns" id="right-block">
            <div class="medium-12 columns" id="buy-steps-block">
                <div class="medium-12">
                    <h3>ЗАВЕРШЕНИЕ</h3>
                </div>
                <div class="medium-9 medium-offset-2 columns" id="buy-steps">
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-shopping-bag buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Корзина</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-truck buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Доставка</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-3 columns">
                        <a href="#">
                            <div class="buy-step-div">
                                <i class="fa fa-eye buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span>Подтверждение</span>
                        </a>
                    </div>
                    <div class="medium-1 columns">
                        <hr class="buy-step-hr">
                    </div>
                    <div class="buy-step medium-2 columns">
                        <a href="#">
                            <div class="buy-step-div buy-step-div-active">
                                <i class="fa fa-check buy-step-icon" aria-hidden="true"></i>
                            </div>
                            <span class="step-name step-name-active">Завершение</span>
                        </a>
                    </div>
                </div>
                <div class="medium-12 columns">
                    Ошибка формирования заказа.
                </div>
            </div>
        </div>
    </div>
@endsection
